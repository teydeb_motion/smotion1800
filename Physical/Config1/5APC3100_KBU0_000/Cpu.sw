﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.8.2.72?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="XSensIMU_1" Source="IMU.XSensIMU_36.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="XSensIMU_2" Source="IMU.XSensIMU_32.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="XSensIMU_3" Source="IMU.XSensIMU_34.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="AxisHandli" Source="Source.SystemHandling.AxisHandle.AxisHandling.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="HexaHandli" Source="HexaHandling.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2">
    <Task Name="MainCtrl" Source="Source.SystemHandling.MachineUnit.MainCtrl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="AlarmCtrl" Source="Source.SystemHandling.Auxiliary.AlarmCtrl.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="SendData" Source="UICommunication.SendData.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#3">
    <Task Name="FwdKinemat" Source="FwdKinematic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#4">
    <Task Name="LEDManagem" Source="LEDManagement.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Clocks" Source="Source.SystemHandling.Auxiliary.Clocks.prg" Memory="UserROM" Language="ANSIC" Debugging="true" Disabled="true" />
    <Task Name="VelocityL1" Source="VelocityLimit.VelocityLimit.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5">
    <Task Name="GetData" Source="UICommunication.GetData.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#6">
    <Task Name="MassCalcul" Source="MassCalculation.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#7">
    <Task Name="PowerMeter" Source="PowerMeter.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="TimeCalcul" Source="TimeCalculation.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#8" />
  <DataObjects>
    <DataObject Name="Acp10sys" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <NcDataObjects>
    <NcDataObject Name="AxInit" Source="Source.SystemHandling.AxisHandle.MotionConfig.AxInit.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="AcpParTab" Source="Source.SystemHandling.AxisHandle.MotionConfig.AcpParTab.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="acp10etxen" Source="Source.SystemHandling.AxisHandle.MotionConfig.acp10etxen.dob" Memory="UserROM" Language="Ett" />
    <NcDataObject Name="gAxPwra" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxPwrobj.gAxPwra.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPwri" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxPwrobj.gAxPwri.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxisQ1a" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ1obj.gAxisQ1a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxisQ1i" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ1obj.gAxisQ1i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxisQ2a" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ2obj.gAxisQ2a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxisQ2i" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ2obj.gAxisQ2i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxisQ3a" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ3obj.gAxisQ3a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxisQ3i" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ3obj.gAxisQ3i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxisQ4a" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ4obj.gAxisQ4a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxisQ4i" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ4obj.gAxisQ4i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxisQ5a" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ5obj.gAxisQ5a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxisQ5i" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ5obj.gAxisQ5i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxisQ6a" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ6obj.gAxisQ6a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxisQ6i" Source="Source.SystemHandling.AxisHandle.MotionConfig.gAxisQ6obj.gAxisQ6i.dob" Memory="UserROM" Language="Ax" />
  </NcDataObjects>
  <VcDataObjects>
    <VcDataObject Name="Visu2" Source="Source.HMI.Visu2.dob" Memory="UserROM" Language="Vc" WarningLevel="2" />
  </VcDataObjects>
  <Binaries>
    <BinaryObject Name="vcpfmtcx" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfapc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfntttf" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbmp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcshared" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdvnc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu203" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfile" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arial" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchspot" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcmgr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcxml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsint" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbtn" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsloc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu202" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccovl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccpopup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccline" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctext" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcbclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbar" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="visvc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcrt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccnum" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccstr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccddbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arialbd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccshape" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdsw" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccurl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcnet" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arsvcreg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="vcchtml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vctcal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccdt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpkat" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu201" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="acp10cfg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Acp10map" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AX_Q4" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AX_Q5" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AX_Q6" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AX_Q1" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AX_Q2" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AX_Q3" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Alarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="WebXs" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AX_PWR" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfar00" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config_2" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config_3" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config_4" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config_5" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config_1" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu1" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu102" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu101" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu103" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="operator" Source="Libraries.operator.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIecCon" Source="Libraries.AsIecCon.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="visapi" Source="Libraries.visapi.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsARCfg" Source="Libraries.AsARCfg.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="DataObj" Source="Libraries.DataObj.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsUSB" Source="Libraries.AsUSB.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpAlarm" Source="Libraries.MpAlarm.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpAxis" Source="Libraries.MpAxis.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpCom" Source="Libraries.MpCom.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="CommonFun" Source="Libraries.CommonFun.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="SysDiag" Source="Libraries.SysDiag.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="AsMem" Source="Libraries.AsMem.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="IecCheck" Source="Libraries.IecCheck.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpWebXs" Source="Libraries.MpWebXs.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsArSdm" Source="Libraries.AsArSdm.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="CycAxCon" Source="Libraries.CycAxCon.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="Acp10sdc" Source="Libraries.Acp10sdc.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10man" Source="Libraries.Acp10man.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10par" Source="Libraries.Acp10par.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="NcGlobal" Source="Libraries.NcGlobal.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10sim" Source="Libraries.Acp10sim.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10_MC" Source="Libraries.Acp10_MC.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsUDP" Source="Libraries.AsUDP.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsTCP" Source="Libraries.AsTCP.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="dvframe" Source="Libraries.dvframe.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="ArCan" Source="Libraries.ArCan.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="MTLinAlg" Source="Libraries.MTLinAlg.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="powerlnk" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="vcresman" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="fileio" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>