/* Libraries */
#include <bur/plctypes.h>
#include <AsDefault.h>

BOOL blinkLed(double time,double TTime)
{
	if ((int)(TTime/time)%2==1)
		return 0;
	else
		return 1;
}

void LEDStatus(BOOL LED_Green, BOOL LED_Red, BOOL LED_T_Green, BOOL LED_T_Red, BOOL LED_T_Yellow, BOOL Buzzer)
{
	DO_GreenLED   = LED_Green;		    // Checking    Button LED Status		
	DO_RedLED     = LED_Red;			// Initialize  Button LED Status
	DO_TL_Green   = LED_T_Green;
	DO_TL_Red	  = LED_T_Red;
	DO_TL_Yellow  = LED_T_Yellow;
	DO_Buzzer     = Buzzer;
}

void _CYCLIC ProgramCyclic(void)
{
	switch(Sys_State)		
	{
		case SYSTEM_ENERGIZED:
			LEDStatus(blinkLed(0.5, Time_Total), 0, 0, 0, 0, 0);
			break;
		
		case SYSTEM_CHECKING:
			LEDStatus(0, blinkLed(0.250, Time_Total), 0, 0, 0, 0);
			break;
		
		case SYSTEM_READY_FOR_INITIALIZING:
			LEDStatus(1, 0, 0, 0, 0, 0);
			break;
		
		case SYSTEM_READY_FOR_DATA:
			LEDStatus(1, 0, 0, 0, 0, 0);
			break;
		
		case SYSTEM_READY_FOR_TRAINING:
			LEDStatus(1, 0, 0, 0, 0, 0);
			break;
		
		case SYSTEM_ON_TRAINING:
			LEDStatus(1, 0, 1, 0, 0, 1);
			break;
		
		case SYSTEM_PARK_MODE:
			LEDStatus(1, 1, 0, 0, 0, 0);
			break;
		
		case SYSTEM_PASSIVE:
			LEDStatus(0, 1, 0, 0, 0, 0);
			break;
		
		case SYSTEM_ON_ERROR:
			LEDStatus(0, blinkLed(0.5, Time_Total), 0, 0, 1, 0);
			break;
		
		case SYSTEM_EMERGENCY:
			LEDStatus(0, blinkLed(0.250, Time_Total), 0, 1, 0, 0);
			break;
		
		default:
			break;
	}	
}
