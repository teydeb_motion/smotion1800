/* Libraries */
#include <bur/plctypes.h>
#include <AsDefault.h>

BOOL blinkLed(double time,double TTime)
{
	if ((int)(TTime/time)%2==1)
		return 0;
	else
		return 1;
}

void LEDStatus(BOOL LED_Green, BOOL LED_Red, BOOL LED_TopLamp)
{
	GreenLED = LED_Green;		// Checking    Button LED Status		
	RedLED   = LED_Red;			// Initialize  Button LED Status
	TopLampLED = LED_TopLamp;
}

void _CYCLIC ProgramCyclic(void)
{
	switch(Sys_State)		
	{
		case SYSTEM_ENERGIZED:
			LEDStatus(blinkLed(0.5, Time_Total), 0, 0);
			break;
		
		case SYSTEM_CHECKING:
			LEDStatus(0, blinkLed(0.250, Time_Total), 0);
			break;
		
		case SYSTEM_READY_FOR_INITIALIZING:
			LEDStatus(1, 0, 0);
			break;
		
		case SYSTEM_READY_FOR_DATA:
			LEDStatus(1, 0, 0);
			break;
		
		case SYSTEM_READY_FOR_TRAINING:
			LEDStatus(1, 0, 0);
			break;
		
		case SYSTEM_ON_TRAINING:
			LEDStatus(1, 0, 1);
			break;
		
		case SYSTEM_PARK_MODE:
			LEDStatus(1, 1, 0);
			break;
		
		case SYSTEM_PASSIVE:
			LEDStatus(0, 1, 0);
			break;
		
		case SYSTEM_ON_ERROR:
			LEDStatus(0, blinkLed(0.5, Time_Total),0);
			break;
		
		case SYSTEM_EMERGENCY:
			LEDStatus(0, blinkLed(0.250, Time_Total),0);
			break;
		
		default:
			break;
	}	
}
