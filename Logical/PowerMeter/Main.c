/* Libraries */
#include <bur/plctypes.h>
#include <AsDefault.h>

void _CYCLIC ProgramCyclic(void)
{
	PowerMeterFB.Enable = 1;
	PowerMeterFB.Axis = gUHandle.Ax[0].Axis;
	PowerMeterFB.Mode = mcONLY_PSM;
	PowerMeterFB.IntervalTime = 1000;	// ms
	MC_BR_PowerMeter(&PowerMeterFB);	// FB Call
	
	Total_ConsumedEnergy    += PowerMeterFB.PowerData.ConsumedEnergy;
	Total_RegeneratedEnergy += PowerMeterFB.PowerData.RegeneratedEnergy;
}
