#ifndef SanlabFunctionsRefGen
#define SanlabFunctionsRefGen
/* Inverse Kinematic Calculation */
void InverseKinematic(struct RobotStruct *Rob); 
/* Motion Cueing Function */
void GetMotionCueingFilterGains();
/* Position limitation for safety */
double POSLIM(double Length);
/* Assigns data to send data array */
void SendDataAssignment(struct Motion2Host* M2H);
/* Updates Motor States */
void UpdateInterp(struct InterpStruct * IS);
/* Updates Motor States */
void UpdateLink(struct LegStruct * Lin);
/* Updates Motor States */
void UpdateMotor(struct MotorStruct * MS);
/* Derivative function */
double DER(double NewData, double OldData, double dT);
/* Filter Function to use in derivation */
double FIL(struct FilterStruct *FS, double dT, double U0);
/* Low Pass Filter function using "Backward Euler" */
double LPF1(struct MotorStruct *MS, struct FilterStruct *FS, double U0);
/* High Pass Filter function using "Backward Euler"	*/
double HPF(struct FilterStruct *FS, double U0);
/* Disturbance Observer function using "Backward Euler" */
double DOB(struct MotorStruct *MS, struct FilterStruct *FS);
/* Integration function using "Backward Euler" */
double INT1(struct IntegratorStruct *IS, double U0);
/* Saturation function */
double SAT(double Input, double SaturationLevel);
/* Degree to Radian conversion */
double D2R(double Input);
/* Motor position to link position conversion */
double MP2LP(double Input);
/* Motor velocity to link velocity conversion */
double MV2LV(double Input);
/* Motor acceleration to link acceleration conversion */
double MA2LA(double Input);
// Link position to motor position conversion 
double LP2MP(double Input);
/* Link velocity to motor velocity conversion */
double LV2MV(double Input);
/* Link acceleration to motor acceleration conversion */
double LA2MA(double Input);
/* Smooth rise function from value 0 in time T1 to value 1 in time T2 */
double SigmoidRise(double Time, double T1, double T2);
/* Smooth stop function from value 1 in time T1 to value 0 in time T2 */
double SigmoidStop(double Time, double T1, double T2);
/* Smooth turn function from value 1 in time T1 to value -1 in time T2 */
double SigmoidTurn(double Time, double T1, double T2);
// Interpolation function
// Calculate A,B,C Coeffs of A*T^3 + B*T^2 + C*T + D = Y, T is a static array whose inverse is calculated offline with LSPB method
void Interpolate(struct InterpStruct *IS);
/* Reference Motor Position */
void MotorRefPos(double MotPos1, double MotPos2, double MotPos3, double MotPos4, double MotPos5, double MotPos6);
/* Reference Motor Velocity */
void MotorRefVel();
/* Reference Motor Acceleration */
void MotorRefAcc();
/* Reference Velocity Set to Zero */
void RefVelZero();
/* Reference Acceleration Set to Zero */
void RefAccZero();
/* Assign Reference Position */
void AssignRefPos(LREAL Value);
/* Motor Temperature Control */
void ReleaseIfMotorTempsOk();
/* Temperature Control */
void TemperatureControl(INT MotorIndex);
/* Check Motor Temperature */
void CheckForMotorTemperature();
/* Torque Control */
void TorqueControl(INT MotorIndex);
/* Check Motor Torque */
void CheckForMotorTorques();
/* Release Motor Torque */
void ReleaseIfMotorTorquesOk();
/* Check Emergency Flag */
void CheckForEmergencyFlag();
/* Check External Sensor */
void CheckforExternalSensors();
void EncoderControl(INT MotIndex);
/* Check Limit Sensor */
void CheckforLimitSensor();
/* Checking Start Control */
void IsCheckingStarted();
/* Initialize Start Control */
void IsInitializeStarted();
/* Position Control for Neutral Position */
extern void Stroke_PositionControl(LREAL MaxStroke, LREAL MinStroke);
void VelocityControl(LREAL MaxStroke, LREAL MinStroke, LREAL MaxGain, LREAL MinGain);
void CalculateMotResPos();
void CalculateMotResVel();
void CalculateMotResAcc();
void CalculateStroke();
/* Reference Leg Position Calculation Function */
void CalculateLegRefPos();
/* Reference Leg Velocity Calculation Function */
void CalculateLegRefVel();
/* Reference Leg Acceleration Calculation Function */
void CalculateLegRefAcc();
/* Leg Position Calculation Function */
void CalculateLegPos();
/* Leg Velocity Calculation Function */
void CalculateLegVel();
/* Leg Acceleration Calculation Function */
void CalculateLegAcc();
void CalculateMotorPower();
void SetFlagZero();
/* Stroke Control Maintenance */
void StrokeControlMaintenance(BOOL Condition1, BOOL Condition2, BOOL Condition3, BOOL Condition4, BOOL Condition5, BOOL Condition6, DINT Park_State);
/* Tumble Control for Roll */
void TumbleControlforRoll();
/* Tumble Control for Pitch */
void TumbleControlforPitch();
/* Tumble Limit for Roll */
void TumbleLimitforRoll();
/* Tumble Limit for Pitch */
void TumbleLimitforPitch();
/* DOB Calculation */
void DOBCalculationforMotor1();
void DOBCalculationforMotor2();
void DOBCalculationforMotor3();
void DOBCalculationforMotor4();
void DOBCalculationforMotor5();
void DOBCalculationforMotor6();
void AssignSigmoidRise(double RiseTime, double ProcessTime);
void EncoderFailsControl(INT MotorIndex);
void CheckforEncoder();
void BooltoByte();
/* Position limitation for safety */
double INITSAT(LREAL Vel, LREAL Val1, LREAL Val2);
LREAL LSPB(int index);
void LPSB_aCalculation(LREAL Act_Stroke, LREAL StartVel, LREAL StopVel, INT index);
void DummyVelCalculation_NeutraltoDownPosition(LREAL Act_Stroke, INT index);
void DummyVelCalculation_DowntoNeutralPosition(LREAL Act_Stroke, INT index);
void DummyVelCalculation_NeutraltoUpPosition(LREAL Act_Stroke, INT index);
void DummyVelCalculation_UptoNeutralPosition(LREAL Act_Stroke, INT index);
void Check_StrokeControl(LREAL Act_Stroke, INT FlagNumber);
void GoDownStrokeControl();						// Asagi inme hareketinde yumusatmalar icin yapilmasi gereken tanimlama ve ayarlamalar yapilir.
/* Platform Go Down Velocity Reference */
void PlatformGoDownVelReference();
#endif