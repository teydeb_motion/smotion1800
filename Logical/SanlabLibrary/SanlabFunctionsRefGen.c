// Ters Kinematik Hesaplamasi
void InverseKinematic(RobotStruct *Rob)
{
	// Re ile Rb degerlerinin carpimi
	double Cros_ReRb = Rb * Re;
	// Re ile Rb degerlerinin carpiminin 2 kati
	double Two_Cros_ReRb = 2 * Rb * Re;
	
	// X(Surge) eksen referans pozisyon degeri
	double Ex = Rob->EndX[0];
	// X(Surge) eksen referans pozisyon degerinin karesi
	double PowEx = Ex * Ex;
	
	// Y(Sway) eksen referans pozisyon degeri
	double Ey = Rob->EndY[0];
	// Y(Sway) eksen referans pozisyon degerinin karesi
	double PowEy = Ey * Ey;
	
	// Z(Heave) eksen referans pozisyon degeri
	double Ez = Rob->EndZ[0];
	// Z(Heave) eksen referans pozisyon degerinin karesi
	double PowEz = Ez * Ez;
	
	// R(Roll) eksen referans pozisyon degeri
	double Er = Rob->EndR[0];
	
	// P(Pitch) eksen referans pozisyon degeri
	double Ep = Rob->EndP[0];
	
	// q(yaw) eksen referans pozisyon degeri
	double Eq = Rob->EndQ[0];
	
	// Ust mafsal aci degerleri
	double _T1 = Rob->T1;
	double _T2 = Rob->T2;
	double _T3 = Rob->T3;
	double _T4 = Rob->T4;
	double _T5 = Rob->T5;
	double _T6 = Rob->T6;
	
	// Alt mafsal aci degerleri
	double _B1 = Rob->B1;
	double _B2 = Rob->B2;
	double _B3 = Rob->B3;
	double _B4 = Rob->B4;
	double _B5 = Rob->B5;
	double _B6 = Rob->B6;
	
	// Ters kinematik hesaplamasininin CPU kullanimini azaltmak icin olusturulan degerler
	double _Gain01 = Two_Re * Ex;
	double _Gain02 = Two_Re * Ey;
	double _Gain03 = Two_Re * Ez;
	double _Gain04 = sin(Er) * sin(Ep);
	double _Gain05 = cos(Er) * cos(Eq);
	double _Gain06 = cos(Er) * sin(Eq);
	double _Gain07 = cos(Ep) * cos(Eq);
	double _Gain08 = cos(Ep) * sin(Eq);
	double _Gain09 = Two_Cros_ReRb * _Gain04;
	double _Gain10 = Pow_Rb + Pow_Re + PowEx + PowEy + PowEz;
	double _Gain11 = - Two_Rb * Ex;
	double _Gain12 = Two_Rb * Ey;
	double _Gain13 = _Gain03;
	double _Gain14 = _Gain01 * _Gain07;
	double _Gain15 = _Gain02 * _Gain05;
	double _Gain16 = _Gain02 * _Gain08;
	double _Gain17 = _Gain01 * _Gain06;
	double _Gain18 = _Gain03 * sin(Er) * cos(Ep);
	double _Gain19 = _Gain01 * _Gain04 * cos(Eq);
	double _Gain20 = _Gain02 * _Gain04 * sin(Eq);
	double _Gain21 = Two_Cros_ReRb * _Gain06;
	double _Gain22 = Two_Cros_ReRb * _Gain05;
	double _Gain23 = Two_Cros_ReRb * _Gain08;
	double _Gain24 = Two_Cros_ReRb * _Gain07;
	double _Gain25 = _Gain09 * sin(Eq);
	double _Gain26 = _Gain09 * cos(Eq);
		
	// 6 eksene ait bacak pozisyon degerleri
	Rob->Link1[0] = sqrt(_Gain10 + _Gain11 * cos(_B1) - _Gain12 * sin(_B1) - _Gain13 * sin(Ep) * cos(_T1) + _Gain14 * cos(_T1) + _Gain15 * sin(_T1) + _Gain16 * cos(_T1) - _Gain17 * sin(_T1) + _Gain18 * sin(_T1) + _Gain19 * sin(_T1) + _Gain20 * sin(_T1) + _Gain21 * cos(_B1) * sin(_T1) - _Gain22 * sin(_B1) * sin(_T1) - _Gain23 * sin(_B1) * cos(_T1) - _Gain24 * cos(_B1) * cos(_T1) - _Gain25 * sin(_B1) * sin(_T1) - _Gain26 * cos(_B1) * sin(_T1));
	Rob->Link2[0] = sqrt(_Gain10 + _Gain11 * cos(_B2) - _Gain12 * sin(_B2) - _Gain13 * sin(Ep) * cos(_T2) + _Gain14 * cos(_T2) + _Gain15 * sin(_T2) + _Gain16 * cos(_T2) - _Gain17 * sin(_T2) + _Gain18 * sin(_T2) + _Gain19 * sin(_T2) + _Gain20 * sin(_T2) + _Gain21 * cos(_B2) * sin(_T2) - _Gain22 * sin(_B2) * sin(_T2) - _Gain23 * sin(_B2) * cos(_T2) - _Gain24 * cos(_B2) * cos(_T2) - _Gain25 * sin(_B2) * sin(_T2) - _Gain26 * cos(_B2) * sin(_T2));
	Rob->Link3[0] = sqrt(_Gain10 + _Gain11 * cos(_B3) - _Gain12 * sin(_B3) - _Gain13 * sin(Ep) * cos(_T3) + _Gain14 * cos(_T3) + _Gain15 * sin(_T3) + _Gain16 * cos(_T3) - _Gain17 * sin(_T3) + _Gain18 * sin(_T3) + _Gain19 * sin(_T3) + _Gain20 * sin(_T3) + _Gain21 * cos(_B3) * sin(_T3) - _Gain22 * sin(_B3) * sin(_T3) - _Gain23 * sin(_B3) * cos(_T3) - _Gain24 * cos(_B3) * cos(_T3) - _Gain25 * sin(_B3) * sin(_T3) - _Gain26 * cos(_B3) * sin(_T3));
	Rob->Link4[0] = sqrt(_Gain10 + _Gain11 * cos(_B4) - _Gain12 * sin(_B4) - _Gain13 * sin(Ep) * cos(_T4) + _Gain14 * cos(_T4) + _Gain15 * sin(_T4) + _Gain16 * cos(_T4) - _Gain17 * sin(_T4) + _Gain18 * sin(_T4) + _Gain19 * sin(_T4) + _Gain20 * sin(_T4) + _Gain21 * cos(_B4) * sin(_T4) - _Gain22 * sin(_B4) * sin(_T4) - _Gain23 * sin(_B4) * cos(_T4) - _Gain24 * cos(_B4) * cos(_T4) - _Gain25 * sin(_B4) * sin(_T4) - _Gain26 * cos(_B4) * sin(_T4));
	Rob->Link5[0] = sqrt(_Gain10 + _Gain11 * cos(_B5) - _Gain12 * sin(_B5) - _Gain13 * sin(Ep) * cos(_T5) + _Gain14 * cos(_T5) + _Gain15 * sin(_T5) + _Gain16 * cos(_T5) - _Gain17 * sin(_T5) + _Gain18 * sin(_T5) + _Gain19 * sin(_T5) + _Gain20 * sin(_T5) + _Gain21 * cos(_B5) * sin(_T5) - _Gain22 * sin(_B5) * sin(_T5) - _Gain23 * sin(_B5) * cos(_T5) - _Gain24 * cos(_B5) * cos(_T5) - _Gain25 * sin(_B5) * sin(_T5) - _Gain26 * cos(_B5) * sin(_T5));
	Rob->Link6[0] = sqrt(_Gain10 + _Gain11 * cos(_B6) - _Gain12 * sin(_B6) - _Gain13 * sin(Ep) * cos(_T6) + _Gain14 * cos(_T6) + _Gain15 * sin(_T6) + _Gain16 * cos(_T6) - _Gain17 * sin(_T6) + _Gain18 * sin(_T6) + _Gain19 * sin(_T6) + _Gain20 * sin(_T6) + _Gain21 * cos(_B6) * sin(_T6) - _Gain22 * sin(_B6) * sin(_T6) - _Gain23 * sin(_B6) * cos(_T6) - _Gain24 * cos(_B6) * cos(_T6) - _Gain25 * sin(_B6) * sin(_T6) - _Gain26 * cos(_B6) * sin(_T6));
}

/* Motion Cueing Function */
void GetMotionCueingFilterGains()
{
	HPFX1.G  = M_TWOPI * H2MSimulator.Surge1stHPGain;				// 1st high pass filter gain of translational X 
	HPFX2.G  = M_TWOPI * H2MSimulator.Surge2ndHPGain;				// 2nd high pass filter gain of translational X
	HPFX3.G  = M_TWOPI * H2MSimulator.Surge2ndHPGain;				// 3rd high pass filter gain of translational X
	HPFY1.G  = M_TWOPI * H2MSimulator.Sway1stHPGain;				// 1st high pass filter gain of translational Y
	HPFY2.G  = M_TWOPI * H2MSimulator.Sway2ndHPGain;				// 2nd high pass filter gain of translational Y
	HPFY3.G  = M_TWOPI * H2MSimulator.Sway2ndHPGain;				// 3rd high pass filter gain of translational Y
	HPFZ1.G  = M_TWOPI * H2MSimulator.Heave1stHPGain;				// 1st high pass filter gain of translational Z
	HPFZ2.G  = M_TWOPI * H2MSimulator.Heave2ndHPGain;				// 2nd high pass filter gain of translational Z
	HPFZ3.G  = M_TWOPI * H2MSimulator.Heave2ndHPGain;				// 3rd high pass filter gain of translational Z
	HPFR1.G  = M_TWOPI * H2MSimulator.Roll2ndHPGain;				// 1st high pass filter gain of rotational X
	HPFR2.G  = M_TWOPI * H2MSimulator.Roll2ndHPGain;				// 2nd high pass filter gain of rotational X
	HPFP1.G  = M_TWOPI * H2MSimulator.Pitch2ndHPGain;				// 1st high pass filter gain of rotational Y
	HPFP2.G  = M_TWOPI * H2MSimulator.Pitch2ndHPGain;				// 2nd high pass filter gain of rotational Y
	HPFQ1.G  = M_TWOPI * H2MSimulator.Yaw2ndHPGain;					// 1st high pass filter gain of rotational Z
	HPFQ2.G  = M_TWOPI * H2MSimulator.Yaw2ndHPGain;					// 2nd high pass filter gain of rotational Z
	LPFTCR.G = M_TWOPI * H2MSimulator.RollTiltLPGain;				// Low pass filter gain of tilt coordination for rotational X
	LPFTCP.G = M_TWOPI * H2MSimulator.PitchTiltLPGain;				// Low pass filter gain of tilt coordination for rotational Y	
}

/* Position limitation for safety */
double POSLIM(double Length)
{
	if (Length < MotMaxMargin)
		Length = MotMaxMargin;
	else if (Length > MotMinMargin)
		Length = MotMinMargin;

	return Length;
}

/* Updates Motor States */
void UpdateInterp(InterpStruct * IS)									
{
	int i;
	for (i = 1; i < 3 /* Interpolation Memory */; i++) 
	{
		IS->DataInput[3 - i] = IS->DataInput[3 - i - 1];
	}
}

/* Updates Motor States */
void UpdateMotor(MotorStruct * MS)									
{
	MS->Pos[1]    = MS->Pos[0];
	MS->Vel[1]    = MS->Vel[0];
	MS->Acc[1]    = MS->Acc[0];
	MS->IDes[1]   = MS->IDes[0];
	MS->IDis[1]   = MS->IDis[0];
	MS->IRef[1]   = MS->IRef[0];
	MS->IAct[1]   = MS->IAct[0];
	MS->RefPos[1] = MS->RefPos[0];
	MS->RefVel[1] = MS->RefVel[0];
	MS->RefAcc[1] = MS->RefAcc[0];
}

/* Derivative function */
double DER(double NewData, double OldData, double dT)
{
	return ((1 / dT) * (NewData - OldData));
}

/* Low Pass Filter function using "Backward Euler" */
double LPF(FilterStruct *FS, double U0)
{
	FS->Y0 = (FS->Y1 + FS->G * dT * U0) / (FS->G * dT + 1);
	FS->Y1 = FS->Y0;
	FS->U1 = U0;
	return FS->Y0;
}

/* High Pass Filter function using "Backward Euler"	*/
double HPF(FilterStruct *FS, double U0)
{
	FS->Y0 = (FS->Y1 - FS->U1 + U0) / (1+(dT * FS->G));
	FS->Y1 = FS->Y0;
	FS->U1 = U0;
	return FS->Y0;
}

/* Disturbance Observer function using "Backward Euler" */
double DOB(struct MotorStruct *MS, struct FilterStruct *FS)
{
	return((1 / MS->Kn) * (LPF(FS, ((MS->IAct[0] * MS->Kn) + (MS->VelVOB[0] * MS->Mn * FS->G))) - (MS->VelVOB[0] * MS->Mn * FS->G)));
}

/* Integration function using "Backward Euler" */
double INT1(struct IntegratorStruct *IS, double U0)
{
	IS->Y0 = dT * U0 + IS->Y1;
	IS->Y1 = IS->Y0;
	IS->U1 = U0;
	return IS->Y0;
}

/* Saturation function */
double SAT(double Input, double SaturationLevel)
{
	if (Input > SaturationLevel)
		Input = SaturationLevel;
	else if (Input < (-1 * SaturationLevel))
		Input = -1 * SaturationLevel;
	
	return Input;
}

/* Degree to Radian conversion */
double D2R(double Input)
{
	return(Input * M_TWOPI / 360);
}

/* Motor position to link position conversion */
double MP2LP(double Input)
{
	return((Input * (ballScrewPitch / ConvRate) / M_TWOPI) + NeutralLegLength);
}

/* Motor velocity to link velocity conversion */
double MV2LV(double Input)
{
	return(Input * (ballScrewPitch / ConvRate) / M_TWOPI);
}

/* Motor acceleration to link acceleration conversion */
double MA2LA(double Input)
{
	return(Input * (ballScrewPitch / ConvRate) / M_TWOPI);
}

// Link position to motor position conversion 
double LP2MP(double Input)
{
	return((Input - NeutralLegLength) * M_TWOPI / (ballScrewPitch / ConvRate));
}

/* Link velocity to motor velocity conversion */
double LV2MV(double Input)
{
	return(Input * M_TWOPI / (ballScrewPitch / ConvRate));
}

/* Link acceleration to motor acceleration conversion */
double LA2MA(double Input)
{
	return(Input * M_TWOPI / (ballScrewPitch / ConvRate));
}

/* Smooth rise function from value 0 in time T1 to value 1 in time T2 */
double SigmoidRise(double Time, double T1, double T2)
{
	return(1 / (1 + exp((-10 / (T2 - T1)) * (Time - (T1 + T2) / 2))));
}

/* Smooth stop function from value 1 in time T1 to value 0 in time T2 */
double SigmoidStop(double Time, double T1, double T2)
{
	return(1 - (1 / (1 + exp((-10 / (T2 - T1)) * (Time - (T1 + T2) / 2)))));
}

/* Smooth turn function from value 1 in time T1 to value -1 in time T2 */
double SigmoidTurn(double Time, double T1, double T2)
{
	return((2 - (2 / (1 + exp((-10 / (T2 - T1)) * (Time - (T1 + T2)/2))))) - 1);
}

// Interpolation function
// Calculate A,B,C Coeffs of A*T^3 + B*T^2 + C*T + D = Y, T is a static array whose inverse is calculated offline with LSPB method
void Interpolate(InterpStruct *IS)								
{
	IS->Coeffs[0] =  2 * IS->DataInput[1] -  2 * IS->DataInput[0]; 			// Coefficient A
	IS->Coeffs[1] = -9 * IS->DataInput[1] +  9 * IS->DataInput[0]; 			// Coefficient B
	IS->Coeffs[2] = 12 * IS->DataInput[1] - 12 * IS->DataInput[0]; 			// Coefficient C
	IS->Coeffs[3] = -4 * IS->DataInput[1] +  5 * IS->DataInput[0]; 			// Coefficient D
}

/* Reference Motor Position */
void MotorRefPos(double MotPos1, double MotPos2, double MotPos3, double MotPos4, double MotPos5, double MotPos6)
{
	Mot1.RefPos[0] = /*POSLIM(*/MotPos1 + Mot1.MotorOffsetForTraining/*)*/;											// Reference motor position (Result of Kinematic)
	Mot2.RefPos[0] = POSLIM(MotPos2 + Mot2.MotorOffsetForTraining);											// Reference motor position (Result of Kinematic)
	Mot3.RefPos[0] = POSLIM(MotPos3 + Mot3.MotorOffsetForTraining);											// Reference motor position (Result of Kinematic)
	Mot4.RefPos[0] = POSLIM(MotPos4 + Mot4.MotorOffsetForTraining);											// Reference motor position (Result of Kinematic)
	Mot5.RefPos[0] = POSLIM(MotPos5 + Mot5.MotorOffsetForTraining);											// Reference motor position (Result of Kinematic)
	Mot6.RefPos[0] = POSLIM(MotPos6 + Mot6.MotorOffsetForTraining);
}

/* Reference Motor Velocity */
void MotorRefVel()
{
	Mot1.RefVel[0] = /*SAT(LPF(&FRVM1, */DER(Mot1.RefPos[0], Mot1.RefPos[1], dT)/*), SatRefVel)*/;			// Reference motor velocity 
	Mot2.RefVel[0] = /*SAT(LPF(&FRVM2, */DER(Mot2.RefPos[0], Mot2.RefPos[1], dT)/*), SatRefVel)*/;			// Reference motor velocity 
	Mot3.RefVel[0] = /*SAT(LPF(&FRVM3, */DER(Mot3.RefPos[0], Mot3.RefPos[1], dT)/*), SatRefVel)*/;			// Reference motor velocity 
	Mot4.RefVel[0] = /*SAT(LPF(&FRVM4, */DER(Mot4.RefPos[0], Mot4.RefPos[1], dT)/*), SatRefVel)*/;			// Reference motor velocity 
	Mot5.RefVel[0] = /*SAT(LPF(&FRVM5, */DER(Mot5.RefPos[0], Mot5.RefPos[1], dT)/*), SatRefVel)*/;			// Reference motor velocity 
	Mot6.RefVel[0] = /*SAT(LPF(&FRVM6, */DER(Mot6.RefPos[0], Mot6.RefPos[1], dT)/*), SatRefVel)*/;			// Reference motor velocity 
}

/* Reference Motor Velocity */
void MotorRefVel_Rep(double MotVel1, double MotVel2, double MotVel3, double MotVel4, double MotVel5, double MotVel6)
{
	Mot1.RefVel[0] = /*SAT(*/LPF(&FRVM1, MotVel1)/* SatRefVel)*/;			// Reference motor velocity 
	Mot2.RefVel[0] = /*SAT(*/LPF(&FRVM2, MotVel2)/* SatRefVel)*/;			// Reference motor velocity 
	Mot3.RefVel[0] = /*SAT(*/LPF(&FRVM3, MotVel3)/* SatRefVel)*/;			// Reference motor velocity 
	Mot4.RefVel[0] = /*SAT(*/LPF(&FRVM4, MotVel4)/* SatRefVel)*/;			// Reference motor velocity 
	Mot5.RefVel[0] = /*SAT(*/LPF(&FRVM5, MotVel5)/* SatRefVel)*/;			// Reference motor velocity 
	Mot6.RefVel[0] = /*SAT(*/LPF(&FRVM6, MotVel6)/* SatRefVel)*/;			// Reference motor velocity 
}

/* Reference Motor Acceleration */
void MotorRefAcc()
{
	Mot1.RefAcc[0] = /*SAT(LPF(&FRAM1, */DER(Mot1.RefVel[0], Mot1.RefVel[1], dT)/*), SatRefAcc)*/;			// Reference motor acceleration 
	Mot2.RefAcc[0] = /*SAT(LPF(&FRAM2, */DER(Mot2.RefVel[0], Mot2.RefVel[1], dT)/*), SatRefAcc)*/;			// Reference motor acceleration 
	Mot3.RefAcc[0] = /*SAT(LPF(&FRAM3, */DER(Mot3.RefVel[0], Mot3.RefVel[1], dT)/*), SatRefAcc)*/;			// Reference motor acceleration 
	Mot4.RefAcc[0] = /*SAT(LPF(&FRAM4, */DER(Mot4.RefVel[0], Mot4.RefVel[1], dT)/*), SatRefAcc)*/;			// Reference motor acceleration 
	Mot5.RefAcc[0] = /*SAT(LPF(&FRAM5, */DER(Mot5.RefVel[0], Mot5.RefVel[1], dT)/*), SatRefAcc)*/;			// Reference motor acceleration 
	Mot6.RefAcc[0] = /*SAT(LPF(&FRAM6, */DER(Mot6.RefVel[0], Mot6.RefVel[1], dT)/*), SatRefAcc)*/;			// Reference motor acceleration
}

/* Reference Motor Acceleration */
void MotorRefAcc_Rep(double MotAcc1, double MotAcc2, double MotAcc3, double MotAcc4, double MotAcc5, double MotAcc6)
{
	Mot1.RefAcc[0] = /*SAT(*/LPF(&FRAM1, MotAcc1)/*, SatRefAcc)*/;			// Reference motor acceleration 
	Mot2.RefAcc[0] = /*SAT(*/LPF(&FRAM2, MotAcc2)/*, SatRefAcc)*/;			// Reference motor acceleration 
	Mot3.RefAcc[0] = /*SAT(*/LPF(&FRAM3, MotAcc3)/*, SatRefAcc)*/;			// Reference motor acceleration 
	Mot4.RefAcc[0] = /*SAT(*/LPF(&FRAM4, MotAcc4)/*, SatRefAcc)*/;			// Reference motor acceleration 
	Mot5.RefAcc[0] = /*SAT(*/LPF(&FRAM5, MotAcc5)/*, SatRefAcc)*/;			// Reference motor acceleration 
	Mot6.RefAcc[0] = /*SAT(*/LPF(&FRAM6, MotAcc6)/*, SatRefAcc)*/;			// Reference motor acceleration
}

/* Reference Velocity Set to Zero */
void RefVelZero()
{
	Mot1.RefVel[0] = 0;
	Mot2.RefVel[0] = 0;
	Mot3.RefVel[0] = 0;
	Mot4.RefVel[0] = 0;
	Mot5.RefVel[0] = 0;
	Mot6.RefVel[0] = 0;
}	

/* Reference Acceleration Set to Zero */
void RefAccZero()
{
	Mot1.RefAcc[0] = 0;
	Mot2.RefAcc[0] = 0;
	Mot3.RefAcc[0] = 0;
	Mot4.RefAcc[0] = 0;
	Mot5.RefAcc[0] = 0;
	Mot6.RefAcc[0] = 0;
}

/* Assign Reference Position */
void AssignRefPos(LREAL Value)
{
	Mot1.RefPos[0] = Value;
	Mot2.RefPos[0] = Value;
	Mot3.RefPos[0] = Value;
	Mot4.RefPos[0] = Value;
	Mot5.RefPos[0] = Value;
	Mot6.RefPos[0] = Value;
}

/* Check Emergency Flag */
void CheckForEmergencyFlag()
{
	FlagEMG = gMpAxisBasic_0[1].Info.DigitalInputsStatus.DriveEnable && gMpAxisBasic_0[2].Info.DigitalInputsStatus.DriveEnable &&
		gMpAxisBasic_0[3].Info.DigitalInputsStatus.DriveEnable && gMpAxisBasic_0[4].Info.DigitalInputsStatus.DriveEnable &&
		gMpAxisBasic_0[5].Info.DigitalInputsStatus.DriveEnable && gMpAxisBasic_0[6].Info.DigitalInputsStatus.DriveEnable;
}

void Axcon_ErrorReset(int Index)
{
	if(AxCon_0[Index].Internal.Step == AXCON_ERROR)
		AxCon_0[Index].Internal.bDisable = 0;
	else if(AxCon_0[Index].Internal.Step == AXCON_DISABLED)
		AxCon_0[Index].Internal.bDisable = 1;
}

BOOL Control_EncoVal(int Index)
{
	if (AxCon_0[Index].Internal.EncRawPos != 0) return 1;
	else return 0; 
}

void Check_EncoValue()
{
	EncoValFlag = Control_EncoVal(1) && Control_EncoVal(2) && Control_EncoVal(3) && Control_EncoVal(4) && Control_EncoVal(5) && Control_EncoVal(6);
}

/* Checking Start Control */
void IsCheckingStarted()
{
	Sys_State = (AxCon_0[1].Active && AxCon_0[2].Active && AxCon_0[3].Active && AxCon_0[4].Active && AxCon_0[5].Active && AxCon_0[6].Active && EncoValFlag)? SYSTEM_CHECKING:SYSTEM_ENERGIZED;
}

/* Initialize Start Control */
void IsInitializeStarted()
{
	Sys_State = (H2MMain.command == COMMAND_INITIALIZE) ? SYSTEM_INITIALIZING : SYSTEM_READY_FOR_INITIALIZING;
	Time_Initialize = Time_Total;
}

/* Position Control for Neutral Position */
void Stroke_PositionControl(LREAL MaxStroke, LREAL MinStroke)
{	
	PosControl[0] = ((Stroke[0] < MaxStroke) && (Stroke[0] > MinStroke)) ? 1 : 0;	// Stroke Control for Neutral Positon Motor 1.
	PosControl[1] = ((Stroke[1] < MaxStroke) && (Stroke[1] > MinStroke)) ? 1 : 0;	// Stroke Control for Neutral Positon Motor 2.				
	PosControl[2] = ((Stroke[2] < MaxStroke) && (Stroke[2] > MinStroke)) ? 1 : 0;	// Stroke Control for Neutral Positon Motor 3.
	PosControl[3] = ((Stroke[3] < MaxStroke) && (Stroke[3] > MinStroke)) ? 1 : 0;	// Stroke Control for Neutral Positon Motor 4.
	PosControl[4] = ((Stroke[4] < MaxStroke) && (Stroke[4] > MinStroke)) ? 1 : 0;	// Stroke Control for Neutral Positon Motor 5.
	PosControl[5] = ((Stroke[5] < MaxStroke) && (Stroke[5] > MinStroke)) ? 1 : 0; // Stroke Control for Neutral Positon Motor 6.
}

void VelocityControl(LREAL MaxStroke, LREAL MinStroke, LREAL MaxGain, LREAL MinGain)
{
	Mot1.RefVel[0] = (Stroke[0] > MaxStroke) ? MaxGain * DummyVel[0] : ((Stroke[0] < MinStroke) ? MinGain * (DummyVel[0]) : SAT(LPF(&FRVM1, DER(Mot1.RefPos[0], Mot1.RefPos[1], dT)), 408));
	Mot2.RefVel[0] = (Stroke[1] > MaxStroke) ? MaxGain * DummyVel[1] : ((Stroke[1] < MinStroke) ? MinGain * (DummyVel[1]) : SAT(LPF(&FRVM2, DER(Mot2.RefPos[0], Mot2.RefPos[1], dT)), 408));
	Mot3.RefVel[0] = (Stroke[2] > MaxStroke) ? MaxGain * DummyVel[2] : ((Stroke[2] < MinStroke) ? MinGain * (DummyVel[2]) : SAT(LPF(&FRVM3, DER(Mot3.RefPos[0], Mot3.RefPos[1], dT)), 408));
	Mot4.RefVel[0] = (Stroke[3] > MaxStroke) ? MaxGain * DummyVel[3] : ((Stroke[3] < MinStroke) ? MinGain * (DummyVel[3]) : SAT(LPF(&FRVM4, DER(Mot4.RefPos[0], Mot4.RefPos[1], dT)), 408));
	Mot5.RefVel[0] = (Stroke[4] > MaxStroke) ? MaxGain * DummyVel[4] : ((Stroke[4] < MinStroke) ? MinGain * (DummyVel[4]) : SAT(LPF(&FRVM5, DER(Mot5.RefPos[0], Mot5.RefPos[1], dT)), 408));
	Mot6.RefVel[0] = (Stroke[5] > MaxStroke) ? MaxGain * DummyVel[5] : ((Stroke[5] < MinStroke) ? MinGain * (DummyVel[5]) : SAT(LPF(&FRVM6, DER(Mot6.RefPos[0], Mot6.RefPos[1], dT)), 408));					
}

void CalculateMotResPos()
{
	Mot1.Pos[0]  = M_TWOPI * (AxCon_0[1].Internal.EncRawPos - Motor_Offset_Var[0]) / motEncVal;	// Position Calculation for Motor1
	Mot2.Pos[0]  = M_TWOPI * (AxCon_0[2].Internal.EncRawPos - Motor_Offset_Var[1]) / motEncVal;	// Position Calculation for Motor2
	Mot3.Pos[0]  = M_TWOPI * (AxCon_0[3].Internal.EncRawPos - Motor_Offset_Var[2]) / motEncVal;	// Position Calculation for Motor3
	Mot4.Pos[0]  = M_TWOPI * (AxCon_0[4].Internal.EncRawPos - Motor_Offset_Var[3]) / motEncVal;	// Position Calculation for Motor4
	Mot5.Pos[0]  = M_TWOPI * (AxCon_0[5].Internal.EncRawPos - Motor_Offset_Var[4]) / motEncVal;	// Position Calculation for Motor5
	Mot6.Pos[0]  = M_TWOPI * (AxCon_0[6].Internal.EncRawPos - Motor_Offset_Var[5]) / motEncVal;	// Position Calculation for Motor6
}

void CalculateMotResVel()
{
	Mot1.Vel[0]  = DER(Mot1.Pos[0], Mot1.Pos[1], dT);
	Mot2.Vel[0]  = DER(Mot2.Pos[0], Mot2.Pos[1], dT);
	Mot3.Vel[0]  = DER(Mot3.Pos[0], Mot3.Pos[1], dT);
	Mot4.Vel[0]  = DER(Mot4.Pos[0], Mot4.Pos[1], dT);
	Mot5.Vel[0]  = DER(Mot5.Pos[0], Mot5.Pos[1], dT);
	Mot6.Vel[0]  = DER(Mot6.Pos[0], Mot6.Pos[1], dT);
}

void CalculateMotResVelVOB()
{
	Mot1.VelVOB[0]  = LPF(&FVM1, Mot1.Vel[0]);
	Mot2.VelVOB[0]  = LPF(&FVM2, Mot2.Vel[0]);
	Mot3.VelVOB[0]  = LPF(&FVM3, Mot3.Vel[0]);
	Mot4.VelVOB[0]  = LPF(&FVM4, Mot4.Vel[0]);
	Mot5.VelVOB[0]  = LPF(&FVM5, Mot5.Vel[0]);
	Mot6.VelVOB[0]  = LPF(&FVM6, Mot6.Vel[0]);
}

void CalculateMotResAcc()
{
	Mot1.Acc[0]  = LPF(&FAM1, DER(Mot1.Vel[0], Mot1.Vel[1], dT));							// Acceleration Calculation for Motor1
	Mot2.Acc[0]  = LPF(&FAM2, DER(Mot2.Vel[0], Mot2.Vel[1], dT));							// Acceleration Calculation for Motor2
	Mot3.Acc[0]  = LPF(&FAM3, DER(Mot3.Vel[0], Mot3.Vel[1], dT));							// Acceleration Calculation for Motor3
	Mot4.Acc[0]  = LPF(&FAM4, DER(Mot4.Vel[0], Mot4.Vel[1], dT));							// Acceleration Calculation for Motor4
	Mot5.Acc[0]  = LPF(&FAM5, DER(Mot5.Vel[0], Mot5.Vel[1], dT));							// Acceleration Calculation for Motor5
	Mot6.Acc[0]  = LPF(&FAM6, DER(Mot6.Vel[0], Mot6.Vel[1], dT));							// Acceleration Calculation for Motor6		
}

void CalculateStroke()
{
	Stroke[0]    = (Mot1.Pos[0] * ballScrewPitch) / (M_TWOPI * ConvRate);												// Stroke Calculation for Leg1
	Stroke[1]    = (Mot2.Pos[0] * ballScrewPitch) / (M_TWOPI * ConvRate);												// Stroke Calculation for Leg2
	Stroke[2]    = (Mot3.Pos[0] * ballScrewPitch) / (M_TWOPI * ConvRate);												// Stroke Calculation for Leg3
	Stroke[3]    = (Mot4.Pos[0] * ballScrewPitch) / (M_TWOPI * ConvRate);												// Stroke Calculation for Leg4
	Stroke[4]    = (Mot5.Pos[0] * ballScrewPitch) / (M_TWOPI * ConvRate);												// Stroke Calculation for Leg5
	Stroke[5]    = (Mot6.Pos[0] * ballScrewPitch) / (M_TWOPI * ConvRate);												// Stroke Calculation for Leg6
}

/* Reference Leg Position Calculation Function */
void CalculateLegRefPos()
{
	Leg1.RefPos[0] = MP2LP(-Mot1.RefPos[0]);
	Leg2.RefPos[0] = MP2LP(-Mot2.RefPos[0]);
	Leg3.RefPos[0] = MP2LP(-Mot3.RefPos[0]);
	Leg4.RefPos[0] = MP2LP(-Mot4.RefPos[0]);
	Leg5.RefPos[0] = MP2LP(-Mot5.RefPos[0]);
	Leg6.RefPos[0] = MP2LP(-Mot6.RefPos[0]);
}

/* Reference Leg Velocity Calculation Function */
void CalculateLegRefVel()
{
	Leg1.RefVel[0] = MV2LV(-Mot1.RefVel[0]);
	Leg2.RefVel[0] = MV2LV(-Mot2.RefVel[0]);
	Leg3.RefVel[0] = MV2LV(-Mot3.RefVel[0]);
	Leg4.RefVel[0] = MV2LV(-Mot4.RefVel[0]);
	Leg5.RefVel[0] = MV2LV(-Mot5.RefVel[0]);
	Leg6.RefVel[0] = MV2LV(-Mot6.RefVel[0]);
}

/* Reference Leg Acceleration Calculation Function */
void CalculateLegRefAcc()
{
	Leg1.RefAcc[0] = MA2LA(-Mot1.RefAcc[0]);
	Leg2.RefAcc[0] = MA2LA(-Mot2.RefAcc[0]);
	Leg3.RefAcc[0] = MA2LA(-Mot3.RefAcc[0]);
	Leg4.RefAcc[0] = MA2LA(-Mot4.RefAcc[0]);
	Leg5.RefAcc[0] = MA2LA(-Mot5.RefAcc[0]);
	Leg6.RefAcc[0] = MA2LA(-Mot6.RefAcc[0]);
}

/* Leg Position Calculation Function */
void CalculateLegPos()
{
	Leg1.Pos[0] = MP2LP(-Mot1.Pos[0]);
	Leg2.Pos[0] = MP2LP(-Mot2.Pos[0]);
	Leg3.Pos[0] = MP2LP(-Mot3.Pos[0]);
	Leg4.Pos[0] = MP2LP(-Mot4.Pos[0]);
	Leg5.Pos[0] = MP2LP(-Mot5.Pos[0]);
	Leg6.Pos[0] = MP2LP(-Mot6.Pos[0]);
}

/* Leg Velocity Calculation Function */
void CalculateLegVel()
{
	Leg1.Vel[0] = MV2LV(-Mot1.Vel[0]);
	Leg2.Vel[0] = MV2LV(-Mot2.Vel[0]);
	Leg3.Vel[0] = MV2LV(-Mot3.Vel[0]);
	Leg4.Vel[0] = MV2LV(-Mot4.Vel[0]);
	Leg5.Vel[0] = MV2LV(-Mot5.Vel[0]);
	Leg6.Vel[0] = MV2LV(-Mot6.Vel[0]);
}

/* Leg Acceleration Calculation Function */
void CalculateLegAcc()
{
	Leg1.Acc[0] = MA2LA(-Mot1.Acc[0]);
	Leg2.Acc[0] = MA2LA(-Mot2.Acc[0]);
	Leg3.Acc[0] = MA2LA(-Mot3.Acc[0]);
	Leg4.Acc[0] = MA2LA(-Mot4.Acc[0]);
	Leg5.Acc[0] = MA2LA(-Mot5.Acc[0]);
	Leg6.Acc[0] = MA2LA(-Mot6.Acc[0]);
}

void CalculateMotorPower()
{
	Mot1.Power = gMpAxisBasic_0[1].Velocity * gMpAxisBasic_0[1].Info.CyclicRead.Torque.Value * M_TWOPI / motEncVal;
	Mot2.Power = gMpAxisBasic_0[2].Velocity * gMpAxisBasic_0[2].Info.CyclicRead.Torque.Value * M_TWOPI / motEncVal;
	Mot3.Power = gMpAxisBasic_0[3].Velocity * gMpAxisBasic_0[3].Info.CyclicRead.Torque.Value * M_TWOPI / motEncVal;
	Mot4.Power = gMpAxisBasic_0[4].Velocity * gMpAxisBasic_0[4].Info.CyclicRead.Torque.Value * M_TWOPI / motEncVal;
	Mot5.Power = gMpAxisBasic_0[5].Velocity * gMpAxisBasic_0[5].Info.CyclicRead.Torque.Value * M_TWOPI / motEncVal;
	Mot6.Power = gMpAxisBasic_0[6].Velocity * gMpAxisBasic_0[6].Info.CyclicRead.Torque.Value * M_TWOPI / motEncVal;
}

void SetFlagZero()
{
	memset(&F_1, 0, sizeof(F_1));				// Asagi inme hareketinde 1. kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_2, 0, sizeof(F_2));				// Asagi inme hareketinde 2. kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_3, 0, sizeof(F_3));				// Asagi inme hareketinde 3. kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_4, 0, sizeof(F_4));				// Asagi inme hareketinde 4. kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_5, 0, sizeof(F_5));				// A�a�� pozisyondan Yukari cikma hareketinde 1. Kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_6, 0, sizeof(F_6));				// A�a�� pozisyondan Yukari cikma hareketinde 2. Kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_7, 0, sizeof(F_7));				// Neutral pozisyondan Yukari cikma hareketinde 1. Kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_8, 0, sizeof(F_8));				// Neutral pozisyondan Yukari cikma hareketinde 2. Kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_9, 0, sizeof(F_8));				// Neutral pozisyondan Yukari cikma hareketinde 2. Kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	memset(&F_10, 0, sizeof(F_8));				// Neutral pozisyondan Yukari cikma hareketinde 2. Kosulda hesaplama islemine 1 kere girmesi icin flag sifirlanir.
	
	memset(&DummyVel, 0, sizeof(DummyVel));		// Hesaplanacak olan hizin icine atilacagi degisken sifirlanir.
}

/* Stroke Control Maintenance */
void StrokeControlMaintenance(BOOL Condition1, BOOL Condition2, BOOL Condition3, BOOL Condition4, BOOL Condition5, BOOL Condition6, DINT Park_State)
{
	Counter_Initial = (Condition1 && Condition2 && Condition3 && Condition4 && Condition5 && Condition6) ? (Counter_Initial + 1) : 0;
	
	if(Counter_Initial < 2500)
	{
		SetPowerMotors(1);
		ParkingProcess = 1;
	}
	else if (Counter_Initial >= 2500)					// If 1 sec passed.
	{
		Counter_Initial = 2500;
		SetPowerMotors(0);								// Motors power off.
		ParkStatus = Park_State;
		SetFlagZero();
		memset(&F_InitStroke, 0, sizeof(F_InitStroke));
		ParkingProcess = 0;
	}	
}

/* Tumble Control for Roll */
void TumbleControlforRoll()
{
	if 		((-M_PI_2 <= H2MSimulator.axisEuler.Roll) && (H2MSimulator.axisEuler.Roll <= M_PI_2))
		EulerRollFinal = H2MSimulator.axisEuler.Roll;
	else if ((M_PI_2 < H2MSimulator.axisEuler.Roll) && (H2MSimulator.axisEuler.Roll <= M_PI))
		EulerRollFinal = M_PI - H2MSimulator.axisEuler.Roll;
	else if ((-M_PI <= H2MSimulator.axisEuler.Roll) && (H2MSimulator.axisEuler.Roll < -M_PI_2))
		EulerRollFinal = -M_PI - H2MSimulator.axisEuler.Roll;	
}

/* Tumble Control for Pitch */
void TumbleControlforPitch()
{
	if 		((-M_PI_2 <= H2MSimulator.axisEuler.Pitch) && (H2MSimulator.axisEuler.Pitch <= M_PI_2))
		EulerPitchFinal = H2MSimulator.axisEuler.Pitch;
	else if ((M_PI_2 < H2MSimulator.axisEuler.Pitch) && (H2MSimulator.axisEuler.Pitch <= M_PI))
		EulerPitchFinal = M_PI - H2MSimulator.axisEuler.Pitch;
	else if ((-M_PI <= H2MSimulator.axisEuler.Pitch) && (H2MSimulator.axisEuler.Pitch < -M_PI_2))
		EulerPitchFinal = -M_PI - H2MSimulator.axisEuler.Pitch;
}

/* Tumble Limit for Roll */
void TumbleLimitforRoll()
{
	if (fabsf(H2MSimulator.axisAngular.Roll) > 3)
	{
		Counter_RollTumble++;
		if (Counter_RollTumble >= 2000)
			Counter_RollTumble = 2000;
	}
	else
	{
		Counter_RollTumble--;
		if (Counter_RollTumble <= 0)
			Counter_RollTumble = 0;
	}
}

/* Tumble Limit for Pitch */
void TumbleLimitforPitch()
{
	if (fabsf(H2MSimulator.axisAngular.Pitch) > 3)
	{
		Counter_PitchTumble++;
		if (Counter_PitchTumble >= 2000)
			Counter_PitchTumble = 2000;
	}
	else
	{
		Counter_PitchTumble--;
		if (Counter_PitchTumble <= 0)
			Counter_PitchTumble = 0;
	}
}

/* DOB Calculation */
void DOBCalculation(MotorStruct *Mot, FilterStruct *FS, int index, AxCon_Buffer_typ *Buf)
{
	if (AxCon_0[index].Active) 
	{
		Mot->IDis[0] = DOB(Mot, FS);										// Calculate Disturbance Current for Motor1
		Mot->IDes[0] = Mot->AccDes * (Mot->Mn / Mot->Kn);					// Conversion of Desired Acceleration to Desired Current for Motor1
		Mot->IRef[0] = Mot->IDes[0] + Mot->IDis[0];							// Generation of Reference Current for Motor1
		Mot->IAct[0] = SAT(Mot->IRef[0], CurrentSat);						// Saturates Reference Current to Obtain Actual Current for Driver1		
		Buf[index].Buffer[0] = (float)Mot->IAct[0];
	} 
	else
		Buf[index].Buffer[0] = 0;
}

void State_BooltoByte()
{
	Byte_DriverState  	  = AxCon_0[1].Enable         * 2 + AxCon_0[2].Enable         * 4 + AxCon_0[3].Enable         * 8 + AxCon_0[4].Enable         * 16 + AxCon_0[5].Enable         * 32 + AxCon_0[6].Enable         * 64;
	Byte_MotorState   	  = AxCon_0[1].Info.PoweredOn * 2 + AxCon_0[2].Info.PoweredOn * 4 + AxCon_0[3].Info.PoweredOn * 8 + AxCon_0[4].Info.PoweredOn * 16 + AxCon_0[5].Info.PoweredOn * 32 + AxCon_0[6].Info.PoweredOn * 64;
	Byte_DriverPower      = gMpAxisBasic_0[1].PowerOn * 2 + gMpAxisBasic_0[2].PowerOn * 4 + gMpAxisBasic_0[3].PowerOn * 8 + gMpAxisBasic_0[4].PowerOn * 16 + gMpAxisBasic_0[5].PowerOn * 32 + gMpAxisBasic_0[6].PowerOn * 64;
	
	if(SystemInitialized == Completed || Error_LimitSensor)
		Byte_LimitSensor = !DI_LimitSensor[0] * 2 + !DI_LimitSensor[1] * 4 + !DI_LimitSensor[2] * 8 + !DI_LimitSensor[3] * 16 + !DI_LimitSensor[4] * 32 + !DI_LimitSensor[5] * 64;
	else Byte_LimitSensor = 0;
}

/* Position limitation for safety */
double INITSAT(LREAL Vel, LREAL Val1, LREAL Val2)
{
	double min;
	double max;
	
	max = (Val1 > Val2) ? Val1 : Val2;
	min = (Val1 > Val2) ? Val2 : Val1;
		
	if (Vel > max)
		Vel = max;	
	else if (Vel < min)
		Vel = min;

	return Vel;
}

LREAL LSPB(int index)
{
	return a0[index] + a1[index] * (Time_Total - DummyTime[index]) + a2[index] * pow((Time_Total - DummyTime[index]),2) + a3[index] * pow((Time_Total - DummyTime[index]),3);
}

void LPSB_aCalculation(LREAL Act_Stroke, LREAL StartVel, LREAL StopVel, INT index)
{
	DummyTime[index] = Time_Total;
	t0[index] = 0;
	Mp[index] = (M_TWOPI * Act_Stroke * ConvRate ) / ballScrewPitch;
	tf[index] = 2 * fabs(Mp[index]) / fabs(StartVel + StopVel);

	a0[index] = (StartVel*(- pow(tf[index],3) + 3*t0[index]*pow(tf[index],2)))/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3)) - (StopVel*(- pow(t0[index],3) + 3*tf[index]*pow(t0[index],2)))/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3)) - (t0[index]*pow(tf[index],2)*v0)/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2)) - (pow(t0[index],2)*tf[index]*vf)/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2));
	a1[index] = (v0*(pow(tf[index],2) + 2*t0[index]*tf[index]))/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2)) + (vf*(pow(t0[index],2) + 2*tf[index]*t0[index]))/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2)) - (6*StartVel*t0[index]*tf[index])/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3)) + (6*StopVel*t0[index]*tf[index])/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3));
	a2[index] = (3*StartVel*(t0[index] + tf[index]))/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3)) - (vf*(2*t0[index] + tf[index]))/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2)) - (v0*(t0[index] + 2*tf[index]))/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2)) - (3*StopVel*(t0[index] + tf[index]))/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3));
	a3[index] =  v0/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2)) + vf/(pow(t0[index],2) - 2*t0[index]*tf[index] + pow(tf[index],2)) - (2*StartVel)/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3)) + (2*StopVel)/(pow(t0[index],3) - 3*pow(t0[index],2)*tf[index] + 3*t0[index]*pow(tf[index],2) - pow(tf[index],3));
}

void DummyVelCalculation_NeutraltoDownPosition(LREAL Act_Stroke, INT index)
{
	if (Act_Stroke < (Down_StrokeLim1 + SmoothStroke))
	{
		if (F_InitStroke[index] == 1 || Act_Stroke < Down_StrokeLim1)
		{
			if (!F_1[index])
			{
				LPSB_aCalculation(SmoothStroke, InitVel, Down_VelMax, index);
				F_1[index] = 1;
			}
			DummyVel[index] =  (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), Down_VelMax, InitVel) : Down_VelMax;
		}
		else if (F_InitStroke[index] == 2)
		{
			if (!F_2[index])
			{
				LPSB_aCalculation(((Down_StrokeLim1 + SmoothStroke) - Act_Stroke), InitVel, Down_VelMax, index);
				F_2[index] = 1;
			}
			DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), Down_VelMax, InitVel) : Down_VelMax;
		}
	}
	else if((Act_Stroke >= (Down_StrokeLim1 + SmoothStroke)) && (Act_Stroke < (Down_StrokeLim1 + (2 * SmoothStroke))))
	{
		if (F_InitStroke[index] == 3)
		{
			if (!F_3[index])
			{
				LPSB_aCalculation(((Down_StrokeLim1 + (2 * SmoothStroke)) - Act_Stroke), InitVel, Down_VelMin, index);
				F_3[index] = 1;
			}
			DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), Down_VelMin, InitVel) : Down_VelMin;
		}
		else
		{
			if (!F_4[index])
			{
				LPSB_aCalculation(((Down_StrokeLim1 + (2 * SmoothStroke)) - Act_Stroke), Down_VelMax, Down_VelMin, index);
				F_4[index] = 1;
			}
			DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), Down_VelMax, Down_VelMin) : Down_VelMin;
		}
	}
	else if(Act_Stroke >= (Down_StrokeLim1 + (2 * SmoothStroke)) || F_InitStroke[index] == 4)
		DummyVel[index] = (/*DI_LimitSensor[2]*/ DummyLimitSensor)? Down_VelMin : InitVel;
}

void DummyVelCalculation_DowntoNeutralPosition(LREAL Act_Stroke, INT index)
{
	if (Act_Stroke > Neutral_StrokeLim)									
	{
		if (!F_5[index])
		{
			LPSB_aCalculation(SmoothStroke, InitVel, Up_VelMax, index);
			F_5[index] = 1;	
		}
		DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), InitVel, Up_VelMax) : Up_VelMax;
	}
	else if (Act_Stroke <= Neutral_StrokeLim)
	{
		if (!F_6[index])
		{
			LPSB_aCalculation(SmoothStroke, Up_VelMax, Up_VelMin, index);
			F_6[index] = 1;	
		}
		DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), Up_VelMin, Up_VelMax) : Up_VelMin;
	}
}

void DummyVelCalculation_NeutraltoUpPosition(LREAL Act_Stroke, INT index)
{
	if(Act_Stroke > Up_StrokeLim)
	{
		if(!F_7[index])
		{
			LPSB_aCalculation(SmoothStroke, InitVel, Up_VelMax, index);
			F_7[index] = 1;
		}
		DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), InitVel, Up_VelMax) : Up_VelMax;
	}
	else if(Act_Stroke <= Up_StrokeLim)
	{ 
		if (!F_8[index])
		{
			LPSB_aCalculation(SmoothStroke, Up_VelMax, Up_VelMin, index);
			F_8[index] = 1;	
		}
		DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), Up_VelMin, Up_VelMax) : Up_VelMin;
	}
}

void DummyVelCalculation_UptoNeutralPosition(LREAL Act_Stroke, INT index)
{
	if(Act_Stroke < -0.05)
	{
		if(!F_9[index])
		{
			LPSB_aCalculation(SmoothStroke, InitVel, Down_VelMax, index);
			F_9[index] = 1;
		}
		DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), InitVel, Down_VelMax) : Down_VelMax;
	}
	else if(Act_Stroke >= -0.05)
	{
		if (!F_10[index])
		{
			LPSB_aCalculation(SmoothStroke, Down_VelMax, Down_VelMin, index);
			F_10[index] = 1;	
		}
		DummyVel[index] = (Time_Total < DummyTime[index] + tf[index]) ? INITSAT(LSPB(index), Down_VelMax, Down_VelMin) : Down_VelMin;
	}
}

void Check_StrokeControl(LREAL Act_Stroke, INT FlagNumber)
{
	if (Act_Stroke < Down_StrokeLim1)
		F_InitStroke[FlagNumber] = 1;
		
	else if((Act_Stroke >= Down_StrokeLim1) && (Act_Stroke < (Down_StrokeLim1 + SmoothStroke)))
		F_InitStroke[FlagNumber] = 2;
		
	else if((Act_Stroke >= (Down_StrokeLim1 + SmoothStroke)) && (Act_Stroke < (Down_StrokeLim1 + (2 * SmoothStroke))))
		F_InitStroke[FlagNumber] = 3;
		
	else if(Act_Stroke >= (Down_StrokeLim1 + (2 * SmoothStroke)))
		F_InitStroke[FlagNumber] = 4;
}

void GoDownStrokeControl()						// Asagi inme hareketinde yumusatmalar icin yapilmasi gereken tanimlama ve ayarlamalar yapilir.
{
	SetFlagZero();	
	
	Check_StrokeControl(Stroke[0], 0);			// 1. Motorun yumusatmasinin baslayacagi stroke araligi icin atamalar yapilir.
	Check_StrokeControl(Stroke[1], 1);			// 2. Motorun yumusatmasinin baslayacagi stroke araligi icin atamalar yapilir.
	Check_StrokeControl(Stroke[2], 2);          // 3. Motorun yumusatmasinin baslayacagi stroke araligi icin atamalar yapilir.
	Check_StrokeControl(Stroke[3], 3);          // 4. Motorun yumusatmasinin baslayacagi stroke araligi icin atamalar yapilir.
	Check_StrokeControl(Stroke[4], 4);          // 5. Motorun yumusatmasinin baslayacagi stroke araligi icin atamalar yapilir.
	Check_StrokeControl(Stroke[5], 5);          // 6. Motorun yumusatmasinin baslayacagi stroke araligi icin atamalar yapilir.
}

/* Platform Go Down Velocity Reference */
void PlatformGoDownVelReference()
{		
	DummyVelCalculation_NeutraltoDownPosition(Stroke[0], 0);
	DummyVelCalculation_NeutraltoDownPosition(Stroke[1], 1);
	DummyVelCalculation_NeutraltoDownPosition(Stroke[2], 2);
	DummyVelCalculation_NeutraltoDownPosition(Stroke[3], 3);
	DummyVelCalculation_NeutraltoDownPosition(Stroke[4], 4);
	DummyVelCalculation_NeutraltoDownPosition(Stroke[5], 5);

	Mot1.RefVel[0] = (DummyLimitSensor && AxCon_0[1].Info.PoweredOn) ? DummyVel[0] : 0.0;		//Motor 1 goes down with "Go_To_Vel" velocity until reaching limit sensors.
	Mot2.RefVel[0] = (DummyLimitSensor && AxCon_0[2].Info.PoweredOn) ? DummyVel[1] : 0.0;		//Motor 2 goes down with "Go_To_Vel" velocity until reaching limit sensors.
	Mot3.RefVel[0] = (DummyLimitSensor && AxCon_0[3].Info.PoweredOn) ? DummyVel[2] : 0.0;		//Motor 3 goes down with "Go_To_Vel" velocity until reaching limit sensors.
	Mot4.RefVel[0] = (DummyLimitSensor && AxCon_0[4].Info.PoweredOn) ? DummyVel[3] : 0.0;		//Motor 4 goes down with "Go_To_Vel" velocity until reaching limit sensors.
	Mot5.RefVel[0] = (DummyLimitSensor && AxCon_0[5].Info.PoweredOn) ? DummyVel[4] : 0.0;		//Motor 5 goes down with "Go_To_Vel" velocity until reaching limit sensors.
	Mot6.RefVel[0] = (DummyLimitSensor && AxCon_0[6].Info.PoweredOn) ? DummyVel[5] : 0.0;		//Motor 6 goes down with "Go_To_Vel" velocity until reaching limit sensors.			
	
//	Mot1.RefVel[0] = (DI_LimitSensor[2] && AxCon_0[1].Info.PoweredOn) ? DummyVel[0] : 0.0;		//Motor 1 goes down with "Go_To_Vel" velocity until reaching limit sensors.
//	Mot2.RefVel[0] = (DI_LimitSensor[2] && AxCon_0[2].Info.PoweredOn) ? DummyVel[1] : 0.0;		//Motor 2 goes down with "Go_To_Vel" velocity until reaching limit sensors.
//	Mot3.RefVel[0] = (DI_LimitSensor[2] && AxCon_0[3].Info.PoweredOn) ? DummyVel[2] : 0.0;		//Motor 3 goes down with "Go_To_Vel" velocity until reaching limit sensors.
//	Mot4.RefVel[0] = (DI_LimitSensor[2] && AxCon_0[4].Info.PoweredOn) ? DummyVel[3] : 0.0;		//Motor 4 goes down with "Go_To_Vel" velocity until reaching limit sensors.
//	Mot5.RefVel[0] = (DI_LimitSensor[2] && AxCon_0[5].Info.PoweredOn) ? DummyVel[4] : 0.0;		//Motor 5 goes down with "Go_To_Vel" velocity until reaching limit sensors.
//	Mot6.RefVel[0] = (DI_LimitSensor[2] && AxCon_0[6].Info.PoweredOn) ? DummyVel[5] : 0.0;		//Motor 6 goes down with "Go_To_Vel" velocity until reaching limit sensors.			
}

void KnMnAtama()
{
	Mot1.Kn = DummyKn;							// Nominal Torque Constant of Motor1 (Nm/A) 
	Mot2.Kn = DummyKn;							// Nominal Torque Constant of Motor2 (Nm/A) 
	Mot3.Kn = DummyKn;							// Nominal Torque Constant of Motor3 (Nm/A) 
	Mot4.Kn = DummyKn;							// Nominal Torque Constant of Motor4 (Nm/A) 
	Mot5.Kn = DummyKn;							// Nominal Torque Constant of Motor5 (Nm/A) 
	Mot6.Kn = DummyKn;							// Nominal Torque Constant of Motor6 (Nm/A)  
		
	Mot1.Mn = DummyMn;						// Nominal Inertia of Motor1 (kgm^2)
	Mot2.Mn = DummyMn;						// Nominal Inertia of Motor2 (kgm^2)
	Mot3.Mn = DummyMn;						// Nominal Inertia of Motor3 (kgm^2)
	Mot4.Mn = DummyMn;						// Nominal Inertia of Motor4 (kgm^2)
	Mot5.Mn = DummyMn;						// Nominal Inertia of Motor5 (kgm^2)
	Mot6.Mn = DummyMn;						// Nominal Inertia of Motor6 (kgm^2)
	
	FVM1.G = DummyVOB;							// Vel Filter Gain for Mot1
	FVM2.G = DummyVOB;							// Vel Filter Gain for Mot2
	FVM3.G = DummyVOB;							// Vel Filter Gain for Mot3
	FVM4.G = DummyVOB;							// Vel Filter Gain for Mot4
	FVM5.G = DummyVOB;							// Vel Filter Gain for Mot5
	FVM6.G = DummyVOB;							// Vel Filter Gain for Mot6
}

void CalculateEncoderOffset()
{
	if (Flag_EncoderCalculate)
	{
		Motor_Offset_Var[0] = AxCon_0[1].Internal.EncRawPos - EncoOffset;
		Motor_Offset_Var[1] = AxCon_0[2].Internal.EncRawPos - EncoOffset;
		Motor_Offset_Var[2] = AxCon_0[3].Internal.EncRawPos - EncoOffset;
		Motor_Offset_Var[3] = AxCon_0[4].Internal.EncRawPos - EncoOffset;
		Motor_Offset_Var[4] = AxCon_0[5].Internal.EncRawPos - EncoOffset;
		Motor_Offset_Var[5] = AxCon_0[6].Internal.EncRawPos - EncoOffset;
		
		Flag_EncoderCalculate = 0;
	}
}

void DriverErrorState()
{
	Error_Gmp[0] = gMpAxisBasic_0[0].Info.Diag.Internal.Severity;
	Error_Gmp[1] = gMpAxisBasic_0[1].Info.Diag.Internal.Severity;
	Error_Gmp[2] = gMpAxisBasic_0[2].Info.Diag.Internal.Severity;
	Error_Gmp[3] = gMpAxisBasic_0[3].Info.Diag.Internal.Severity;
	Error_Gmp[4] = gMpAxisBasic_0[4].Info.Diag.Internal.Severity;
	Error_Gmp[5] = gMpAxisBasic_0[5].Info.Diag.Internal.Severity;
	Error_Gmp[6] = gMpAxisBasic_0[6].Info.Diag.Internal.Severity;
	
	Error_Axc[0] = AxCon_0[0].Internal.Step;
	Error_Axc[1] = AxCon_0[1].Internal.Step;
	Error_Axc[2] = AxCon_0[2].Internal.Step;
	Error_Axc[3] = AxCon_0[3].Internal.Step;
	Error_Axc[4] = AxCon_0[4].Internal.Step;
	Error_Axc[5] = AxCon_0[5].Internal.Step;
	Error_Axc[6] = AxCon_0[6].Internal.Step;

//	if(Reset_Gmp)
//	{
//		gMpAxisBasic_0[1].ErrorReset = 1;
//		gMpAxisBasic_0[2].ErrorReset = 1;
//		gMpAxisBasic_0[3].ErrorReset = 1;
//		gMpAxisBasic_0[4].ErrorReset = 1;
//		gMpAxisBasic_0[5].ErrorReset = 1;
//		gMpAxisBasic_0[6].ErrorReset = 1;
//		Reset_Gmp = 0;
//	}
}