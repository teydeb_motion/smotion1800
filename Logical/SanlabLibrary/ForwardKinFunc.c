double ErrCalc(LegStruct *Leg, double B1, double T1)
{
	double _x  = FwdKin.XPos[0];
	double _y  = FwdKin.YPos[0];
	double _z  = FwdKin.ZPos[0];
	double _r  = FwdKin.RollPos[0];
	double _p  = FwdKin.PitchPos[0];
	double _q  = FwdKin.YawPos[0];
	
	double Gain1  = pow(Rb, 2) + pow(Re, 2) + pow(_x, 2) + pow(_y, 2) + pow(_z, 2);
	
	double Gain2  = Two_Rb * _x;
	double Gain3  = Two_Rb * _y;
	double Gain4  = Two_Re * _z;
	double Gain5  = Two_Re * _x;
	double Gain6  = Two_Re * _y;
	double Gain7  = Two_Rb * Re;
	double Gain8  = sin(_r) * sin(_p) * cos(_q);
	double Gain9  = sin(_r) * sin(_p) * sin(_q);
	double Gain10 = cos(_p) * cos(_q);
	double Gain11 = cos(_p) * sin(_q);
	double Gain12 = cos(_r) * cos(_q);
	double Gain13 = cos(_r) * sin(_q);
	double Gain14 = sin(_r) * cos(_p);
	double Gain15 = cos(B1) * sin(T1);
	double Gain16 = sin(B1) * sin(T1);
	
	return (Gain1 - pow(Leg->Pos[0], 2) - Gain2 * cos(B1) - Gain3 * sin(B1) - Gain4 * sin(_p) * cos(T1) + Gain5 * Gain10 * cos(T1) + Gain6 * Gain12 * sin(T1) + Gain6 * Gain11 * cos(T1) - Gain5 * Gain13 * sin(T1) + Gain4 * Gain14 * sin(T1) + Gain5 * Gain8 * sin(T1) + Gain6 * Gain9 * sin(T1) + Gain7 * (Gain13 * Gain15 - Gain12 * Gain16 - Gain11 * sin(B1) * cos(T1) - Gain10 * cos(B1) * cos(T1) - Gain9 * Gain16 - Gain8 * Gain15));
}

void ErrorFunc(RobotStruct *Rob)
{
	ErrFunc[0][0] = ErrCalc(&Leg1, Rob->B1, Rob->T1);
	ErrFunc[0][1] = ErrCalc(&Leg2, Rob->B2, Rob->T2);
	ErrFunc[0][2] = ErrCalc(&Leg3, Rob->B3, Rob->T3);
	ErrFunc[0][3] = ErrCalc(&Leg4, Rob->B4, Rob->T4);
	ErrFunc[0][4] = ErrCalc(&Leg5, Rob->B5, Rob->T5);
	ErrFunc[0][5] = ErrCalc(&Leg6, Rob->B6, Rob->T6);
}

void Jacob(int index, Fwd_Kinematic *FwdKin, RobotStruct *SMotion, double B, double T)
{
	double _X  = FwdKin->XPos[0];
	double _Y  = FwdKin->YPos[0];
	double _Z  = FwdKin->ZPos[0];
	double _R  = FwdKin->RollPos[0];
	double _P  = FwdKin->PitchPos[0];
	double _Q  = FwdKin->YawPos[0];
	double Two_ReRb = Rb * Re * 2;
	double TwoRe_X = Two_Re * _X;
	double TwoRe_Y = Two_Re * _Y;
	double TwoRe_Z = Two_Re * _Z;
	double SinPQ = sin(_P) * sin(_Q);
	double SinRQ = sin(_R) * sin(_Q);
	double CosRQ = cos(_R) * cos(_Q);
	double SinBT = sin(B) * sin(T);
	double SinRP = sin(_R) * sin(_P);
	double SinBCosT = sin(B) * cos(T);
	double CosPQ = cos(_P) * cos(_Q);
	double SinQT = sin(_Q) * sin(T);
	double CosQSinT = cos(_Q) * sin(T);
	double CosBSinT = cos(B) * sin(T);
	double SinRCosP = sin(_R) * cos(_P);
	double CosPSinQ = cos(_P) * sin(_Q);
	double SinPQT = SinPQ * sin(T);
	double CosBT = cos(B) * cos(T);
	
	J[0][index] = 2 * _X - Two_Rb * cos(B) + Two_Re * (SinRP * CosQSinT + CosPQ * cos(T) - cos(_R) * SinQT);
	J[1][index] = 2 * _Y - Two_Rb * sin(B) + Two_Re * (sin(_R) * SinPQT + CosPSinQ * cos(T) + CosRQ * sin(T));
	J[2][index] = 2 * _Z - Two_Re * (sin(_P) * cos(T) +  SinRCosP * sin(T));
	J[3][index] = - TwoRe_Y * sin(_R) * CosQSinT + TwoRe_X * SinRQ * sin(T) - Two_ReRb * cos(_R) * SinPQ * SinBT + TwoRe_Z * cos(_R) * cos(_P) * sin(T) - Two_ReRb * cos(_R) * sin(_P) * cos(_Q) * CosBSinT + TwoRe_X * cos(_R) * sin(_P) * CosQSinT - Two_ReRb * SinRQ * CosBSinT + TwoRe_Y * cos(_R) * SinPQT + Two_ReRb * sin(_R) * cos(_Q) * SinBT;
	J[4][index] = - TwoRe_Z * cos(_P) * cos(T) - TwoRe_X * sin(_P) * cos(_Q) * cos(T) - TwoRe_Y * SinPQ * cos(T) - Two_ReRb * sin(_R) * CosPQ * CosBSinT - TwoRe_Z * SinRP * sin(T) - Two_ReRb * SinRCosP * sin(_Q) * SinBT + TwoRe_X * sin(_R) * CosPQ * sin(T) + Two_ReRb * sin(_P) * cos(_Q) * CosBT + TwoRe_Y * SinRCosP * SinQT + Two_ReRb * SinPQ * SinBCosT;
	J[5][index] = - TwoRe_X * CosPSinQ * cos(T) + Two_ReRb * CosRQ * CosBSinT - TwoRe_Y * cos(_R) * SinQT + Two_ReRb * cos(_R) * sin(_Q) * SinBT + TwoRe_Y * CosPQ * cos(T) + Two_ReRb * sin(_R) * SinPQ * CosBSinT - TwoRe_X * CosRQ * sin(T) - Two_ReRb * SinRP * cos(_Q) * SinBT - TwoRe_X * sin(_R) * SinPQT + Two_ReRb * CosPSinQ * CosBT + TwoRe_Y * SinRP * CosQSinT - Two_ReRb * CosPQ * SinBCosT;	
}

void ForwardKinematicCalculation(Fwd_Kinematic *FwdKin, RobotStruct *SMotion)
{
	Jacob(0, FwdKin, SMotion, SMotion->B1, SMotion->T1);
	Jacob(1, FwdKin, SMotion, SMotion->B2, SMotion->T2);
	Jacob(2, FwdKin, SMotion, SMotion->B3, SMotion->T3);
	Jacob(3, FwdKin, SMotion, SMotion->B4, SMotion->T4);
	Jacob(4, FwdKin, SMotion, SMotion->B5, SMotion->T5);
	Jacob(5, FwdKin, SMotion, SMotion->B6, SMotion->T6);
	
	ErrorFunc(SMotion);
	
	MatrixInverseStatusID = MTLinAlgMatrixInverse(&Matrix_MatrixType, &InverseMatrix_MatrixType);
	MTLinAlgMatrixMultiplication(&ErrorFunc_MatrixType, &InverseMatrix_MatrixType, &Iteration_MatrixType);
	
	FwdKin->XPos[0]     -= Iteration[0][0];
	FwdKin->YPos[0]     -= Iteration[0][1];
	FwdKin->ZPos[0]     -= Iteration[0][2];
	FwdKin->RollPos[0]  -= Iteration[0][3];
	FwdKin->PitchPos[0] -= Iteration[0][4];
	FwdKin->YawPos[0]   -= Iteration[0][5];	
}

void UpdateKinematic(Fwd_Kinematic *FWD)
{
	FWD->XPos[1]       = FWD->XPos[0];
	FWD->YPos[1]       = FWD->YPos[0];
	FWD->ZPos[1]       = FWD->ZPos[0];
	FWD->RollPos[1]    = FWD->RollPos[0];
	FWD->PitchPos[1]   = FWD->PitchPos[0];
	FWD->YawPos[1]     = FWD->YawPos[0];
	
	FWD->XVel[1]       = FWD->XVel[0];
	FWD->YVel[1]       = FWD->YVel[0];
	FWD->ZVel[1]       = FWD->ZVel[0];
	FWD->RollVel[1]    = FWD->RollVel[0];
	FWD->PitchVel[1]   = FWD->PitchVel[0];
	FWD->YawVel[1]     = FWD->YawVel[0];
	
	FWD->XAcc[1]       = FWD->XAcc[0];
	FWD->YAcc[1]       = FWD->YAcc[0];
	FWD->ZAcc[1]       = FWD->ZAcc[0];
	FWD->RollAcc[1]    = FWD->RollAcc[0];
	FWD->PitchAcc[1]   = FWD->PitchAcc[0];
	FWD->YawAcc[1]     = FWD->YawAcc[0];
}

/* Derivative function */
double DER(double NewData, double OldData, double dT)
{
	return ((1 / dT) * (NewData - OldData));
}

void CalcAxisVel()
{
	FwdKin.XVel[0] = DER(FwdKin.XPos[0], FwdKin.XPos[1], dT_Fwd);
	FwdKin.YVel[0] = DER(FwdKin.YPos[0], FwdKin.YPos[1], dT_Fwd);
	FwdKin.ZVel[0] = DER(FwdKin.ZPos[0], FwdKin.ZPos[1], dT_Fwd);
	FwdKin.RollVel[0] = DER(FwdKin.RollPos[0], FwdKin.RollPos[1], dT_Fwd);
	FwdKin.PitchVel[0] = DER(FwdKin.PitchPos[0], FwdKin.PitchPos[1], dT_Fwd);
	FwdKin.YawVel[0] = DER(FwdKin.YawPos[0], FwdKin.YawPos[1], dT_Fwd);	
}

void CalcAxisAcc()
{
	FwdKin.XAcc[0] = DER(FwdKin.XVel[0], FwdKin.XVel[1], dT_Fwd);
	FwdKin.YAcc[0] = DER(FwdKin.YVel[0], FwdKin.YVel[1], dT_Fwd);
	FwdKin.ZAcc[0] = DER(FwdKin.ZVel[0], FwdKin.ZVel[1], dT_Fwd);
	FwdKin.RollAcc[0] = DER(FwdKin.RollVel[0], FwdKin.RollVel[1], dT_Fwd);
	FwdKin.PitchAcc[0] = DER(FwdKin.PitchVel[0], FwdKin.PitchVel[1], dT_Fwd);
	FwdKin.YawAcc[0] = DER(FwdKin.YawVel[0], FwdKin.YawVel[1], dT_Fwd);	
}
