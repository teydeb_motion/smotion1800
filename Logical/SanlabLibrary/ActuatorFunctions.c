/* Speed Zero Close */
void SpeedZeroClose(int i, struct  MotorStruct * Mot,struct AxCon* axC,double TotTime){
	
	EMG_Flag = 1;
	if (TotTime > Time_Emergency + 0.5 && EMG_Flag)
	{
		axC->Power = 0;
		if (!(axC->Info.PoweredOn))
		{
			Mot->IRef[0] = 0.0;
			Mot->IDes[0] = 0.0;
			Mot->IDis[0] = 0.0;
			Mot->IAct[0] = 0.0;
			axC->Enable = 0;
			if (!(axC->Active))
				EMG_Flag_Close[i - 1] = 1;
		}
	}
	else
	{
		PosControl[i - 1] = 0;
		Mot->RefVel[0] = 0;
		Mot->RefAcc[0] = 0;
		EMG_Flag_Close[i - 1]=0;
	}
}

// LSPB icin kullanilan A matrisinin parametre atamasi yapilir.
void Assign_SplineData(Spline_Coeff * _Mot, MotorStruct * Mot)
{
	// A: A[0], B: A[1], C: A[2], D: A[3]
	_Mot->A =  0.25 * (Mot->EndPos - Mot->NeutralPos);
	_Mot->B = -0.75 * (Mot->EndPos - Mot->NeutralPos);
	_Mot->C = 0;
	_Mot->D = Mot->EndPos;
}

// Hareket bitimlerinden sonra orta konuma gidecek sekilde hareket profili olusturulur.
BOOL EndTraining(double TotalTime, double EndTrainingTime)	
{
	// LSPB profilinin olusturulmasi icin kullanilan 0 - 2 saniye aras�nda zaman verisini olusturur.
	double EndTime = TotalTime - EndTrainingTime;
	
	// --------> Denklem Cikarimi <--------
	//		q0: Sinyal baslangic pozisyonu
	//		v0: Sinyal baslangic hizi
	//		qf: Sinyal bitis pozisyonu
	//		vf: Sinyal bitis hizi
	//		t0: Sinyal baslangic zamani
	//		tf: Sinyal bitis zamani
	//
	//		Denklem Cikarimi
	//		|----------------------------------|
	//		| M = [ 1  t0  t0^2  t0^3    ;     |
	//		|       0  1   2*t0  3*t0^2  ;     |
	//		|       1  tf  tf^2  tf^3    ;     |
	//		|       0  1   2*tf  3*tf^2  ];    |
	//		|                                  |
	//		| B = [q0; v0; qf; vf];            |
	//		| A = inv(M) * B;                  |
	//		|                                  |
	//		| a1 = A(1);                       |
	//		| a2 = A(2);                       |
	//		| a3 = A(3);                       |
	//		| a4 = A(4);                       |
	//		|----------------------------------|
	//
	//		A = [Spline.Motx.A; Spline.Motx.B; Spline.Motx.C; Spline.Motx.D];
	//		B = [Motx.EndPos; 0(v0); Motx.NeutralPos; 0(vf)];
	//		M = [{0 0 0 1}; {0 0 1 0}; {8 4 2 1}; {12 4 1 0}];
	//		inv(M) = [{0.25 0.25 -0.25 0.25}; {-0.75 -1 0.75 -0.5}; {0 1 0 0}; {1 0 0 0}];
	//		A = inv(M) * B;
	//		A = [{0.25 * (Motx.EndPos - Motx.NeutralPos)}; {-0.75 * (Motx.EndPos - Motx.NeutralPos)}; {0}; {Motx.EndPos}];
	// --------------------------------------
	
	// Kullanilacak A matrisinin parametreleri atanir.
	Assign_SplineData(&(Spline.Mot1), &Mot1);
	Assign_SplineData(&(Spline.Mot2), &Mot2);
	Assign_SplineData(&(Spline.Mot3), &Mot3);
	Assign_SplineData(&(Spline.Mot4), &Mot4);
	Assign_SplineData(&(Spline.Mot5), &Mot5);
	Assign_SplineData(&(Spline.Mot6), &Mot6);
	
	// 2. ve 3. kuvvetini alma islemi surekli tekrarlanmamasi icin bir kere hesaplanarak denklemlerin icerisinde gecici olarak olusturulan bu deger kullanilir.
	double EndTimeCross2 = pow(EndTime, 2);
	double EndTimeCross3 = pow(EndTime, 3);
	
	// Pozisyon Denklemi: A * x^3 + B * x^2 + C * x + D
	Mot1.RefPos[0] = Spline.Mot1.A * EndTimeCross3 + Spline.Mot1.B * EndTimeCross2 + Spline.Mot1.C * EndTime + Spline.Mot1.D;
	Mot2.RefPos[0] = Spline.Mot2.A * EndTimeCross3 + Spline.Mot2.B * EndTimeCross2 + Spline.Mot2.C * EndTime + Spline.Mot2.D;
	Mot3.RefPos[0] = Spline.Mot3.A * EndTimeCross3 + Spline.Mot3.B * EndTimeCross2 + Spline.Mot3.C * EndTime + Spline.Mot3.D;
	Mot4.RefPos[0] = Spline.Mot4.A * EndTimeCross3 + Spline.Mot4.B * EndTimeCross2 + Spline.Mot4.C * EndTime + Spline.Mot4.D;
	Mot5.RefPos[0] = Spline.Mot5.A * EndTimeCross3 + Spline.Mot5.B * EndTimeCross2 + Spline.Mot5.C * EndTime + Spline.Mot5.D;
	Mot6.RefPos[0] = Spline.Mot6.A * EndTimeCross3 + Spline.Mot6.B * EndTimeCross2 + Spline.Mot6.C * EndTime + Spline.Mot6.D;
	
	// Hiz Denklemi: 3 * A * x^2 + 2 * B * x + C;
	Mot1.RefVel[0] = 3 * Spline.Mot1.A * EndTimeCross2 + 2 * Spline.Mot1.B * EndTime + Spline.Mot1.C;
	Mot2.RefVel[0] = 3 * Spline.Mot2.A * EndTimeCross2 + 2 * Spline.Mot2.B * EndTime + Spline.Mot2.C;
	Mot3.RefVel[0] = 3 * Spline.Mot3.A * EndTimeCross2 + 2 * Spline.Mot3.B * EndTime + Spline.Mot3.C;
	Mot4.RefVel[0] = 3 * Spline.Mot4.A * EndTimeCross2 + 2 * Spline.Mot4.B * EndTime + Spline.Mot4.C;
	Mot5.RefVel[0] = 3 * Spline.Mot5.A * EndTimeCross2 + 2 * Spline.Mot5.B * EndTime + Spline.Mot5.C;
	Mot6.RefVel[0] = 3 * Spline.Mot6.A * EndTimeCross2 + 2 * Spline.Mot6.B * EndTime + Spline.Mot6.C;
	
	// Ivme Denklemi: 6 * A * x + B;
	Mot1.RefAcc[0] = 6 * Spline.Mot1.A * EndTime + 2 * Spline.Mot1.B;
	Mot2.RefAcc[0] = 6 * Spline.Mot2.A * EndTime + 2 * Spline.Mot2.B;
	Mot3.RefAcc[0] = 6 * Spline.Mot3.A * EndTime + 2 * Spline.Mot3.B;
	Mot4.RefAcc[0] = 6 * Spline.Mot4.A * EndTime + 2 * Spline.Mot4.B;
	Mot5.RefAcc[0] = 6 * Spline.Mot5.A * EndTime + 2 * Spline.Mot5.B;
	Mot6.RefAcc[0] = 6 * Spline.Mot6.A * EndTime + 2 * Spline.Mot6.B;
	
	// Yumusatma suresi doldugunda flag 1 yapilarak dondurulur.
	if (TotalTime >= (EndTrainingTime + 2) && EndTrainingStatus == 0)
		EndTrainingStatus = 1;

	return EndTrainingStatus;
}

// Motor enablelari kontrol edilir.
void ReleaseMotors()
{
	// 6 motor icin enable durumlari aktif edilir.
	AxCon_0[1].Enable = 1;
	AxCon_0[2].Enable = 1;
	AxCon_0[3].Enable = 1;
	AxCon_0[4].Enable = 1;
	AxCon_0[5].Enable = 1;	
	AxCon_0[6].Enable = 1;
}

// Motorlarin frenlerinin aktiflik ve pasiflik durumu kontrol edilir.
void SetPowerMotors(BOOL PowerState)
{
	// 6 motor icin fren durumlari aktif veya pasif hale getirilir.
	AxCon_0[1].Power = PowerState;
	AxCon_0[2].Power = PowerState;
	AxCon_0[3].Power = PowerState;
	AxCon_0[4].Power = PowerState;
	AxCon_0[5].Power = PowerState;
	AxCon_0[6].Power = PowerState;
}

// Pozisyon kontrolu aktif edilir.
void SetPositionControl(BOOL ActiveState)
{
	// 6 motor icin KPos * (RefPos - ResPos) degiskeni kontrol edilir.
	PosControl[0] = ActiveState;
	PosControl[1] = ActiveState;
	PosControl[2] = ActiveState;
	PosControl[3] = ActiveState;
	PosControl[4] = ActiveState;
	PosControl[5] = ActiveState;
}

// Motorlarin son pozisyon bilgilerinin atamalari yapilir.
void UpdateMotorOffsets()
{
	// Referans pozisyon icin ofset degeri belirlenir.
	Mot1.MotorOffsetForTraining = Mot1.Pos[0];
	Mot2.MotorOffsetForTraining = Mot2.Pos[0];
	Mot3.MotorOffsetForTraining = Mot3.Pos[0];
	Mot4.MotorOffsetForTraining = Mot4.Pos[0];
	Mot5.MotorOffsetForTraining = Mot5.Pos[0];
	Mot6.MotorOffsetForTraining = Mot6.Pos[0];
}

// Acceleration desired hesaplamasinda kullanilan signum fonksiyonu.
double sgnfunc(double input)
{
	if (input > 0) return 1;
	if (input < 0) return -1;
	return 0;
}

// Maksimum ve minimum degere gore limitleme islemi yaptirilir.
double SATMinMax(double Input, double UpLimit, double DownLimit)
{
	if (Input > UpLimit)
		return UpLimit;
	else if (Input <DownLimit)
		return DownLimit;
	else
		return Input;
}

// Acceleration desired hesaplama islemleri.
void AccDesCalculation(MotorStruct *Mot, INT MotorIndex)
{
	// Dummy olarak AccDes hesaplanir
	Mot->DummyAccDes = AxCon_0[MotorIndex].Info.PoweredOn ? (KAcc * (Mot->RefAcc[0]) + KVel * (Mot->RefVel[0] - Mot->Vel[0]) + PosControl[MotorIndex - 1] * (KPos * (Mot->RefPos[0] - Mot->Pos[0]))): 0;
	// Hesaplanan AccDes'e gore signum degeri hesaplanir.
	//	Mot->SGNVal      = fabs(Mot->DummyAccDes) * SGNRate * sgnfunc((Mot->RefPos[0] - Mot->Pos[0]) + SGNCoeffVel * (Mot->RefVel[0] - Mot->Vel[0]));
	
	// Initialize islemi tamamlandi ise limitlemeyi dahil etmeli.
//	if(SystemInitialized == Completed)
	if(Sys_State == SYSTEM_ON_TRAINING)
	{
		// Maksimum ve minimum degere gore AccDes'ler limitlenir.
		Mot->DummyVel  = SATMinMax(Mot->DummyAccDes, Vel_NegLim.AccDes[MotorIndex-1], Vel_PosLim.AccDes[MotorIndex-1]) - Mot->Vel[0];
		Mot->DummyVel2 = SATMinMax(Mot->DummyAccDes, Vel_PosLim.AccDes[MotorIndex-1], Vel_NegLim.AccDes[MotorIndex-1]);
		Mot->AccDes = Mot->DummyAccDes;
	}
	else
		Mot->AccDes = Mot->DummyAccDes;
}
