void SetErrorState(BYTE *ErrorState)
{
	*ErrorState  = 1;						// Error status variable is assigned 1.
	SYSTEM_Error = 1;			// System state changes to System on Error State.
}

void ReleaseErrorState(BYTE *ErrorState)
{
	*ErrorState  = 0;						// Error status variable is assigned 1.
	SYSTEM_Error = 0;			// System state changes to System on Error State.
	Sys_State = SYSTEM_PASSIVE;			// The system goes into "SYSTEM_ON_PASSIVE" state.
}

void EncoderControl(INT MotIndex)
{
	if (!DI_LimitSensor[MotIndex])
	{
//		if (gMpAxisBasic_0[0].PowerOn && (AxCon_0[MotIndex + 1].Internal.EncRawPos < Motor_Bottom_Encoder_LS[MotIndex]) && (Sys_State != SYSTEM_INITIALIZING && Sys_State != SYSTEM_GO_TO_ZERO))
//		{
//			LimitSensorCrash[MotIndex] = 1;
//			SetErrorState(&Error_LimitSensorCrash);
//			SystemInitialized = NotCompleted;
//		}
		if (SystemInitialized && (Sys_State != SYSTEM_PARK_MODE))
		{
			SetErrorState(&Error_LimitSensor);
			SystemInitialized = NotCompleted;
		}
	}	
}

/* Check Limit Sensor */
void Check_LimitSensor()
{
	EncoderControl(0);
	EncoderControl(1);
	EncoderControl(2);
	EncoderControl(3);
	EncoderControl(4);
	EncoderControl(5);
}

void Check_SmokeSensor()
{
	if (DI_SmokeSensor) SetErrorState(&Error_SmokeSensor);	// Duman sens�r aktif oldu ise sistem duman sens�r hatas�na d���r�l�r.
}

void Check_TotalMass()
{
	if (SystemInitialized && (SMotion.Mass > SMotion.MaxMass)) SetErrorState(&Error_OverMass);
}

void Check_IMUConnection()
{
	IMUCountVals[1] = IMUCountVals[0];					// IMU'nun 1 onceki cycle veri okuma say�s� bir de�i�kene atan�r.						 
	IMUCountVals[0] = IMU_EulerAngles.successCount;		// IMU'nun o anki veri okuma say�s� de�i�kene atan�r.	
	
	if (IMUCountVals[0] == IMUCountVals[1])				// �ki de�i�ken ayn� ise veri okunam�yordur
	{
		IMU_ErrorCount++;								// Error degeri 1 artt�r�l�r
		if (IMU_ErrorCount > 12500)						// 5 saniye boyunca sinyal gelmez ise
		{
			SetIMUVarZero();
			Warning_IMU = 1;
			IMU_ErrorCount = 12500;
		}
	}
	else 
	{
		Warning_IMU = 0;
		IMU_ErrorCount = 0;							// Error de�eri s�f�rlan�r.
	}
}

/* Check Motor Temperatures*/
void Check_Temperature(INT index, double ActualTemp, double MinTemp, double MaxTemp)
{
	if (ActualTemp < MinTemp || ActualTemp > MaxTemp)		// 2. motor �zerindeki termokupldan �l��len s�cakl�k de�erleri alt ve �st limitlerin d���nda ise
		Warning_Temp[index] = 1;															    // Sistem motor 2 s�cakl�k hata durumuna getirilir.
	else if (((MinTemp + 0.5) < ActualTemp) && (ActualTemp < (MaxTemp - 0.5)))
		Warning_Temp[index] = 0;
}

/* Check Motor Temperature */
void Check_MotorTemperature()
{
	Check_Temperature(0, gMpAxisBasic_0[1].Info.CyclicRead.MotorTemperature.Value, MinTempValue, MaxTempValue);
	Check_Temperature(1, gMpAxisBasic_0[2].Info.CyclicRead.MotorTemperature.Value, MinTempValue, MaxTempValue);
	Check_Temperature(2, gMpAxisBasic_0[3].Info.CyclicRead.MotorTemperature.Value, MinTempValue, MaxTempValue);
	Check_Temperature(3, gMpAxisBasic_0[4].Info.CyclicRead.MotorTemperature.Value, MinTempValue, MaxTempValue);
	Check_Temperature(4, gMpAxisBasic_0[5].Info.CyclicRead.MotorTemperature.Value, MinTempValue, MaxTempValue);
	Check_Temperature(5, gMpAxisBasic_0[6].Info.CyclicRead.MotorTemperature.Value, MinTempValue, MaxTempValue);

	if (Warning_Temp[0] || Warning_Temp[1] || Warning_Temp[2] || Warning_Temp[3] || Warning_Temp[4] || Warning_Temp[5])
		Warning_MotorTemp = 1;
	else
		Warning_MotorTemp = 0;
}

/* Torque Control */
void TorqueControl(INT MotorIndex)
{
	if (fabs(gMpAxisBasic_0[MotorIndex + 1].Info.CyclicRead.Torque.Value) > MaxTorqueValue)
	{
		Counter_Torque[MotorIndex] = Counter_Torque[MotorIndex] + 1;
		if (Counter_Torque[MotorIndex] > 7500)
		{
			Counter_Torque[MotorIndex] = 0;
			MotorHighTorque[MotorIndex] = 1;
			SetErrorState(&Error_Torque);
			
			SystemInitialized = NotCompleted;		
		}
	}
	else Counter_Torque[MotorIndex] = 0;
}

/* Check Motor Torque */
void Check_MotorTorque()
{
	TorqueControl(0);		// Check Torque Motor 1.
	TorqueControl(1);		// Check Torque Motor 2.
	TorqueControl(2);		// Check Torque Motor 3.
	TorqueControl(3);		// Check Torque Motor 4.
	TorqueControl(4);		// Check Torque Motor 5.
	TorqueControl(5);		// Check Torque Motor 6.
}

/* Release Motor Torque */
void ReleaseIfMotorTorquesOk()
{
	if ((fabs(gMpAxisBasic_0[1].Info.CyclicRead.Torque.Value) < MaxTorqueValue) && (fabs(gMpAxisBasic_0[2].Info.CyclicRead.Torque.Value) < MaxTorqueValue) &&
		(fabs(gMpAxisBasic_0[3].Info.CyclicRead.Torque.Value) < MaxTorqueValue) && (fabs(gMpAxisBasic_0[4].Info.CyclicRead.Torque.Value) < MaxTorqueValue) &&
		(fabs(gMpAxisBasic_0[5].Info.CyclicRead.Torque.Value) < MaxTorqueValue) && (fabs(gMpAxisBasic_0[6].Info.CyclicRead.Torque.Value) < MaxTorqueValue))
	{
		memset((unsigned long)&(MotorHighTorque), 0, sizeof(MotorHighTorque));
		ReleaseErrorState(&Error_Torque);
	}
}


void EncoderFailsControl(INT MotorIndex)
{
	if (AxCon_0[MotorIndex + 1].Internal.EncRawPos > (Motor_Bottom_Encoder[MotorIndex] + EncoderOffset))
	{
		MotorEncoderFails[MotorIndex] = 1;
		SetErrorState(&Error_MotorEncoder);
	}
}

 /* Switch Button Control Funciton */
void SwitchButtonControl()
{
	if (DI_OnOffSwitch) 					// A�-Kapa anahtar� 1 konumunda ise
		SetErrorState(&Error_SwitchOn);
	else 									// A�-Kapa anahtar� 0 konumunda ise
		Sys_State = SYSTEM_PASSIVE;		// Sistem durumuna sistem pasif olarak de�i�tir
}

void Check_MotorEncoder()
{
//	EncoderFailsControl(0);
//	EncoderFailsControl(1);
//	EncoderFailsControl(2);
//	EncoderFailsControl(3);
//	EncoderFailsControl(4);
//	EncoderFailsControl(5);
}

void Error_BooltoByte()
{
	Byte_LimitSensorCrash = LimitSensorCrash[0]  * 2 + LimitSensorCrash[1]  * 4 + LimitSensorCrash[2]  * 8 + LimitSensorCrash[3]  * 16 + LimitSensorCrash[4]  * 32 + LimitSensorCrash[5]  * 64;
	Byte_HighTemperature  = Warning_Temp[0]      * 2 + Warning_Temp[1]      * 4 + Warning_Temp[2]      * 8 + Warning_Temp[3]      * 16 + Warning_Temp[4]      * 32 + Warning_Temp[5]      * 64;
	Byte_EncoderState     = MotorEncoderFails[0] * 2 + MotorEncoderFails[1] * 4 + MotorEncoderFails[2] * 8 + MotorEncoderFails[3] * 16 + MotorEncoderFails[4] * 32 + MotorEncoderFails[5] * 64;
	Byte_HighTorque       = MotorHighTorque[0]   * 2 + MotorHighTorque[1]   * 4 + MotorHighTorque[2]   * 8 + MotorHighTorque[3]   * 16 + MotorHighTorque[4]   * 32 + MotorHighTorque[5]   * 64;
	Byte_MotCable         = !Control_EncoVal(1)	 * 2 + !Control_EncoVal(2)  * 4 + !Control_EncoVal(3)  * 8 + !Control_EncoVal(4)  * 16 + !Control_EncoVal(5)  * 32 + Control_EncoVal(6)   * 64;
}


void Check_DriverSafety()
{
	if (!FlagEMG && Flag_ErrorReset && (Time_Total > (Time_ErrorReset + 0.1)))
	{
		if((!DI_LightBarrier))
			SetErrorState(&Error_LightBarrier);
		else if (!DI_ControlBox)
			SetErrorState(&Error_ControlBoxConnection);
		else if (!DI_EMGButton && !Error_ControlBoxConnection && !Error_LightBarrier)
			Error_Emergency = 1;
	}
}

void Check_ControlBoxAndLightBarrier()
{
	if (!DI_ControlBox)
		SetErrorState(&Error_ControlBoxConnection);
}

void SetMpAxisErrorReset(int command)
{
	gMpAxisBasic_0[1].ErrorReset = command;
	gMpAxisBasic_0[2].ErrorReset = command;
	gMpAxisBasic_0[3].ErrorReset = command;
	gMpAxisBasic_0[4].ErrorReset = command;
	gMpAxisBasic_0[5].ErrorReset = command;
	gMpAxisBasic_0[6].ErrorReset = command;
}