/* Initialize Paramaters Function */
void InitializeParameters()
{
	PNEU.OK = 0;
	Time_Total = 0;						// Timer of main code
	Error_Emergency = 0;					// Error_Emergency variable set to 0.	

	if (SMotion.MotionType == SMOTION1800)
	{
		//		KPos = 185;							// Acceleration error convergence rate 
		//		KVel = 32;							// Weight of velocity over position in error
		//		KAcc = 0.7;
		
		KPos = 42;							// Acceleration error convergence rate 
		KVel = 13;							// Weight of velocity over position in error
		KAcc = 0.9;

		MaxStroke = -0.3;
		Up_StrokeMax = MaxStroke + 0.015;
		Up_StrokeMin = MaxStroke + 0.013;
		EndEffectorNeutralHeight = 1.23;
		NeutralLegLength = 1.5696;
		ConvRate = 2;
		FwdKin.ZPos[0] += 1.23;
		SMotion.MaxMass = 1850;
		Up_VelMax = -40;
		Down_VelMax = 40;
		
		MotMaxMargin = -190;
		MotMinMargin = 183;
	}
	else if (SMotion.MotionType == SMOTION2500)
	{
		KPos = 15;							// Acceleration error convergence rate 
		KVel = 4;							// Weight of velocity over position in error
		KAcc = 1;

		MaxStroke = -0.35;
		Up_StrokeMax = MaxStroke + 0.015;
		Up_StrokeMin = MaxStroke + 0.013;
		EndEffectorNeutralHeight = 1.4;
		NeutralLegLength = 1.7731;
		ConvRate = 1.39;
		FwdKin.ZPos[0] += 1.40;
		SMotion.MaxMass = 2550;
		Up_VelMax = -27;
		Down_VelMax = 27;
		
		//		MotMaxMargin = -190;
		//		MotMinMargin = 183;
	}
	else if (SMotion.MotionType == SMOTION3000)
	{
		KPos = 15;							// Acceleration error convergence rate 
		KVel = 4;							// Weight of velocity over position in error
		KAcc = 1;

		MaxStroke = -0.3;
		Up_StrokeMax = MaxStroke + 0.015;
		Up_StrokeMin = MaxStroke + 0.013;
		EndEffectorNeutralHeight = 1.23;
		NeutralLegLength = 1.5696;
		ConvRate = 1.39;
		FwdKin.ZPos[0] += 1.23;
		SMotion.MaxMass = 3050;
		Up_VelMax = -27;
		Down_VelMax = 27;
		
		MotMaxMargin = -131.8;
		MotMinMargin = 128.5;
	}

	Up_RefPos = ((Up_StrokeMax + Up_StrokeMin) / (2 * ballScrewPitch)) * ConvRate * M_TWOPI;
	DO_LifeCycle = 1;
		
	EMG_Flag = 0;
	Check_Flag = 0;	
}

void AssignMn()
{
	Mot1.Mn = DummyMn;						// Nominal Inertia of Motor1 (kgm^2)
	Mot2.Mn = DummyMn;						// Nominal Inertia of Motor2 (kgm^2)
	Mot3.Mn = DummyMn;						// Nominal Inertia of Motor3 (kgm^2)
	Mot4.Mn = DummyMn;						// Nominal Inertia of Motor4 (kgm^2)
	Mot5.Mn = DummyMn;						// Nominal Inertia of Motor5 (kgm^2)
	Mot6.Mn = DummyMn;						// Nominal Inertia of Motor6 (kgm^2)
}

void AssignKn()
{
	Mot1.Kn = DummyKn;						// Nominal Inertia of Motor1 (kgm^2)
	Mot2.Kn = DummyKn;						// Nominal Inertia of Motor2 (kgm^2)
	Mot3.Kn = DummyKn;						// Nominal Inertia of Motor3 (kgm^2)
	Mot4.Kn = DummyKn;						// Nominal Inertia of Motor4 (kgm^2)
	Mot5.Kn = DummyKn;						// Nominal Inertia of Motor5 (kgm^2)
	Mot6.Kn = DummyKn;						// Nominal Inertia of Motor6 (kgm^2)
}

/* Initialize Structs Function */
void InitializeStructs()
{
	if (SMotion.MotionType == SMOTION1800)
	{
		SMotion.B1 = D2R(53.86);				// degree
		SMotion.B2 = D2R(66.14);				// degree
		SMotion.B3 = D2R(173.86);				// degree
		SMotion.B4 = D2R(186.14);				// degree
		SMotion.B5 = D2R(293.86);				// degree
		SMotion.B6 = D2R(306.14);				// degree
				
		SMotion.T1 = D2R(6.72);					// degree
		SMotion.T2 = D2R(113.28);				// degree
		SMotion.T3 = D2R(126.72);				// degree
		SMotion.T4 = D2R(233.28);				// degree
		SMotion.T5 = D2R(246.72);				// degree
		SMotion.T6 = D2R(353.28);				// degree	
			
		SMotion.Rb = 1.31;						// m
		SMotion.Re = 1.06;						// m
		
		DummyKn = 1.197;
		
		Mot1.Kn = DummyKn;							// Nominal Torque Constant of Motor1 (Nm/A) 
		Mot2.Kn = DummyKn;							// Nominal Torque Constant of Motor2 (Nm/A) 
		Mot3.Kn = DummyKn;							// Nominal Torque Constant of Motor3 (Nm/A) 
		Mot4.Kn = DummyKn;							// Nominal Torque Constant of Motor4 (Nm/A) 
		Mot5.Kn = DummyKn;							// Nominal Torque Constant of Motor5 (Nm/A) 
		Mot6.Kn = DummyKn;							// Nominal Torque Constant of Motor6 (Nm/A)  
		
		DummyMn = 0.00033;
		
		Mot1.Mn = DummyMn;						// Nominal Inertia of Motor1 (kgm^2)
		Mot2.Mn = DummyMn;						// Nominal Inertia of Motor2 (kgm^2)
		Mot3.Mn = DummyMn;						// Nominal Inertia of Motor3 (kgm^2)
		Mot4.Mn = DummyMn;						// Nominal Inertia of Motor4 (kgm^2)
		Mot5.Mn = DummyMn;						// Nominal Inertia of Motor5 (kgm^2)
		Mot6.Mn = DummyMn;						// Nominal Inertia of Motor6 (kgm^2)

		DummyDOB = 2500;
		
		FDM1.G = DummyDOB;							// Dis Filter Gain for Mot1
		FDM2.G = DummyDOB;							// Dis Filter Gain for Mot2
		FDM3.G = DummyDOB;							// Dis Filter Gain for Mot3
		FDM4.G = DummyDOB;							// Dis Filter Gain for Mot4
		FDM5.G = DummyDOB;							// Dis Filter Gain for Mot5
		FDM6.G = DummyDOB;													

		CurrentSat = 45;						// Main current saturation rate (A)
	}
	else if (SMotion.MotionType == SMOTION2500)
	{
		SMotion.B1 = D2R(53.5);				    // degree
		SMotion.B2 = D2R(66.5);				    // degree
		SMotion.B3 = D2R(173.5);				// degree
		SMotion.B4 = D2R(186.5);				// degree
		SMotion.B5 = D2R(293.5);				// degree
		SMotion.B6 = D2R(306.5);				// degree
		
		SMotion.T1 = D2R(6.5);					// degree
		SMotion.T2 = D2R(113.5);				// degree
		SMotion.T3 = D2R(126.5);				// degree
		SMotion.T4 = D2R(233.5);				// degree
		SMotion.T5 = D2R(246.5);				// degree
		SMotion.T6 = D2R(353.5);				// degree	
	
		SMotion.Rb = 1.48;						// m
		SMotion.Re = 1.12;						// m
		
		Mot1.Kn = 1.1526;							// Nominal Torque Constant of Motor1 (Nm/A) 
		Mot2.Kn = 1.1526;							// Nominal Torque Constant of Motor2 (Nm/A) 
		Mot3.Kn = 1.1526;							// Nominal Torque Constant of Motor3 (Nm/A) 
		Mot4.Kn = 1.1526;							// Nominal Torque Constant of Motor4 (Nm/A) 
		Mot5.Kn = 1.1526;							// Nominal Torque Constant of Motor5 (Nm/A) 
		Mot6.Kn = 1.1526;							// Nominal Torque Constant of Motor6 (Nm/A)  
	
		Mot1.Mn = 0.0043;						// Nominal Inertia of Motor1 (kgm^2)
		Mot2.Mn = 0.0043;						// Nominal Inertia of Motor2 (kgm^2)
		Mot3.Mn = 0.0043;						// Nominal Inertia of Motor3 (kgm^2)
		Mot4.Mn = 0.0043;						// Nominal Inertia of Motor4 (kgm^2)
		Mot5.Mn = 0.0043;						// Nominal Inertia of Motor5 (kgm^2)
		Mot6.Mn = 0.0043;						// Nominal Inertia of Motor6 (kgm^2)

		FDM1.G = 2000;							// Dis Filter Gain for Mot1
		FDM2.G = 2000;							// Dis Filter Gain for Mot2
		FDM3.G = 2000;							// Dis Filter Gain for Mot3
		FDM4.G = 2000;							// Dis Filter Gain for Mot4
		FDM5.G = 2000;							// Dis Filter Gain for Mot5
		FDM6.G = 2000;													

		CurrentSat = 85;						// Main current saturation rate (A)
	}
	else if (SMotion.MotionType == SMOTION3000)
	{
		SMotion.B1 = D2R(53.86);				// degree
		SMotion.B2 = D2R(66.14);				// degree
		SMotion.B3 = D2R(173.86);				// degree
		SMotion.B4 = D2R(186.14);				// degree
		SMotion.B5 = D2R(293.86);				// degree
		SMotion.B6 = D2R(306.14);				// degree
				
		SMotion.T1 = D2R(6.72);					// degree
		SMotion.T2 = D2R(113.28);				// degree
		SMotion.T3 = D2R(126.72);				// degree
		SMotion.T4 = D2R(233.28);				// degree
		SMotion.T5 = D2R(246.72);				// degree
		SMotion.T6 = D2R(353.28);				// degree	
			
		SMotion.Rb = 1.31;						// m
		SMotion.Re = 1.06;						// m
		
		Mot1.Kn = 1.1526;							// Nominal Torque Constant of Motor1 (Nm/A) 
		Mot2.Kn = 1.1526;							// Nominal Torque Constant of Motor2 (Nm/A) 
		Mot3.Kn = 1.1526;							// Nominal Torque Constant of Motor3 (Nm/A) 
		Mot4.Kn = 1.1526;							// Nominal Torque Constant of Motor4 (Nm/A) 
		Mot5.Kn = 1.1526;							// Nominal Torque Constant of Motor5 (Nm/A) 
		Mot6.Kn = 1.1526;							// Nominal Torque Constant of Motor6 (Nm/A)  
	
		Mot1.Mn = 0.0043;						// Nominal Inertia of Motor1 (kgm^2)
		Mot2.Mn = 0.0043;						// Nominal Inertia of Motor2 (kgm^2)
		Mot3.Mn = 0.0043;						// Nominal Inertia of Motor3 (kgm^2)
		Mot4.Mn = 0.0043;						// Nominal Inertia of Motor4 (kgm^2)
		Mot5.Mn = 0.0043;						// Nominal Inertia of Motor5 (kgm^2)
		Mot6.Mn = 0.0043;						// Nominal Inertia of Motor6 (kgm^2)

		FDM1.G = 2000;							// Dis Filter Gain for Mot1
		FDM2.G = 2000;							// Dis Filter Gain for Mot2
		FDM3.G = 2000;							// Dis Filter Gain for Mot3
		FDM4.G = 2000;							// Dis Filter Gain for Mot4
		FDM5.G = 2000;							// Dis Filter Gain for Mot5
		FDM6.G = 2000;													

		CurrentSat = 85;						// Main current saturation rate (A)
	}
		
	Mot1.Vm = 315;							// Maximum Velocity of Motor1 (rad/s)
	Mot2.Vm = 315;							// Maximum Velocity of Motor2 (rad/s)
	Mot3.Vm = 315;							// Maximum Velocity of Motor3 (rad/s)
	Mot4.Vm = 315;							// Maximum Velocity of Motor4 (rad/s)
	Mot5.Vm = 315;							// Maximum Velocity of Motor5 (rad/s)
	Mot6.Vm = 315;							// Maximum Velocity of Motor6 (rad/s)
	
	DummyVOB = 2500;
	
	FVM1.G = 2500;							// Vel Filter Gain for Mot1
	FVM2.G = 2500;							// Vel Filter Gain for Mot2
	FVM3.G = 2500;							// Vel Filter Gain for Mot3
	FVM4.G = 2500;							// Vel Filter Gain for Mot4
	FVM5.G = 2500;							// Vel Filter Gain for Mot5
	FVM6.G = 2500;							// Vel Filter Gain for Mot6

	FAM1.G = 15000;							// Acc Filter Gain for Mot1
	FAM2.G = 15000;							// Acc Filter Gain for Mot2
	FAM3.G = 15000;							// Acc Filter Gain for Mot3
	FAM4.G = 15000;							// Acc Filter Gain for Mot4
	FAM5.G = 15000;							// Acc Filter Gain for Mot5
	FAM6.G = 15000;							// Acc Filter Gain for Mot6

	FRVM1.G = 15000;							// Reference Velocity Filter Gain for Mot1
	FRVM2.G = 15000;							// Reference Velocity Filter Gain for Mot2
	FRVM3.G = 15000;							// Reference Velocity Filter Gain for Mot3
	FRVM4.G = 15000;							// Reference Velocity Filter Gain for Mot4
	FRVM5.G = 15000;							// Reference Velocity Filter Gain for Mot5
	FRVM6.G = 15000;							// Reference Velocity Filter Gain for Mot6

	FRAM1.G = 15000;							// Reference Acceleration Filter Gain for Mot1
	FRAM2.G = 15000;							// Reference Acceleration Filter Gain for Mot2
	FRAM3.G = 15000;							// Reference Acceleration Filter Gain for Mot3
	FRAM4.G = 15000;							// Reference Acceleration Filter Gain for Mot4
	FRAM5.G = 15000;							// Reference Acceleration Filter Gain for Mot5
	FRAM6.G = 15000;							// Reference Acceleration Filter Gain for Mot6

	memset((unsigned long)&(D00.Coeffs), 0, sizeof(D00.Coeffs));
	memset((unsigned long)&(D01.Coeffs), 0, sizeof(D01.Coeffs));
	memset((unsigned long)&(D02.Coeffs), 0, sizeof(D02.Coeffs));
	memset((unsigned long)&(D03.Coeffs), 0, sizeof(D03.Coeffs));
	memset((unsigned long)&(D04.Coeffs), 0, sizeof(D04.Coeffs));
	memset((unsigned long)&(D05.Coeffs), 0, sizeof(D05.Coeffs));
}

/* Initial System Parameter Function */
void InitializeSystemParameters()
{
	gURecData.AxEnabled[0] = 1;
	gURecData.AxEnabled[1] = 1;
	gURecData.AxEnabled[2] = 1;
	gURecData.AxEnabled[3] = 1;
	gURecData.AxEnabled[4] = 1;
	gURecData.AxEnabled[5] = 1;	
	gURecData.AxEnabled[6] = 1;
	
	for (i = 0; i < 6; i++)
	{
		AxCon_0[i+1].Buffer	= &(Buffer[i+1]);
		AxCon_0[i+1].Mode = axCon_MODE_CYCLIC_CURRENT;
		AxCon_0[i+1].Parameters = &(AxConPar[i+1]);
		AxCon_0[i+1].MpAxis = &(gMpAxisBasic_0[i+1]);	 
		AxConPar[i+1].CycCurrent.MaxCurrent	= CurrentSat;
		AxCon_0[i+1].Enable = 1;
		
		gMpAxisBasicPar[i+1].Home.Mode = mpAXIS_HOME_MODE_ABSOLUTE;
		gMpAxisBasicPar[i+1].CyclicRead.TorqueMode = 1;
		gMpAxisBasicPar[i+1].CyclicRead.MotorTempMode = 1;

		ReadMotCurrent[i].Enable = 1;
		ReadMotCurrent[i].ParID = ACP10PAR_ICTRL_ISQ_ACT;
		ReadMotCurrent[i].DataAddress = &MotCurrent[i];
		ReadMotCurrent[i].DataType = ncPAR_TYP_REAL;
		ReadMotCurrent[i].Mode = mcONE_RECORD;
	}
	
	/*parameterize structure for input "Matrix" of function MTLinAlgMatrixInverse*/
	Matrix_MatrixType.Handle =  &J[0][0];
	Matrix_MatrixType.Columns = 6;
	Matrix_MatrixType.Rows = 6;
	
	/*parameterize structure for input "Result" of function MTLinAlgMatrixInverse*/
	InverseMatrix_MatrixType.Handle =  &Jinv[0][0];
	InverseMatrix_MatrixType.Columns = 6;
	InverseMatrix_MatrixType.Rows = 6;
	
	Iteration_MatrixType.Handle =  &Iteration[0][0];
	Iteration_MatrixType.Columns = 6;
	Iteration_MatrixType.Rows = 1;
	
	ErrorFunc_MatrixType.Handle =  &ErrFunc[0][0];
	ErrorFunc_MatrixType.Columns = 6;
	ErrorFunc_MatrixType.Rows = 1;
}

/* Set Zero to Acceleration Desired */
void AccDesZero()
{
	Mot1.AccDes = 0;
	Mot2.AccDes = 0;
	Mot3.AccDes = 0;
	Mot4.AccDes = 0;
	Mot5.AccDes = 0;
	Mot6.AccDes = 0;
}

void VelZero()
{
	Mot1.Vel[0] = 0;
	Mot2.Vel[0] = 0;
	Mot3.Vel[0] = 0;
	Mot4.Vel[0] = 0;
	Mot5.Vel[0] = 0;
	Mot6.Vel[0] = 0;
}

void AccZero()
{
	Mot1.Acc[0] = 0;
	Mot2.Acc[0] = 0;
	Mot3.Acc[0] = 0;
	Mot4.Acc[0] = 0;
	Mot5.Acc[0] = 0;
	Mot6.Acc[0] = 0;
}

void DummyAccZero()
{
	Mot1.DummyAccDes = 0;
	Mot2.DummyAccDes = 0;
	Mot3.DummyAccDes = 0;
	Mot4.DummyAccDes = 0;
	Mot5.DummyAccDes = 0;
	Mot6.DummyAccDes = 0;
}

/* Refresh Parameter Function */
void Refresh_MotionParams()
{
	Buffer[1].Buffer[0] = 0;	// MOTOR 1
	Buffer[2].Buffer[0] = 0;	// MOTOR 2
	Buffer[3].Buffer[0] = 0;	// MOTOR 3
	Buffer[4].Buffer[0] = 0;	// MOTOR 4
	Buffer[5].Buffer[0] = 0;	// MOTOR 5
	Buffer[6].Buffer[0] = 0;	// MOTOR 6
	
	AssignRefPos(0);
	RefVelZero();
	RefAccZero();
	
	VelZero();
	AccZero();
	
	AccDesZero();
	DummyAccZero();
}

/* Filter Struct Refresh */
void FilterStructRefresh(FilterStruct *FS)
{
	FS->Y0 = 0;         // Current Output Value
	FS->Y1 = 0;         // Previous Output Value
	FS->U0 = 0;         // Current Input Value
	FS->U1 = 0;         // Previous Input Value
}

/* Integral Struct Refresh */
void IntegralStructRefresh(IntegratorStruct *IS)
{
	IS->Y0 = 0;         // Current Output Value
	IS->Y1 = 0;         // Previous Output Value
	IS->U0 = 0;         // Current Input Value
	IS->U1 = 0;         // Previous Input Value
}

/* Simulator Struct Refresh */
void SimulatorStructRefresh()
{
	IntegralStructRefresh(&ISX1);
	IntegralStructRefresh(&ISX2); 
	IntegralStructRefresh(&ISY1); 
	IntegralStructRefresh(&ISY2); 
	IntegralStructRefresh(&ISZ1); 
	IntegralStructRefresh(&ISZ2); 
	IntegralStructRefresh(&ISR1); 
	IntegralStructRefresh(&ISP1); 
	IntegralStructRefresh(&ISQ1);
	
	FilterStructRefresh(&HPFX1);
	FilterStructRefresh(&HPFX2);
	FilterStructRefresh(&HPFX3);
	FilterStructRefresh(&HPFY1);
	FilterStructRefresh(&HPFY2);
	FilterStructRefresh(&HPFY3);
	FilterStructRefresh(&HPFZ1);
	FilterStructRefresh(&HPFZ2);
	FilterStructRefresh(&HPFZ3);
	FilterStructRefresh(&HPFR1);
	FilterStructRefresh(&HPFR2);
	FilterStructRefresh(&HPFP1);
	FilterStructRefresh(&HPFP2);
	FilterStructRefresh(&HPFQ1);
	FilterStructRefresh(&HPFQ2);
	FilterStructRefresh(&LPFTCR);
	FilterStructRefresh(&LPFTCP);
	
	SmoothTelemetryVar = 0;
}

void Refresh_Passive()
{
	Axcon_ErrorReset(1);
	Axcon_ErrorReset(2);
	Axcon_ErrorReset(3);
	Axcon_ErrorReset(4);
	Axcon_ErrorReset(5);
	Axcon_ErrorReset(6);
}

void Refresh_Energized()
{
	R_F_I_Flag = 0;							// Set the emergency flag zero
}

void Refresh_Checking()
{
	Time_Training = 0;						// Set system training time to zero
	PlayIndex = 0;							// Set play index of motion references to zero for initial condition
}

void Refresh_ReadyforInitializing()
{
	ParkingProcess = 0;
	TorqueTotal = 0;
	IsRampSafe = 0;											// Keep Safe for ramp Sensor
	Time_Initialize = 0;
	
	Refresh_MotionParams();
}

void Refresh_ReadyforData()
{
	BRRequestedPacket = 0;
	RefVelZero();
	RefAccZero();
}

void Refresh_ReadyforTraining()
{
	Buffer[1].Buffer[0] = 0;	// MOTOR 1
	Buffer[2].Buffer[0] = 0;	// MOTOR 2
	Buffer[3].Buffer[0] = 0;	// MOTOR 3
	Buffer[4].Buffer[0] = 0;	// MOTOR 4
	Buffer[5].Buffer[0] = 0;	// MOTOR 5
	Buffer[6].Buffer[0] = 0;	// MOTOR 6
	
	VelZero();
	AccZero();
	
	AccDesZero();
	DummyAccZero();
	
	SimulatorStructRefresh();											// Refresh simulator struct variables.
	RefVelZero();
	RefAccZero();
	RefManualAxis();
				
	memset((unsigned long)&(SMotion.EndX), 0, sizeof(SMotion.EndX));
	memset((unsigned long)&(SMotion.EndY), 0, sizeof(SMotion.EndY));
	memset((unsigned long)&(SMotion.EndR), 0, sizeof(SMotion.EndR));
	memset((unsigned long)&(SMotion.EndP), 0, sizeof(SMotion.EndP));
	memset((unsigned long)&(SMotion.EndQ), 0, sizeof(SMotion.EndQ));

	SMotion.EndZ[0] = EndEffectorNeutralHeight;
	SMotion.EndZ[1] = EndEffectorNeutralHeight;
}

void Refresh_StopTraining()
{
	EndTrainingStatus = 0;						// Set end training status to zero again
	PlayIndex = 0;								// Set motor RefPos buffer play index to zero
}

void Refresh_SettingPressure()
{	
	RefVelZero();
	RefAccZero();
}

void RefManualAxis()
{
	H2MManual.axisTrans.Surge = 0;
	H2MManual.axisTrans.Sway = 0;
	H2MManual.axisTrans.Heave = 0;
	H2MManual.axisAngular.Roll = 0;
	H2MManual.axisAngular.Pitch = 0;
	H2MManual.axisAngular.Yaw = 0;
}
