(*IMU CAN Variable Types*)

TYPE
	ArCanReceive_type : 	STRUCT 
		step : ArCanReceive_steps := STEP_WAIT;
		ArCanSetBitTimingRegisters_0 : ArCanSetBitTimingRegisters;
		ArCanReceiver_0 : ArCanReceive;
		receiverDeviceName : STRING[80];
		receivedIterationCounter : UDINT;
		lastReceiveTimestamp : UDINT;
		lostFrames : UDINT;
		successCount : UDINT;
		errorCount : UINT;
		lastError : DINT;
		stopTest : BOOL := FALSE;
		data : ARRAY[0..600]OF BYTE;
	END_STRUCT;
	ArCanReceive_steps : 
		(
		STEP_ERROR := 255,
		STEP_DEINIT := 254,
		STEP_WAIT_FOR_DATA := 10,
		STEP_INIT_RECEIVER := 2,
		STEP_INIT_BAUDRATE := 1,
		STEP_WAIT := 0
		);
	Struct_IMU : 	STRUCT 
		Surge : IMU_TransData;
		Sway : IMU_TransData;
		Heave : IMU_TransData;
		Roll : IMU_RotData;
		Pitch : IMU_RotData;
		Yaw : IMU_RotData;
	END_STRUCT;
	IMU_TransData : 	STRUCT 
		AccValue : LREAL;
		AccLimit : LREAL;
		AccResolution : LREAL;
	END_STRUCT;
	IMU_RotData : 	STRUCT 
		AngResolution : LREAL;
		VelResolution : LREAL;
		VelLimit : LREAL;
		AngLimit : LREAL;
		VelValue : LREAL;
		AngValue : LREAL;
	END_STRUCT;
END_TYPE
