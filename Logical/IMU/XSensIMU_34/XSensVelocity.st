PROGRAM _INIT
	IMU_Velocity.receiverDeviceName := 'SL2.IF3';		// CAN haberle�mesi i�in kullan�lan interface.
	IMU_Velocity.step := STEP_INIT_BAUDRATE;		// Ba�lang�� "step"i olarak "STEP_INIT_BAUDRATE" atamas� yap�l�r.
END_PROGRAM

PROGRAM _CYCLIC	
	
	CASE IMU_Velocity.step OF			// "step" durumlar� kontrol edilir.
		
		////////////////////////////////////////////////////////////STEP WAIT////////////////////////////////////////////////////////////
				
		STEP_WAIT:							// Haberle�menin ba�lamas� beklenir.
				
			////////////////////////////////////////////////////////STEP INIT BAUDRATE///////////////////////////////////////////////////////
		
		STEP_INIT_BAUDRATE:					// ArCanSetBitTimingRegisters_0 s�f�rlamas� yap�l�r.
			
			IMU_Velocity.ArCanSetBitTimingRegisters_0.Execute := TRUE;										// Fonksiyon ba�lamas�n� tetikleyen de�i�ken 1 yap�l�r.
			IMU_Velocity.ArCanSetBitTimingRegisters_0.DeviceName := IMU_Velocity.receiverDeviceName;	// "DeviceName" atamas� yap�l�r.
			IMU_Velocity.ArCanSetBitTimingRegisters_0.Register0 := 0;										// BaudRate de�erine g�re de�i�ir. (500.000 BaudRate - 0x00)
			IMU_Velocity.ArCanSetBitTimingRegisters_0.Register1 := 35;										// BaudRate de�erine g�re de�i�ir. (500.000 BaudRate - 0X1C)
			IMU_Velocity.ArCanSetBitTimingRegisters_0();													// Parametreler yenilenir.
			
			IF (IMU_Velocity.ArCanSetBitTimingRegisters_0.Done = TRUE) THEN									// Fonksiyonun bitti�ini belirten de�i�ken 1 oldu ise yani fonksiyon bitti ise.
				IMU_Velocity.step := STEP_INIT_RECEIVER;													// "step" de�i�kenine "STEP_INIT_RECEIVER" atamas� yap�l�r.
			ELSIF (IMU_Velocity.ArCanSetBitTimingRegisters_0.Error = TRUE) THEN								// Hata durumu oldu ise.
				IMU_Velocity.lastError := IMU_Velocity.ArCanSetBitTimingRegisters_0.StatusID;			// "lastError" de�i�kenine "ArCanReceiver" de�i�keninin "statusID" de�i�keni atan�r. Yani hangi stepte
				// hata durumu oldu�unu ��renmeyi sa�lar.
				IMU_Velocity.errorCount := IMU_Velocity.errorCount + 1;									// "ErrorCount"	de�i�keni bir artt�r�larak haberle�mede olu�an toplam hata say�s� ��renilir.
				IMU_Velocity.step := STEP_ERROR;															// "step" de�i�kenine "STEP_ERROR" atamas� yap�l�r.
			END_IF;
			
			////////////////////////////////////////////////////////STEP INIT RECEIVER///////////////////////////////////////////////////////
		
		STEP_INIT_RECEIVER:					// CAN haberle�me parametrelerine atama yap�l�r.
			
			IMU_Velocity.ArCanReceiver_0.Enable := FALSE;										// Giri� "false" oldu�u s�rece fonksiyon blo�u �al��maz.
			IMU_Velocity.ArCanReceiver_0.DeviceName := IMU_Velocity.receiverDeviceName;		// CAN haberle�me interface ad�n�n atamas� yap�l�r.				
			IMU_Velocity.ArCanReceiver_0.ID := 34;												// Verinin okunaca�� ID atamas� yap�l�r.
			IMU_Velocity.ArCanReceiver_0.IDMask := arCAN_RECEIVE_SINGLE_ID;						// Tek ID'den veri al�m� yap�l�r.
			IMU_Velocity.ArCanReceiver_0.Format := arCAN_11BIT;									// 11 Bit se�ilir.
			IMU_Velocity.ArCanReceiver_0.QueueSize	:= 12;										// Veri alma kuyru�unun uzunlu�u.
		
			IMU_Velocity.step := STEP_WAIT_FOR_DATA;											// "step" de�i�kenine "STEP_WAIT_FOR_DATA" atamas� yap�l�r.
			
			IF (IMU_Velocity.ArCanReceiver_0.Error = TRUE) THEN									// Hata durumu oldu ise.
				IMU_Velocity.errorCount := IMU_Velocity.errorCount + 1;						// "ErrorCount"	de�i�keni bir artt�r�larak haberle�mede olu�an toplam hata say�s� ��renilir.
				IMU_Velocity.lastError := IMU_Velocity.ArCanReceiver_0.StatusID;			// "lastError" de�i�kenine "ArCanReceiver" de�i�keninin "statusID" de�i�keni atan�r. Yani hangi stepte
				// hata durumu oldu�unu ��renmeyi sa�lar.
				IMU_Velocity.step := STEP_ERROR;												// "step" de�i�kenine "STEP_ERROR" atamas� yap�l�r.
			END_IF;			
			
			////////////////////////////////////////////////////////STEP WAIT FOR DATA///////////////////////////////////////////////////////
		
		STEP_WAIT_FOR_DATA:					// Yeni data kontrol� yap�l�r.
			
			IF NOT IMU_Velocity.ArCanReceiver_0.Busy AND NOT IMU_Velocity.ArCanReceiver_0.Active THEN		// CanReceiver me�gul de�ilse ve veri alm�yorsa.
				IMU_Acceleration.ArCanReceiver_0.Enable := TRUE;													// "Enable" de�i�keni 1 yap�larak veri almaya haz�r hale getirilir.
			END_IF;
			
			IF EDGEPOS(IMU_Velocity.ArCanReceiver_0.NewDataValid) THEN				// Son fonksiyon blo�u �al��t���nda yeni veri geldiyse.							
				IMU_Velocity.successCount := IMU_Velocity.successCount + 1;		// Ba�ar�l� al�nan toplam veri say�s� artt�r�l�r.
				
				memset(ADR(IMU_Velocity.data[0]),0,SIZEOF(IMU_Velocity.data));																								// Belirtilen adrese veri yazmay� sa�lar.
				memcpy( ADR(IMU_Velocity.data[0]), ADR(IMU_Velocity.ArCanReceiver_0.ReceivedFrame), IMU_Velocity.ArCanReceiver_0.ReceivedFrame.DataLength);				// Bellek alan�n� kopyalar.
				memcpy( ADR(IMU_Velocity.receivedIterationCounter), ADR(IMU_Velocity.ArCanReceiver_0.ReceivedFrame), SIZEOF(IMU_Velocity.receivedIterationCounter));	// Al�nan verileri ve zaman�n� kaydetme.
				IMU_Velocity.lastReceiveTimestamp := IMU_Velocity.ArCanReceiver_0.Timestamp.UTCSeconds;							// Son verinin al�n�d�� zaman				
				IMU_Velocity.lostFrames := IMU_Velocity.lostFrames + IMU_Velocity.ArCanReceiver_0.NumberOfLostFrames;		// "Enable" de�i�keni 1 oldu�undan bu yana al�namayan veri say�s�.
				IMU_Velocity.ArCanReceiver_0.Enable := FALSE;																		// Veri alma i�lemi durdurulur.
				
			ELSIF (IMU_Velocity.ArCanReceiver_0.Error) THEN								// Hata durumu oldu ise.
				IMU_Velocity.errorCount := IMU_Velocity.errorCount + 1;				// "ErrorCount"	de�i�keni bir artt�r�larak haberle�mede olu�an toplam hata say�s� ��renilir.
				IMU_Velocity.lastError := IMU_Velocity.ArCanReceiver_0.StatusID;	// "lastError" de�i�kenine "ArCanReceiver" de�i�keninin "statusID" de�i�keni atan�r. Yani hangi stepte
				// hata durumu oldu�unu ��renmeyi sa�lar.
				IMU_Velocity.step := STEP_ERROR;										// "step" de�i�kenine "STEP_ERROR" atamas� yap�l�r.
			END_IF;
						
						
			IF (IMU_Velocity.stopTest = TRUE) THEN		// "stopTest" de�i�keni 1 oldu�u zaman
				IMU_Velocity.stopTest := FALSE;			// "stopTest" de�i�keni 0 yap�l�r.
				IMU_Velocity.step := STEP_DEINIT;		// "step" de�i�kenine "STEP_DEINIT" atamas� yap�l�r.
			END_IF;
			
			///////////////////////////////////////////////////////////STEP DEINIT//////////////////////////////////////////////////////////
		
		STEP_DEINIT:					// Veri bekleme durumuna al�n�r.
			
			IMU_Velocity.ArCanReceiver_0.Enable := FALSE;		// "Enable" de�eri 0 yap�larak veri al�m� durdurulur.			
			IMU_Velocity.step := STEP_WAIT;						// "step" de�i�kenine "STEP_WAIT" atamas� yap�l�r.
		
			///////////////////////////////////////////////////////////STEP ERROR//////////////////////////////////////////////////////////
		
		STEP_ERROR:						// Error Durumu
			
			IMU_Velocity.ArCanReceiver_0.Enable := FALSE;		// "Enable" de�eri 0 yap�larak veri al�m� durdurulur.						
			
		ELSE

	END_CASE;
	
	IMU_Velocity.ArCanReceiver_0();		// Parametreler yenilenir.
	
END_PROGRAM

PROGRAM _EXIT
	IMU_Velocity.ArCanReceiver_0.Enable	:= FALSE;		// "Enable" de�eri 0 yap�larak veri al�m� durdurulur.
	IMU_Velocity.ArCanReceiver_0();						// Parametreler yenilenir.
END_PROGRAM