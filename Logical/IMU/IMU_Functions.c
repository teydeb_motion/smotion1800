// IMU'dan okunan veriler sifirlanir.
void SetIMUVarZero()
{
	// IMU baglantisi koptugu zaman veriler sifirlansin.
	memset(&(IMU.Roll.AngValue) , 0, sizeof(IMU.Roll.AngValue));
	memset(&(IMU.Pitch.AngValue), 0, sizeof(IMU.Pitch.AngValue));
	memset(&(IMU.Yaw.AngValue)	, 0, sizeof(IMU.Yaw.AngValue));
	memset(&(IMU.Roll.VelValue) , 0, sizeof(IMU.Roll.VelValue));
	memset(&(IMU.Pitch.VelValue), 0, sizeof(IMU.Pitch.VelValue));
	memset(&(IMU.Yaw.VelValue)  , 0, sizeof(IMU.Yaw.VelValue));
	memset(&(IMU.Surge.AccValue), 0, sizeof(IMU.Surge.AccValue));
	memset(&(IMU.Sway.AccValue) , 0, sizeof(IMU.Sway.AccValue));
	memset(&(IMU.Heave.AccValue), 0, sizeof(IMU.Heave.AccValue));
}

// IMU'dan okunan verilerin hesaplanma islemlerinde kullanilan maksimum ve cozunurluk degerleri atanir.
void Set_DefaultIMUValues()
{
	// Surge ekseninde ivme degeri okunabilir.
	// Okunabilen maksimum ivme ve cozunurluk degeri urun datasheetưnden bakilarak atanir.
	IMU.Surge.AccLimit = 100;
	IMU.Surge.AccResolution = 0.0039;
	
	// Sway ekseninde ivme degeri okunabilir.
	// Okunabilen maksimum ivme ve cozunurluk degeri urun datasheetưnden bakilarak atanir.
	IMU.Sway.AccLimit = 100;
	IMU.Sway.AccResolution = 0.0039;
	
	// Heave ekseninde ivme degeri okunabilir.
	// Okunabilen maksimum ivme ve cozunurluk degeri urun datasheetưnden bakilarak atanir.
	IMU.Heave.AccLimit = 100;
	IMU.Heave.AccResolution = 0.0039;
	
	// Roll ekseninde aci ve acisal hiz degeri okunabilir.
	// Okunabilen degerler icin maksimum deger ve cozunurluk degeri urun datasheetưnden bakilarak atanir.
	IMU.Roll.AngLimit = 90;
	IMU.Roll.AngResolution = 0.0078;
	IMU.Roll.VelLimit = 35;
	IMU.Roll.VelResolution = 0.0020;
	
	// Roll ekseninde aci ve acisal hiz degeri okunabilir.
	// Okunabilen degerler icin maksimum deger ve cozunurluk degeri urun datasheetưnden bakilarak atanir.
	IMU.Pitch.AngLimit = 180;
	IMU.Pitch.AngResolution = 0.0078;
	IMU.Pitch.VelLimit = 35;
	IMU.Pitch.VelResolution = 0.0020;	
	
	// Roll ekseninde aci ve acisal hiz degeri okunabilir.
	// Okunabilen degerler icin maksimum deger ve cozunurluk degeri urun datasheetưnden bakilarak atanir.
	IMU.Yaw.AngLimit = 180;
	IMU.Yaw.AngResolution = 0.0078;
	IMU.Yaw.VelLimit = 35;
	IMU.Yaw.VelResolution = 0.0020;
}

// IMU'dan okunan Euler Aci verilerinin anlamlandirilma islemleri
void Calculate_Ang()
{
	// Roll, Pitch ve Yaw eksenlerinde okunan veriler icin gecici diziler olusturulur.
	char Buffer_RollEuler[2], Buffer_PitchEuler[2], Buffer_YawEuler[2];
	
	// Euler acilari icin toplamda 6 byte veri bulunmakta. Gelen veriler ile ilgili bilgiler asagidaki tablodaki gibidir.
	//|-------------------------------------------------------------------------|
	//| Field	|	Range	|	Resolution	|	Unit	|	Size	|	Offset  |
	//|-------------------------------------------------------------------------|
	//| Roll	|	+-180	|	0.0078		|	deg		|	2		|	0		|
	//| Pitch	|	+-90	|	0.0078		|	deg		|	2		|	2	    |
	//| Yaw		|	+-180	|	0.0078		|	deg		|	2		|	4	    |
	//|-------------------------------------------------------------------------|
	
	// IMU'nun konumlandirilmasindan dolayi roll ile pitch eksen acilari terslenir.
	// IMU'dan okunan veriler gecici olusturulan bufer dizisine atanir.
	// Okunan degerin anlamlansirilmasi ornek olarak yaw ekseni ele alinirsa, 
	// 1-> (IMU.Yaw.AngLimit / IMU.Yaw.AngResolution) sonucu ile max aci degerinde IMU'dan okunacak deger bulunur.
	// 2-> (IMU.Yaw.AngLimit / (IMU.Yaw.AngLimit / IMU.Yaw.AngResolution)) * IMUVal islemi ile de IMUVal degeri max oldugunda sonucun AngLimit cikmasini saglayan bir esitlik olusturulmus olur.
	
	// Roll Ekseni Euler Aci Degeri
	Buffer_RollEuler[0]  = IMU_EulerAngles.data[3];
	Buffer_RollEuler[1]  = IMU_EulerAngles.data[2];
	IMU.Roll.AngValue 	 = (IMU.Roll.AngLimit / (IMU.Roll.AngLimit / IMU.Roll.AngResolution)) * (*((INT*)Buffer_RollEuler));
	
	// Pitch Ekseni Euler Aci Degeri
	Buffer_PitchEuler[0] = IMU_EulerAngles.data[1];
	Buffer_PitchEuler[1] = IMU_EulerAngles.data[0];
	IMU.Pitch.AngValue   = (IMU.Pitch.AngLimit / (IMU.Pitch.AngLimit / IMU.Pitch.AngResolution)) * (*((INT*)Buffer_PitchEuler));		
	
	// Yaw Ekseni Euler Aci Degeri
	Buffer_YawEuler[0]   = IMU_EulerAngles.data[5];
	Buffer_YawEuler[1]   = IMU_EulerAngles.data[4];
	IMU.Yaw.AngValue	 = (IMU.Yaw.AngLimit / (IMU.Yaw.AngLimit / IMU.Yaw.AngResolution)) * (*((INT*)Buffer_YawEuler));
}

// IMU'dan okunan Acisal Hiz verilerinin anlamlandirilma islemleri
void Calculate_AngVel()
{
	// Roll, Pitch ve Yaw eksenlerinde okunan veriler icin gecici diziler olusturulur.
	char Buffer_RollVel[2], Buffer_PitchVel[2], Buffer_YawVel[2];
	
	// Acisal Hiz icin toplamda 6 byte veri bulunmakta. Gelen veriler ile ilgili bilgiler asagidaki tablodaki gibidir.
	//|-------------------------------------------------------------------------|
	//| Field	|	Range	|	Resolution	|	Unit	|	Size	|	Offset  |
	//|-------------------------------------------------------------------------|
	//| Roll	|	+-35	|	0.0020		|	rad/s	|	2		|	0		|
	//| Pitch	|	+-35	|	0.0020		|	rad/s	|	2		|	2	    |
	//| Yaw		|	+-35	|	0.0020		|	rad/s	|	2		|	4	    |
	//|-------------------------------------------------------------------------|
	
	// Roll Ekseni Acisal Hiz Degeri
	Buffer_RollVel[0] = IMU_Velocity.data[1];
	Buffer_RollVel[1] = IMU_Velocity.data[0];
	IMU.Roll.VelValue = (IMU.Roll.VelLimit / (IMU.Roll.VelLimit / IMU.Roll.VelResolution)) * (*((INT*)Buffer_RollVel));
	
	// Pitch Ekseni Acisal Hiz Degeri
	Buffer_PitchVel[0] = IMU_Velocity.data[3];
	Buffer_PitchVel[1] = IMU_Velocity.data[2];
	IMU.Pitch.VelValue = (IMU.Pitch.VelLimit / (IMU.Pitch.VelLimit / IMU.Pitch.VelResolution)) * (*((INT*)Buffer_PitchVel));
	
	// Yaw Ekseni Acisal Hiz Degeri
	Buffer_YawVel[0] = IMU_Velocity.data[5];
	Buffer_YawVel[1] = IMU_Velocity.data[4];
	IMU.Yaw.VelValue = (IMU.Yaw.VelLimit / (IMU.Yaw.VelLimit / IMU.Yaw.VelResolution)) * (*((INT*)Buffer_YawVel));
}

// IMU'dan okunan Oteleme Ivme verilerinin anlamlandirilma islemleri
void Calculate_Acc()
{
	// Surge, Sway ve Heave eksenlerinde okunan veriler icin gecici diziler olusturulur.
	char Buffer_SurgeAcc[2], Buffer_SwayAcc[2], Buffer_HeaveAcc[2];
	
	// Öteleme Ivme icin toplamda 6 byte veri bulunmakta. Gelen veriler ile ilgili bilgiler asagidaki tablodaki gibidir.
	//|-------------------------------------------------------------------------|
	//| Field	|	Range	|	Resolution	|	Unit	|	Size	|	Offset  |
	//|-------------------------------------------------------------------------|
	//| Roll	|	+-100	|	0.0039		|	rad/s2	|	2		|	0		|
	//| Pitch	|	+-100	|	0.0039		|	rad/s2	|	2		|	2	    |
	//| Yaw		|	+-100	|	0.0039		|	rad/s2	|	2		|	4	    |
	//|-------------------------------------------------------------------------|
	
	// Surge Ekseni Oteleme Ivme Degeri
	Buffer_SurgeAcc[0] = IMU_Acceleration.data[1];
	Buffer_SurgeAcc[1] = IMU_Acceleration.data[0];
	IMU.Surge.AccValue = (IMU.Surge.AccLimit / (IMU.Surge.AccLimit / IMU.Surge.AccResolution)) * (*((INT*)Buffer_SurgeAcc));		
	
	// Sway Ekseni Oteleme Ivme Degeri
	Buffer_SwayAcc[0] = IMU_Acceleration.data[3];
	Buffer_SwayAcc[1] = IMU_Acceleration.data[2];
	IMU.Sway.AccValue = (IMU.Sway.AccLimit / (IMU.Sway.AccLimit /IMU.Sway.AccResolution)) * (*((INT*)Buffer_SwayAcc));
	
	// Heave Ekseni Oteleme Ivme Degeri
	Buffer_HeaveAcc[0] = IMU_Acceleration.data[5];
	Buffer_HeaveAcc[1] = IMU_Acceleration.data[4];
	IMU.Heave.AccValue = (IMU.Heave.AccLimit / (IMU.Heave.AccLimit / IMU.Heave.AccResolution)) * (*((INT*)Buffer_HeaveAcc));
}

// IMU'dan okunan degerler hesaplanir.
void Calculate_IMUValues()
{
	// Oteleme eksenleri icin ivme, acisal eksenler icin aci ve acisal hiz degerleri hesaplanir.
	Calculate_Ang();
	Calculate_AngVel();
	Calculate_Acc();
}