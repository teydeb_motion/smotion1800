PROGRAM _INIT
	IMU_EulerAngles.receiverDeviceName := 'SL2.IF3';	// CAN haberle�mesi i�in kullan�lan interface.
	IMU_EulerAngles.step := STEP_INIT_BAUDRATE;			// Ba�lang�� "step"i olarak "STEP_INIT_BAUDRATE" atamas� yap�l�r.
END_PROGRAM

PROGRAM _CYCLIC	
	
	CASE IMU_EulerAngles.step OF		// "step" durumlar� kontrol edilir.
		
		////////////////////////////////////////////////////////////STEP WAIT////////////////////////////////////////////////////////////
		
		STEP_WAIT:						// Haberle�menin ba�lamas� beklenir.
			
			////////////////////////////////////////////////////////STEP INIT BAUDRATE///////////////////////////////////////////////////////
					
		STEP_INIT_BAUDRATE:				// ArCanSetBitTimingRegisters_0 s�f�rlamas� yap�l�r.
			
			IMU_EulerAngles.ArCanSetBitTimingRegisters_0.Execute := TRUE;										// Fonksiyon ba�lamas�n� tetikleyen de�i�ken 1 yap�l�r.
			IMU_EulerAngles.ArCanSetBitTimingRegisters_0.DeviceName := IMU_EulerAngles.receiverDeviceName;		// "DeviceName" atamas� yap�l�r.
			IMU_EulerAngles.ArCanSetBitTimingRegisters_0.Register0 := 0;										// BaudRate de�erine g�re de�i�ir. (500.000 BaudRate - 0x00)
			IMU_EulerAngles.ArCanSetBitTimingRegisters_0.Register1 := 35;										// BaudRate de�erine g�re de�i�ir. (500.000 BaudRate - 0X1C)	
			IMU_EulerAngles.ArCanSetBitTimingRegisters_0();														// Parametreler yenilenir.
			
			IF (IMU_EulerAngles.ArCanSetBitTimingRegisters_0.Done = TRUE) THEN									// Fonksiyonun bitti�ini belirten de�i�ken 1 oldu ise yani fonksiyon bitti ise.
				IMU_EulerAngles.step := STEP_INIT_RECEIVER;														// "step" de�i�kenine "STEP_INIT_RECEIVER" atamas� yap�l�r.
			ELSIF (IMU_EulerAngles.ArCanSetBitTimingRegisters_0.Error = TRUE) THEN								// Hata durumu oldu ise.
				IMU_EulerAngles.lastError := IMU_EulerAngles.ArCanSetBitTimingRegisters_0.StatusID;				// "lastError" de�i�kenine "ArCanReceiver" de�i�keninin "statusID" de�i�keni atan�r. Yani hangi stepte
				// hata durumu oldu�unu ��renmeyi sa�lar.
				IMU_EulerAngles.errorCount := IMU_EulerAngles.errorCount + 1;									// "ErrorCount"	de�i�keni bir artt�r�larak haberle�mede olu�an toplam hata say�s� ��renilir.
				IMU_EulerAngles.step := STEP_ERROR;																// "step" de�i�kenine "STEP_ERROR" atamas� yap�l�r.
			END_IF;
			
			////////////////////////////////////////////////////////STEP INIT RECEIVER///////////////////////////////////////////////////////
			
		STEP_INIT_RECEIVER:				// CAN haberle�me parametrelerine atama yap�l�r.
			
			IMU_EulerAngles.ArCanReceiver_0.Enable := TRUE;											// Giri� "True" oldu�u s�rece fonksiyon blo�u etkin �al���r. 
			IMU_EulerAngles.ArCanReceiver_0.DeviceName := IMU_EulerAngles.receiverDeviceName;		// CAN haberle�me interface ad�n�n atamas� yap�l�r.	
			IMU_EulerAngles.ArCanReceiver_0.ID := 32;												// Verinin okunaca�� ID atamas� yap�l�r.
			IMU_EulerAngles.ArCanReceiver_0.IDMask := arCAN_RECEIVE_SINGLE_ID;						// Tek ID'den veri al�m� yap�l�r.
			IMU_EulerAngles.ArCanReceiver_0.Format := arCAN_11BIT;									// 11 Bit se�ilir.
			IMU_EulerAngles.ArCanReceiver_0.QueueSize	:= 12;										// Veri alma kuyru�unun uzunlu�u.
		
			IF (IMU_EulerAngles.ArCanReceiver_0.Active = TRUE) THEN									// Fonksiyon blo�u �al��t��� zaman yani veri almaya haz�r oldu�unda.
				IMU_EulerAngles.step := STEP_WAIT_FOR_DATA;											// "step" de�i�kenine "STEP_WAIT_FOR_DATA" atamas� yap�l�r.
			ELSIF (IMU_EulerAngles.ArCanReceiver_0.Error = TRUE) THEN								// Hata durumu oldu ise.
				IMU_EulerAngles.errorCount := IMU_EulerAngles.errorCount + 1;						// "ErrorCount"	de�i�keni bir artt�r�larak haberle�mede olu�an toplam hata say�s� ��renilir.
				IMU_EulerAngles.lastError := IMU_EulerAngles.ArCanReceiver_0.StatusID;				// "lastError" de�i�kenine "ArCanReceiver" de�i�keninin "statusID" de�i�keni atan�r. Yani hangi stepte
				// hata durumu oldu�unu ��renmeyi sa�lar.
				IMU_EulerAngles.step := STEP_ERROR;													// "step" de�i�kenine "STEP_ERROR" atamas� yap�l�r.
			END_IF;			
						
			////////////////////////////////////////////////////////STEP WAIT FOR DATA///////////////////////////////////////////////////////
			
		STEP_WAIT_FOR_DATA:				// Yeni data kontrol� yap�l�r.
			
			IF NOT IMU_EulerAngles.ArCanReceiver_0.Busy AND NOT IMU_EulerAngles.ArCanReceiver_0.Active THEN			// CanReceiver me�gul de�ilse ve veri alm�yorsa.
				IMU_Velocity.ArCanReceiver_0.Enable := TRUE;														// "Enable" de�i�keni 1 yap�larak veri almaya haz�r hale getirilir.
			END_IF;
			
			IF EDGEPOS(IMU_EulerAngles.ArCanReceiver_0.NewDataValid) THEN											// Son fonksiyon blo�u �al��t���nda yeni veri geldiyse.
				IMU_EulerAngles.successCount := IMU_EulerAngles.successCount + 1;									// Ba�ar�l� al�nan toplam veri say�s� artt�r�l�r.
				
				memset(ADR(IMU_EulerAngles.data[0]),0,SIZEOF(IMU_EulerAngles.data));																								// Belirtilen adrese veri yazmay� sa�lar.
				memcpy( ADR(IMU_EulerAngles.data[0]), ADR(IMU_EulerAngles.ArCanReceiver_0.ReceivedFrame), IMU_EulerAngles.ArCanReceiver_0.ReceivedFrame.DataLength);				// Bellek alan�n� kopyalar.
				memcpy( ADR(IMU_EulerAngles.receivedIterationCounter), ADR(IMU_EulerAngles.ArCanReceiver_0.ReceivedFrame), SIZEOF(IMU_EulerAngles.receivedIterationCounter));		// Al�nan verileri ve zaman�n� kaydetme.
				IMU_EulerAngles.lastReceiveTimestamp := IMU_EulerAngles.ArCanReceiver_0.Timestamp.UTCSeconds;						// Son verinin al�n�d�� zaman					
				IMU_EulerAngles.lostFrames := IMU_EulerAngles.lostFrames + IMU_EulerAngles.ArCanReceiver_0.NumberOfLostFrames;		// "Enable" de�i�keni 1 oldu�undan bu yana al�namayan veri say�s�.
				IMU_EulerAngles.ArCanReceiver_0.Enable := FALSE;																	// Veri alma i�lemi durdurulur.
				
			ELSIF (IMU_EulerAngles.ArCanReceiver_0.Error) THEN								// Hata durumu oldu ise.
				IMU_EulerAngles.errorCount := IMU_EulerAngles.errorCount + 1;				// "ErrorCount"	de�i�keni bir artt�r�larak haberle�mede olu�an toplam hata say�s� ��renilir.
				IMU_EulerAngles.lastError := IMU_EulerAngles.ArCanReceiver_0.StatusID;		// "lastError" de�i�kenine "ArCanReceiver" de�i�keninin "statusID" de�i�keni atan�r. Yani hangi stepte
				// hata durumu oldu�unu ��renmeyi sa�lar.
				IMU_EulerAngles.step := STEP_ERROR;											// "step" de�i�kenine "STEP_ERROR" atamas� yap�l�r.
			END_IF;
						
			IF (IMU_EulerAngles.stopTest = TRUE) THEN		// "stopTest" de�i�keni 1 oldu�u zaman
				IMU_EulerAngles.stopTest := FALSE;			// "stopTest" de�i�keni 0 yap�l�r.
				IMU_EulerAngles.step := STEP_DEINIT;		// "step" de�i�kenine "STEP_DEINIT" atamas� yap�l�r.
			END_IF;
			
			///////////////////////////////////////////////////////////STEP DEINIT//////////////////////////////////////////////////////////
			
		STEP_DEINIT:			// Veri bekleme durumuna al�n�r.
			
			IMU_EulerAngles.ArCanReceiver_0.Enable := FALSE;	// "Enable" de�eri 0 yap�larak veri al�m� durdurulur.	
			IMU_EulerAngles.step := STEP_WAIT;					// "step" de�i�kenine "STEP_WAIT" atamas� yap�l�r.
			
			///////////////////////////////////////////////////////////STEP ERROR//////////////////////////////////////////////////////////
			
		STEP_ERROR:				// Error Durumu
			
			IMU_EulerAngles.ArCanReceiver_0.Enable := FALSE;	// "Enable" de�eri 0 yap�larak veri al�m� durdurulur.					
			
		ELSE

	END_CASE;
	
	IMU_EulerAngles.ArCanReceiver_0();			// Parametreler yenilenir.
	
END_PROGRAM

PROGRAM _EXIT
	IMU_EulerAngles.ArCanReceiver_0.Enable	:= FALSE;	// "Enable" de�eri 0 yap�larak veri al�m� durdurulur.
	IMU_EulerAngles.ArCanReceiver_0();					// Parametreler yenilenir.
END_PROGRAM