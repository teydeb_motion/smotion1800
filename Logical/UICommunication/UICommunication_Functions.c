// Motorlara ait referans ve sonuc pozisyon, hiz ve ivme degerlerinin atamasi yapilir.
void AssignMotDatas(LREAL Mot[], MotorStruct *_MotStruct)
{
	Mot[0] = -_MotStruct->RefPos[0];						// Motor referans pozisyon atamasi yapilir.
	Mot[1] = -_MotStruct->Pos[0];							// Motor sonuc 	  pozisyon atamasi yapilir.
	Mot[2] = -_MotStruct->RefVel[0];						// Motor referans hiz 	   atamasi yapilir.
	Mot[3] = -_MotStruct->Vel[0];							// Motor sonuc 	  hiz 	   atamasi yapilir.
	Mot[4] = -_MotStruct->RefAcc[0];						// Motor referans ivme 	   atamasi yapilir.
	Mot[5] = -_MotStruct->Acc[0];							// Motor sonuc    ivme 	   atamasi yapilir.
}

// Bacaklara ait referans ve sonuc pozisyon, hiz ve ivme degerlerinin atamasi yapilir.
void AssignLegDatas(LREAL Leg[], LegStruct *_LegStruct)
{
	Leg[0] = _LegStruct->RefPos[0];							// Bacak referans pozisyon atamasi yapilir.
	Leg[1] = _LegStruct->Pos[0];							// Bacak sonuc 	  pozisyon atamasi yapilir.
	Leg[2] = _LegStruct->RefVel[0];							// Bacak referans hiz 	   atamasi yapilir.
	Leg[3] = _LegStruct->Vel[0];							// Bacak sonuc 	  hiz 	   atamasi yapilir.
	Leg[4] = _LegStruct->RefAcc[0];							// Bacak referans ivme 	   atamasi yapilir.
	Leg[5] = _LegStruct->Acc[0];							// Bacak sonuc    ivme 	   atamasi yapilir.
}

// Arayuze gonderilecek olan verilerin atamasi yapiliyor.
void SendDataAssignment(Motion2Host* M2H)
{
	// 6 Motora ait degerlerin atanamasinin yapildigi fonksiyon cagirilir.
	AssignMotDatas(M2H->Mot1, &Mot1);		
	AssignMotDatas(M2H->Mot2, &Mot2);
	AssignMotDatas(M2H->Mot3, &Mot3);
	AssignMotDatas(M2H->Mot4, &Mot4);
	AssignMotDatas(M2H->Mot5, &Mot5);
	AssignMotDatas(M2H->Mot6, &Mot6);
	
	// 6 Bacaga ait degerlerin atanamasinin yapildigi fonksiyon cagirilir.
	AssignLegDatas(M2H->Leg1, &Leg1);
	AssignLegDatas(M2H->Leg2, &Leg2);
	AssignLegDatas(M2H->Leg3, &Leg3);
	AssignLegDatas(M2H->Leg4, &Leg4);
	AssignLegDatas(M2H->Leg5, &Leg5);
	AssignLegDatas(M2H->Leg6, &Leg6);
	
	// Olculen agirlik, pnomatikten set edilen ve okunan bar degeri atanir.
	M2H->MassInfo[0] = SMotion.Mass;
	M2H->MassInfo[1] = PNEU.Set;
	M2H->MassInfo[2] = PNEU.Res;

	// Sistemin toplam ve calisma zamani atanir.
	M2H->Time_Running        = Time_Total;
	M2H->Time_SystemTraining = Time_Training;

	// Duz kinematikten hesaplanan 6 eksene ait pozisyon degerleri atanir.
	M2H->XPos     = FwdKin.XPos[0];
	M2H->YPos     = FwdKin.YPos[0];
	M2H->ZPos     = FwdKin.ZPos[0];
	M2H->RollPos  = FwdKin.RollPos[0];
	M2H->PitchPos = FwdKin.PitchPos[0];
	M2H->YawPos   = FwdKin.YawPos[0];
	
	// Duz kinematikten hesaplanan 6 eksene ait hiz degerleri atanir.
	M2H->XVel     = FwdKin.XVel[0];
	M2H->YVel     = FwdKin.YVel[0];
	M2H->ZVel     = FwdKin.ZVel[0];
	M2H->RollVel  = FwdKin.RollVel[0];
	M2H->PitchVel = FwdKin.PitchVel[0];
	M2H->YawVel   = FwdKin.YawVel[0];
	
	// Duz kinematikten hesaplanan 6 eksene ait ivme degerleri atanir.
	M2H->XAcc     = FwdKin.XAcc[0];
	M2H->YAcc     = FwdKin.YAcc[0];
	M2H->ZAcc     = FwdKin.ZAcc[0];
	M2H->RollAcc  = FwdKin.RollAcc[0];
	M2H->PitchAcc = FwdKin.PitchAcc[0];
	M2H->YawAcc   = FwdKin.YawAcc[0];
	
	// IMU'dan okunan aci, acisal hiz ve oteleme ivme degerleri atanir.
	M2H->IMUData[0] = IMU.Roll.AngValue;
	M2H->IMUData[1] = IMU.Pitch.AngValue;
	M2H->IMUData[2] = IMU.Yaw.AngValue;
	M2H->IMUData[3] = IMU.Roll.VelValue;
	M2H->IMUData[4] = IMU.Pitch.VelValue;
	M2H->IMUData[5] = IMU.Yaw.VelValue;
	M2H->IMUData[6] = IMU.Surge.AccValue;
	M2H->IMUData[7] = IMU.Sway.AccValue;
	M2H->IMUData[8] = IMU.Heave.AccValue;
	
	// 6 Motora ait akim degerleri atanir.
	memcpy(&(M2H->Mot_Current), &MotCurrent, sizeof(M2H->Mot_Current));
	
	// 6 Motora ait tork degerleri atanir.
	M2H->Mot_Torque[0] = gMpAxisBasic_0[1].Info.CyclicRead.Torque.Value;
	M2H->Mot_Torque[1] = gMpAxisBasic_0[2].Info.CyclicRead.Torque.Value;
	M2H->Mot_Torque[2] = gMpAxisBasic_0[3].Info.CyclicRead.Torque.Value;
	M2H->Mot_Torque[3] = gMpAxisBasic_0[4].Info.CyclicRead.Torque.Value;
	M2H->Mot_Torque[4] = gMpAxisBasic_0[5].Info.CyclicRead.Torque.Value;
	M2H->Mot_Torque[5] = gMpAxisBasic_0[6].Info.CyclicRead.Torque.Value;
	
	// 6 Motora ait sicaklik degerleri atanir.
	M2H->Mot_Temperature[0] = gMpAxisBasic_0[1].Info.CyclicRead.MotorTemperature.Value;
	M2H->Mot_Temperature[1] = gMpAxisBasic_0[2].Info.CyclicRead.MotorTemperature.Value;
	M2H->Mot_Temperature[2] = gMpAxisBasic_0[3].Info.CyclicRead.MotorTemperature.Value;
	M2H->Mot_Temperature[3] = gMpAxisBasic_0[4].Info.CyclicRead.MotorTemperature.Value;
	M2H->Mot_Temperature[4] = gMpAxisBasic_0[5].Info.CyclicRead.MotorTemperature.Value;
	M2H->Mot_Temperature[5] = gMpAxisBasic_0[6].Info.CyclicRead.MotorTemperature.Value;
	
	// 6 Motora ait power olma durumlari atanir.
	M2H->Mot_Power[0] = Mot1.Power;
	M2H->Mot_Power[1] = Mot2.Power;
	M2H->Mot_Power[2] = Mot3.Power;
	M2H->Mot_Power[3] = Mot4.Power;
	M2H->Mot_Power[4] = Mot5.Power;
	M2H->Mot_Power[5] = Mot6.Power;
	
	// Sistemde toplamda aktif, reaktif, toplam harcanan ve rejenere edilen enerji degerleri atanir.
	M2H->Power[0] = PowerMeterFB.PowerData.AverageActivePower;
	M2H->Power[1] = PowerMeterFB.PowerData.AverageReactivePower;
	M2H->Power[2] = Total_ConsumedEnergy;
	M2H->Power[3] = Total_RegeneratedEnergy;
	
	// Sistem zamaninin atamasi yapilir.
	memcpy(&(M2H->Time_System), &Time_SystemAll, sizeof(M2H->Time_System));
	
	// Sistem calisma zamaninin atamasi yapilir.
	memcpy(&(M2H->Time_Working), &Time_SystemWorking, sizeof(M2H->Time_Working));
	
	// Sistem durumlari atanir.
	M2H->State_Motion = Sys_State;
	
	// Paket sayisi atanir.
	M2H->BRPacketNo = BRRequestedPacket;
	
	// Hata ve Uyari durumlari atanir.
	M2H->State_Error[0]  = Error_LimitSensor;
	M2H->State_Error[1]  = Error_SmokeSensor;
	M2H->State_Error[2]  = Error_MotorEncoder;
	M2H->State_Error[3]  = Error_LightBarrier;
	M2H->State_Error[4]  = Error_StandBy;
	M2H->State_Error[5]  = Error_OverMass;
	M2H->State_Error[6]  = Warning_MotorTemp;
	M2H->State_Error[7]  = Error_Torque;
	M2H->State_Error[8]  = Error_LimitSensorCrash;
	M2H->State_Error[9]  = Error_SwitchOn;
	M2H->State_Error[10] = Error_3DMouse;
	M2H->State_Error[11] = Warning_IMU;
	M2H->State_Error[12] = Error_ControlBoxConnection;
	M2H->State_Error[13] = Error_MotCable;
	
	// Park modunda bacaklarin hareket halinde olma durumu atanir.
	M2H->ParkingProcess = ParkingProcess;
	
	// Sistem acildiginda acil durdurma butonunun basili olma durumunu belirten deger atanir.
	M2H->EMGStatus = ButtonUP;
	
	// 6 limit sensorun 1 veya 0 olma durumu gonderilir.
	M2H->Byte_LimitSensor = Byte_LimitSensor;
	
	// 6 surucunun enable veya disable olma durumu gonderilir.
	M2H->Byte_DriverState = Byte_DriverState;
	
	// 6 surucunun power olup olmama durumu gonderilir.
	M2H->Byte_DriverPower = Byte_DriverPower;
	
	// 6 motorun power olup olmama durumu gonderilir.
	M2H->Byte_MotorState = Byte_MotorState;
	
	// 6 limt sensorun arizali olma durumu gonderilir.
	M2H->Byte_LimitSensorCrash = Byte_LimitSensorCrash;
	
	// 6 motor enkoder durumunu bir byteda gonderilir.
	M2H->Byte_EncoderState = Byte_EncoderState;
	
	// 6 motorun yuksek sicaklik limitine ulasma durumu gonderilir.
	M2H->Byte_HighTemperature = Byte_HighTemperature;
	
	// 6 motorun yuksek tork limitine ulasma durumu gonderilir.
	M2H->Byte_HighTorque = Byte_HighTorque;
	
	// 6 motorun kablosunun cikip cikmama durumu gonderilir.
	M2H->Byte_MotCable = Byte_MotCable;
}