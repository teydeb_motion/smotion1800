void GetData(int PortNumber)
{
	switch (Server.sStep) 
	{	
		case 0:																					// Open UDP Port  
		{
			Server.UdpOpen_0.enable = 1;
			Server.UdpOpen_0.port = PortNumber;													// Port to listen 
			UdpOpen(&Server.UdpOpen_0); 														// Call the Function 
				
			if( Server.UdpOpen_0.status == 0 )  												// UdpOpen successful -> Go to Recieve data case  //|| Client.UdpOpen_0.status == udpERR_ALREADY_EXIST
				Server.sStep = 2;	
			else if( Server.UdpOpen_0.status == ERR_FUB_BUSY )  								// UdpOpen not finished -> redo  			
				Server.sStep = 0 ;	
				else  																			// Goto Error Step  
					Server.sStep = 10;
		}
		break;
		
		case 2:	// Receive Data from the Server  
		{
			Server.UdpRecv_0.enable = 1;
			Server.UdpRecv_0.ident = Server.UdpOpen_0.ident;  									// Connection Ident from AsUDP.UDP_Open  
			Server.UdpRecv_0.pIpAddr = (UDINT)&IpAddress;
			Server.UdpRecv_0.pData	= (UDINT)&(DataFromNetwork);  								// Where to store the incoming data  
			Server.UdpRecv_0.datamax = sizeof(DataFromNetwork);  								// Lenght of data buffer  
			UdpRecv(&Server.UdpRecv_0);															// Call the Function
			if( Server.UdpRecv_0.status == 0 )  												// Data was received successfully -> Send next packet  
			{
				Server.sStep = 3;
				Server.no_data_received_count = 0;
			}
			else if (Server.UdpRecv_0.status == udpERR_NO_DATA )  								// No data received - wait  
				Server.no_data_received_count = Server.no_data_received_count + 1;				// Increase if no data are received  
			if( Server.no_data_received_count > 200 )											// No data timeout for TC1 [10ms]  
				Server.no_data_received_count = 0;
			else if (Server.UdpRecv_0.status == ERR_FUB_BUSY)  									// UdpRecv not finished -> redo 
				Server.sStep = 10;
			else  																				// Goto Error Step  and redo
				Server.sStep = 10; 
				Server.sStep = 2;
			}
			break;
		
		case 3: 																				// Close connection  
		{
			Server.UdpClose_0.enable = 1;
			Server.UdpClose_0.ident = Server.UdpOpen_0.ident; 									// Connection Ident from AsUDP.UDP_Open  				 
			UdpClose(&Server.UdpClose_0);  														// Call the Function
				
			if( Server.UdpClose_0.status == 0 )  												// Close successful -> Reopen the interface  
				Server.sStep = 0;
			else if(Server.UdpClose_0.status == ERR_FUB_BUSY)  									// Busy -> Goto Error Step	
				Server.sStep = 10;
			else  																				// Goto Error Step  
				Server.sStep = 10;
		}
		break;
		
		case 10: 																				// Here some error Handling has to be implemented  
			if (Server.UdpOpen_0.status == udpERR_ALREADY_EXIST)
				Server.sStep = 3;
			break;
	}
}
