#include <bur/plctypes.h>
#include <AsUDP.h>
#include <math.h>
#include <AsDefault.h>
#include <sys_lib.h>

#include "../FunctionGetData.c"

void _CYCLIC ProgramCyclic(void)
{
	GetData(GetPortNumber);
	
	if (DataFromNetwork[0] == SecurityID)
	{
		UINT i;
		UINT length;
	
		// ByteSize_Signal
		// Tek pakette at�lacak veri boyutunu belirtir. 
		// 400(Paket boyutu)*18(Pos,Hiz,Ivme veri sayisi)*8(LREAL Byte sayisi)+13(Kalan tek bytlel�k degerler)
		if (DataFromNetwork[3] == 14) length = ByteSize_Signal;
		else if (DataFromNetwork[3] == 10) length = ByteSize_Manual;
	
		char TemporaryBuffer_All[length];	
		char TemporaryBuffer_Main[ByteSize_Main];
	
		for (i=0; i<length; i++) 
			TemporaryBuffer_All[i] = DataFromNetwork[i];

		for (i=0; i<ByteSize_Main; i++) 
			TemporaryBuffer_Main[i] = DataFromNetwork[i];
	 
		if (strcmp(IpAddress,HostIP)==0)
		{
			H2MMain = *((H2M_Main*)TemporaryBuffer_Main);
		
			if (H2MMain.formID == Simulator)
				H2MSimulator = *((H2M_Simulator*)TemporaryBuffer_All);
				
			else if ((H2MMain.formID == ManualAxisControl) && (length == ByteSize_Manual))
				H2MManual = *((H2M_Manual*)TemporaryBuffer_All);
				
			else if (((H2MMain.formID == SignalGenerator) ||(H2MMain.formID == PlayFromFile) || (H2MMain.formID == SignalReplication)) && (length == ByteSize_Signal))
				H2MSignal = *((H2M_Signal*)TemporaryBuffer_All);
		}
	}
	
	if (Sys_State == SYSTEM_GETTING_DATA)
	{
		if (H2MSignal.packetInfo == PACKET_OK)
		{	
			if (H2MSignal.packetNo == 0)
				BRRequestedPacket = 1;
			
			if (H2MSignal.packetNo == BRRequestedPacket)
			{
				for (ArrayIndex = 0; ArrayIndex < PacketSize; ArrayIndex++)
				{
					// Pozisyon
					ArrayMot1_Pos[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot1Pos[ArrayIndex];
					ArrayMot2_Pos[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot2Pos[ArrayIndex];
					ArrayMot3_Pos[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot3Pos[ArrayIndex];
					ArrayMot4_Pos[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot4Pos[ArrayIndex];
					ArrayMot5_Pos[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot5Pos[ArrayIndex];
					ArrayMot6_Pos[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot6Pos[ArrayIndex];
					
//					if(H2MSignal.mainForm.formID == SignalReplication)
//					{
					// Hiz
					ArrayMot1_Vel[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot1Vel[ArrayIndex];
					ArrayMot2_Vel[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot2Vel[ArrayIndex];
					ArrayMot3_Vel[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot3Vel[ArrayIndex];
					ArrayMot4_Vel[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot4Vel[ArrayIndex];
					ArrayMot5_Vel[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot5Vel[ArrayIndex];
					ArrayMot6_Vel[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot6Vel[ArrayIndex];
//						
//						// Ivme
					ArrayMot1_Acc[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot1Acc[ArrayIndex];
					ArrayMot2_Acc[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot2Acc[ArrayIndex];
					ArrayMot3_Acc[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot3Acc[ArrayIndex];
					ArrayMot4_Acc[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot4Acc[ArrayIndex];
					ArrayMot5_Acc[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot5Acc[ArrayIndex];
					ArrayMot6_Acc[ArrayIndex+PacketSize*(H2MSignal.packetNo-1)] = H2MSignal.mot6Acc[ArrayIndex];
//					}
				}
				BRRequestedPacket++;
			}
		}	

		if (H2MMain.command == COMMAND_CANCEL_DATA)
		{
			BRRequestedPacket = 0;
			Sys_State = SYSTEM_READY_FOR_DATA;		
		}
		
		if (H2MSignal.packetInfo == PACKET_COMPLETED)
		{
			BRRequestedPacket = 0;
			Time_TrainingStop = H2MSignal.totalDataSize / 2500.0;
			MotorRefPos(ArrayMot1_Pos[0], ArrayMot2_Pos[0], ArrayMot3_Pos[0], ArrayMot4_Pos[0], ArrayMot5_Pos[0], ArrayMot6_Pos[0]);
			Sys_State = SYSTEM_READY_FOR_TRAINING;            
		}
	}
}

double POSLIM(double Length)
{
	if (Length < MotMaxMargin)
		Length = MotMaxMargin;
	else if (Length > MotMinMargin)
		Length = MotMinMargin;

	return Length;
}

/* Reference Motor Position */
void MotorRefPos(double MotPos1, double MotPos2, double MotPos3, double MotPos4, double MotPos5, double MotPos6)
{
	Mot1.RefPos[0] = POSLIM(MotPos1 + Mot1.MotorOffsetForTraining);                                            // Reference motor position (Result of Kinematic)
	Mot2.RefPos[0] = POSLIM(MotPos2 + Mot2.MotorOffsetForTraining);                                            // Reference motor position (Result of Kinematic)
	Mot3.RefPos[0] = POSLIM(MotPos3 + Mot3.MotorOffsetForTraining);                                            // Reference motor position (Result of Kinematic)
	Mot4.RefPos[0] = POSLIM(MotPos4 + Mot4.MotorOffsetForTraining);                                            // Reference motor position (Result of Kinematic)
	Mot5.RefPos[0] = POSLIM(MotPos5 + Mot5.MotorOffsetForTraining);                                            // Reference motor position (Result of Kinematic)
	Mot6.RefPos[0] = POSLIM(MotPos6 + Mot6.MotorOffsetForTraining);                                            // Reference motor position (Result of Kinematic)
}


