void SendData(int PortNumber)
{	
	switch (Client1.sStep) 
	{	
		case 0:																		// Open UDP Port  
		{
			Client1.UdpOpen_0.enable = 1;
			Client1.UdpOpen_0.port = PortNumber;									// Port to listen 
			UdpOpen(&Client1.UdpOpen_0); 											// Call the Function 
			
			if( Client1.UdpOpen_0.status == 0 )  									// UdpOpen successful -> Go to Recieve data case 
				Client1.sStep = 1;	
			else if( Client1.UdpOpen_0.status == ERR_FUB_BUSY ) 					// UdpOpen not finished -> redo  			
				Client1.sStep = 0;	
			else  																	// Goto Error Step  
				Client1.sStep = 10;
		}
		break;
		
		case 1:	      
		{ 
			strcpy(&Client1.server_address, &HostIP);						// IP Address of the Server
			Client1.server_portnumber = PortNumber;									// Port number of the Server				
			Client1.UdpSend_0.enable = 1;
			Client1.UdpSend_0.ident = Client1.UdpOpen_0.ident;						// Connection Ident from AsUDP.UDP_Open
			Client1.UdpSend_0.pHost = (UDINT)&(Client1.server_address);	
			Client1.UdpSend_0.port = Client1.server_portnumber;		
			Client1.UdpSend_0.pData = (UDINT)&(DataToNetwork);						// Which data to send
			Client1.UdpSend_0.datalen = sizeof(Motion2Host);						// Lenght of data to send
			Client1.UdpSend_0.flags = 0;
			
			UdpSend(&Client1.UdpSend_0);											// Call the Function
			UdpSend(&Client1.UdpSend_0);
			
			if (Client1.UdpSend_0.status == 0 )										// Data sent 
				Client1.sStep = 1;
			else if ( Client1.UdpSend_0.status == ERR_FUB_BUSY ) 					// UdpSend not finished -> redo						
				Client1.sStep = 1;													// Busy 
			else 											 						// Goto Error Step
				Client1.sStep = 10;
		} 
		break; 
		
		case 3: 																	// Close connection  
		{
			Client1.UdpClose_0.enable = 1;
			Client1.UdpClose_0.ident = Client1.UdpOpen_0.ident; 					// Connection Ident from AsUDP.UDP_Open  				 
			UdpClose(&Client1.UdpClose_0);  										// Call the Function
			
			if( Client1.UdpClose_0.status == 0 )  									// Close successful -> Reopen the interface  
				Client1.sStep = 0;
			else if(Client1.UdpClose_0.status == ERR_FUB_BUSY)  					// Busy -> Goto Error Step	
				Client1.sStep = 3;
			else  																	// Goto Error Step  
				Client1.sStep = 10;
		}
		break;
		
		case 10: 																	// Here some error Handling has to be implemented  
			if (Client1.UdpOpen_0.status == udpERR_ALREADY_EXIST)
				Client1.sStep = 1;
			if (Client1.UdpOpen_0.status == ERR_FUB_BUSY)
				Client1.sStep = 0;
			break;
	}	
}
