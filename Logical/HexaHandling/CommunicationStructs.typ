
TYPE
	mLoc_typ : {REDUND_UNREPLICABLE} 	STRUCT 
	ii : {REDUND_UNREPLICABLE} USINT; (*Internal iteration variable*)
	bEvrOk : {REDUND_UNREPLICABLE} BOOL; (*Dummy boolean flag*)
	bFirstInStep : {REDUND_UNREPLICABLE} USINT; (*First time in a step*)
	END_STRUCT;
	LibAsUDP1_ST_typ : 	STRUCT  (*Datatyp for global Variables*)
		receive_data : ARRAY[0..50]OF STRING[85]; (*Data which has been received*)
		send_data : ARRAY[0..500]OF USINT; (*Data which should be sent*)
	END_STRUCT;
END_TYPE
