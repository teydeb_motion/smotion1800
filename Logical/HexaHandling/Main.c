/* Libraries */
#include <bur/plctypes.h>  
#include <AsUDP.h>
#include <math.h>  
#include <AsDefault.h>
#include <sys_lib.h> 
//#include "../SanlabLibrary/SanlabFunctionsRefGen.h"

/* External Libraries */
//#include "../SanlabLibrary/deneme.h"
#include "../SanlabLibrary/ActuatorFunctions.c"
#include "../SanlabLibrary/SanlabFunctionsRefGen.c"
#include "../UICommunication/FunctionSendData.c"
#include "../SanlabLibrary/InitialParameters.c"
#include "../SanlabLibrary/Error_Functions.c"
#include "../IMU/IMU_Functions.c"
#include "../UICommunication/UICommunication_Functions.c"

//#include "../SanlabLibrary/Math_Functions.c"
//#include "../SanlabLibrary/M2HDataSend.c"

void _INIT MainControlInit(void)
{
	InitializeParameters();				// Parameters will be initialized externally.
	InitializeStructs();				// Initialize Structs.
	InitializeSystemParameters();		// Initialize System Parameters.
	Refresh_MotionParams();				// Refresh Parameters.
	Set_DefaultIMUValues();
	SwitchButtonControl();
	Reset_Gmp = 1;
	Flag_ErrorReset = 0;
	
	Check_ControlBoxAndLightBarrier();
	DummyLimitSensor = 0;
}

void _CYCLIC MainControlCyclic(void)
{
	// IMU'dan okunan degerler hesaplanir.
	Calculate_IMUValues();
	DriverErrorState();
	CalculateEncoderOffset();
	AssignMn();
	AssignKn();
	
	if(DI_ErrorReset && !Flag_ErrorReset)
	{
		Flag_ErrorReset = 1;
		Time_ErrorReset = Time_Total;
	}
	
	//////////////////////////////////////////////////////////// Emergency + Start Stop Switch /////////////////////////////////////////////////
	
	if ((DI_OnOffSwitch) && !SYSTEM_Error && !Error_Emergency && FlagEMG && (Time_Total > Time_Delay))		// Is Cabin or Master Emergency button unlocked?
	{
		gMpAxisBasic_0[0].Power = 1;								// Power the main supply
	}	

	EMG_Flag = !((DI_OnOffSwitch) && !SYSTEM_Error && !Error_Emergency);		// The flag for SpeedZeroClose function.
	
	if (gMpAxisBasic_0[0].PowerOn && (DI_OnOffSwitch) && !Error_Emergency)
	{	
		switch (Sys_State)
		{	
			////////////////////////////////////////////////////////// SYSTEM ENERGIZED //////////////////////////////////////////////////////////
			case SYSTEM_ENERGIZED: 							// Energize the system with the user control
				
				Refresh_Energized();
				
				if (DI_StartButton)						// Does start button pressed
					ReleaseMotors();					// Enable motors
				
				IsCheckingStarted();					// If all motors are active, go to SYSTEM_CHECKING mode
				
				break;
	
			////////////////////////////////////////////////////////// SYSTEM CHECKING ///////////////////////////////////////////////////////////
			case SYSTEM_CHECKING: 						// System Checking for Initial conditions
					
				Refresh_Checking();
				
				/// Check if Pneumatic System is exist.
				
				if (PNEU.Exist)
				{
					if (!PNEU.OK)
					{
						if (!(fabs(Stroke[5]) < 0.005) || !(fabs(Stroke[4]) < 0.005) || !(fabs(Stroke[3]) < 0.005) || !(fabs(Stroke[2]) < 0.005) || !(fabs(Stroke[1]) < 0.005) || !(fabs(Stroke[0]) < 0.005))
							Re_Initialize = 1;
					}
				}
								
				if (!Check_Flag)                                          
				{
					Time_CheckStart = Time_Total;     					// Set the delay value as much as Check_Time value
					Check_Flag = 1;										// Set Checking flag to "1"
				}
				else if(Time_Total < (Time_CheckStart + Time_Check))            
				{
					SetPowerMotors(1);									// Power All Motors
					RefVelZero();										// Set RefVel values of motors to '0'
				
					//TODO: Motorlar�n aktif olup olmama durumu ile ilgili bir kontrol eklenecek
				}
				else
				{
					Sys_State = SYSTEM_READY_FOR_INITIALIZING;			// If all checking operations are done, get the system ready to start
					Check_Flag = 0;										// Set checking flag to '0'
				}
				
				break;		
				
			/////////////////////////////////////////////////// SYSTEM READY FOR INITIALIZING ////////////////////////////////////////////////////
			case SYSTEM_READY_FOR_INITIALIZING:
				
				Refresh_ReadyforInitializing();
				GoDownStrokeControl();									// Yumusatma icin gerekli ayarlamalar yapilir.									// Keep Safe for ramp Sensor
				SetPowerMotors(0);										// Power Off Motors
				SetPositionControl(0);									// Set Position control off
				
				if (!R_F_I_Flag)                            
				{
					Time_RFIStart = Time_Total;							// Getting initial time for System Ready For Initializing Stage
					R_F_I_Flag = 1;										// Set Flag true
				}
				else if(Time_Total < (Time_RFIStart + Time_RFI))		// If System is passive for R_F_I_Time time          
					IsInitializeStarted();								// Check if Initialize Button clicked on the User Interface
				else
				{
					SetErrorState(&Error_StandBy);
					R_F_I_Flag = 0;										// If User waited more than R_F_I_Time time, system will go on passive mode
				}
				
				break;			
			
			//////////////////////////////////////////////////////// SYSTEM INITIALIZING /////////////////////////////////////////////////////////
			case SYSTEM_INITIALIZING:

				PlotTrigger = 1;					
				SetPowerMotors(1);							// Enable motor brakes.
				PlatformGoDownVelReference();				// Platform goes to closing position with constant speed
				
				// If all actuators see the limit sensor, set reference speeds to zero.
				// If all RefVel values are zero, start counter. After 2 seconds, platform will go home position.
				
				Counter_Initial = (((Mot1.RefVel[0] == 0) && (Mot2.RefVel[0] == 0) && (Mot3.RefVel[0] == 0) && (Mot4.RefVel[0] == 0) && (Mot5.RefVel[0] == 0) && (Mot6.RefVel[0] == 0))) ? (Counter_Initial + 1) : 0;
				Sys_State = (Counter_Initial == 5000) ? SYSTEM_GO_TO_ZERO : SYSTEM_INITIALIZING;
				Counter_Initial = (Counter_Initial > 5000) ? 0 : Counter_Initial;
				
				break;		
				
			///////////////////////////////////////////////////////// SYSTEM GO TO ZERO //////////////////////////////////////////////////////////
			case SYSTEM_GO_TO_ZERO:
				
				Stroke_PositionControl(Neutral_StrokeMax,Neutral_StrokeMin);
				
				DummyVelCalculation_DowntoNeutralPosition(Stroke[0], 0);
				DummyVelCalculation_DowntoNeutralPosition(Stroke[1], 1);
				DummyVelCalculation_DowntoNeutralPosition(Stroke[2], 2);
				DummyVelCalculation_DowntoNeutralPosition(Stroke[3], 3);
				DummyVelCalculation_DowntoNeutralPosition(Stroke[4], 4);
				DummyVelCalculation_DowntoNeutralPosition(Stroke[5], 5);
				
				VelocityControl(Neutral_StrokeMax, Neutral_StrokeMin, 1.0, -1.0);				// Continue moving in velocity control if platform doesn't in position control condition		
				MotorRefAcc();																					// Calculate Motor Reference Acc for DOB
				
				// If Platform is in neutral height, set Sys_State value to System Ready for Data
				if((fabs(Stroke[5]) < 0.001) && (fabs(Stroke[4]) < 0.001) && (fabs(Stroke[3]) < 0.001) && (fabs(Stroke[2]) < 0.001) && (fabs(Stroke[1]) < 0.001) && (fabs(Stroke[0]) < 0.001))
				{
					if (PNEU.Exist)
					{
						if (Re_Initialize)
						{
							Re_Initialize = 0;
							Sys_State = SYSTEM_READY_FOR_INITIALIZING;
						}
						else if (!PNEU.OK)
						{
							Refresh_SettingPressure();
							Time_Pressure = Time_Total;
							SetPowerMotors(0);						// Motors power off
							SystemInitialized = Completed;			// Set System Initialized flag true
							Sys_State = SYSTEM_SETTING_PRESSURE;
						}
						else
						{
							SystemInitialized = Completed;			// Set System Initialized flag true
							Sys_State = SYSTEM_READY_FOR_DATA;
						}
					}
					else
					{
						SystemInitialized = Completed;			// Set System Initialized flag true
						Sys_State = SYSTEM_READY_FOR_DATA;
					}
				}
				else
					Sys_State = SYSTEM_GO_TO_ZERO;
		
				break;

			/////////////////////////////////////////////////////// SYSTEM READY FOR DATA ////////////////////////////////////////////////////////
			case SYSTEM_READY_FOR_DATA:
				
				PlotTrigger = 0;
				Refresh_ReadyforData();
				SetPowerMotors(0);						// Motors power off
				SetPositionControl(0);					// Disable position control of motors
				UpdateMotorOffsets();					// Update Motor Encoder Offsets		
				
				if (H2MMain.formID == ManualAxisControl || H2MMain.formID == Simulator )	// Check if user opened Simulator or Manual Axis Control page
					Sys_State = SYSTEM_READY_FOR_TRAINING;
				else if ((H2MMain.formID == SignalGenerator) || (H2MMain.formID == PlayFromFile) || (H2MMain.formID == SignalReplication))		// Check if user opened Signal Generator, Play From File or Signal Replication pages
				{
					if (H2MMain.command == COMMAND_SEND_DATA)														// If user sent prepared data to the controller.
						Sys_State = SYSTEM_GETTING_DATA;
				}
				else if (H2MMain.formID == ParkMode)
				{
					OldMotionState = ParkMode;
					GoDownStrokeControl();									// Yumusatma icin gerekli ayarlamalar yapilir.
					SetFlagZero();
					Sys_State = SYSTEM_PARK_MODE;	
				}
				
				break;
			
			///////////////////////////////////////////////////////// SYSTEM PARK MODE ///////////////////////////////////////////////////////////
			case SYSTEM_PARK_MODE:
				
				if (H2MMain.command == COMMAND_PLATFORM_GO_DOWN)			// Check if user clicked Platform Go Down button for park condition
				{	
					SetPositionControl(0);   										// Disable motor position control
				
					if (Counter_Initial != 2500)
						PlatformGoDownVelReference();									// As in SYSTEM_INITIALIZING mode, the platform will move down with constant speed
					
					StrokeControlMaintenance((DI_LimitSensor[0] == 0),(DI_LimitSensor[1] == 0),(DI_LimitSensor[2] == 0),(DI_LimitSensor[3] == 0),(DI_LimitSensor[4] == 0),(DI_LimitSensor[5] == 0), DOWN);
				}
				
				else if (H2MMain.command == COMMAND_PLATFORM_GO_CENTER)		// Check if user clicked Platform Go Center button in order to cancel park condition
				{
					AssignRefPos(0);											// Set Reference motor positions to zero				
					Stroke_PositionControl(Neutral_StrokeMax, Neutral_StrokeMin);
					
					if(Counter_Initial != 2500)
					{
						if(ParkStatus == UP)
						{
							DummyVelCalculation_UptoNeutralPosition(Stroke[0], 0);
							DummyVelCalculation_UptoNeutralPosition(Stroke[1], 1);
							DummyVelCalculation_UptoNeutralPosition(Stroke[2], 2);
							DummyVelCalculation_UptoNeutralPosition(Stroke[3], 3);
							DummyVelCalculation_UptoNeutralPosition(Stroke[4], 4);
							DummyVelCalculation_UptoNeutralPosition(Stroke[5], 5);
							
							VelocityControl(Neutral_StrokeMax, Neutral_StrokeMin, -1.0, 1.0);
						}
						else if(ParkStatus == DOWN)
						{
							DummyVelCalculation_DowntoNeutralPosition(Stroke[0], 0);
							DummyVelCalculation_DowntoNeutralPosition(Stroke[1], 1);
							DummyVelCalculation_DowntoNeutralPosition(Stroke[2], 2);
							DummyVelCalculation_DowntoNeutralPosition(Stroke[3], 3);
							DummyVelCalculation_DowntoNeutralPosition(Stroke[4], 4);
							DummyVelCalculation_DowntoNeutralPosition(Stroke[5], 5);
							
							VelocityControl(Neutral_StrokeMax, Neutral_StrokeMin, 1.0, -1.0);
						}		
						MotorRefAcc();												// Calculate motor reference acceleration for DOB
					}
					
					StrokeControlMaintenance(((Stroke[5] < Neutral_StrokeMax) && (Stroke[5] > Neutral_StrokeMin)), ((Stroke[4] < Neutral_StrokeMax) && (Stroke[4] > Neutral_StrokeMin)),
						((Stroke[3] < Neutral_StrokeMax) && (Stroke[3] > Neutral_StrokeMin)), ((Stroke[2] < Neutral_StrokeMax) && (Stroke[2] > Neutral_StrokeMin)),
						((Stroke[1] < Neutral_StrokeMax) && (Stroke[1] > Neutral_StrokeMin)), ((Stroke[0] < Neutral_StrokeMax) && (Stroke[0] > Neutral_StrokeMin)), NEUTRAL);
				}
					
				else if (H2MMain.command == COMMAND_PLATFORM_GO_UP)
				{
					AssignRefPos(Up_RefPos);											// Set Reference motor positions to zero				
					Stroke_PositionControl(Up_StrokeMax, Up_StrokeMin);
					
					if(Counter_Initial != 2500)
					{
						DummyVelCalculation_NeutraltoUpPosition(Stroke[0], 0);
						DummyVelCalculation_NeutraltoUpPosition(Stroke[1], 1);
						DummyVelCalculation_NeutraltoUpPosition(Stroke[2], 2);
						DummyVelCalculation_NeutraltoUpPosition(Stroke[3], 3);
						DummyVelCalculation_NeutraltoUpPosition(Stroke[4], 4);
						DummyVelCalculation_NeutraltoUpPosition(Stroke[5], 5);
					
						VelocityControl(Up_StrokeMax, Up_StrokeMin, 1.0, -1.0);									// Continue moving in velocity control if platform doesn't in position control condition		
						MotorRefAcc();													// Calculate motor reference acceleration for DOB
					}
					
					StrokeControlMaintenance(((Stroke[5] < Up_StrokeMax) && (Stroke[5] > Up_StrokeMin)), ((Stroke[4] < Up_StrokeMax) && (Stroke[4] > Up_StrokeMin)),
						((Stroke[3] < Up_StrokeMax) && (Stroke[3] > Up_StrokeMin)), ((Stroke[2] < Up_StrokeMax) && (Stroke[2] > Up_StrokeMin)),
						((Stroke[1] < Up_StrokeMax) && (Stroke[1] > Up_StrokeMin)), ((Stroke[0] < Up_StrokeMax) && (Stroke[0] > Up_StrokeMin)), UP);
				}
				
				else
					Counter_Initial = 2500;
				
				if ((OldMotionState == ParkMode) && (H2MMain.formID != ParkMode) && Counter_Initial == 2500)
				{
					SystemInitialized = NotCompleted;
					R_F_I_Flag = 0;
					Sys_State = SYSTEM_READY_FOR_INITIALIZING;						// Set Sys_State to System Ready for Training condition	
				}
				
				break;

			/////////////////////////////////////////////////////// SYSTEM GETTING DATA //////////////////////////////////////////////////////////
			case SYSTEM_GETTING_DATA:
				
				// Please check GetData project
				
				break; 
			
			//////////////////////////////////////////////////// SYSTEM READY FOR TRAINING ///////////////////////////////////////////////////////
			case SYSTEM_READY_FOR_TRAINING:
				
				SetPositionControl(1);					// Enable position control of motors.
				Refresh_ReadyforTraining();
				SetPowerMotors(0);						// Motors power off
//				UpdateMotorOffsets();
				
				if (H2MMain.formID == 0)
				{
					Sys_State = SYSTEM_READY_FOR_DATA; // Go back to System Ready For Data condition
				}											
				else if ((H2MMain.formID == SignalGenerator) || (H2MMain.formID == PlayFromFile) || (H2MMain.formID == SignalReplication))		// Did user required recalculate references?
				{
					if (H2MSignal.packetInfo == PACKET_RECALCULATE)
						Sys_State = SYSTEM_READY_FOR_DATA;							// Go back to System Ready For Data condition
				}
				else if(H2MMain.formID == Simulator)									// If user opened Simulator Reference Page
				{
					if (H2MMain.command == COMMAND_PLATFORM_GO_DOWN)				// and pressed Platform Go Down button
					{
						ParkingProcess = 0;
						OldMotionState = Simulator;
						GoDownStrokeControl();									// Yumusatma icin gerekli ayarlamalar yapilir.
						Sys_State = SYSTEM_PARK_MODE;								// Go back to System on Park Mode condition
					}
				}
				
				if (H2MMain.command == COMMAND_START_TRAINING)		// Did user Clicked Start Simulation button on User Interface?
				{
					CounterReadUDP = 0;												// Set Counter Read UDP value to zero for initial condition
					Time_TrainingStart = Time_Total;								// Save Training Start Time value
					Sys_State = SYSTEM_ON_TRAINING;									// Set Sys_State to System on Training condition
				}
				
				break;
			
			/////////////////////////////////////////////////////// SYSTEM ON TRAINING ///////////////////////////////////////////////////////////
			case SYSTEM_ON_TRAINING:
				
				// BR tarafindan kayit alinirken kullanilan flag.
				PlotTrigger = 1;
				
				// Motor frenleri acilir.
				SetPowerMotors(1);
				// Arayuze aktarilan 
				Time_Training = Time_Total - Time_TrainingStart;
				
				// Sinyal Uretici, Dosyadan Veri Oynatma veya Sinyal Replikasyonu penceresinden veri geldi ise,
				if ((H2MMain.formID == SignalGenerator) || (H2MMain.formID == PlayFromFile) || (H2MMain.formID == SignalReplication))
				{
					// Referans veri suresi boyunca
					if (Time_Total < Time_TrainingStart + Time_TrainingStop)
					{
						// Gelen veri sayisi kadar,
						if (PlayIndex < H2MSignal.totalDataSize)
						{
							// Referans pozisyonlarin atamasi yapilir.
							MotorRefPos(ArrayMot1_Pos[PlayIndex], ArrayMot2_Pos[PlayIndex], ArrayMot3_Pos[PlayIndex], ArrayMot4_Pos[PlayIndex], ArrayMot5_Pos[PlayIndex], ArrayMot6_Pos[PlayIndex]);
							if(H2MMain.formID == SignalReplication)
							{
								MotorRefVel_Rep(ArrayMot1_Vel[PlayIndex] , ArrayMot2_Vel[PlayIndex] , ArrayMot3_Vel[PlayIndex] , ArrayMot4_Vel[PlayIndex] , ArrayMot5_Vel[PlayIndex] , ArrayMot6_Vel[PlayIndex]);
								MotorRefAcc_Rep(ArrayMot1_Acc[PlayIndex], ArrayMot2_Acc[PlayIndex], ArrayMot3_Acc[PlayIndex], ArrayMot4_Acc[PlayIndex], ArrayMot5_Acc[PlayIndex], ArrayMot6_Acc[PlayIndex]);
							}
							else
							{
								MotorRefVel();
								MotorRefAcc();
							}
							PlayIndex++;
						}
					}
					else if (Time_Total >= Time_TrainingStart + Time_TrainingStop) // If TotalTime reached end time.
					{
						Sys_State = SYSTEM_READY_FOR_STOP_TRAINING;
						Time_Stop = Time_Total;
					}
				}
				else if ((H2MMain.formID == ManualAxisControl) || (H2MMain.formID == Simulator))	// A��lan referans sayfas� Manuel Eksen Kontrol� veya Simulator sayfas�ysa.
				{
					if ((H2MMain.formID == ManualAxisControl))								// E�er Manuel Eksen Kontrol� sayfas� a��ld�ysa
					{
						SMotion.EndX[0] += dT * (-H2MManual.axisTrans.Surge   / 350.0) * 0.05; // Surge (X) Ekseni Referans� Hesaplama
						SMotion.EndY[0] += dT * (-H2MManual.axisTrans.Sway    / 350.0) * 0.05; // Sway (Y) Ekseni Referans� Hesaplama
						SMotion.EndZ[0] += dT * (-H2MManual.axisTrans.Heave / 350.0) * 0.05;		// Heave (Z) Ekseni Referans� Hesaplama
						SMotion.EndR[0] += dT * D2R((-H2MManual.axisAngular.Roll  / 350.0) * 5.0);	 // Roll Ekseni Referans� Hesaplama
						SMotion.EndP[0] += dT * D2R((-H2MManual.axisAngular.Pitch / 350.0) * 5.0);	 // Pitch Ekseni Referans� Hesaplama
						SMotion.EndQ[0] += dT * D2R((-H2MManual.axisAngular.Yaw   / 350.0) * 5.0);	 // Yaw Ekseni Referans� Hesaplama
						
						InverseKinematic(&SMotion);		// Ters Kinematik Hesaplamasi
						
						if (SMotion.Link1[0] > 1.87 || SMotion.Link1[0] < 1.27 ||
							SMotion.Link2[0] > 1.87 || SMotion.Link2[0] < 1.27 ||
							SMotion.Link3[0] > 1.87 || SMotion.Link3[0] < 1.27 ||
							SMotion.Link4[0] > 1.87 || SMotion.Link4[0] < 1.27 ||
							SMotion.Link5[0] > 1.87 || SMotion.Link5[0] < 1.27 ||
							SMotion.Link6[0] > 1.87 || SMotion.Link6[0] < 1.27 )
						{
							SMotion.EndX[0] = SMotion.EndX[1];
							SMotion.EndY[0] = SMotion.EndY[1];
							SMotion.EndZ[0] = SMotion.EndZ[1];
							SMotion.EndR[0] = SMotion.EndR[1];
							SMotion.EndP[0] = SMotion.EndP[1];
							SMotion.EndQ[0] = SMotion.EndQ[1];
							
							InverseKinematic(&SMotion);		// Ters Kinematik Hesaplamasi
						}
						
						MotorRefPos(-LP2MP(SMotion.Link1[0]), -LP2MP(SMotion.Link2[0]), -LP2MP(SMotion.Link3[0]), -LP2MP(SMotion.Link4[0]), -LP2MP(SMotion.Link5[0]), -LP2MP(SMotion.Link6[0]));  	// Motor RefPos Hesaplama.
						MotorRefVel();								// Motor RefVel Hesaplama.
						MotorRefAcc();								// Motor RefAcc Hesaplama.
						
						SMotion.EndX[1] = SMotion.EndX[0];
						SMotion.EndY[1] = SMotion.EndY[0];
						SMotion.EndZ[1] = SMotion.EndZ[0];
						SMotion.EndR[1] = SMotion.EndR[0];
						SMotion.EndP[1] = SMotion.EndP[0];
						SMotion.EndQ[1] = SMotion.EndQ[0];	
					}
				
					if ((H2MMain.formID == Simulator))		// If Reference Page is Simulator
					{
						GetMotionCueingFilterGains();			// Update Motion Cueing Parameters
						TumbleControlforRoll();					// Roll Angle for Vehicle Overturn
						TumbleControlforPitch();				// Pitch Angle for Vehicle Overturn
						TumbleLimitforRoll();
						TumbleLimitforPitch();
						
						RollTumble  = 1.0 - Counter_RollTumble  / 2500;
						PitchTumble = 1.0 - Counter_PitchTumble / 2500;
						
						if (Time_Total < Time_TrainingStart + 3)							//	Smoothing Telemetry data for 3 secs from start of game.
							SmoothTelemetryVar = (Time_Total - Time_TrainingStart) / 3.0;
						else
							SmoothTelemetryVar = 1;
						
						if (CounterReadUDP == ReadUDP) 
						{
							CounterReadUDP = 0;				// Counts cycles to use in reading data via UDP 
							
							UpdateInterp(&D00);				// Update Interpolate Variables  
							UpdateInterp(&D01);				// Update Interpolate Variables
							UpdateInterp(&D02);				// Update Interpolate Variables
							UpdateInterp(&D03);				// Update Interpolate Variables
							UpdateInterp(&D04);				// Update Interpolate Variables
							UpdateInterp(&D05);				// Update Interpolate Variables
							UpdateInterp(&D06);				// Update Interpolate Variables
							UpdateInterp(&D07);				// Update Interpolate Variables
							
							D00.DataInput[0] = SAT(H2MSimulator.SurgeAccGain * 0.08 * H2MSimulator.axisTrans.Surge, 5);		// Saturates data to prevent any abnormal data
							D01.DataInput[0] = SAT(H2MSimulator.SwayAccGain  * 0.08 * H2MSimulator.axisTrans.Sway, 5);		// Saturates data to prevent any abnormal data
							D02.DataInput[0] = SAT(H2MSimulator.HeaveAccGain * 0.08 * H2MSimulator.axisTrans.Heave, 4);		// Saturates data to prevent any abnormal data
					
							D03.DataInput[0] = H2MSimulator.RollVelGain  * 0.1 * H2MSimulator.axisAngular.Roll;			// Saturates data to prevent any abnormal data
							D04.DataInput[0] = H2MSimulator.PitchVelGain * 0.1 * H2MSimulator.axisAngular.Pitch;			// Saturates data to prevent any abnormal data
							D05.DataInput[0] = H2MSimulator.YawVelGain   * 0.1 * H2MSimulator.axisAngular.Yaw;			// Saturates data to prevent any abnormal data
							D06.DataInput[0] = EulerRollFinal;															// Saturates data to prevent any abnormal data
							D07.DataInput[0] = EulerPitchFinal;															// Saturates data to prevent any abnormal data
	
							Interpolate(&D00);				// Interpolate Data
							Interpolate(&D01);				// Interpolate Data
							Interpolate(&D02);				// Interpolate Data
							Interpolate(&D03);				// Interpolate Data
							Interpolate(&D04);				// Interpolate Data
							Interpolate(&D05);				// Interpolate Data
							Interpolate(&D06);				// Interpolate Data
							Interpolate(&D07);				// Interpolate Data
						}
						
						TI = (1 + dTI * CounterReadUDP);
						
						VortInp00 = SmoothTelemetryVar * (D00.Coeffs[0] * pow(TI,3) + D00.Coeffs[1] * pow(TI,2) + D00.Coeffs[2] * TI + D00.Coeffs[3]); 		// Interpolated LinAccX 
						VortInp01 = SmoothTelemetryVar * (D01.Coeffs[0] * pow(TI,3) + D01.Coeffs[1] * pow(TI,2) + D01.Coeffs[2] * TI + D01.Coeffs[3]);		// Interpolated LinAccY 
						VortInp02 = SmoothTelemetryVar * (D02.Coeffs[0] * pow(TI,3) + D02.Coeffs[1] * pow(TI,2) + D02.Coeffs[2] * TI + D02.Coeffs[3]);		// Interpolated LinAccZ 
						VortInp03 = SmoothTelemetryVar * (D03.Coeffs[0] * pow(TI,3) + D03.Coeffs[1] * pow(TI,2) + D03.Coeffs[2] * TI + D03.Coeffs[3]);		// Interpolated AngVelR 
						VortInp04 = SmoothTelemetryVar * (D04.Coeffs[0] * pow(TI,3) + D04.Coeffs[1] * pow(TI,2) + D04.Coeffs[2] * TI + D04.Coeffs[3]);		// Interpolated AngVelP
						VortInp05 = SmoothTelemetryVar * (D05.Coeffs[0] * pow(TI,3) + D05.Coeffs[1] * pow(TI,2) + D05.Coeffs[2] * TI + D05.Coeffs[3]);		// Interpolated AngVelQ
						VortInp06 = SmoothTelemetryVar * (D06.Coeffs[0] * pow(TI,3) + D06.Coeffs[1] * pow(TI,2) + D06.Coeffs[2] * TI + D06.Coeffs[3]);		// Interpolated EulAngR
						VortInp07 = SmoothTelemetryVar * (D07.Coeffs[0] * pow(TI,3) + D07.Coeffs[1] * pow(TI,2) + D07.Coeffs[2] * TI + D07.Coeffs[3]);		// Interpolated EulAngP
						
						SMotion.EndX[0]  = INT1(&ISX2, INT1(&ISX1,  HPF(&HPFX3, HPF(&HPFX2, HPF(&HPFX1, VortInp00)))));
						SMotion.EndY[0]  = INT1(&ISY2, INT1(&ISY1,  HPF(&HPFY3, HPF(&HPFY2, HPF(&HPFY1, VortInp01)))));
						SMotion.EndZ[0]  = INT1(&ISZ2, INT1(&ISZ1,  HPF(&HPFZ3, HPF(&HPFZ2, HPF(&HPFZ1, VortInp02))))) + EndEffectorNeutralHeight;
						SMotion.EndR[0]  = INT1(&ISR1, HPF(&HPFR2, HPF(&HPFR1, VortInp03))) + H2MSimulator.RollTiltGain  * 4 * LPF(&LPFTCR, VortInp01) * (1 / Gravity) + RollTumble * 1 * H2MSimulator.RollEulGain * VortInp06 ;
						SMotion.EndP[0]  = INT1(&ISP1, HPF(&HPFP2, HPF(&HPFP1, VortInp04))) + H2MSimulator.PitchTiltGain * 4 * LPF(&LPFTCP, VortInp00) * ((-1) / Gravity) + PitchTumble * 1 * H2MSimulator.PitchEulGain * VortInp07;
						SMotion.EndQ[0]  = INT1(&ISQ1, HPF(&HPFQ2, HPF(&HPFQ1, VortInp05)));
						
						InverseKinematic(&SMotion);
						
						MotorRefPos(LP2MP(SMotion.Link1[0]), LP2MP(SMotion.Link2[0]), LP2MP(SMotion.Link3[0]), LP2MP(SMotion.Link4[0]), LP2MP(SMotion.Link5[0]), LP2MP(SMotion.Link6[0]));  	// Calculate Motor Ref Pos.
						MotorRefVel();								// Calculate Motor Ref Vel.
						MotorRefAcc();								// Calculate Motor Ref Acc.
						
						CounterReadUDP++;
					}
				}
				
				if (H2MMain.command == COMMAND_STOP_TRAINING) //Arayuz tarafindan hareketi bitir dediysek.
				{
					Sys_State = SYSTEM_READY_FOR_STOP_TRAINING;			// Set Sys_State to System Ready for Stop Training condition
					Time_Stop = Time_Total;								// Save the stop time
				}
				
				break;
			
			///////////////////////////////////////////////// SYSTEM READY FOR STOP TRAINING /////////////////////////////////////////////////////
			case SYSTEM_READY_FOR_STOP_TRAINING:
				
				if (Time_Total > Time_Stop && Time_Total < Time_Stop + 2)			// Wait for 2 secs.
				{

					RefVelZero();
					RefAccZero();	
					
					Mot1.EndPos = Mot1.Pos[0];									// Save the last leg position
					Mot2.EndPos = Mot2.Pos[0];									// Save the last leg position
					Mot3.EndPos = Mot3.Pos[0];									// Save the last leg position
					Mot4.EndPos = Mot4.Pos[0];									// Save the last leg position
					Mot5.EndPos = Mot5.Pos[0]; 									// Save the last leg position
					Mot6.EndPos = Mot6.Pos[0];									// Save the last leg position
				}
				
				if (Time_Total > Time_Stop + 2)										// Position of the End Effector is pulled to zero position (Neutral Position) in 2 secs.
					EndTrainingStatus = EndTraining(Time_Total, Time_Stop + 2);					
				
				if (EndTrainingStatus)
					Sys_State = SYSTEM_STOP_TRAINING;
				
				break;
			
			////////////////////////////////////////////////////// SYSTEM STOP TRAINING //////////////////////////////////////////////////////////		
			case SYSTEM_STOP_TRAINING:
				
				PlotTrigger = 0;
				
				Refresh_StopTraining();
				SetPowerMotors(0);							// Power Off Motors
				
				UpdateMotorOffsets();
				Sys_State = SYSTEM_READY_FOR_TRAINING;		// Go back to System Ready for Training condition
				
				break;
			
			default:
				break;
		}
		
		if ((SystemInitialized && (H2MMain.formID != OldMotionGUIForm)) && (PNEU.OK || !PNEU.Exist))			// If different form is opened, set Sys_State back to System Ready for Data or System Ready for Training
		{
			if (H2MMain.formID == PlayFromFile || H2MMain.formID == SignalGenerator || H2MMain.formID == SignalReplication)
				Sys_State = SYSTEM_READY_FOR_DATA;
			else if (H2MMain.formID == ManualAxisControl)
				Sys_State = SYSTEM_READY_FOR_TRAINING;
		
			OldMotionGUIForm =  H2MMain.formID;
		}
	}
	
	if (Sys_State == SYSTEM_ON_ERROR)								// If System on Error Mode
	{	
		if (H2MMain.command == COMMAND_ERROR_RESET || DI_ErrorReset)					// Check if the user clicked error reset button (From interface or button)
		{
			if (Error_LimitSensor)									// If the error caused by limit sensor
				ReleaseErrorState(&Error_LimitSensor);
			if (Error_StandBy)										// If the error caused by Passivity	
				ReleaseErrorState(&Error_StandBy);
			if (Error_Torque)									// If the error caused by Motor High Torque
				ReleaseIfMotorTorquesOk();
		
			if (Error_SwitchOn)								// Switch Control
			{
				if (!DI_OnOffSwitch)
					ReleaseErrorState(&Error_SwitchOn);
			}
			
			/* Hata durumlarinin kontrolu yapildiktan sonra ac-kapa anahtarinin kontrolu yapilir. */
			if (!(Error_LimitSensor || Error_OverMass || Error_SmokeSensor || Error_MotorEncoder || Error_StandBy || Error_Torque || Error_LimitSensorCrash || Error_SwitchOn || Error_Emergency || Error_ControlBoxConnection))
				SwitchButtonControl();
		}	
	}
	
	if (Sys_State == SYSTEM_PASSIVE)
	{
		if (Time_Total > 15)
		{
			Check_EncoValue();
			if(!EncoValFlag && !Error_MotCable)
				SetErrorState(&Error_MotCable);
		}
	}

	if (Sys_State == SYSTEM_EMERGENCY)
	{
		if (FlagEMG)
		{
			Error_Emergency = 0;
			Sys_State = SYSTEM_PASSIVE;
		}
	}
	
	///////////////////////// WARNING DURUMLARI /////////////////////////
	
	Check_MotorTemperature();			// "Motor Sicaklik" uyari durumlarini kontrol eder.
//	Check_IMUConnection();				// "IMU Baglanti" hata durumlarini kontrol eder.

	/////////////////////////////////////////////////////////////////////
	
	CheckForEmergencyFlag();										// Emergency Button Check

	if(FlagEMG)
		ButtonUP = 1;
	

	
	
	if (!((DI_OnOffSwitch) && !SYSTEM_Error && !Error_Emergency))				// If user pressed emergency button or system felt on error mode,
	{    
		if (!SpeedZeroFlag)
		{
			Time_Emergency = Time_Total;
			SpeedZeroFlag = 1;
		}
		
		SpeedZeroClose(1, &Mot1, &AxCon_0[1], Time_Total);			// Set zero speed of Motor 1.
		SpeedZeroClose(2, &Mot2, &AxCon_0[2], Time_Total);			// Set zero speed of Motor 2.
		SpeedZeroClose(3, &Mot3, &AxCon_0[3], Time_Total);			// Set zero speed of Motor 3.
		SpeedZeroClose(4, &Mot4, &AxCon_0[4], Time_Total);			// Set zero speed of Motor 4.
		SpeedZeroClose(5, &Mot5, &AxCon_0[5], Time_Total);			// Set zero speed of Motor 5.
		SpeedZeroClose(6, &Mot6, &AxCon_0[6], Time_Total);			// Set zero speed of Motor 6.
	
		if ((EMG_Flag_Close[0] * EMG_Flag_Close[1] * EMG_Flag_Close[2] * EMG_Flag_Close[3] * EMG_Flag_Close[4] * EMG_Flag_Close[5]) || Error_Emergency)	//	If all motors are in zero speed condition
		{ 
			gMpAxisBasic_0[0].Power = 0;							// Turn off the Main Power Supply
			Refresh_MotionParams();									// Refresh Some System Parameters
			SetPositionControl(0);									// Disable Position Control
		}
		
		if (!(DI_OnOffSwitch))											// If System Closed.
			Sys_State = SYSTEM_PASSIVE;				
		if (SYSTEM_Error)											// If System on Error Mode.
			Sys_State = SYSTEM_ON_ERROR;				
		if (Error_Emergency)											// If System on Emergency.
			Sys_State = SYSTEM_EMERGENCY;				
		
		Check_Flag = 0;
		SystemInitialized = NotCompleted;
		EMGFlag = 1;
	}
	
	Check_DriverSafety();				// "Kontrol Kutusu Baglanti" hata durumlarini kontrol eder.

	if (Sys_State != SYSTEM_EMERGENCY)
	{
		Error_BooltoByte();					// Hata durumlari ile ilgili verileri byte'a donusturur.
		State_BooltoByte();					// Motor ve surucu durumlari ile ilgili verileri byte'a donusturur.
		
//		Check_LimitSensor();				// "Limit sensor" ve "Limit Sensor Ariza" hata durumlarini kontrol eder.
		Check_SmokeSensor();				// "Duman Sensoru" hata durumlarini kontrol eder.
		Check_TotalMass();					// "Toplam Agirlik" hata durumlarini kontrol eder.
		Check_MotorTorque();				// "Motor Tork" hata durumlarini kontrol eder.
		Check_MotorEncoder();				// "Motor Enkoder" hata durumlarini kontrol eder.
	}
	
	if ((DI_OnOffSwitch) && EMGFlag && !SYSTEM_Error && !Error_Emergency && FlagEMG && (Time_Total > Time_Delay))		// After Emergency or System Error (Solved) condition, go back to system energized mode
	{
		Sys_State = SYSTEM_ENERGIZED;
		SpeedZeroFlag = 0;
		EMGFlag = 0;
	}
	
	// Calculate Motor Variables //
	CalculateMotResPos();
	CalculateMotResVel();
	CalculateMotResVelVOB();
	CalculateMotResAcc();
	CalculateStroke();
	CalculateMotorPower();

	// Calculate Actuator Variables //
	CalculateLegRefPos();					//Calculate Leg Reference Position (m)
	CalculateLegRefVel();					//Calculate Leg Reference Velocity (m/s)
	CalculateLegRefAcc();					//Calculate Leg Reference Acceleration (m/s2)
	CalculateLegPos();						//Calculate Leg Response Position (m)
	CalculateLegVel();						//Calculate Leg Response Velocity (m/s)
	CalculateLegAcc();						//Calculate Leg Response Acceleration (m/s2)
	
	if (EndTrainingStatus)						// If training mode is stopped
		AccDesZero();							// Set AccDes value to zero
	else
	{
		AccDesCalculation(&Mot1, 1);							// 1. Motor icin AccDes hesaplanir.
		AccDesCalculation(&Mot2, 2);							// 2. Motor icin AccDes hesaplanir.
		AccDesCalculation(&Mot3, 3);							// 3. Motor icin AccDes hesaplanir.
		AccDesCalculation(&Mot4, 4);							// 4. Motor icin AccDes hesaplanir.
		AccDesCalculation(&Mot5, 5);							// 5. Motor icin AccDes hesaplanir.
		AccDesCalculation(&Mot6, 6);							// 6. Motor icin AccDes hesaplanir.
	}
	
	DOBCalculation(&Mot1, &FDM1, 1, Buffer);					// 1. Motor icin DOB hesaplamalari yapilir.
	DOBCalculation(&Mot2, &FDM2, 2, Buffer);					// 2. Motor icin DOB hesaplamalari yapilir.
	DOBCalculation(&Mot3, &FDM3, 3, Buffer);					// 3. Motor icin DOB hesaplamalari yapilir.
	DOBCalculation(&Mot4, &FDM4, 4, Buffer);					// 4. Motor icin DOB hesaplamalari yapilir.
	DOBCalculation(&Mot5, &FDM5, 5, Buffer);					// 5. Motor icin DOB hesaplamalari yapilir.
	DOBCalculation(&Mot6, &FDM6, 6, Buffer);					// 6. Motor icin DOB hesaplamalari yapilir.

	UpdateMotor(&Mot1);											// 1. Motor parametreleri guncellenir.
	UpdateMotor(&Mot2);											// 2. Motor parametreleri guncellenir.
	UpdateMotor(&Mot3);											// 3. Motor parametreleri guncellenir.
	UpdateMotor(&Mot4);											// 4. Motor parametreleri guncellenir.
	UpdateMotor(&Mot5);											// 5. Motor parametreleri guncellenir.
	UpdateMotor(&Mot6);											// 6. Motor parametreleri guncellenir.

	AxCon(&AxCon_0[1]);											// 1. Motor icin donanim atamalari yapilir.
	AxCon(&AxCon_0[2]);											// 2. Motor icin donanim atamalari yapilir.
	AxCon(&AxCon_0[3]);											// 3. Motor icin donanim atamalari yapilir.
	AxCon(&AxCon_0[4]);											// 4. Motor icin donanim atamalari yapilir.
	AxCon(&AxCon_0[5]);											// 5. Motor icin donanim atamalari yapilir.
	AxCon(&AxCon_0[6]);											// 6. Motor icin donanim atamalari yapilir.

	ReadMotCurrent[0].Axis = &gAxisQ1;
	ReadMotCurrent[1].Axis = &gAxisQ2;
	ReadMotCurrent[2].Axis = &gAxisQ3;
	ReadMotCurrent[3].Axis = &gAxisQ4;
	ReadMotCurrent[4].Axis = &gAxisQ5;
	ReadMotCurrent[5].Axis = &gAxisQ6;
	
	MC_BR_CyclicRead(&ReadMotCurrent[0]);
	MC_BR_CyclicRead(&ReadMotCurrent[1]);
	MC_BR_CyclicRead(&ReadMotCurrent[2]);
	MC_BR_CyclicRead(&ReadMotCurrent[3]);
	MC_BR_CyclicRead(&ReadMotCurrent[4]);
	MC_BR_CyclicRead(&ReadMotCurrent[5]);
	
	SendDataAssignment(&M2H);									// Assign M2H Values
	memcpy((unsigned long)&DataToNetwork, (unsigned long)&M2H, sizeof(M2H));
	
	Time_Total += dT;											// Update Total Time
		
	Scope01 = MP2LP(-Mot1.RefPos[0]);
	Scope02 = MP2LP(-Mot2.RefPos[0]);
	Scope03 = MP2LP(-Mot3.RefPos[0]);
	Scope04 = MP2LP(-Mot4.RefPos[0]);
	Scope05 = MP2LP(-Mot5.RefPos[0]);
	Scope06 = MP2LP(-Mot6.RefPos[0]);
    
	Scope07 = MP2LP(-Mot1.Pos[0]);
	Scope08 = MP2LP(-Mot2.Pos[0]);
	Scope09 = MP2LP(-Mot3.Pos[0]);
	Scope10 = MP2LP(-Mot4.Pos[0]);
	Scope11 = MP2LP(-Mot5.Pos[0]);
	Scope12 = MP2LP(-Mot6.Pos[0]);
	
	Scope13 = Mot1.Vel[0];
	Scope14 = Mot2.Vel[0];
	Scope15 = Mot3.Vel[0];
	Scope16 = Mot4.Vel[0];
	Scope17 = Mot5.Vel[0];
	Scope18 = Mot6.Vel[0];
	
	Scope19 = gMpAxisBasic_0[1].Info.CyclicRead.Torque.Value;
	Scope20 = gMpAxisBasic_0[2].Info.CyclicRead.Torque.Value;
	Scope21 = gMpAxisBasic_0[3].Info.CyclicRead.Torque.Value;
	Scope22 = gMpAxisBasic_0[4].Info.CyclicRead.Torque.Value;
	Scope23 = gMpAxisBasic_0[5].Info.CyclicRead.Torque.Value;
	Scope24 = gMpAxisBasic_0[6].Info.CyclicRead.Torque.Value;
	
	Scope25 = gMpAxisBasic_0[1].Info.CyclicRead.MotorTemperature.Value;
	Scope26 = gMpAxisBasic_0[2].Info.CyclicRead.MotorTemperature.Value;
	Scope27 = gMpAxisBasic_0[3].Info.CyclicRead.MotorTemperature.Value;
	Scope28 = gMpAxisBasic_0[4].Info.CyclicRead.MotorTemperature.Value;
	Scope29 = gMpAxisBasic_0[5].Info.CyclicRead.MotorTemperature.Value;
	Scope30 = gMpAxisBasic_0[6].Info.CyclicRead.MotorTemperature.Value;
	
	Scope37 = Mot1.Power;
	Scope38 = Mot2.Power;
	Scope39 = Mot3.Power;
	Scope40 = Mot4.Power;
	Scope41 = Mot5.Power;
	Scope42 = Mot6.Power;

	DummyEnco[0] = AxCon_0[1].Internal.EncRawPos;
	DummyEnco[1] = AxCon_0[2].Internal.EncRawPos;
	DummyEnco[2] = AxCon_0[3].Internal.EncRawPos;
	DummyEnco[3] = AxCon_0[4].Internal.EncRawPos;
	DummyEnco[4] = AxCon_0[5].Internal.EncRawPos;
	DummyEnco[5] = AxCon_0[6].Internal.EncRawPos;
}
