// Matris degiskenlerinin tanimlamalari yapilir.
void Define_MatrixType()
{
	// Jacobian matrisi + ve - hiz limitleri icin bir kere hesaplanacagi icin Vel-PosLim ve Vel_NegLim ixerisinde tanimlanmadi.
	JacobianVel_MT.Handle   = &(JacobianVel[0][0]);
	JacobianVel_MT.Columns  = 6;
	JacobianVel_MT.Rows     = 6;
	
	// Pozitif eksen hiz limitleri icin kullanilacak matris elemanlari tanimlanir.
	Define_MatrixTypeFunction(&Vel_PosLim);
	// Negatif eksen hiz limitleri icin kullanilacak matris elemanlari tanimlanir.
	Define_MatrixTypeFunction(&Vel_NegLim);
}

// Limiting structinin elemanlarinin matris degiskenlerinin tanimlamalari yapilir.
void Define_MatrixTypeFunction(Limiting *LimStruct)
{
	// Hesaplanan bacak limitlerinin tutuldugu matrisin atamalari yapilir.
	LimStruct->LegVel_MT.Handle   = &(LimStruct->LegVel[0][0]);
	LimStruct->LegVel_MT.Columns  = 1;
	LimStruct->LegVel_MT.Rows     = 6;
	
	// Eksen limitlerinin tutuldugu matrisin atamalari yapilir.
	LimStruct->LimValue_MT.Handle  = &(LimStruct->LimValue[0][0]);
	LimStruct->LimValue_MT.Columns = 1;
	LimStruct->LimValue_MT.Rows    = 6;
}

// Derece-radyan donusum fonksiyonu
double D2R(double Input)
{
	return(Input * M_TWOPI / 360);
}

void Define_VelLimit()
{
	H2MLimit.X_PosVel = 0.7;
	H2MLimit.Y_PosVel = 0.7;
	H2MLimit.Z_PosVel = 0.55;
	H2MLimit.R_PosVel = D2R(6.0);
	H2MLimit.P_PosVel = D2R(6.0);
	H2MLimit.Q_PosVel = D2R(6.0);
	
	H2MLimit.X_NegVel = -0.7;
	H2MLimit.Y_NegVel = -0.7;
	H2MLimit.Z_NegVel = -0.55;
	H2MLimit.R_NegVel = D2R(-35.0);
	H2MLimit.P_NegVel = D2R(-35.0);
	H2MLimit.Q_NegVel = D2R(-40.0);
}

// Jacobian matrisi hesaplanir.
void JacobianCalculation(int index, Fwd_Kinematic *FwdKin, double B, double T)
{
	double _X, _Y, _Z, _R, _P, _Q, Pow_X, Pow_Y, Pow_Z, Two_ReRb, TwoRe_X, TwoRe_Y, TwoRe_Z;
	double Gain1, Gain2, Gain3, Gain4, Gain5, Gain6, Gain7, Gain8, Gain9, Gain10, Gain11, Gain12, Gain13, Gain14, Gain15, Gain16, Gain17, Gain18;
	double DummyDivide;
	
	_X    	= FwdKin->XPos[0];
	_Y    	= FwdKin->YPos[0];
	_Z    	= FwdKin->ZPos[0];
//	_R    	= D2R(FwdKin->RollPos[0]);
//	_P    	= D2R(FwdKin->PitchPos[0]);
//	_Q    	= D2R(FwdKin->YawPos[0]);
	_R    	= FwdKin->RollPos[0];
	_P    	= FwdKin->PitchPos[0];
	_Q    	= FwdKin->YawPos[0];
	Pow_X 	= _X * _X;
	Pow_Y 	= _Y * _Y;
	Pow_Z 	= _Z * _Z;
	Two_ReRb = Rb * Re * 2;
	TwoRe_X  = Two_Re * _X;
	TwoRe_Y  = Two_Re * _Y;
	TwoRe_Z  = Two_Re * _Z;
	
	Gain1    = Two_Re * cos(T);
	Gain2    = cos(_P) * cos(_Q);
	Gain3    = Two_Re * sin(T);
	Gain4    = cos(_R) * sin(_Q);
	Gain5    = cos(_Q) * sin(_P) * sin(_R);
	Gain6    = cos(T) * sin(_P);
	Gain7    = cos(T) * cos(_P) * sin(_Q);
	Gain8	 = sin(T) * cos(_Q) * cos(_R);
	Gain9    = cos(B) * sin(T);
	Gain10   = sin(T) * sin(_P) * sin(_Q) * sin(_R);
	Gain11   = cos(_P) * sin(_Q);
	Gain12   = sin(_P) * sin(_Q) * sin(_R);
	Gain13   = cos(_P) * sin(_R);
	Gain14   = sin(T) * cos(_Q) * sin(_R);
	Gain15   = sin(_Q) * sin(_R);
	Gain16   = sin(T) * cos(_R) * sin(_P) * sin(_Q);
	Gain17   = cos(T) * cos(_Q) * sin(_P);
	Gain18   = sin(B) * sin(T);
	
	// Bolum kismi ayni oldugu i�in hesaplamasi 1 kere yapilir.
	DummyDivide = pow((Pow_Rb+Pow_Re+Pow_X+Pow_Y+Pow_Z-Two_Rb*_X*cos(B)-Two_Rb*_Y*sin(B)-TwoRe_Z*Gain6+TwoRe_X*cos(T)*Gain2+TwoRe_Y*Gain7+TwoRe_Y*Gain8-TwoRe_X*sin(T)*Gain4+TwoRe_Z*sin(T)*Gain13-Two_ReRb*cos(B)*cos(T)*Gain2-Two_ReRb*sin(B)*Gain7+Two_ReRb*Gain9*Gain4-Two_ReRb*sin(B)*Gain8+TwoRe_X*sin(T)*Gain5+TwoRe_Y*Gain10-Two_ReRb*Gain9*Gain5-Two_ReRb*sin(B)*Gain10), 0.5);
	
	JacobianVel[0][index] =  (0.5 * (2.0 * _X - Two_Rb * cos(B) + Gain1 * Gain2 - Gain3 * Gain4 + Gain3 * Gain5)) / DummyDivide;
	JacobianVel[1][index] =  (0.5 * (2.0 * _Y - Two_Rb * sin(B) + Gain1 * Gain11 + Gain3 * cos(_Q) * cos(_R) + Gain3 * Gain12)) / DummyDivide;
	JacobianVel[2][index] =  (0.5 * (2.0 * _Z - Gain1 * sin(_P) + Gain3 * Gain13)) / DummyDivide;
	JacobianVel[3][index] =  (0.5 * (TwoRe_Z * sin(T) * cos(_P) * cos(_R) - TwoRe_Y * Gain14 + TwoRe_X * sin(T) * Gain15 - Two_ReRb * Gain9 * Gain15 + Two_ReRb * sin(B) * Gain14 + TwoRe_X * Gain8 * sin(_P) + TwoRe_Y * Gain16 - Two_ReRb * sin(B) * Gain16 - Two_ReRb * cos(B) * Gain8 * sin(_P))) / DummyDivide;
	JacobianVel[4][index] = -(0.5 * (TwoRe_Z * cos(T) * cos(_P) + TwoRe_X * Gain17 + TwoRe_Y * Gain6 * sin(_Q) + TwoRe_Z * sin(T) * sin(_P) * sin(_R) - Two_ReRb * cos(B) * Gain17 - Two_ReRb * sin(B) * Gain6 * sin(_Q) - TwoRe_X * sin(T) * Gain2 * sin(_R) - TwoRe_Y * sin(T) * Gain11 * sin(_R) + Two_ReRb * Gain18 * Gain11 * sin(_R) + Two_ReRb * Gain9 * Gain2 * sin(_R))) / DummyDivide;
	JacobianVel[5][index] =  (0.5 * (TwoRe_Y * cos(T) * Gain2 - TwoRe_X * Gain7 - TwoRe_X * Gain8 - TwoRe_Y * sin(T) * Gain4 + Two_ReRb * cos(B) * Gain7 - Two_ReRb * sin(B) * cos(T) * Gain2 + Two_ReRb * cos(B) * Gain8 + Two_ReRb * Gain18 * Gain4 + TwoRe_Y * sin(T) * Gain5 - TwoRe_X * Gain10 + Two_ReRb * Gain9 * Gain12 - Two_ReRb * Gain18 * Gain5)) / DummyDivide;
}

void Assign_VelLim(Limiting *LimStruct, H2M_Limit *GUILimStruct, int ControlFlag)
{
	// Pozitif eksen hiz limitlerinin atamalari yapilir.
	if(ControlFlag == 1)
	{
		LimStruct->LimValue[0][0] = GUILimStruct->X_PosVel;
		LimStruct->LimValue[0][1] = GUILimStruct->Y_PosVel;
		LimStruct->LimValue[0][2] = GUILimStruct->Z_PosVel;
		LimStruct->LimValue[0][3] = GUILimStruct->R_PosVel;
		LimStruct->LimValue[0][4] = GUILimStruct->P_PosVel;
		LimStruct->LimValue[0][5] = GUILimStruct->Q_PosVel;
	}
	// Negatif eksen hiz limitlerinin atamalari yapilir.
	else if(ControlFlag == 2)
	{
		LimStruct->LimValue[0][0] = GUILimStruct->X_NegVel;
		LimStruct->LimValue[0][1] = GUILimStruct->Y_NegVel;
		LimStruct->LimValue[0][2] = GUILimStruct->Z_NegVel;
		LimStruct->LimValue[0][3] = GUILimStruct->R_NegVel;
		LimStruct->LimValue[0][4] = GUILimStruct->P_NegVel;
		LimStruct->LimValue[0][5] = GUILimStruct->Q_NegVel;
	}
}

// Bacak hizindan motor hizlari hesaplanir.
double LV2MV(double Input)
{
	// Bacak hizlari motor hizlarina cevrilir.
	return(Input * M_TWOPI / (ballScrewPitch / ConvRate));
}

// Motor hizlari hesaplanir.
void Calculate_MotVel(Limiting *LimStruct)
{
	// Hesaplanan bacak hizlari motor hizlarina donusturulerek sature edilir.
	LimStruct->MotVel[0] = /*SAT(*/LV2MV(LimStruct->LegVel[0][0])/*, SatValue)*/;
	LimStruct->MotVel[1] = /*SAT(*/LV2MV(LimStruct->LegVel[0][1])/*, SatValue)*/;
	LimStruct->MotVel[2] = /*SAT(*/LV2MV(LimStruct->LegVel[0][2])/*, SatValue)*/;
	LimStruct->MotVel[3] = /*SAT(*/LV2MV(LimStruct->LegVel[0][3])/*, SatValue)*/;
	LimStruct->MotVel[4] = /*SAT(*/LV2MV(LimStruct->LegVel[0][4])/*, SatValue)*/;
	LimStruct->MotVel[5] = /*SAT(*/LV2MV(LimStruct->LegVel[0][5])/*, SatValue)*/;
}

// Acceleration Desired degerleri hesaplanir.
void Calculate_AccDes(Limiting *LimStruct)
{
	// Hiz limitlemede kullanilacak olan acc desired degerleri hesaplanir.
	LimStruct->AccDes[0] = KAcc * (Mot1.RefAcc[0]) + KVel * (LimStruct->MotVel[0] - Mot1.Vel[0]);
	LimStruct->AccDes[1] = KAcc * (Mot2.RefAcc[0]) + KVel * (LimStruct->MotVel[1] - Mot2.Vel[0]);
	LimStruct->AccDes[2] = KAcc * (Mot3.RefAcc[0]) + KVel * (LimStruct->MotVel[2] - Mot3.Vel[0]);
	LimStruct->AccDes[3] = KAcc * (Mot4.RefAcc[0]) + KVel * (LimStruct->MotVel[3] - Mot4.Vel[0]);
	LimStruct->AccDes[4] = KAcc * (Mot5.RefAcc[0]) + KVel * (LimStruct->MotVel[4] - Mot5.Vel[0]);
	LimStruct->AccDes[5] = KAcc * (Mot6.RefAcc[0]) + KVel * (LimStruct->MotVel[5] - Mot6.Vel[0]);
}

// Hesaplama islemleri gerceklestirilir.
void Calculate_VelLim(Limiting *LimStruct, int Flag)
{
	////////// Hiz Limitleme //////////
	// E   : Eksen pozisyonlar�  (m-�)
	// E'  : Eksen h�zlar�	     (m/s-rad/s)
	// E'' : Eksen ivmeleri	 	 (m/s�-rad/s�)
	    
	// L   : Bacak pozisyonlar�  (m)
	// L'  : Bacak h�zlar�		 (m/s)
	// L'' : Bacak ivmeleri	 	 (m/s�)
	    
	// J   : Jacobian matrisi
	// J^-1: jacobian matris tersi
	
	// L' = J^-1*E'
	
	//	| Leg1Vel |   | J11 J12 J13 J14 J15 J16 |   | XVel |
	//	| Leg2Vel |   |  .   .               .  |   | YVel |
	//	| Leg3Vel |   |  .       .           .  |   | ZVel |
	//	| Leg4Vel | = |  .           .       .  | . | RVel |
	//	| Leg5Vel |   |  .               .   .  |   | PVel |
	//	| Leg6Vel |   | j61  .   .   .   .	J66 |   | QVel |
	
	///////////////////////////////////
	
	// Limit degerlerinin matrise atamasi yapilir.
	//	Assign_VelLim(LimStruct, &H2MLimit, Flag);
	
	// Jacobian matrisi ile limitler carpilarak eksenden bacaga gecilir.
	MTLinAlgMatrixMultiplication(&(JacobianVel_MT), &(LimStruct->LimValue_MT), &(LimStruct->LegVel_MT));
	
	// Maksimum motor hizlari hesaplanir.
	Calculate_MotVel(LimStruct);
	
	// Maksimum acc desired hesaplanir.
	Calculate_AccDes(LimStruct);
}