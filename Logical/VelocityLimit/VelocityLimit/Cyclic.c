/* Libraries */
#include <bur/plctypes.h>  
#include <AsUDP.h>
#include <math.h>  
#include <AsDefault.h>
#include <sys_lib.h> 

/* External Libraries */
#include "../VelocityLimiting_Functions.c"

void _INIT ProgramInit(void)
{
	// Matris tanimlamalari gerceklestirilir.
	Define_MatrixType();
	Define_VelLimit();
	
	// Limit degerlerinin matrise atamasi yapilir.
	Assign_VelLim(&Vel_PosLim, &H2MLimit, 1);
	Assign_VelLim(&Vel_NegLim, &H2MLimit, 2);
}

void _CYCLIC ProgramCyclic(void)
{
	// Duz kinematik ciktisindan elde edilen degerler kullanildigi icin forward kinematik flaginin 1 olmasi beklenir.
//	if(SystemInitialized = Completed && FlagFwd)
	if(Sys_State == SYSTEM_ON_TRAINING)
	{
		// Jacobian matrisi hesaplanir.
		JacobianCalculation(0, &FwdKin, SMotion.B1, SMotion.T1);
		JacobianCalculation(1, &FwdKin, SMotion.B2, SMotion.T2);
		JacobianCalculation(2, &FwdKin, SMotion.B3, SMotion.T3);
		JacobianCalculation(3, &FwdKin, SMotion.B4, SMotion.T4);
		JacobianCalculation(4, &FwdKin, SMotion.B5, SMotion.T5);
		JacobianCalculation(5, &FwdKin, SMotion.B6, SMotion.T6);
		
		// Acceleration desired'in hesaplanmasi icin gerekli islemler gerceklestirilir.
		Calculate_VelLim(&Vel_PosLim, 1);
		Calculate_VelLim(&Vel_NegLim, 2);
	}
}