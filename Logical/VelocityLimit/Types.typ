
TYPE
	Limiting : 	STRUCT 
		LegVel : ARRAY[0..0,0..5]OF LREAL;
		LimValue : ARRAY[0..0,0..5]OF LREAL;
		LegVel_MT : MTLinAlgMatrixType;
		LimValue_MT : MTLinAlgMatrixType;
		MotVel : ARRAY[0..5]OF LREAL;
		AccDes : ARRAY[0..5]OF LREAL;
	END_STRUCT;
END_TYPE
