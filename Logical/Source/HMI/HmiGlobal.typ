(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: HMI
 * File: HmiGlobal.typ
 * Author: abdullahoglf
 * Created: March 10, 2014
 ********************************************************************
 * Data types of package HMI
 ********************************************************************)
(*********************************************************************************************

HMI variables

*********************************************************************************************)

TYPE
	Main_EncSettings_Steps_enm : 
		(
		MAIN_ENC_HOME_TO_LIM_SW := 0, (*Home to limit switch*)
		MAIN_ENC_W_HOME_DONE := 5, (*Wait until the homing completed*)
		MAIN_ENC_GET_OFFSET := 10, (*Get the actual encoder raw offset*)
		MAIN_ENC_SET_NEW_OFFSET := 15 (*Set new offset (according to the set position)*)
		);
	mTempHMI_typ : 	STRUCT  (*Temp structure for the sub tass*)
		ii : USINT; (*Internal iteration variable*)
		bFirstInStep : BOOL; (*Flag: first time in a step of a case machine*)
		iLoopEnd : USINT;
		bEvrOk : BOOL; (*Flag: dummy boolean*)
		str : STRING[250];
		iTemp : USINT;
		rTemp2 : REAL;
		rTemp : REAL;
		uAdr : UDINT;
		jj : DINT;
		cmdAllAxes : BOOL;
		FloorNumber : USINT;
		ActVelStep : LREAL;
		ActVel : LREAL;
		AxisIndex : ARRAY[0..55]OF BOOL;
		ij : USINT;
		OldIndex : USINT;
		k : USINT;
		encSettingStep : Main_EncSettings_Steps_enm;
		oldEncSettingStep : Main_EncSettings_Steps_enm;
		rAux : USINT; (*Aux real variable*)
		bFirstInStepLoc : BOOL;
	END_STRUCT;
	gHMI_typ : 	STRUCT  (*Structure for handling visu*)
		General : gHMI_general_typ; (*General HMI variables*)
		HomeP : gHMI_Home_typ; (*Home page variables*)
		Manual : gHMI_Manual_typ; (*Manual page variables*)
		RecipePage : gHMI_RecipePage_typ; (*Recepie page variables*)
		IO_Control : gHMI_IO_Control_typ; (*page variables*)
		HwCfg : gHmi_HwCfg_typ; (*Hw configuration page*)
		SysDiag : gHmi_SysDiag_typ;
		Others : gHMI_Others_typ; (*Others global variables related to HMI*)
		DrvPar : gHmi_DrvPar_typ; (*Driver parameter page variables*)
		MotorSetup : gHMI_MotorSetupPage_typ; (*Motor setup page *)
		Alarm : gHMIAlarm_typ; (*Alarms conection*)
		ProgInspect : ARRAY[0..9]OF stProgInspect_typ; (*Programs inspection variables - monitoring of machine program blocks status*)
	END_STRUCT;
END_TYPE

(*           General variables*)

TYPE
	gHMI_general_typ : 	STRUCT 
		OldPage : UINT;
		ActualPage : UINT; (*Actually shown visu page*)
		ChangeUnit : USINT;
		CurrentUnit : USINT;
		CurrentLanguage : USINT; (*Current language datapoint*)
		ChangeLanguage : USINT; (*Datapoint for changing the current language*)
		YckmSpecificDisplay : USINT;
		YckmBrekaerSpecificDisplay : USINT;
		FclSpecificDisplay : USINT;
		FcmSpecificDisplay : USINT;
		OckmSpecificDisplay : USINT;
		AxesRefStatus : ARRAY[0..3]OF USINT;
		AxesPwrStatus : ARRAY[0..3]OF USINT;
		ChangePage : UINT; (*Index of new page to show*)
		ActUserLevel : USINT; (*Actually loged user*)
		Blink_0_5s : USINT; (*Variable that is blinking every 0.5 s*)
		ShowForService : USINT; (*Status datapoint for elements shown only to Admin and Service*)
		ShowForSuperAdmin : USINT;
		ShowForAdmin : USINT; (*Status datapoint for elements shown only to Admin*)
		ActResolution : USINT; (*Act Resolution of HMI - 0 - WXGA; 1 - QVGA*)
	END_STRUCT;
END_TYPE

(*           Home page visu handling*)

TYPE
	gHMI_Home_typ : 	STRUCT  (*Machine Home page global variables*)
		Cmd : gHMI_HomeCommand_typ; (*Commands from home page*)
		Status : gHMI_HomeStatus_typ;
		SpindleCurrentFiltered : REAL; (*Actual spindle current after filtering*)
		SpindleCurrent : REAL; (*Actual spindle current*)
	END_STRUCT;
	gHMI_HomeCommand_typ : 	STRUCT  (*Structure with commands comming from the home page*)
		btnConfirmTool : ARRAY[0..1]OF BOOL; (*Cmd to confirm the actual tool*)
		btnStartStop : BOOL; (*Equivalent to HW button*)
		btnReset : BOOL; (*Equivalent to HW button*)
		btn_E_Stop : BOOL; (*Equivalent to HW button*)
		btnStop : BOOL;
		btnPark : BOOL;
	END_STRUCT;
	gHMI_HomeStatus_typ : 	STRUCT  (*Status datapoints*)
		NOT_USED : USINT; (*Not used variable, only to get rid of warning about "defined empty structure"*)
	END_STRUCT;
END_TYPE

(*           Manual Page Visu handling*)

TYPE
	gHMI_Manual_typ : 	STRUCT 
		ActiveAx : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF BOOL; (*Indication which axis is selected in visu - identical indexes with main program*)
		SetPos : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF REAL; (*Position where to move selected axis*)
	END_STRUCT;
END_TYPE

(*            Recepie Page Visu handling*)

TYPE
	gHMI_RecipePage_typ : 	STRUCT 
		PopUp : USINT;
		ActRecepieName : STRING[255]; (*Name of actually used recepie*)
		LoadAllMachinePar : BOOL; (*0 - load just recepie; 1 - load all machine params*)
	END_STRUCT;
END_TYPE

(*            IO control page Visu handling*)

TYPE
	gHMI_IO_Control_typ : 	STRUCT 
		SelectedSlotOld : USINT;
		SelectedSlot : USINT; (*Selected IO card slot*)
		FoceValue : ARRAY[0..15]OF UDINT; (*Force value for the IO channel*)
		MaxForceValueLimit : INT;
		MinForceValueLimit : INT;
		ForceStatus : ARRAY[0..15]OF BOOL; (*Force status of the active IO card*)
		ChannelInverted : ARRAY[0..15]OF BOOL; (*Invert status of the IO channel*)
		PhysicalStatus : ARRAY[0..15]OF UDINT; (*Physical value of the datapoint*)
		NameSelector : ARRAY[0..15]OF UINT;
		DatafieldStatus : ARRAY[0..15]OF USINT; (*Controls the visibility of the datafields on the HMI*)
		MaxDpIndex : USINT; (*Number of chanels in curently selected card*)
		ForceIOErrString : STRING[100]; (*Error text from FroceIO block*)
		ForceIOErrID : UINT; (*Error ID from ForceIO block*)
	END_STRUCT;
END_TYPE

(*            Motor setup page typ*)

TYPE
	gHMI_MotorSetupPage_typ : 	STRUCT  (*Motor Setup page structure*)
		Cmd : gHmi_MotorSetupPage_Cmd_typ; (*Cmd structure*)
		Status : gHMI_MotorSetupPage_Status_typ; (*Status structure for the page*)
		PositionController : gHmi_MotorSetupPage_Position_typ; (*Postion controller structure*)
		HighlightApply : BOOL;
		BmpSelector : USINT;
		SpeedController : gHmi_MotorSetupPage_Speed_typ; (*Speed controller structure*)
		MC_BR_SaveAxisPar_0 : MC_BR_SaveAxisPar; (*FB to save the data in the axis structure to an Init table*)
		MC_BR_InitAxisSubjectPar_0 : MC_BR_InitAxisSubjectPar; (*FB to initialize a specific part of the axis structure*)
		SelectedAxis : USINT; (*Selected axis on motor tuning page*)
	END_STRUCT;
	gHmi_MotorSetupPage_Cmd_typ : 	STRUCT  (*Cmd structure for the motor setup page*)
		ErrorAck : BOOL; (*Cmd for acknowledging errors*)
		Save : BOOL; (*Cmd for saving the new controller values to the init table of the respected axis*)
	END_STRUCT;
	gHmi_MotorSetupPage_Speed_typ : 	STRUCT 
		Output_Speed_FilterType : UINT; (*Speed controller filter type: 0 - OFF; 1 - LowPass; 2 - Notch.      (output to the HMI)*)
		Output_Speed_FilterCoeffA0 : REAL; (*Filter coefficient a0                             (output to the HMI)*)
		Output_Speed_FilterCoeffA1 : REAL; (*Filter coefficient a1                             (output to the HMI)*)
		Output_Speed_Kv : REAL; (*Speed controller proportional gain      (output to the HMI)*)
		Output_Speed_Tn : REAL; (*Speed controller integration time        (output to the HMI)*)
		Input_Speed_FilterType : UINT; (*Speed controller filter type: 0 - OFF; 1 - LowPass; 2 - Notch.      (output to the HMI)*)
		Input_Speed_FilterCoeffA0 : REAL; (*Filter coefficient a0                             *)
		Input_Speed_FilterCoeffA1 : REAL; (*Filter coefficient a1                             *)
		Input_Speed_Kv : REAL; (*Speed controller proportional gain      *)
		Input_Speed_Tn : REAL; (*Speed controller integration time        *)
	END_STRUCT;
	gHmi_MotorSetupPage_Position_typ : 	STRUCT 
		Output_Pos_Kv : REAL; (*Position controller proportional gain     (output to the HMI)*)
		Output_Pos_tn : REAL; (*Position controller integration time       (output to the HMI)*)
		Input_Pos_Kv : REAL; (*Position controller proportional gain     *)
		Input_Pos_tn : REAL; (*Position controller integration time       *)
	END_STRUCT;
	gHMI_MotorSetupPage_Status_typ : 	STRUCT 
		btnAck : USINT; (*Locking status datapoint control*)
		btnStop : USINT; (*Locking status datapoint control*)
		btnStart : USINT; (*Locking status datapoint control*)
	END_STRUCT;
END_TYPE

(*           Other HMI related global varialbes*)

TYPE
	gHMI_Others_typ : 	STRUCT 
		CmdLubricationOn : BOOL;
		HideLoginPopup : BOOL;
		PopUpRequestIndex : USINT;
		PopUpRequest : USINT;
		PopUpBtnCancStat : USINT;
		CmdCancel : BOOL;
		CmdOk : BOOL;
		ConfirmActTool : ARRAY[0..1]OF BOOL;
		PopUpBtnOkStat : USINT;
		DailyCounter : UDINT; (*Daily counter -- not permanent, resettible*)
	END_STRUCT;
END_TYPE

(*           Alarms*)

TYPE
	gHMIAlarm_typ : 	STRUCT  (*Alarms conection*)
		Actual : ARRAY[0..ERR_MAX_CNT_MINUS_1]OF BOOL; (*Actual alarm images*)
		Ack : ARRAY[0..ERR_MAX_CNT_MINUS_1]OF BOOL;
		AddInfo : ARRAY[0..19]OF STRING[100];
		Old : ARRAY[0..ERR_MAX_CNT_MINUS_1]OF BOOL; (*Alarm images from last cycle*)
		AxesAlarmInfo : ARRAY[0..6]OF gHMIAlarm_AxisAlarmInfo_typ; (*Used for filtering alarms in global area - to avoid covering Error, by newer warning*)
	END_STRUCT;
	gHMIAlarm_AxisAlarmInfo_typ : 	STRUCT 
		Count : UDINT;
		ID : UDINT;
		Text : ARRAY[0..3]OF STRING[80];
	END_STRUCT;
END_TYPE

(**)
(*System Diagnostics*)

TYPE
	gHmi_SysDiagAxStat_typ : 	STRUCT 
		NetworkInit : USINT;
		NetworkPhase : USINT;
		ErrorID : UINT;
		Error : USINT;
		HomingOk : USINT;
		NodeNr : UINT;
		ControllerReady : USINT;
		ControllerStatus : USINT;
		ReferenceSwitch : USINT;
		NegHwSwitch : USINT;
		PosHwSwitch : USINT;
		Trigger1 : USINT;
		Trigger2 : USINT;
		ActPosition : REAL;
		ActVelocity : REAL;
	END_STRUCT;
	gHmi_SysDiag_typ : 	STRUCT 
		TempEnableMem : ARRAY[0..3]OF USINT;
		AxDiagActivated : BOOL; (*Flag to activate axis service mode*)
		AxisStatus : ARRAY[0..3]OF gHmi_SysDiagAxStat_typ;
	END_STRUCT;
	gHmi_HwCfg_typ : 	STRUCT 
		CfgChangeReq : BOOL; (*Configuration change request*)
	END_STRUCT;
	gHmi_DrvPar_typ : 	STRUCT 
		SelectedAxIdx : USINT;
		CopyReq : BOOL;
	END_STRUCT;
END_TYPE

(*

------------- HMI Permanent variables ------------*)

TYPE
	gHMI_Permanent_typ : 	STRUCT  (*Permanent variables related to HMI*)
		SaveParToInitTable : BOOL;
		CycleCounter : UDINT; (*Cycle counter (not resettible)*)
		HwCfg : gHMI_Permanent_HwCfg_typ;
		UserPasswords : ARRAY[0..2]OF STRING[20]; (*Stored passwords to coresponding users levels*)
		UserNames : ARRAY[0..2]OF STRING[20]; (*Stored user names*)
		Unitgroup : USINT;
		Language : USINT; (*Index of selected language*)
		MachineLock : gHMI_Permanent_MachineLock_typ;
	END_STRUCT;
	gHMI_Permanent_MachineLock_typ : 	STRUCT 
		Day : USINT; (*Date for machine locking*)
		Month : USINT; (*Date for machine locking*)
		Year : UINT; (*Date for machine locking*)
		UnlockPass : INT; (*Code for unlocking the machine*)
		MachineLockEnabled : BOOL; (*Machine will be locked when the that has arrived if this flag is true*)
	END_STRUCT;
	gHMI_Permanenet_HwCfg_Gantry_typ : 	STRUCT 
		Active : BOOL;
		HoldingBrake : BOOL;
		MultiturnEncoder : BOOL;
	END_STRUCT;
	gHMI_Permanent_HwCfg_typ : 	STRUCT 
		LinearMotors : BOOL;
		Labeler : BOOL;
		CAxisRev0 : BOOL;
		Gantry : gHMI_Permanenet_HwCfg_Gantry_typ;
		OCKMEnableTransferToBT : BOOL;
		Loader : USINT;
		AsymmetricalLoader : BOOL;
		MachineType : USINT;
	END_STRUCT;
END_TYPE
