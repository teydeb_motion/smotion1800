(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: UserLogin.st
 * Author: turanskyl
 * Created: April 11, 2014
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 

(* User Login Handling *)
ACTION UserLogin:
	// logOutUser
	IF(stUserLogin.btnLogOut = TRUE) THEN 
		gHMI.General.ActUserLevel := 0;
		strcpy(ADR(stUserLogin.givenPassword), ADR('')); // clear given password 
		stUserLogin.serviceLayerStatusDatapoint	   := 1;	 // hide service layers
		stUserLogin.adminLayerStatusDatapoint	   := 1;	 // hide admin layers
		stUserLogin.superAdminLayerStatusDatapoint := 1;
		
		// log In User - Check if password is correct
	ELSIF (stUserLogin.btnLogIn = TRUE) THEN
		stUserLogin.wrongPassword := FALSE; // reset wrong password flag
		CASE (stUserLogin.selectedUserIndex) OF
			1: // try to log in as a service
				IF(strcmp(ADR(gHMI_Permanent.UserPasswords[1]),ADR(stUserLogin.givenPassword)) = 0) THEN // if password is correct
					gHMI.General.ActUserLevel := 1;	// set service user level
					stUserLogin.serviceLayerStatusDatapoint := 0;	// show service layers
					stUserLogin.adminLayerStatusDatapoint	:= 1;	// hide admin layers
					stGlobalArea.LoginPopUp.PopUp           := 1;   // hide the login popup in case the login was done through it
				ELSE
					stUserLogin.wrongPassword := TRUE;	// set flag that password is wrong	
				END_IF			
			2: // try to log in as admin
				IF(strcmp(ADR(gHMI_Permanent.UserPasswords[2]),ADR(stUserLogin.givenPassword)) = 0) THEN // if password is correct
					gHMI.General.ActUserLevel := 2;	// set service user level
					stUserLogin.serviceLayerStatusDatapoint    := 0;	// show service layers
					stUserLogin.adminLayerStatusDatapoint	   := 0;	// show admin layers
					stGlobalArea.LoginPopUp.PopUp              := 1;    // hide the login popup in case the login was done through it
				ELSIF atoi(ADR(stUserLogin.givenPassword)) = stUserLogin.alternateServPass THEN
					gHMI.General.ActUserLevel := 3;	// set service user level
					stUserLogin.serviceLayerStatusDatapoint    := 0;	// show service layers
					stUserLogin.adminLayerStatusDatapoint	   := 0;	// show admin layers
					stUserLogin.superAdminLayerStatusDatapoint := 0;
					stGlobalArea.LoginPopUp.PopUp              := 1;    // hide the login popup in case the login was done through it					
				ELSE
					stUserLogin.wrongPassword := TRUE;	// set flag that password is wrong
				END_IF		
		END_CASE;
		strcpy(ADR(stUserLogin.givenPassword), ADR(''));	// clear given password
	END_IF // end if log out or log in
	
	// show Wrong password msg to user
	IF(stUserLogin.wrongPassword = TRUE) THEN
		gHMI.Others.PopUpRequest            := SHOW_OBJ;
		gHMI.Others.PopUpRequestIndex       := 200; //('200' index of mesagge text on visu)
		gHMI.Others.PopUpBtnCancStat        := HIDE_OBJ;
		gHMI.Others.PopUpBtnOkStat          := SHOW_OBJ;
		strcpy(ADR(stUserLogin.givenPassword), ADR(''));	// clear given password 
	END_IF
	
	//Hide Wrong password msg after confirmation
	IF(gHMI.Others.CmdOk) THEN
		gHMI.Others.CmdOk             := FALSE;
		gHMI.Others.PopUpRequest      := HIDE_OBJ;
		stUserLogin.wrongPassword     := FALSE;	// Rst wrong password flag
		stGlobalArea.LoginPopUp.PopUp := HIDE_OBJ;		
	END_IF
	
	// reset btn commands
	stUserLogin.btnLogIn 	:= FALSE;
	stUserLogin.btnLogOut 	:= FALSE;	
	
END_ACTION // End of UserLogin



(* Auto User Logout *) 
ACTION AutoUserLogOut:
	(* Logging out user after certain time o inactivity - no touch od display; if the option is enabled *)
//	IF gFixData.Options.SW.AutoUserLogout  THEN
//		// Check max. time without activity
//		stUserLogin.AutLogOut.TON_1.IN := TRUE;   // Set up timer
//		stUserLogin.AutLogOut.TON_1.PT := T#300s; // 5 min
//	
//		IF NOT (stUserLogin.AutLogOut.Ready)THEN
//			stUserLogin.AutLogOut.VC_Handle := VA_Setup(1,'Visu');
//			IF(stUserLogin.AutLogOut.VC_Handle <> 0)THEN
//				stUserLogin.AutLogOut.Ready := TRUE;	
//			END_IF
//		ELSE // ready is true
//			IF(NOT UINT_TO_BOOL(VA_Saccess(1,stUserLogin.AutLogOut.VC_Handle))) THEN
//				VA_GetTouchAction(1,stUserLogin.AutLogOut.VC_Handle,0,ADR(stUserLogin.AutLogOut.Touch));
//				VA_Srelease(1,stUserLogin.AutLogOut.VC_Handle);
//				IF(stUserLogin.AutLogOut.Touch.status = 1 ) THEN // btn is released -> reset timer
//					stUserLogin.AutLogOut.TON_1.IN := FALSE;
//				END_IF
//			END_IF
//		END_IF
		// Execute timer
		stUserLogin.AutLogOut.TON_1();
		// Log out user after not activity for certain time
		IF(stUserLogin.AutLogOut.TON_1.Q) THEN
			gHMI.General.ActUserLevel := 0;
			stUserLogin.serviceLayerStatusDatapoint    := 1;
			stUserLogin.adminLayerStatusDatapoint      := 1;
			stUserLogin.superAdminLayerStatusDatapoint := 1; 
		END_IF	
	END_IF;	
END_ACTION

(* Generate the dynamic pass for Superadmin level *)
ACTION PassSettings:
	// update the alternative pass for service level
	stUserLogin.DTStructureGetTime_0.pDTStructure := ADR(stUserLogin.DateStructure);
	stUserLogin.DTStructureGetTime_0.enable       := TRUE;
	stUserLogin.DTStructureGetTime_0();
	
	itoa(stUserLogin.DateStructure.hour,    ADR(stUserLogin.DateString.Hour));
	itoa(stUserLogin.DateStructure.minute,  ADR(stUserLogin.DateString.Minute));
	
	IF stUserLogin.DateStructure.minute < 10 THEN
		auxStr := '0';
		strcat(ADR(auxStr), ADR(stUserLogin.DateString.Minute));
		stUserLogin.DateString.Minute := auxStr;
	END_IF;
	
	IF stUserLogin.DateStructure.hour < 10 THEN
		auxStr := '0';
		strcat(ADR(auxStr), ADR(stUserLogin.DateString.Hour));
		stUserLogin.DateString.Hour := auxStr;
	END_IF;	
	
	// separate the digits of the time
	strMin1  := RIGHT((stUserLogin.DateString.Minute), 1);
	strMin2  := LEFT((stUserLogin.DateString.Minute), 1);
	strHour1 := RIGHT((stUserLogin.DateString.Hour), 1);
	strHour2 := LEFT((stUserLogin.DateString.Hour), 1);
	// if the time is 09:04 the password is 5101
	stUserLogin.alternateServPass := DINT_TO_UINT(((atoi(ADR(strMin1))  + 1) * 1000) MOD 10000 +
									 			  ((atoi(ADR(strMin2))  + 1) * 100) MOD 1000 +
									 			  ((atoi(ADR(strHour1)) + 1) * 10) MOD 100 +
									 		      ((atoi(ADR(strHour2)) + 1) * 1) MOD 10);
END_ACTION