(* Production Protocol *)
ACTION ProductionProt:

	stat := PV_xgetadr(ADR('Clocks:Clocks'), ADR(uAdr), ADR(uSize));
	IF uAdr <> 0 AND stat = 0 THEN
		pClocks ACCESS uAdr;
	END_IF;
	// ----------------------------

	// PLC running time
	stProdProtocol.Par.PlcRunningTime		 := pClocks[CLK_TYP_PLC_ON].TotalTime; // display unit is hours
	
	// Total production time
	stProdProtocol.Par.TotalProductionTime	 := pClocks[CLK_TYP_AUTO_ON].TotalTime; // display unit is hours
END_ACTION