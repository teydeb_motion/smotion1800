(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: DateAndTimePage.st
 * Author: turanskyl
 * Created: April 11, 2014
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 


//**************************************************
//**** Date & Time *********************************
//**************************************************
ACTION DateAndTimePage:
	//Set time and date 
	//----------------------------------------------------
	IF (stDateAndTime.btnSetDateTime = TRUE) THEN
		//Execute FUB
		stDateAndTime.DTStructureSetTime_SetTime(enable := stDateAndTime.btnSetDateTime, pDTStructure := ADR(stDateAndTime.DateAndTimeStructure));	
	END_IF	
	IF FirstInPage THEN 
		stDateAndTime.Month 	:= stDateAndTime.DateTimeSys.month ;
		stDateAndTime.Year  	:= stDateAndTime.DateTimeSys.year; 
		stDateAndTime.Day   	:= stDateAndTime.DateTimeSys.day;
	
		stDateAndTime.Hour 	:= USINT_TO_INT(stDateAndTime.DateTimeSys.hour);
		stDateAndTime.Minute	:= USINT_TO_INT(stDateAndTime.DateTimeSys.minute); 
		stDateAndTime.Second   := USINT_TO_INT(stDateAndTime.DateTimeSys.second);
	END_IF 
	
	stDateAndTime.DateAndTimeStructure.month 	:= stDateAndTime.Month;
	stDateAndTime.DateAndTimeStructure.year  	:= stDateAndTime.Year; 
	stDateAndTime.DateAndTimeStructure.day   	:= stDateAndTime.Day;
	
	stDateAndTime.DateAndTimeStructure.hour    	:= INT_TO_USINT(stDateAndTime.Hour);
	stDateAndTime.DateAndTimeStructure.minute  	:= INT_TO_USINT(stDateAndTime.Minute); 
	stDateAndTime.DateAndTimeStructure.second  	:= INT_TO_USINT(stDateAndTime.Second);
	
	//Control of maximun value of the day depending on the month
	IF ( (stDateAndTime.Month = 1) OR (stDateAndTime.Month = 3) OR (stDateAndTime.Month = 5) OR (stDateAndTime.Month = 7) OR (stDateAndTime.Month = 8) OR (stDateAndTime.Month = 10) OR (stDateAndTime.Month = 12) ) THEN
		//31 days
		stDateAndTime.MaxDay := 31;
	ELSIF ( (stDateAndTime.Month = 4) OR (stDateAndTime.Month = 6) OR (stDateAndTime.Month = 9) OR (stDateAndTime.Month = 11) ) THEN
		//30 days
		stDateAndTime.MaxDay := 30;
	ELSIF ( (stDateAndTime.Month = 2) AND NOT( (stDateAndTime.Year MOD 4) = 0 ) AND ( (stDateAndTime.Year MOD 100) <> 0 OR (stDateAndTime.Year MOD 400) = 0) ) THEN	
		//28 days
		stDateAndTime.MaxDay := 28;
	ELSIF ( (stDateAndTime.Month = 2) AND ( (stDateAndTime.Year MOD 4) = 0 ) AND ( (stDateAndTime.Year MOD 100) <>0 OR (stDateAndTime.Year MOD 400) = 0) ) THEN	
		//29 days
		stDateAndTime.MaxDay := 29;
	END_IF			
	
	//Get time and date
	//-----------------------------------------------------------
	stDateAndTime.DTStructureGetTime_System.enable 				:= TRUE;
	stDateAndTime.DTStructureGetTime_System.pDTStructure 		:= ADR(stDateAndTime.DateTimeSys);
	//Execute FUB
	stDateAndTime.DTStructureGetTime_System();
	
	//If the input fot the date and time are 0, set them to the actual one
	IF (stDateAndTime.DateAndTimeStructure.year = 0) THEN
		stDateAndTime.DateAndTimeStructure		:= stDateAndTime.DateTimeSys;
	END_IF	
END_ACTION