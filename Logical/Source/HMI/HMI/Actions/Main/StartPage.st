(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: StartPage.st
 * Author: turanskyl
 * Created: April 11, 2014
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 

//**************************************************
//**** START PAGE **********************************
//**************************************************
ACTION StartPage:
	
	// Change to the last active language
	gHMI.General.ChangeLanguage := gHMI_Permanent.Language;
	
	// Control start page time
	TON_StartPage.IN := TRUE;
	temp := 100 / DINT_TO_REAL((TIME_TO_DINT(TON_StartPage.PT)));	// Calculate full range
	stStartPage.BarGraphProgress	:= REAL_TO_INT(temp * DINT_TO_REAL(TIME_TO_DINT(TON_StartPage.ET)));	// Calculate actualy covered range
	IF TON_StartPage.Q THEN
		TON_StartPage.IN        := FALSE;
		gHMI.General.ChangePage := PAGE_HOME;
	END_IF;		
					
END_ACTION // start page
