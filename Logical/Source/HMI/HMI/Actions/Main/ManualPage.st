(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    tufekcio
 * Created:   January 17, 2017/5:04 PM 
 *********************************************************************************)


//**************************************************
//**** Manual Page *********************************
//**************************************************
ACTION ManualPage:
	FOR mTemp.ii  := 0 TO  MAX_AX_TOT_MINUS_ONE DO
		IF gURecData.AxEnabled[mTemp.ii] THEN 
			gUFixData.Ax[mTemp.ii].AxPar.Jog.Velocity 	:= stManPage.JogSpeed[mTemp.ii];
			gUHandle.Unit.Cmd.Manual.JogNeg[mTemp.ii] 	:= stManPage.Cmd.JogNeg AND  stManPage.SelectedAxis[mTemp.ii];
			gUHandle.Unit.Cmd.Manual.JogPos[mTemp.ii] 	:= stManPage.Cmd.JogPos AND  stManPage.SelectedAxis[mTemp.ii];
			IF stManPage.Cmd.GoToSetPos AND stManPage.SelectedAxis[mTemp.ii] THEN 
				IF 	gMpAxisBasicPar[mTemp.ii].Position 			<>  gHMI.Manual.SetPos[mTemp.ii]   THEN 
					gMpAxisBasicPar[mTemp.ii].Position 			:= gHMI.Manual.SetPos[mTemp.ii];
					gMpAxisBasic_0[mTemp.ii].Update 			:= TRUE ; 
				END_IF ; 
				
				IF   gMpAxisBasic_0[mTemp.ii].UpdateDone  THEN 
					gMpAxisBasic_0[mTemp.ii].Update := FALSE; 
				END_IF ; 
				
				IF EDGENEG(gUHandle.Unit.Cmd.Manual.GoPos[mTemp.ii])  THEN 
					 stManPage.Cmd.GoToSetPos := FALSE ;
				END_IF ; 
				
				IF EDGEPOS  (stManPage.Cmd.Stop) THEN 
					gUHandle.Unit.Cmd.Manual.GoPos[mTemp.ii] := FALSE ; 
					stManPage.Cmd.Stop 						 := FALSE ; 
					stManPage.Cmd.GoToSetPos				 := FALSE ;
				END_IF; 
				
					
				IF NOT gMpAxisBasic_0[mTemp.ii].Update   THEN 
					gUHandle.Unit.Cmd.Manual.GoPos[mTemp.ii] := stManPage.Cmd.GoToSetPos;
				END_IF  
					
			END_IF ;
	
				
		END_IF  ;
	END_FOR;
	
END_ACTION