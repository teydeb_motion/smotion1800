(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: AlarmPage.st
 * Author: turanskyl
 * Created: April 11, 2014
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 

(* Alarm page handling *)
ACTION AlarmPage: 
	stAlarms.Axis.LockAckOne   := UNLOCK_OBJ;
	stAlarms.MaxAxSelectDP     := MAX_AX_TOT;
	
	// --------------------------------------
	// handle the visibility of the details layer
	IF stAlarms.ShowDetails > stAlarmsOld.ShowDetails THEN
		stAlarms.AxisIndex    := 3; // start with index of the first axis to make it easier for the operator
		stAlarms.ShowDetails  := FALSE;
		stAlarms.LayerCtrl    := SHOW_OBJ;
	ELSIF stAlarms.Axis.cmdBack > stAlarmsOld.Axis.cmdBack THEN
		stAlarms.Axis.cmdBack := FALSE;
		stAlarms.LayerCtrl    := HIDE_OBJ;
	END_IF;
	
	// check for layer active so that the indexes are made sure to be within the array boundaries
	IF stAlarms.LayerCtrl = SHOW_OBJ THEN
		IF stAlarms.AxisIndex < 3 THEN
			stAlarms.Axis.LockAckOne := LOCK_OBJ;
		END_IF;
		// --------------------------------------
		// handle the axis navigation
		IF stAlarms.Axis.BtnNextAxis > stAlarmsOld.Axis.BtnNextAxis THEN
			stAlarms.Axis.BtnNextAxis := FALSE;
			// check the axis index boundary
			IF stAlarms.AxisIndex < stAlarms.MaxAxSelectDP THEN
				stAlarms.AxisIndex := stAlarms.AxisIndex + 1;
			ELSE
				stAlarms.AxisIndex := 0;
			END_IF; // end of the boundary check
		ELSIF stAlarms.Axis.BtnPrewAxis > stAlarmsOld.Axis.BtnPrewAxis THEN
			stAlarms.Axis.BtnPrewAxis := FALSE;
			// check the axis index boundary
			IF stAlarms.AxisIndex > 0 THEN
				stAlarms.AxisIndex := stAlarms.AxisIndex - 1;
			ELSE
				stAlarms.AxisIndex := stAlarms.MaxAxSelectDP;
			END_IF; // end of the boundary check
		END_IF; // end of the nav. handling

		
		// --------------------------------------
//		memcpy(ADR(gHMI.Alarm.AxesAlarmInfo[2].Text), ADR(gRecipe_Ctrl.Out.Stat.Error.Text), SIZEOF(gHMI.Alarm.AxesAlarmInfo[6].Text));
		
		// copy the alarm info of the selected axis
		memcpy(ADR(stAlarms.Axis.ErrInfo), ADR(gHMI.Alarm.AxesAlarmInfo[stAlarms.AxisIndex]), SIZEOF(stAlarms.Axis.ErrInfo));
	END_IF; // end of the layer active check


	// copy the old structure to the new one
	memcpy(ADR(stAlarmsOld), ADR(stAlarms), SIZEOF(stAlarmsOld));

END_ACTION	