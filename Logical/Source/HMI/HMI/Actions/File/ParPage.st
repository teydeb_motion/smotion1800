(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HmiControl
 * File: ParPage.st
 * Author: abdullahoglf
 * Created: January 19, 2015
 *******************************************************************)

(* Global action to control parameter page *)
ACTION ParPage:
	// --------------------------------------
	// control the layer & tab visibility
	memset(ADR(stParPage.Hide), 1, SIZEOF(stParPage.Hide));           // first hide all
	memset(ADR(stParPage.Highlight), 0, SIZEOF(stParPage.Highlight)); // reset all the highlight datapoints
	stParPage.Hide.ParLayerCtrl[stParPage.SetLayer] := SHOW_OBJ;      // display the selected layer
	stParPage.Highlight[stParPage.SetLayer]         := TRUE;          // higlight the active layer
	
	IF stParPage.SetLayer = 0  THEN
		stParPage.SetLayer := 1;
	END_IF;


	stParPage.OldSetLayer := stParPage.SetLayer;
END_ACTION