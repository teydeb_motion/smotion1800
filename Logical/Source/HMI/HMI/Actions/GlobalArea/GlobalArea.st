(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: GlobalArea.st
 * Author: turanskyl
 * Created: April 11, 2014
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 

(* Global Area *)
ACTION GlobaArea:
	
	(* --- *)
	PassSettings; // actually set the super admin password
	(* --- *)
	(* --- *)
	HelpHandling; // help handling
	(* --- *)
	
	// --------------------------------------
	// user login popup handling
	IF stGlobalArea.LoginPopUp.cmdShow THEN
		stGlobalArea.LoginPopUp.PopUp     := SHOW_OBJ;
		stGlobalArea.LoginPopUp.cmdShow   := FALSE;
	ELSIF stGlobalArea.LoginPopUp.cmdCancel OR gHMI.Others.HideLoginPopup THEN
		stGlobalArea.LoginPopUp.PopUp     := HIDE_OBJ;
		gHMI.Others.HideLoginPopup        := FALSE;
		stGlobalArea.LoginPopUp.cmdCancel := FALSE;
	END_IF; // end of the pop up handling

	
	// if the popup is active call the login action
	IF stGlobalArea.LoginPopUp.PopUp = SHOW_OBJ THEN
		UserLogin;
	END_IF;
	
	IF gHMI.General.ActualPage  = PAGE_USER  	THEN
		stGlobalArea.LoginPopUp.loginLock := LOCK_OBJ ;
	ELSE
		stGlobalArea.LoginPopUp.loginLock := UNLOCK_OBJ;	
	END_IF;
	
	// Get actual system time
	stStartPage.DTStructureGetTime_0.enable 			:= TRUE;
	stStartPage.DTStructureGetTime_0.pDTStructure 		:= ADR(stStartPage.DT_Structure_0);
	stStartPage.DTStructureGetTime_0();	//Execute FUB	

	// Check machine locking
	IF (gHMI_Permanent.MachineLock.Year   <= stStartPage.DT_Structure_0.year    AND
		gHMI_Permanent.MachineLock.Month  <= stStartPage.DT_Structure_0.month   AND
		gHMI_Permanent.MachineLock.Day    <= stStartPage.DT_Structure_0.day     AND
		gHMI_Permanent.MachineLock.MachineLockEnabled = TRUE)	THEN
		gHMI.General.ChangePage  := PAGE_MACHINE_LOCK;	// Go to machine lock page	
		TON_StartPage.IN         := FALSE;
	END_IF // Machine lock 
	
	//-------------------------------------------------------
	// Page buttons highliting
	memset(ADR(stGlobalArea.BtnHighlit), 0, SIZEOF(stGlobalArea.BtnHighlit)); // reset all highlights
	// set highliting based on current page
	IF(gHMI.General.ActualPage = PAGE_ALARMS_ACTUAL) THEN
		stGlobalArea.BtnHighlit.Alarms := 1;
	ELSIF(gHMI.General.ActualPage = PAGE_HOME) THEN
		stGlobalArea.BtnHighlit.Home := 1;
	ELSIF(gHMI.General.ActualPage = PAGE_MANUAL) THEN
		stGlobalArea.BtnHighlit.Manual := 1;
	END_IF // end page highlitings

	// set the alarm prio filter
	IF stGlobalArea.Alarm.AlarmAct THEN
		stGlobalArea.Alarm.AlarmPrioFilter := 1;
	ELSE
		stGlobalArea.Alarm.AlarmPrioFilter := 255;
	END_IF;

	
	//----------------------------------------------
	//---- Page navigation 
	// Page was changed or should be changed -> execute page nav. action
	IF (stNavArrows.In.actPage <> UINT_TO_INT(gHMI.General.ActualPage) OR stNavArrows.In.pageNavigCmd <> 0)THEN		
		// Update inputs	
		stNavArrows.In.actPage := UINT_TO_INT(gHMI.General.ActualPage);
//		PageArrowNavigation;	// execute action - in future can become function block
		stNavArrows.In.pageNavigCmd := 0;	// rst command	
		
		// Update outputs	
		IF(stNavArrows.Out.nextPage <> 0) THEN
			gHMI.General.ChangePage := stNavArrows.Out.nextPage;
		END_IF
	END_IF


END_ACTION // end of globalArea



(* Top Menu Handling *)
ACTION TopMenuHandling:
	//---------------------------------------------- 
	//Check if top menu for loops should be executed
	// Menu btn was pressed
	FOR i:=0 TO MENU_ITEMS_MINUS_1 BY 1 DO		// Go trhough all menus
		IF(topMenu.btnMenu[i] = TRUE) THEN 	// menu btn is pressed or Hotspot to hide all is pressed
			executeForLoops := TRUE;	//execute for loops just when change occurs to save CPU performence
			i := MENU_ITEMS_MINUS_1;
		END_IF // end if menu btn is pressed
	END_FOR;
	
	// Page has changed
	IF (oldPage <> gHMI.General.ActualPage) THEN
		executeForLoops := TRUE;
		stGlobalArea.HideTopMenu_QVGA	:= TRUE;
	END_IF
	
	//--------------------------------
	//Execute top menu handling in needed
	IF(executeForLoops = TRUE) THEN 
		executeForLoops	:= FALSE; // reset cmd
		
		FOR i:=0 TO MENU_ITEMS_MINUS_1 BY 1 DO		// Go trhough all menus
		
			//-- Menu btn is pressed and was hidden
			IF (topMenu.btnMenu[i] = TRUE AND topMenu.HideBtn[i*10] = TRUE) THEN 
				// Hide all top menu btns
				memset(ADR(topMenu.HideBtn), 1, SIZEOF(topMenu.HideBtn));
				// Show column coresponding to pressed menu btn
				FOR j:=(i*10) TO (i*10 + MAX_MENU_ITEMS[i]) BY 1 DO 
					topMenu.HideBtn[j] := FALSE;
				END_FOR;
				i := MENU_ITEMS_MINUS_1; //End for cycle
						
				//--- Menu btn is pressed and was shown - hide it 
			ELSIF (topMenu.btnMenu[i] = TRUE AND topMenu.HideBtn[i*10] = FALSE) THEN 
				FOR j:=(i*10) TO (i*10 + MAX_MENU_ITEMS[i]) BY 1 DO // Hide column coresponding to pressed menu btn
					topMenu.HideBtn[j] := TRUE;
				END_FOR;
						
				//--- Page has changed 
			ELSIF (oldPage <> gHMI.General.ActualPage) THEN
				// Hide all top menu btns
				memset(ADR(topMenu.HideBtn), 1, SIZEOF(topMenu.HideBtn));
			END_IF;
			
		END_FOR; // end of go through all menus
		
		FOR i:=0 TO MENU_ITEMS_MINUS_1 BY 1 DO	// Reset menu btns
			topMenu.btnMenu[i] := 0;
		END_FOR;
		
		
	END_IF; // end of top menu handling
	
	oldPage := gHMI.General.ActualPage;	// Update old page value
	
	
END_ACTION // end of TopMenuHandling



(* Page Arrow Navigation *)
ACTION PageArrowNavigation: 
		// init variables before going through algorithm
	//-------------------------------------------------------
	stNavArrows.Out.pageNavigationError	:= 0;	// no error
	j := 9;		// last possible index in each level
	stNavArrows.Internal.pageExist := FALSE;	// rst. flag taht page was found
	stNavArrows.Internal.lastPageInLevel 	:= 0;		// reset variable with index of last page in current level.
	stNavArrows.Out.nextPage := 0; 					// reset page number - where to go


	//Hide or show navigation arrows coresponding to actual position in HMI
	//-------------------------------------------------------
	///show all navigation buttons
	stNavArrows.Out.hideNavigationLeft	:= FALSE;
	stNavArrows.Out.hideNavigationRight	:= FALSE;
	stNavArrows.Out.hideNavigationUp	:= FALSE;

	IF(gHMI.General.ActualPage < 999) THEN // HMI is in one of Template pages - navigation left and right is not possible
		stNavArrows.Out.hideNavigationLeft	:= TRUE;
		stNavArrows.Out.hideNavigationRight	:= TRUE;
		stNavArrows.Out.hideNavigationUp	:= FALSE;
	ELSIF (gHMI.General.ActualPage = 1000) THEN // HMI is in main page - navigation up is not possible
		stNavArrows.Out.hideNavigationUp	:= TRUE;
	END_IF


	// Obtain actual position in menu -> set menu levels
	//-------------------------------------------------------
	IF (stNavArrows.In.actPage < 1000) THEN	// if page index is < 1000 (common HMI pages) -> add 0 in front of string
		tempActPage := INT_TO_DINT(stNavArrows.In.actPage);
		itoa(tempActPage, ADR(stNavArrows.Internal.string2));		// convert acutal page number to string
		strcpy(ADR(stNavArrows.Internal.string1),ADR('0'));		// add 0 to beginign of string
		strcat(ADR(stNavArrows.Internal.string1),ADR(stNavArrows.Internal.string2));	// conect with page index number
	ELSE					// page index > 999 - machine pages
		tempActPage := INT_TO_DINT(stNavArrows.In.actPage);
		itoa(tempActPage, ADR(stNavArrows.Internal.string1));	// convert acutal page number to string
	END_IF
	stNavArrows.Internal.pToSourceString := ADR(stNavArrows.Internal.string1);		// get pointer to string

	memset(ADR(stNavArrows.Internal.menuLevels),0,SIZEOF(stNavArrows.Internal.menuLevels));	// set all menu levels to 0
	FOR i := 0 TO 3 BY 1 DO		// fill [USINT] array stNavArrows.Internal.menuLevels with actual position in HMI
		pLetter ACCESS stNavArrows.Internal.pToSourceString + i;	// access coresponding letter in string
		stNavArrows.Internal.menuLevels[i] 	:= pLetter - 48; 	// convert ascii to int - 48 is ascii value of '0'
		IF(stNavArrows.Internal.menuLevels[i] <> 0) THEN			// store currently active level
			stNavArrows.Internal.activeLevel		:= INT_TO_USINT(i);	
		END_IF	
	END_FOR


	// If navigation arrow ( <, >, ^, ) command is present -> get last defined page index
	//------------------------------------------------------------------
	IF(stNavArrows.In.pageNavigCmd <> 0) THEN 
		// calculate last possible page number in actual level
		FOR i:= 3 TO (3-USINT_TO_INT(stNavArrows.Internal.activeLevel) + 1) BY -1 DO 
			stNavArrows.Internal.lastPageInLevel := stNavArrows.Internal.lastPageInLevel + (stNavArrows.Internal.menuLevels[3-i] * REAL_TO_UINT(pow(10,i)));
		END_FOR
		stNavArrows.Internal.lastPageInLevel := stNavArrows.Internal.lastPageInLevel + (9 * REAL_TO_INT(pow(10,i)));
	
		WHILE (j > 0 ) DO // search for last defined page in actual level
			stNavArrows.Internal.lastPageInLevel := stNavArrows.Internal.lastPageInLevel - (REAL_TO_INT(pow(10,(3-stNavArrows.Internal.activeLevel))));
			FOR i:=0 TO PAGES_ARRAY_SIZE BY 1 DO	
				IF( stNavArrows.In.pageIndexes[i] = UINT_TO_INT(stNavArrows.Internal.lastPageInLevel)  ) THEN // page number is defined
					stNavArrows.Internal.pageExist := TRUE;
					i := PAGES_ARRAY_SIZE;	// stop for loop
				END_IF
			END_FOR
		
			IF(stNavArrows.Internal.pageExist = TRUE OR (j=1))	THEN	// stNavArrows.Internal.lastPageInLevel was found or lowest possible index is reached
				j := 0;	// stop while loop
			ELSE	// continou searching			
				j:=j-1;
			END_IF
		END_WHILE	// end search for last defined page
	END_IF

	
	// Execute navigation arrow commands
	//------------------------------------------------------------
	CASE stNavArrows.In.pageNavigCmd OF
	
		//--- 0 - no command active
		NAV_CMD_DO_NOTHING: 
		// do nothing - no cmd selected
	
		//--- 1 - cmd to move left in current level
		NAV_CMD_LEFT:
			IF(stNavArrows.Internal.menuLevels[stNavArrows.Internal.activeLevel] > 1) THEN	// curent page is not first in actual level
				stNavArrows.Internal.menuLevels[stNavArrows.Internal.activeLevel] := stNavArrows.Internal.menuLevels[stNavArrows.Internal.activeLevel] - 1;
				FOR i:= 3 TO 0 BY -1 DO // go through levels and create page number
					stNavArrows.Out.nextPage := stNavArrows.Out.nextPage + (stNavArrows.Internal.menuLevels[3-i] * REAL_TO_UINT(pow(10,i)));
				END_FOR
			ELSE	// current page is first in actual level
				stNavArrows.Out.nextPage := stNavArrows.Internal.lastPageInLevel;	// go to the last page in level
			END_IF
	
		//--- 2 - cmd to move right in current level
		NAV_CMD_RIGHT:
			IF(stNavArrows.In.actPage < UINT_TO_INT(stNavArrows.Internal.lastPageInLevel)) THEN // current page is not last defined in acutal level
				stNavArrows.Internal.menuLevels[stNavArrows.Internal.activeLevel] := stNavArrows.Internal.menuLevels[stNavArrows.Internal.activeLevel] + 1;
			ELSE		// curent page is last defined in act. level - move to first page in level
				stNavArrows.Internal.menuLevels[stNavArrows.Internal.activeLevel] := 1;
			END_IF
			// create page number
			FOR i:= 3 TO 0 BY -1 DO // go through levels and create page number
				stNavArrows.Out.nextPage := stNavArrows.Out.nextPage + (stNavArrows.Internal.menuLevels[3-i] * REAL_TO_UINT(pow(10,i)));
			END_FOR
	
		//--- 3 - cmd to move level up or home (arrow up)
		NAV_CMD_UP:
			IF(stNavArrows.Internal.activeLevel > 0) THEN // HMI is not showing highest level
				// Get index of last item in history buffer	
				i := 0; 
				WHILE (stNavArrows.In.pageHistoryBuffer[i] <> 0) DO
					i:= i + 1;	
				END_WHILE
				IF(i>=2)THEN	// Check if index is big enough (to avoid wrong mem. access)
					stNavArrows.Out.nextPage := stNavArrows.In.pageHistoryBuffer[i-2]; // go one level up in hist. buffer
				ELSE	// go to highes page in hist buffer
					stNavArrows.Out.nextPage := stNavArrows.In.pageHistoryBuffer[0]; // go one level up in hist. buffer
				END_IF
		
				// Obtain actual position in menu -> set menu levels	
				IF (stNavArrows.Out.nextPage < 999) THEN	// if page index is < 1000 add 0 in front of string
					itoa(stNavArrows.Out.nextPage, ADR(stNavArrows.Internal.string2));	// convert acutal page number to string
					strcpy(ADR(stNavArrows.Internal.string1),ADR('0'));		// add 0 to beginign of string
					strcat(ADR(stNavArrows.Internal.string1),ADR(stNavArrows.Internal.string2));	// conect with pate index number
				ELSE
					itoa(stNavArrows.Out.nextPage, ADR(stNavArrows.Internal.string1));	// convert acutal page number to string
				END_IF
				stNavArrows.Internal.pToSourceString := ADR(stNavArrows.Internal.string1);		// get pointer to string

				FOR i := 0 TO 3 BY 1 DO		// fill [USINT] array stNavArrows.Internal.menuLevels with actual menu "levels"
					pLetter ACCESS stNavArrows.Internal.pToSourceString + i;	// access coresponding letter in string
					stNavArrows.Internal.menuLevels[i] 	:= pLetter - 48; 	// convert ascii to int - 48 is ascii value of '0'
					IF(stNavArrows.Internal.menuLevels[i] <> 0) THEN			// store currently active level
						stNavArrows.Internal.activeLevel		:= INT_TO_USINT(i);	
					END_IF	
				END_FOR
			
			ELSE //HMI is in highest menu -> go to home screen
				stNavArrows.Out.nextPage := 1000;
			END_IF
				
	END_CASE


	// History buffer hadnling
	//----------------------------------------------		
	// Get index of last item in buffer	
	i := 0; 
	WHILE (stNavArrows.In.pageHistoryBuffer[i] <> 0 AND( i < SIZE_PAGE_HIST_BUFFER) ) DO	// page hist. buffer is not 0 and max. index is not reached
		i:= i + 1;
	END_WHILE
	stNavArrows.Internal.histBufLastIndex := INT_TO_USINT(i-1); // store index of last valid page in hist. buffer
	IF(stNavArrows.Internal.histBufLastIndex > SIZE_PAGE_HIST_BUFFER)THEN
		stNavArrows.Internal.histBufLastIndex := 0;
	END_IF

	//Update actual page if it was changed using navigation arrows
	IF(stNavArrows.Out.nextPage <> 0) THEN // change page using menu arrows
		stNavArrows.In.actPage := UINT_TO_INT(stNavArrows.Out.nextPage);
	END_IF

	// Execute hist buffer handling
	IF(stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex] <> stNavArrows.In.actPage OR stNavArrows.Out.nextPage <> 0) THEN
		// Create stNavArrows.Internal.stNavArrows.Internal.menuLevelsOld array
		IF (stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex] < 999) THEN	// if page index is < 999 add 0 in front of string
			itoa(stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex], ADR(stNavArrows.Internal.string2));	// last page in hist buffer number to string
			strcpy(ADR(stNavArrows.Internal.string1),ADR('0'));		// add 0 to beginign of string
			strcat(ADR(stNavArrows.Internal.string1),ADR(stNavArrows.Internal.string2));	// conect with page index number
		ELSE
			itoa(stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex], ADR(stNavArrows.Internal.string1));	// convert hist buffer page number to string
		END_IF
		stNavArrows.Internal.pToSourceString := ADR(stNavArrows.Internal.string1);		// get pointer to string

		FOR i := 0 TO 3 BY 1 DO		// fill [USINT] array stNavArrows.Internal.stNavArrows.Internal.menuLevelsOld with menu "levels" from hist buffer
			pLetter ACCESS stNavArrows.Internal.pToSourceString + i;	// access coresponding letter in string
			stNavArrows.Internal.menuLevelsOld[i] 	:= pLetter - 48; 	// convert ascii to int - 48 is ascii value of '0'
			IF(stNavArrows.Internal.menuLevelsOld[i] <> 0) THEN			// store currently active level
				stNavArrows.Internal.activeLevelOld		:= INT_TO_USINT(i);	
			END_IF	
		END_FOR
	
	
		//Update stNavArrows.In.pageHistoryBuffer
		IF 	(stNavArrows.Internal.menuLevels[0] = 0 AND stNavArrows.Internal.menuLevelsOld[0] = 0) OR		// no change from machine pages (>999) to common visu (<999)
			(stNavArrows.Internal.menuLevels[0] <> 0 AND stNavArrows.Internal.menuLevelsOld[0] <> 0) THEN
			
			IF(stNavArrows.Internal.activeLevel = stNavArrows.Internal.activeLevelOld) THEN 		// change page without level change
				stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex] := stNavArrows.In.actPage;

			ELSIF (stNavArrows.Internal.activeLevel > stNavArrows.Internal.activeLevelOld) THEN	// Change page with level down
				stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex+1] := stNavArrows.In.actPage;
		
			ELSIF (stNavArrows.Internal.activeLevel < stNavArrows.Internal.activeLevelOld) THEN	// Change page with level up
				stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex-1] := stNavArrows.In.actPage;
				stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex] := 0;	// reset lower menu index
			END_IF 
	
		ELSIF (stNavArrows.Internal.menuLevels[0] = 0 AND stNavArrows.Internal.menuLevelsOld[0] <> 0) THEN // Change from machine pages (>999) to common visu (<999)
			stNavArrows.In.pageHistoryBuffer[stNavArrows.Internal.histBufLastIndex+1] := stNavArrows.In.actPage;
	
		ELSIF (stNavArrows.Internal.menuLevels[0] <> 0 AND stNavArrows.Internal.menuLevelsOld[0] = 0) THEN // Change from common visu (<999) to machine pages (>999)
			// erase from histBuffer all pages lower than actual page
			i := SIZEOF(stNavArrows.In.pageHistoryBuffer)/SIZEOF(stNavArrows.In.pageHistoryBuffer[0]); // get number of elements in array
			i := i-1;	// get index of last element in array
			WHILE (i >= 0) DO
				IF(stNavArrows.In.pageHistoryBuffer[i] < stNavArrows.In.actPage)THEN
					stNavArrows.In.pageHistoryBuffer[i] := 0;
				END_IF
				i:= i - 1;	
			END_WHILE	
			// Set stNavArrows.In.pageHistoryBuffer [0] element to act. page in case that it is 0
			// Occuring in browsering pages like this: 1000->310->311->!2000!
			IF(stNavArrows.In.pageHistoryBuffer[0] = 0)THEN
				stNavArrows.In.pageHistoryBuffer[0] := stNavArrows.In.actPage;	
			END_IF						
		END_IF // update stNavArrows.In.pageHistoryBuffer
	END_IF	//-- End of History buffer handling
	
END_ACTION


ACTION HelpHandling:	
	// --------------------------------------
	// set the html string depending on the actual page & language
	// check for cmd
	IF stGlobalArea.bHelp THEN		
		memset(ADR(sTempString), 0, SIZEOF(sTempString)); // reset the temp buffer	
		memset(ADR(stGlobalArea.sHelpString), 0, SIZEOF(stGlobalArea.sHelpString)); // reset the old address
		// check the language to create the html address		
		IF gHMI.General.CurrentLanguage = 0 THEN // English
			sTempString := 'EN\';
		ELSIF gHMI.General.CurrentLanguage = 1 THEN // Turkish
			sTempString := 'TR\';
		ELSE // two languages are supported for now
			sTempString := 'EN\';
		END_IF; // end of the language check		
		// check for the actual page
		CASE gHMI.General.ActualPage OF
			PAGE_HOME:				
				strcat(ADR(sTempString), ADR('_1000_Home'));
			PAGE_MOTOR_SETUP:		
				strcat(ADR(sTempString), ADR('_322_MotorDiagnose'));
			ELSE
				strcat(ADR(sTempString), ADR('_000_HelpMain'));
		END_CASE; // end of actual page check		
		// add the footer
		strcat(ADR(sTempString), ADR('.htm'));
		// copy the buffer to the address
		stGlobalArea.sHelpString := sTempString;
		gHMI.General.ChangePage  := PAGE_HELP;
	END_IF; // end of cmd check
	stGlobalArea.bHelp := FALSE;
END_ACTION
