

//============================================================================
//					HANDLES MOTOR SETUP PAGES
//		* Tuning, Controller, Encoder, Limits, ParIDs
//		* Does the paramters handling and sets corresponding command to
//		  global area.
//		* Handles also layer management
//	
//============================================================================	


ACTION PageMotorSetup: 
	EncoderSetup;
	memset(ADR(PMotorSetup.LayerContWXGA.LockLayerBtn[0]), UNLOCK_OBJ, SIZEOF(PMotorSetup.LayerContWXGA.LockLayerBtn));
	

	
	
	IF mTemp.bFirstInStep THEN
		// hide all the layers first
		memset(ADR(PMotorSetup.LayerContWXGA.HideLayer), HIDE_OBJ, SIZEOF(PMotorSetup.LayerContWXGA.HideLayer));
		PMotorSetup.LayerContWXGA.HideLayer[1] 			:= DISPLAY_OBJ; // tuning layer is by default shown when the user enters the page for the first time
		PMotorSetup.LayerContWXGA.BtnLayerSelected[1] 	:= 1;
	END_IF;	

	// the user level is not admin, hide all the protected layers
	IF gHMI.General.ActUserLevel < USER_ADMIN THEN
		memset(ADR(PMotorSetup.LayerContWXGA.HideLayer), HIDE_OBJ, SIZEOF(PMotorSetup.LayerContWXGA.HideLayer));
		memset(ADR(PMotorSetup.LayerContWXGA.BtnLayerSelected), 0, SIZEOF(PMotorSetup.LayerContWXGA.BtnLayerSelected));
		PMotorSetup.LayerContWXGA.HideLayer[0] := DISPLAY_OBJ;
		memset(ADR(PMotorSetup.LayerContWXGA.LockLayerBtn[0]), LOCK_OBJ, SIZEOF(PMotorSetup.LayerContWXGA.LockLayerBtn));
	ELSE
		PMotorSetup.LayerContWXGA.HideLayer[0] := HIDE_OBJ;
	END_IF;
	
	
	
	
	// display the selected parameter layer
	FOR mTemp.ii := 0 TO (SIZEOF(PMotorSetup.LayerContWXGA.BtnLayerSelected) / SIZEOF(PMotorSetup.LayerContWXGA.BtnLayerSelected[0])-1) DO
		IF PMotorSetup.LayerContWXGA.BtnLayerSelected[mTemp.ii] = 1 AND PMotorSetup.LayerContWXGA.HideLayer[mTemp.ii] = HIDE_OBJ AND gHMI.General.ActUserLevel >= USER_ADMIN THEN
			IF mTemp.ii = 2 AND   PMotorSetup.LayerContWXGA.HideLayer[4] = DISPLAY_OBJ THEN 
			ELSE 
				
				PMotorSetup.LayerContWXGA.HideLayer[mTemp.ii] 		 := DISPLAY_OBJ;
				memset(ADR(PMotorSetup.LayerContWXGA.BtnLayerSelected[0]), 0, SIZEOF(PMotorSetup.LayerContWXGA.BtnLayerSelected));
				memset(ADR(PMotorSetup.LayerContWXGA.HideLayer[0]), HIDE_OBJ, SIZEOF(PMotorSetup.LayerContWXGA.HideLayer));
				PMotorSetup.LayerContWXGA.BtnLayerSelected[mTemp.ii] := 1;
				PMotorSetup.LayerContWXGA.HideLayer[mTemp.ii]		 := DISPLAY_OBJ;
				PMotorSetup.LayerContWXGA.LockLayerBtn[mTemp.ii]	 := LOCK_OBJ;
			END_IF;
			
		END_IF;
	END_FOR;
	
	IF ((PMotorSetup.LayerContWXGA.HideLayer[2] = DISPLAY_OBJ) AND  (PMotorSetup.LayerContWXGA.BtnLayerSelected[2]= 1 )) THEN 
		IF PMotorSetup.Button.btnRight THEN 
			PMotorSetup.LayerContWXGA.HideLayer[2] := HIDE_OBJ;
			PMotorSetup.LayerContWXGA.HideLayer[4] := DISPLAY_OBJ;
			PMotorSetup.Button.btnRight := FALSE ;
		END_IF;
	ELSIF ((PMotorSetup.LayerContWXGA.HideLayer[2] = HIDE_OBJ) AND  (PMotorSetup.LayerContWXGA.BtnLayerSelected[2] = 1 )) THEN 
		IF PMotorSetup.Button.btnLeft THEN 
			PMotorSetup.LayerContWXGA.HideLayer[4] := HIDE_OBJ;
			PMotorSetup.LayerContWXGA.HideLayer[2] := DISPLAY_OBJ;
			PMotorSetup.Button.btnLeft := FALSE ;
		END_IF;
	ELSE
		PMotorSetup.LayerContWXGA.HideLayer[4] := HIDE_OBJ;
		PMotorSetup.LayerContWXGA.HideLayer[2] := HIDE_OBJ;	
	END_IF; 
	
	
	
	
	
	// navigation through axes
	IF PMotorSetup.AxisNavigation.BtnAxisPlus AND PMotorSetup.Index.SelectedAxis <= (FIRST_VIRT_AX_IND - 2) THEN
		PMotorSetup.AxisNavigation.BtnAxisPlus := FALSE;
		PMotorSetup.Index.SelectedAxis := PMotorSetup.Index.SelectedAxis + 1;
		
	ELSIF PMotorSetup.AxisNavigation.BtnAxisPlus AND PMotorSetup.Index.SelectedAxis = (FIRST_VIRT_AX_IND - 1) THEN
		PMotorSetup.AxisNavigation.BtnAxisPlus := FALSE;
		PMotorSetup.Index.SelectedAxis := FIRST_REAL_AX_IND;
		
	ELSIF PMotorSetup.AxisNavigation.BtnAxisMinus AND PMotorSetup.Index.SelectedAxis >= (FIRST_REAL_AX_IND + 1) THEN
		PMotorSetup.AxisNavigation.BtnAxisMinus := FALSE;
		PMotorSetup.Index.SelectedAxis := PMotorSetup.Index.SelectedAxis - 1;
		
	ELSIF PMotorSetup.AxisNavigation.BtnAxisMinus AND PMotorSetup.Index.SelectedAxis = FIRST_REAL_AX_IND THEN
		PMotorSetup.AxisNavigation.BtnAxisMinus := FALSE;
		PMotorSetup.Index.SelectedAxis := FIRST_VIRT_AX_IND - 1;
		
	ELSE
		PMotorSetup.AxisNavigation.BtnAxisMinus := FALSE;
		PMotorSetup.AxisNavigation.BtnAxisPlus  := FALSE;
	END_IF;
	
	

		
	IF PMotorSetup.Index.OldSelectedAxis <> PMotorSetup.Index.SelectedAxis  OR firstInStep THEN
		PMotorSetup.Enable   := gURecData.AxEnabled[PMotorSetup.Index.SelectedAxis];
	ELSE 	
		IF PMotorSetup.Enable THEN 
			gURecData.AxEnabled[PMotorSetup.Index.SelectedAxis] := PMotorSetup.Enable ;
		ELSE 
			gMpAxisBasic_0[PMotorSetup.Index.SelectedAxis].Power := FALSE ;
			IF NOT gMpAxisBasic_0[PMotorSetup.Index.SelectedAxis].PowerOn THEN 
				gURecData.AxEnabled[PMotorSetup.Index.SelectedAxis]  := PMotorSetup.Enable ;
				gMpAxisBasic_0[PMotorSetup.Index.SelectedAxis].Enable := gURecData.AxEnabled[PMotorSetup.Index.SelectedAxis];
			END_IF ;
		END_IF ;
	END_IF ;

	
	PMotorSetup.Index.OldSelectedAxis					    := PMotorSetup.Index.SelectedAxis;
	
	//============================================================================
	//				FIRST IN PAGE
	//============================================================================	
	IF firstInStep THEN		
		
		(* Load Actual Axis configuration and sets the flag that it is in motor setup page*)		
		PMotorSetup.InitConfig := TRUE;				
		

	END_IF
	//============================================================================		
		
		
	
	//============================================================================
	//							AXIS INDEX LIMITATION
	//			* Limitting of maximum index to real axis no
	//			* Limitting of also selected index 0 to max real axis
	//============================================================================	
	PMotorSetup.Index.MaxAxis 	   := MAX_AX_TOT_MINUS_ONE;
	PMotorSetup.Index.SelectedAxis := LIMIT(0,PMotorSetup.Index.SelectedAxis,PMotorSetup.Index.MaxAxis);
	k							   := PMotorSetup.Index.SelectedAxis;	
	//============================================================================		
	
	
	
	//============================================================================
	//								BUTTON LOCKING
	//			* Unlock all buttons first, then checks if the actual page is 
	//			one of the motor setup pages. if so, locks that button
	//
	//			* 
	//============================================================================	
	(* Lock -> All page buttons first, then unlock only the selected one *)
	memset(ADR(PMotorSetup.Lock),0,SIZEOF(PMotorSetup.Lock));
	


	
		
	//============================================================================
	//						MOTOR SETUP PAGE PROCESS
	//	* Once one of any motor setup page is opened, It loads actual value of
	//	  drive&axis to an internal structure	
	//	* Any change can be done over HMI. if change is done without any problem
	//	  it updates also actual values.
	//	* Save can be used to make new variables permanent.
	//	* Below, It just sets some variables to the AxisHandle and checks for status
	//============================================================================	
	
	//============================================================================
	//		LOAD REQUEST TO AXIS HANDLE - LOADS ACTUAL VALUES FROM DRIVE
	//============================================================================	

	IF firstInStep  OR  PMotorSetup.Button.LoadFromMachine THEN	
		FOR i := 0 TO MAX_AX_TOT_MINUS_ONE DO 
			PMotorSetup.Par.Actual[i].Encoder.Direction								:= gUFixData.Ax[i].Cfg.Axis.Drive.Gearbox.Direction;		
			PMotorSetup.Par.Actual[i].Encoder.GearboxIn								:= gUFixData.Ax[i].Cfg.Axis.Drive.Gearbox.Input;
			PMotorSetup.Par.Actual[i].Encoder.GearboxOut							:= gUFixData.Ax[i].Cfg.Axis.Drive.Gearbox.Output;
			PMotorSetup.Par.Actual[i].Encoder.LoadUnitPerRev						:= gUFixData.Ax[i].Cfg.Axis.Drive.Transformation.ReferenceDistance;
			PMotorSetup.Par.Actual[i].Encoder.Period								:= gUFixData.Ax[i].Cfg.Axis.Axis.PeriodSettings.Period;
			
			PMotorSetup.Par.Actual[i].Limit.Movement.JoltTime						:= gUFixData.Ax[i].Cfg.Axis.Axis.MovementLimits.JerkTime;
			PMotorSetup.Par.Actual[i].Limit.Movement.LagError						:= LREAL_TO_REAL(gUFixData.Ax[i].Cfg.Axis.Axis.MovementLimits.PositionErrorStopLimit);
			
			PMotorSetup.Par.Actual[i].Limit.SoftwareEnd.NegLimit					:= gUFixData.Ax[i].Cfg.Axis.Axis.SoftwareLimitPositions.LowerLimit;
			PMotorSetup.Par.Actual[i].Limit.SoftwareEnd.PosLimit					:= gUFixData.Ax[i].Cfg.Axis.Axis.SoftwareLimitPositions.UpperLimit;
			
			PMotorSetup.Par.Actual[i].Limit.Velocity.Acceleration					:= gUFixData.Ax[i].Cfg.Axis.Axis.MovementLimits.Acceleration;
			PMotorSetup.Par.Actual[i].Limit.Velocity.Deceleration					:= gUFixData.Ax[i].Cfg.Axis.Axis.MovementLimits.Deceleration;
			PMotorSetup.Par.Actual[i].Limit.Velocity.VelocityNeg					:= gUFixData.Ax[i].Cfg.Axis.Axis.MovementLimits.VelocityNegative;
			PMotorSetup.Par.Actual[i].Limit.Velocity.VelocityPos					:= gUFixData.Ax[i].Cfg.Axis.Axis.MovementLimits.VelocityPositive;
			
			PMotorSetup.Par.Actual[i].Controller.Mode								:= gUFixData.Ax[i].Cfg.Axis.Drive.Controller.Mode;
			PMotorSetup.Par.Actual[i].Controller.FF									:= gUFixData.Ax[i].Cfg.Axis.Drive.Controller.FeedForward;
			PMotorSetup.Par.Actual[i].Controller.Position							:= gUFixData.Ax[i].Cfg.Axis.Drive.Controller.Position;
			PMotorSetup.Par.Actual[i].Controller.Speed								:= gUFixData.Ax[i].Cfg.Axis.Drive.Controller.Speed;
   
			PMotorSetup.Par.Actual[i].Controller.LoopFilter.FilterType				:= gUFixData.Ax[i].Cfg.Axis.Drive.Controller.LoopFilters[0].FilterType ;
			PMotorSetup.Par.Actual[i].Controller.LoopFilter.Notch.Bandwidth 		:= gUFixData.Ax[i].Cfg.Axis.Drive.Controller.LoopFilters[0].Notch.Bandwidth;
			PMotorSetup.Par.Actual[i].Controller.LoopFilter.Notch.CenterFrequency 	:= gUFixData.Ax[i].Cfg.Axis.Drive.Controller.LoopFilters[0].Notch.CenterFrequency;
			(* Reset Load flag meanwhile *)
			PMotorSetup.Button.LoadFromMachine := FALSE;
		END_FOR
		
		memcpy(ADR(PMotorSetup.Par.New),ADR(PMotorSetup.Par.Actual),SIZEOF(PMotorSetup.Par.Actual));
		PMotorSetup.InitConfig	:= FALSE;
	END_IF
	//============================================================================	
	
	
	
		
	//============================================================================
	//						ACCESS SELECTED AXIS DATA
	//============================================================================	
	IF ADR(PMotorSetup.Par.Actual[k]) <> 0 THEN
		ptrMotorSetupAct ACCESS ADR(PMotorSetup.Par.Actual[k]);	
	END_IF	
	
	IF ADR(PMotorSetup.Par.New[k]) <> 0 THEN
		ptrMotorSetupNew ACCESS ADR(PMotorSetup.Par.New[k]);
	END_IF
	//============================================================================		
	
	IF EDGEPOS(gURecData.AxEnabled[k]) THEN 
		gUHandle.Unit.Status.AfterEnabledAxis             := TRUE;
		gUHandle.Unit.Cmd.InitilizeAxis                       := TRUE;
	
	END_IF ; 
	IF gMpAxisBasicConfig_0[k].CommandDone  THEN
		gUHandle.Unit.Cmd.InitilizeAxis                    	  := FALSE;
	END_IF ;
	
		
	//============================================================================			
	(* Saving values to the permanent area so that they can be used all time  *)
	IF PMotorSetup.Button.SaveToMachine THEN
		PMotorSetup.Button.SaveToMachine := FALSE;
		gUFixData.Ax[k].Cfg.Axis.Drive.Gearbox.Direction					  := PMotorSetup.Par.New[k].Encoder.Direction;
		gUFixData.Ax[k].Cfg.Axis.Drive.Gearbox.Input						  := PMotorSetup.Par.New[k].Encoder.GearboxIn;
		gUFixData.Ax[k].Cfg.Axis.Drive.Gearbox.Output						  := PMotorSetup.Par.New[k].Encoder.GearboxOut;
		gUFixData.Ax[k].Cfg.Axis.Axis.PeriodSettings.Period				  	  := PMotorSetup.Par.New[k].Encoder.Period;
		gUFixData.Ax[k].Cfg.Axis.Drive.Transformation.ReferenceDistance	  	  := PMotorSetup.Par.New[k].Encoder.LoadUnitPerRev;
		 
		gUFixData.Ax[k].Cfg.Axis.Axis.MovementLimits.JerkTime				  := PMotorSetup.Par.New[k].Limit.Movement.JoltTime;
		gUFixData.Ax[k].Cfg.Axis.Axis.MovementLimits.PositionErrorStopLimit   := PMotorSetup.Par.New[k].Limit.Movement.LagError;		
		
		gUFixData.Ax[k].Cfg.Axis.Axis.SoftwareLimitPositions.LowerLimit	  	  := PMotorSetup.Par.New[k].Limit.SoftwareEnd.NegLimit;
		gUFixData.Ax[k].Cfg.Axis.Axis.SoftwareLimitPositions.UpperLimit	  	  := PMotorSetup.Par.New[k].Limit.SoftwareEnd.PosLimit;
		
		gUFixData.Ax[k].Cfg.Axis.Axis.MovementLimits.Acceleration			  := PMotorSetup.Par.New[k].Limit.Velocity.Acceleration;
		gUFixData.Ax[k].Cfg.Axis.Axis.MovementLimits.Deceleration			  := PMotorSetup.Par.New[k].Limit.Velocity.Deceleration;
		gUFixData.Ax[k].Cfg.Axis.Axis.MovementLimits.VelocityNegative		  := PMotorSetup.Par.New[k].Limit.Velocity.VelocityNeg;
		gUFixData.Ax[k].Cfg.Axis.Axis.MovementLimits.VelocityPositive		  := PMotorSetup.Par.New[k].Limit.Velocity.VelocityPos;
		
		gUFixData.Ax[k].Cfg.Axis.Drive.Controller.Mode					  	  := PMotorSetup.Par.New[k].Controller.Mode;
		gUFixData.Ax[k].Cfg.Axis.Drive.Controller.FeedForward				  := PMotorSetup.Par.New[k].Controller.FF;
		gUFixData.Ax[k].Cfg.Axis.Drive.Controller.Position				 	  := PMotorSetup.Par.New[k].Controller.Position;
		gUFixData.Ax[k].Cfg.Axis.Drive.Controller.Speed					  	  := PMotorSetup.Par.New[k].Controller.Speed;
		
		
		gUFixData.Ax[k].Cfg.Axis.Drive.Controller.LoopFilters[i].FilterType   := PMotorSetup.Par.New[i].Controller.LoopFilter.FilterType;
		gUFixData.Ax[k].Cfg.Axis.Drive.Controller.LoopFilters[i].Notch.Bandwidth		 := PMotorSetup.Par.Actual[i].Controller.LoopFilter.Notch.Bandwidth;
		gUFixData.Ax[k].Cfg.Axis.Drive.Controller.LoopFilters[i].Notch.CenterFrequency	 := PMotorSetup.Par.Actual[i].Controller.LoopFilter.Notch.CenterFrequency;
		
		gUHandle.Unit.Cmd.InitilizeAxis                                  	  := TRUE ;
	

	END_IF	
	//============================================================================		
	(* if saving is done, update also actual ones *)
	IF EDGEPOS(gUHandle.Unit.Status.Initialized) THEN 
		memcpy(ADR(PMotorSetup.Par.Actual[k]),ADR(PMotorSetup.Par.New[k]),SIZEOF(PMotorSetup.Par.New[k]));
	END_IF
	

	//============================================================================
	//						TUNING PROCESS
	//============================================================================	
	IF PMotorSetup.Button.TuningStart THEN
		gMpAxisBasicPar[k].Autotune.Mode 							:= ptrMotorSetupNew.Tuning.Mode;
		gMpAxisBasicPar[k].Autotune.Vertical 						:= ptrMotorSetupNew.Tuning.Vertical;
		gMpAxisBasicPar[k].Autotune.MaxDistance 					:= ptrMotorSetupNew.Tuning.MaxDistance;
		gMpAxisBasicPar[k].Autotune.MaxSpeedPercent 				:= ptrMotorSetupNew.Tuning.MaxSpeed;
		gMpAxisBasicPar[k].Autotune.MaxCurrentPercent 				:= ptrMotorSetupNew.Tuning.MaxCurrent;
		gMpAxisBasicPar[k].Autotune.ProportionalAmplification 		:= ptrMotorSetupNew.Tuning.MaxKv;
		
		(* Start Tuning Process by sending command to AxisHandle *)
		gMpAxisBasic_0[k].Autotune    := TRUE ;
		
		
		(* Check if it is done *)
			
		IF gMpAxisBasic_0[k].TuningDone THEN
			gMpAxisBasicConfig_0[k].Load := TRUE ;
			IF gMpAxisBasicConfig_0[k].CommandDone THEN 
				gUFixData.Ax[k].Cfg.Axis.Drive.Controller.Position 	  	:= gMpAxisBasicCfgType[k].Drive.Controller.Position;
				PMotorSetup.Par.Actual[k].Controller.Position     		:= gMpAxisBasicCfgType[k].Drive.Controller.Position;
				PMotorSetup.Par.New[k].Controller.Position     	  		:= gMpAxisBasicCfgType[k].Drive.Controller.Position;
				gUFixData.Ax[k].Cfg.Axis.Drive.Controller.Speed    	  	:= gMpAxisBasicCfgType[k].Drive.Controller.Speed;
				PMotorSetup.Par.Actual[k].Controller.Speed     	  		:= gMpAxisBasicCfgType[k].Drive.Controller.Speed;
				PMotorSetup.Par.New[k].Controller.Speed     	  		:= gMpAxisBasicCfgType[k].Drive.Controller.Speed;
				gUFixData.Ax[k].Cfg.Axis.Drive.Controller.FeedForward 	:= gMpAxisBasicCfgType[k].Drive.Controller.FeedForward;
				PMotorSetup.Par.Actual[k].Controller.FF     	  		:= gMpAxisBasicCfgType[k].Drive.Controller.FeedForward;
				PMotorSetup.Par.New[k].Controller.FF     	  	 	 	:= gMpAxisBasicCfgType[k].Drive.Controller.FeedForward;
				gMpAxisBasicConfig_0[k].Load := FALSE ;
				PMotorSetup.Button.TuningStart	  := FALSE;
				PMotorSetup.Button.TuningStop	  := FALSE;
				gMpAxisBasic_0[k].Autotune    	  := FALSE ;
			END_IF ; 
		END_IF
	END_IF	
		
	//============================================================================		
	

	
	

	
	

	
	

END_ACTION




ACTION EncoderSetup:
	// ----------------------------
	// btn locking handling 
	// ----------------------------
	gHMI.DrvPar.SelectedAxIdx  := PMotorSetup.Index.SelectedAxis;
	memset(ADR(PMotorSetup.EncSetings.LockBtns), LOCK_OBJ, SIZEOF(PMotorSetup.EncSetings.LockBtns));
	mTemp.ii := gHMI.DrvPar.SelectedAxIdx;
	// check if the selected axis is equipped with a multiturn encoder
	IF gUFixData.Ax[ mTemp.ii ].Cfg.MultiturnEnc THEN
		// ----------------------------
		// jog buttons
		// ----------------------------
		IF gMpAxisBasic_0[ mTemp.ii ].IsHomed AND 
			(gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_STANDSTILL OR 
			gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_DISABLED	 OR
			gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_DISCRETE_MOTION) THEN
			PMotorSetup.EncSetings.LockBtns.btnJogNeg := UNLOCK_OBJ;
			PMotorSetup.EncSetings.LockBtns.btnJogPos := UNLOCK_OBJ;
		END_IF;		
		// ----------------------------
		// home cmd buttons
		// ----------------------------
		IF gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_STANDSTILL OR gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_DISABLED THEN
			PMotorSetup.EncSetings.LockBtns.btnHomeToLimSwitch 	:= UNLOCK_OBJ;
			PMotorSetup.EncSetings.LockBtns.btnLoadFromRecipe  	:= UNLOCK_OBJ;
			PMotorSetup.EncSetings.LockBtns.btnSetPosition	 	:= UNLOCK_OBJ;
		END_IF;		
	END_IF; // end of the multiturn encoder check	
	
	// ----------------------------
	// encoder settings & cmd's mapping
	// ----------------------------
	IF EDGEPOS(PMotorSetup.EncSetings.CmdHomeLimSwitch) THEN
		PMotorSetup.EncSetings.CmdHomeLimSwitch 		:= FALSE;
		gUHandle.Unit.Cmd.MultiturnEncHomeLimSw         := TRUE;
		gUHandle.Settings.MultiEncSettings.LimitSwPos 	:= PMotorSetup.EncSetings.HomingPar.LimitSwitchPos;
		gUHandle.Settings.MultiEncSettings.HomingSpeed	:= PMotorSetup.EncSetings.HomingPar.HomingSpeedSwitch;
		gUHandle.Settings.MultiEncSettings.TriggerSpeed	:= PMotorSetup.EncSetings.HomingPar.HomingSpeedTrigger;
	END_IF;
	IF EDGEPOS(PMotorSetup.EncSetings.CmdSetPosition) THEN
		PMotorSetup.EncSetings.CmdSetPosition 			:= FALSE;
		gUHandle.Unit.Cmd.SetMultiturnEncoder			:= TRUE;
		gUHandle.Settings.MultiEncSettings.SetPosition 	:= PMotorSetup.EncSetings.SetPosition;
	END_IF;
	IF EDGEPOS(PMotorSetup.EncSetings.CmdHomeSetOffset) THEN
		PMotorSetup.EncSetings.CmdHomeSetOffset			:= FALSE;
		gUHandle.Unit.Cmd.MultiturnEncHomeOffsetValue	:= TRUE;
	END_IF;
	
	IF gUFixData.Ax[ mTemp.ii ].Cfg.MultiturnEnc THEN
		gUHandle.Unit.Cmd.Manual.JogPos[ mTemp.ii ]		:= PMotorSetup.EncSetings.CmdJogPos;
		gUHandle.Unit.Cmd.Manual.JogNeg[ mTemp.ii ]		:= PMotorSetup.EncSetings.CmdJogNeg;
		gMpAxisBasicPar[ mTemp.ii ].Jog.Velocity			:= PMotorSetup.EncSetings.HomingPar.HomingSpeedTrigger;
		gMpAxisBasicPar[ mTemp.ii ].Jog.Acceleration		:= PMotorSetup.EncSetings.HomingPar.HomingSpeedTrigger * 4;
		gMpAxisBasicPar[ mTemp.ii ].Jog.Deceleration	    := PMotorSetup.EncSetings.HomingPar.HomingSpeedTrigger * 4;
		PMotorSetup.EncSetings.ActualEncoderPosition		:= LREAL_TO_REAL(gMpAxisBasic_0[ mTemp.ii ].Position);
	ELSE
		gUHandle.Unit.Cmd.Manual.JogPos[ mTemp.ii ]		:= 0;
		gUHandle.Unit.Cmd.Manual.JogNeg[ mTemp.ii ]		:= 0;
		PMotorSetup.EncSetings.ActualEncoderPosition		:= 0;
	END_IF;	
	PMotorSetup.EncSetings.HomingOk						:= gMpAxisBasic_0[ mTemp.ii ].IsHomed;
END_ACTION