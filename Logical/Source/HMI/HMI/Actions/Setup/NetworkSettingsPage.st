(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: NetworkSettingsPage.st
 * Author: turanskyl
 * Created: April 11, 2014
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 

//**************************************************
//**** Network settings ****************************
//**************************************************

ACTION NetworkSettingsPage:
	//IP settings control
	//---------------------------------------------------------------------
//	IF(gHMI.General.ActResolution = QXGA)THEN	// Assign correct Ethernet device name
//		strcpy(ADR(stNetworkSettings.Device), ADR('IF5'));	// PP65
//	ELSE
//		strcpy(ADR(stNetworkSettings.Device), ADR('IF2')); // X20 CPU
//	END_IF
//	
	strcpy(ADR(stNetworkSettings.Device), ADR('IF2')); // X20 CPU
	//Set default IP configuration if the E-Stop is pressed together with the Reset button during the time defined by C_TIMER_DEFAULT_IP
//	IF (gIO.In.General.E_Stop = TRUE) AND (gIO.In.General.btnResetErr = TRUE) THEN
//		//Set timer
//		stNetworkSettings.TON_DefaultIPRest.IN	:= TRUE;
//		//Wait until it is finished
//		IF (stNetworkSettings.TON_DefaultIPRest.Q = TRUE) THEN
//			//Reset timer
//			stNetworkSettings.TON_DefaultIPRest.IN	:= FALSE;
//			//Set command to restore default IP configuration
//			stNetworkSettings.btnSetDefIP := TRUE;
//			stNetworkSettings.btnSetNewIP := FALSE;
//		END_IF
//	ELSE
//		//Reset timer
//		stNetworkSettings.TON_DefaultIPRest.IN		:= FALSE;
//	END_IF	
	
	//Execute timer for setting the command to set default IP configuration
	stNetworkSettings.TON_DefaultIPRest.PT := TIME_DEFAULT_IP_RESTORE;
	stNetworkSettings.TON_DefaultIPRest();
	
	//Get actual IP address
	IF (stNetworkSettings.btnSetNewIP = FALSE) AND (stNetworkSettings.btnSetDefIP = FALSE) AND
		(stNetworkSettings.ActualIPAddress = '') AND (stNetworkSettings.ActualSubnetMask = '') THEN
		stNetworkSettings.CfgGetIPAddr_0(enable := TRUE, pDevice := ADR(stNetworkSettings.Device), pIPAddr := ADR(stNetworkSettings.ActualIPAddress), Len := SIZEOF(stNetworkSettings.ActualIPAddress));
		stNetworkSettings.CfgGetSubnetMask_0(enable := TRUE, pDevice := ADR(stNetworkSettings.Device), pSubnetMask := ADR(stNetworkSettings.ActualSubnetMask), Len := SIZEOF(stNetworkSettings.ActualSubnetMask));
	END_IF	

	//Copy actual settings to new settings if they are empty
	IF (stNetworkSettings.NewIPAddress = '') THEN
		stNetworkSettings.NewIPAddress	:=stNetworkSettings.ActualIPAddress;
	END_IF	
	IF (stNetworkSettings.NewSubnetMask = '') THEN
		stNetworkSettings.NewSubnetMask	:= stNetworkSettings.ActualSubnetMask;
	END_IF	
	
	
	
	//Set new IP address
	IF (stNetworkSettings.btnSetNewIP = TRUE) AND (stNetworkSettings.NewIPAddress <> '') AND (stNetworkSettings.btnSetDefIP = FALSE) THEN
		//Set IP address
		stNetworkSettings.CfgSetIPAddr_0(enable := TRUE,pDevice := ADR(stNetworkSettings.Device), pIPAddr := ADR(stNetworkSettings.NewIPAddress), Option := cfgOPTION_NON_VOLATILE);
		//Set subnet mask
		stNetworkSettings.CfgSetSubnetMask_0(enable := TRUE, pDevice := ADR(stNetworkSettings.Device), pSubnetMask := ADR(stNetworkSettings.NewSubnetMask), Option := cfgOPTION_NON_VOLATILE);
		IF (stNetworkSettings.CfgSetIPAddr_0.status = ERR_OK) AND (stNetworkSettings.CfgSetSubnetMask_0.status = ERR_OK) THEN
			//IP address changed successfully, reset command
			stNetworkSettings.btnSetNewIP := FALSE;
			//Reset actual address
			stNetworkSettings.ActualIPAddress 	:= '';
			stNetworkSettings.ActualSubnetMask	:= '';
		ELSIF ( (stNetworkSettings.CfgSetIPAddr_0.status >= cfgERR_DEVICE_NOT_EXIST) AND (stNetworkSettings.CfgSetIPAddr_0.status <= cfgERR_SYSTEM) ) OR 
			( (stNetworkSettings.CfgSetSubnetMask_0.status >= cfgERR_DEVICE_NOT_EXIST) AND (stNetworkSettings.CfgSetSubnetMask_0.status <= cfgERR_SYSTEM) )  THEN	
			//Error in one of the FUBs, reset command
			stNetworkSettings.btnSetNewIP := FALSE;
		END_IF	
	END_IF	
	
	//Set default IP address
	IF (stNetworkSettings.btnSetDefIP = TRUE) AND (stNetworkSettings.btnSetNewIP = FALSE) THEN
		//Set IP address
		stNetworkSettings.CfgSetIPAddr_1(enable := TRUE, pDevice := ADR(stNetworkSettings.Device), pIPAddr := ADR(C_DEFAULT_IP), Option := cfgOPTION_NON_VOLATILE);
		//Set subnet mask
		stNetworkSettings.CfgSetSubnetMask_1(enable := TRUE, pDevice := ADR(stNetworkSettings.Device), pSubnetMask := ADR(C_DEFAULT_SUBNET_MASK), Option := cfgOPTION_NON_VOLATILE);		
		IF (stNetworkSettings.CfgSetIPAddr_1.status = ERR_OK) AND (stNetworkSettings.CfgSetSubnetMask_1.status = ERR_OK)THEN
			//IP address changed successfully, reset command
			stNetworkSettings.btnSetDefIP := FALSE;
			//Reset actual address
			stNetworkSettings.ActualIPAddress 	:= '';
			stNetworkSettings.ActualSubnetMask	:= '';
		ELSIF ( (stNetworkSettings.CfgSetIPAddr_1.status >= cfgERR_DEVICE_NOT_EXIST) AND (stNetworkSettings.CfgSetIPAddr_1.status <= cfgERR_SYSTEM) ) OR 
			( (stNetworkSettings.CfgSetSubnetMask_1.status >= cfgERR_DEVICE_NOT_EXIST) AND (stNetworkSettings.CfgSetSubnetMask_1.status <= cfgERR_SYSTEM) )  THEN	
			//Error in one of the FUBs, reset command
			stNetworkSettings.btnSetDefIP := FALSE;			
		END_IF	
	END_IF	

END_ACTION