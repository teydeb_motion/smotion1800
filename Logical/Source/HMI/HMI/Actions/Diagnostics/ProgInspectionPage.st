(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: ProgInspectionPage.st
 * Author: turanskyl
 * Created: July 10, 2014
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 

(* Program Inspection Page handling *)
ACTION ProgInspectionPage: 
	
	//----------------------------------------------
	// First time on page
	//----------------------------------------------
	IF(firstInStep = TRUE)THEN // first time on the page
		memset(ADR(stProgInspect.Data), 0, SIZEOF(stProgInspect.Data));	// Reset data
		memset(ADR(stProgInspect.Cmd), 0, SIZEOF(stProgInspect.Cmd));	// Reset commands
		memset(ADR(stProgInspect.Hide.ProgramLine), 1, SIZEOF(stProgInspect.Hide.ProgramLine));	// Hide first all program lines
		stProgInspect.Hide.HistLayer := HIDE_OBJ; 	// Hide hist. layer
		stProgInspect.StartIndex     := 0; 		// Start with data with zero index
		
		//---------------------------------------------------------
		// Get index of last defined element in gHMI.ProgInspect
		stProgInspect.ArraySizeGlobal := UDINT_TO_UINT(SIZEOF(gHMI.ProgInspect) / SIZEOF(gHMI.ProgInspect[0]) - 1);
		//Search for index of last valid data
		FOR stProgInspect.i := 0 TO stProgInspect.ArraySizeGlobal BY 1 DO 
			IF( strcmp(ADR(gHMI.ProgInspect[stProgInspect.i].ProgName),ADR('')) <= 0)THEN	// No program name - end of used data
				IF(stProgInspect.i > 0)THEN	// Avoid writing negative index
					stProgInspect.ArraySizeGlobal := stProgInspect.i - 1;
				ELSE
					stProgInspect.ArraySizeGlobal := 0;
				END_IF
				EXIT; // Exit for loop
			END_IF
		END_FOR
		
		//----------------------------------------------------------
		// Get index OF last defined element in stProgInspect.Data
		stProgInspect.ArraySizeLocal  := UDINT_TO_UINT(SIZEOF(stProgInspect.Data) / SIZEOF(stProgInspect.Data[0]) - 1);
		
		//Array size local is bigger than valid data items in global var.
		IF(stProgInspect.ArraySizeLocal >= stProgInspect.ArraySizeGlobal)THEN
			stProgInspect.Hide.NavArrow 	:= HIDE_OBJ;	// Hide navigation arrows
			stProgInspect.ArraySizeLocal 	:= stProgInspect.ArraySizeGlobal; // Adjust local data array size (remaining indexes will not be used)
		ELSE	// More global valid data than defined local data -> show navigation arrows
			stProgInspect.Hide.NavArrow := SHOW_OBJ;
		END_IF
		
		
	END_IF // First in step
	
	
	
	//---------------------------------------------------
	// Cyclic actions
	//---------------------------------------------------
	
	// Assign Global data to Local variables -> Display them in HMI lines
	FOR stProgInspect.i := 0 TO stProgInspect.ArraySizeLocal BY 1 DO
		stProgInspect.Data[stProgInspect.i] := gHMI.ProgInspect[stProgInspect.i + stProgInspect.StartIndex]; // Copy data
		stProgInspect.Hide.ProgramLine[stProgInspect.i] := SHOW_OBJ; // Display lines
	END_FOR
	
	//---------------------------------------
	// Navigation through lines
	stProgInspect.Lock.IndexMinus :=  NOT(stProgInspect.StartIndex >= 1);
	stProgInspect.Lock.IndexPlus  :=  NOT((stProgInspect.StartIndex + stProgInspect.ArraySizeLocal) < stProgInspect.ArraySizeGlobal);
	
	// Move one line down
	IF(stProgInspect.Cmd.IndexPlus AND (stProgInspect.StartIndex + stProgInspect.ArraySizeLocal) < stProgInspect.ArraySizeGlobal) THEN
		stProgInspect.StartIndex := stProgInspect.StartIndex + 1;
	END_IF	
	// Move one line up
	IF(stProgInspect.Cmd.IndexMinus AND stProgInspect.StartIndex >= 1)THEN
		stProgInspect.StartIndex := stProgInspect.StartIndex - 1;
	END_IF
	stProgInspect.Cmd.IndexMinus 	:= FALSE;	// Reset commands
	stProgInspect.Cmd.IndexPlus		:= FALSE;
	
	//------------------------------------------
	// History layer
	IF(stProgInspect.Hide.HistLayer = SHOW_OBJ )THEN // history layer is shown
		IF( (stProgInspect.Data[stProgInspect.HistSelectedLine].MainHist[0].Time <> stProgInspect.Hist.Data.MainHist[0].Time OR
			stProgInspect.Data[stProgInspect.HistSelectedLine].LocHist[0].Time <> stProgInspect.Hist.Data.LocHist[0].Time OR
			stProgInspect.Hist.FirstTime = TRUE) AND stProgInspect.Hist.FreezeValues = FALSE )THEN // Data has changed - update
			stProgInspect.Hist.Data 		:=	stProgInspect.Data[stProgInspect.HistSelectedLine];	// Update hist data - for change detection purpose
			stProgInspect.Hist.FirstTime 	:= FALSE;
			IF( (SIZEOF(stProgInspect.Hist.TimeLoc)/SIZEOF(stProgInspect.Hist.TimeLoc[0]) - 1) = (SIZEOF(stProgInspect.Data[0].LocHist )/SIZEOF(stProgInspect.Data[0].LocHist[0])-1))THEN
				FOR stProgInspect.i := 0 TO (SIZEOF(stProgInspect.Hist.TimeLoc)/SIZEOF(stProgInspect.Hist.TimeLoc[0]) - 1) BY 1 DO // Convert TIME to TImeStructure
					TIME_TO_TIMEStructure(stProgInspect.Data[stProgInspect.HistSelectedLine].LocHist[stProgInspect.i].Time,ADR(stProgInspect.Hist.TimeLoc[stProgInspect.i]));
					TIME_TO_TIMEStructure(stProgInspect.Data[stProgInspect.HistSelectedLine].MainHist[stProgInspect.i].Time,ADR(stProgInspect.Hist.TimeMain[stProgInspect.i]));
					ConvertTimeToString; // Convert Time Structure to String with miliseconds				
				END_FOR
			END_IF // Array sizes are the same
		END_IF // Data has changed
	ELSE	// Hist layer is not dispalied
		memset(ADR(stProgInspect.Hist.Data),0,SIZEOF(stProgInspect.Hist.Data)); // Erase history data
		stProgInspect.Hist.FirstTime 	:= TRUE;	// Set flag when hist. layer is hidden
		stProgInspect.Hist.FreezeValues := FALSE;	// Reset freeze values flag if layer is hidden
	END_IF // Hist. layer is displayed
END_ACTION



//----------------------------------------------------
//- Convert Time Structure to String with miliseconds
//---------------------------------------------------
ACTION ConvertTimeToString: 
	
	// Convert Local step time to string
	IF( TIME_TO_DINT(stProgInspect.Data[stProgInspect.HistSelectedLine].LocHist[stProgInspect.i].Time)	<> 0 ) THEN // Time is not 0
		//Hours
		itoa(USINT_TO_DINT(stProgInspect.Hist.TimeLoc[stProgInspect.i].hour),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcpy(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
		strcat(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR('h '));
		// Minutes
		itoa(USINT_TO_DINT(stProgInspect.Hist.TimeLoc[stProgInspect.i].minute),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcat(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
		strcat(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR('m '));
		// Seconds
		itoa(USINT_TO_DINT(stProgInspect.Hist.TimeLoc[stProgInspect.i].second),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcat(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
		strcat(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR('s '));
		// miliseconds
		itoa(UINT_TO_DINT(stProgInspect.Hist.TimeLoc[stProgInspect.i].millisec),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcat(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
			strcat(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR('ms'));
	ELSE
		strcpy(ADR(stProgInspect.Hist.StrLoc[stProgInspect.i]), ADR('---'));
	END_IF
	
	// Convert Main step time to string
	IF( TIME_TO_DINT(stProgInspect.Data[stProgInspect.HistSelectedLine].MainHist[stProgInspect.i].Time)	<>  0 ) THEN // Time is not 0
		//Hours
		itoa(USINT_TO_DINT(stProgInspect.Hist.TimeMain[stProgInspect.i].hour),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcpy(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
		strcat(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR('h '));
		// Minutes
		itoa(USINT_TO_DINT(stProgInspect.Hist.TimeMain[stProgInspect.i].minute),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcat(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
		strcat(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR('m '));
		// Seconds
		itoa(USINT_TO_DINT(stProgInspect.Hist.TimeMain[stProgInspect.i].second),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcat(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
		strcat(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR('s '));
		// miliseconds
		itoa(UINT_TO_DINT(stProgInspect.Hist.TimeMain[stProgInspect.i].millisec),ADR(stProgInspect.Hist.StrHelp)); // Convert hour num to ascii
		strcat(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR(stProgInspect.Hist.StrHelp));
		strcat(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR('ms'));
	ELSE
		strcpy(ADR(stProgInspect.Hist.StrMain[stProgInspect.i]), ADR('---'));
	END_IF
	
END_ACTION