(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: MotionStatusPage.st
 * Author:  tufekcio
 * Created: 2016/12:08 PM 
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************) 


//**************************************************
//**** MotionStatusPage ****************************
//**************************************************
ACTION MotionStatusPage:
	//----------------------------------------------
	// Set up page - only once when it is entered
	//----------------------------------------------

    //Unlocked the all index, on Listbox .
    FOR stMotionStatus.k := 0 TO MAX_AX_TOT_MINUS_ONE DO 
	stMotionStatus.OptionDataPoint[stMotionStatus.k]  := 0 ;
	END_FOR;
	

	FOR stMotionStatus.i := 0 TO MOTION_STATUS_FRAME DO 
		
		//================Option data point handling ===============//
		// corresponding to the index cannot be selected during runtime (locked).
		stMotionStatus.OptionDataPoint[stMotionStatus.SelectedIndex[stMotionStatus.i]]  := 1 ;
		//=========================================================//
		
		//if there is a selected Axis, shows the status of axis //
		IF stMotionStatus.SelectedIndex[stMotionStatus.i] <> MAX_AX_TOT_MINUS_ONE AND gMpAxisBasic_0[ stMotionStatus.SelectedIndex[stMotionStatus.i]].Axis <> 0 THEN 
			
			pAxObj	ACCESS gMpAxisBasic_0[ stMotionStatus.SelectedIndex[stMotionStatus.i]].Axis;
           
		    //Gives names for axes //	
			IF NOT gMpAxisBasic_0[ stMotionStatus.SelectedIndex[stMotionStatus.i]].Active  THEN
				strcpy(ADR(stMotionStatus.AxisData[ stMotionStatus.SelectedIndex[stMotionStatus.i]].Name),ADR('NOT INIT.'));	// Axis is not initialized
				strcpy(ADR(stMotionStatus.AxisData[ stMotionStatus.SelectedIndex[stMotionStatus.i]].NodeNum),ADR(' '));			// Set empty node num
			ELSE
				strcpy(ADR(stMotionStatus.AxisData[ stMotionStatus.i ].Name),ADR(gMpAxisBasicCfgType[stMotionStatus.i].AxisName)); // get axis name
				itoa(UINT_TO_DINT(pAxObj.nc_obj_inf.node_nr),ADR(stMotionStatus.AxisData[ stMotionStatus.SelectedIndex[stMotionStatus.i]].NodeNum));	// get axis node number
			END_IF;
			
            //Status of Axes // 
			stMotionStatus.AxisData[stMotionStatus.i].Homing			:= USINT_TO_BOOL(pAxObj.move.homing.status.ok);	
			stMotionStatus.AxisData[stMotionStatus.i].Controler		    := USINT_TO_BOOL(pAxObj.controller.status);
			stMotionStatus.AxisData[stMotionStatus.i].Simulation		:= USINT_TO_BOOL(pAxObj.simulation.status);
			stMotionStatus.AxisData[stMotionStatus.i].ActPos			:= DINT_TO_REAL(pAxObj.monitor.s);
			stMotionStatus.AxisData[stMotionStatus.i].ActVelocity		:= pAxObj.monitor.v;
			stMotionStatus.AxisData[stMotionStatus.i].Enable			:= USINT_TO_BOOL(pAxObj.dig_in.status.enable);
			stMotionStatus.AxisData[stMotionStatus.i].Trig1			    := USINT_TO_BOOL(pAxObj.dig_in.status.trigger1);
			stMotionStatus.AxisData[stMotionStatus.i].Trig2		    	:= USINT_TO_BOOL(pAxObj.dig_in.status.trigger1);
			stMotionStatus.AxisData[stMotionStatus.i].PosEndSwitch	    := USINT_TO_BOOL(pAxObj.dig_in.status.pos_hw_end);
			stMotionStatus.AxisData[stMotionStatus.i].NegEndSwith		:= USINT_TO_BOOL(pAxObj.dig_in.status.neg_hw_end);
			stMotionStatus.AxisData[stMotionStatus.i].RefSwitch		    := USINT_TO_BOOL(pAxObj.dig_in.status.reference);
			stMotionStatus.AxisData[stMotionStatus.i].NwPhase			:= pAxObj.network.phase;
			stMotionStatus.AxisData[stMotionStatus.i].Alarm			    := (pAxObj.message.count.error <> 0);
			stMotionStatus.AxisData[stMotionStatus.i].ControllerReady	:= USINT_TO_BOOL(pAxObj.controller.ready);
           
			stMotionStatus.LayerVisibility[stMotionStatus.i] := SHOW_OBJ;
		ELSE 
		    
			stMotionStatus.LayerVisibility[stMotionStatus.i] := HIDE_OBJ ;
	
		END_IF ;

		
	END_FOR	
	
	
END_ACTION