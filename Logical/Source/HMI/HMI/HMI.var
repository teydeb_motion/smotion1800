(*
********************************************************************
********* Common Menu ****************************************
*********************************************************************)
VAR
	pClocks : REFERENCE TO ARRAY[0..CLK_MAX_NO_CLOCKS_MIN_1] OF UHClock_typ; (*Machine operation related timers*)
END_VAR
(*Production - Top Menu
-----------------------------------------------------------------*)
(*- Recepie Page*)
VAR
	mTemp : mTempHMI_typ;
END_VAR
(*- Production Protocol Page*)
VAR
	stProdProtocol : stProdProtocol_typ; (*Structure for handling production protocol page*)
END_VAR
(*- Machine lock Page*)
VAR
	stMachineLock : stMachineLock_typ; (*Structure for handling machine locking page*)
	stHomePage : stHomePage_typ;
	PManual : PageManualType; (*Structure for handling manual page*)
END_VAR
(**)
(*
User Settings - Top Menu
-----------------------------------------------------------------*)
VAR
	stLanguage : stLanguagePage_typ;
	stUserLogin : UserLogin_typ; (*Structure for handling user loging*)
	stDateAndTime : DateAndTime_typ; (*Structure for handling date and time setings*)
END_VAR
(**** Network settings*)
VAR
	stNetworkSettings : NetworkSettings_typ; (*Structure for handling network settings*)
END_VAR
VAR CONSTANT
	TIME_DEFAULT_IP_RESTORE : TIME := T#10s; (*Time how long should be pressed E-stop with reset for restoring IP*)
END_VAR
(*
Settings - Top Menu
-----------------------------------------------------------------*)
(*- Network settings*)
(*- Hardware config change*)
(**** IO Control*)
VAR
	stIoControlPage : stIoControlPage_typ;
END_VAR
(**** Motor Diagnose*)
(**** Motor Setup page*)
VAR
	k : USINT;
	PMotorSetup : PageMotorSetupType;
	ptrMotorSetupNew : REFERENCE TO PMotorSetupParIngredientsType;
	ptrMotorSetupAct : REFERENCE TO PMotorSetupParIngredientsType;
END_VAR
(*- Motion status page*)
VAR
	pAxObj : REFERENCE TO ACP10AXIS_typ; (*Pointer to the axis type*)
	stMotionStatus : stMotionStatus_typ; (*Structure for handling motion status overview page*)
END_VAR
(*- Machine setup Page*)
VAR
	stMachineSetup : stMachineSetup_typ; (*Controlis of machine setup page*)
END_VAR
(*- Programs Inspection*)
VAR
	stProgInspect : stProgramsInspection_typ; (*Programs Inspection page*)
	stParPage : stParametersPage_typ;
END_VAR
(*
Machine Info - Top Menu
-----------------------------------------------------------------*)
(*
***********************************************************
******* GLOBAL AREA *********************************
************************************************************)
VAR
	stGlobalArea : GlobalArea_typ; (*Structure for handling global area *)
	stStartPage : stStartPage_typ; (*Variable for start page*)
END_VAR
(*
Top Menu Handling
-----------------------------------------------------------------*)
VAR CONSTANT
	QXGA : USINT := 1; (*Constant for active hmi with resolution 320x240*)
	MAX_MENU_ITEMS : ARRAY[0..3] OF INT := [1,3(2)]; (*Nuber of items in each menu column*)
	MENU_ITEMS_MINUS_1 : INT := 3; (*Number of colums in menu template (always 4)*)
END_VAR
VAR
	executeForLoops : BOOL; (*Flag that some top menu btn was pressed and for loops for handling top menu should be executed*)
	topMenu : TopMenu_typ; (*Structure for controling the top menu*)
END_VAR
(*
Navigation arrows
-----------------------------------------------------------------*)
VAR
	pLetter : REFERENCE TO USINT; (*Reference used for reading individual elements from string array*)
END_VAR
VAR CONSTANT
	PAGES_ARRAY_SIZE : INT := 100; (*Number of used pages in HMI*)
	SIZE_PAGE_HIST_BUFFER : INT := 6;
	NAV_CMD_DO_NOTHING : INT := 0; (*Navigation command value (bottom menu arrows)*)
	NAV_CMD_LEFT : INT := 1; (*Navigation command value (bottom menu arrows)*)
	NAV_CMD_RIGHT : INT := 2; (*Navigation command value (bottom menu arrows)*)
	NAV_CMD_UP : INT := 3; (*Navigation command value (bottom menu arrows)*)
END_VAR
VAR
	stNavArrows : stNavArrows_typ; (*Structure to handle page navigation arrows*)
END_VAR
(*
***********************************************************
******** MAIN PAGES **********************************
************************************************************)
VAR
	buildDate : STRING[11] := ''; (*The date the project was compiled and downloaded to the target*)
	mappVersion : STRING[11] := ''; (*Mapp components version*)
	stAlarms : Alarms_typ;
	stAlarmsOld : Alarms_typ;
	stMainSettings : stMainSettingsPage_typ; (*Main settings page*)
	stTestPage : stTestPageType;
	stManPage : stManualPage_typ; (*Manual page*)
END_VAR
(*
********************************************************************
********* OTHER VARIABLES ***********************************
*********************************************************************)
(*Used FB's*)
VAR
	TON_Blink : TON := (0); (*Timer for blinking functionality*)
	TON_StartPage : TON; (*Timer for showing start screen*)
END_VAR
(**** Other variables *****)
VAR
	HwCfgSetByTheUser : gHMI_Permanent_HwCfg_typ := (0); (*Hw cfg from the previous cycle*)
	firstInStep : BOOL := FALSE; (*Flag that case machine is first time in certain step*)
	oldPage : UINT; (*Actual page index in previous cycle*)
	temp : REAL; (*Help variable used for calculations*)
	j : INT; (*help variable for loops*)
	i : INT; (*help variable for loops*)
END_VAR
(*
***************************************************************
*** Page Names constants ***
***************************************************************)
(*Common menu*)
VAR CONSTANT
	PAGE_MACHINE_LOCK : UINT := 99;
	PAGE_PRODUCTION_PROTOCOL : UINT := 120;
	PAGE_USER : UINT := 210;
	PAGE_DATE_AND_TIME : UINT := 220;
	PAGE_LANGUAGE : UINT := 230;
	PAGE_SDM : UINT := 311;
	PAGE_NETWORK_SETTINGS : UINT := 313;
	PAGE_HW_CONFIGURATON : UINT := 314;
	PAGE_PROGRAM_INSPECTION : UINT := 315;
	PAGE_SERVICE : UINT := 320;
	PAGE_MOTOR_SETUP : UINT := 322;
	PAGE_MOTION_STATUS : UINT := 323;
	PAGE_SYSTEM_INFORMATION : UINT := 410;
	PAGE_COMPANY_INFORMATION : UINT := 420;
	PAGE_ALARMS_ACTUAL : UINT := 910;
	PAGE_ALARMS_HISTORY : UINT := 911;
	PAGE_PACK_ML : UINT := 4000;
	PAGE_START : UINT := 999;
	MOTION_STATUS_FRAME : USINT := 1;
END_VAR
(*Main Pages*)
VAR CONSTANT
	PAGE_HOME : UINT := 1000;
	PAGE_MMI_TEST : UINT := 326;
	PAGE_MANUAL : UINT := 500;
END_VAR
VAR
	tempActPage : DINT;
	digitalInputEnd : USINT := 0;
	digitalInputOutputEnd : USINT;
	auxStr : STRING[11];
	strMin1 : STRING[11];
	strMin2 : STRING[11];
	strHour1 : STRING[11];
	hidePrgInspection : USINT := 0;
	strHour2 : STRING[11];
	machineVer : STRING[11] := ''; (*Machine SW version*)
	vcVer : STRING[11] := ''; (*Visual Components runtime version*)
	motionVer : STRING[11] := ''; (*MC runtime version*)
	pMoVer : MoVerStruc_typ; (*Reference to the object version info structure*)
	sTempString : STRING[250];
END_VAR
VAR CONSTANT
	PAGE_HELP : UINT := 4001;
END_VAR
VAR
	cncColorDatapoint : USINT;
	pDpName : REFERENCE TO STRING[250];
	fileNameOld : STRING[255];
	stat : UINT;
	uAdr : UDINT;
	uSize : UDINT;
	rcpIdxOld : USINT := 0;
	oldStep : UINT;
END_VAR
