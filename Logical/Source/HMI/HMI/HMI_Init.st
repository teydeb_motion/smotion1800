


PROGRAM _INIT
	// Set actual block version

	gHMI.General.ActUserLevel := 2 ; //Admin
	
	
	// ----------------------------

	// ----------------------------
	
	// How long should be shown Start screen
	IF(gUHandle.Simulation) THEN	// in simulation
		TON_StartPage.PT	:= T#1.5s;
	ELSE	// In real machine
		TON_StartPage.PT	:= T#25s;
	END_IF
	
	//Init Blinking timer
	TON_Blink.PT := T#1s;

	// Hide HW buttons
	stGlobalArea.HideHwButtons := 1;
	
	// Initialize indexes of all pages used in visu
	memset(ADR(stNavArrows.In.pageIndexes),-1,SIZEOF(stNavArrows.In.pageIndexes)); // init whole array to -1
	// Top menu pages
	stNavArrows.In.pageIndexes[0] := 99;
	stNavArrows.In.pageIndexes[1] := 110;	
	stNavArrows.In.pageIndexes[2] := 120;
	
	stNavArrows.In.pageIndexes[5] := 210;
	stNavArrows.In.pageIndexes[6] := 220;
	stNavArrows.In.pageIndexes[7] := 230;	
	
	stNavArrows.In.pageIndexes[10] := 310;
	stNavArrows.In.pageIndexes[11] := 311; 
	stNavArrows.In.pageIndexes[12] := 312;
	stNavArrows.In.pageIndexes[13] := 313;
	stNavArrows.In.pageIndexes[14] := 314;
	stNavArrows.In.pageIndexes[15] := 315;
	
	stNavArrows.In.pageIndexes[20] := 320;
	stNavArrows.In.pageIndexes[21] := 321;
	stNavArrows.In.pageIndexes[22] := 322;
	stNavArrows.In.pageIndexes[23] := 323;
	stNavArrows.In.pageIndexes[24] := 324;
	stNavArrows.In.pageIndexes[25] := 325;
//	stNavArrows.In.pageIndexes[26] := 326;
//	stNavArrows.In.pageIndexes[27] := 327;
//	stNavArrows.In.pageIndexes[28] := 328;
	
	stNavArrows.In.pageIndexes[32] := 410;
	stNavArrows.In.pageIndexes[33] := 420;
	stNavArrows.In.pageIndexes[34] := 910;
	stNavArrows.In.pageIndexes[35] := 911;
	stNavArrows.In.pageIndexes[36] := 999;
	
//	stNavArrows.In.pageIndexes[42] := 500;//Manual 
	stNavArrows.In.pageIndexes[40] := 501;
	stNavArrows.In.pageIndexes[41] := 502;
	
	stNavArrows.In.pageIndexes[60] := 600; //Oee Navi
	stNavArrows.In.pageIndexes[61] := 610;
	stNavArrows.In.pageIndexes[62] := 620;
	stNavArrows.In.pageIndexes[62] := 630;
	
	// Machine specific pages
	stNavArrows.In.pageIndexes[50] := 1000;//Home
	stNavArrows.In.pageIndexes[51] := 2000;//Packing
	stNavArrows.In.pageIndexes[52] := 3000;//Weighing
	stNavArrows.In.pageIndexes[53] := 4000;//Omac
	// Size of pageIndexes array is = PAGES_ARRAY_SIZE
	
	//Init page hist buffer
	memset(ADR(stNavArrows.In.pageHistoryBuffer),0,SIZEOF(stNavArrows.In.pageHistoryBuffer)); // init whole array to 0
	stNavArrows.In.pageHistoryBuffer[0] := 1000; //set first page to main page

	
//	//Addresses of the names of the variables
//	stRecepie.pVarNames[0] 	:= ADR('gParameters');
//	//Sizes of the variables
////	stRecepie.VarSizes[0]		:= SIZEOF(gParametrsAll);  // init the memory with the bigger structure
//	//Indicate that actual recepie name is unknown
	strcpy(ADR(gHMI.RecipePage.ActRecepieName),ADR('---'));

	// Init the user names & passwords
	gHMI_Permanent.UserNames[0]     := 'OPERATOR';
	gHMI_Permanent.UserNames[1]     := 'SERVICE';
	gHMI_Permanent.UserNames[2]     := 'ADMIN';
	
	IF(gUHandle.Simulation = FALSE) THEN // if simulation is not active
		gHMI_Permanent.UserPasswords[0] := '';
		gHMI_Permanent.UserPasswords[1] := 'serv';
		gHMI_Permanent.UserPasswords[2] := '1234abcd';
	ELSE // disable passfords for simulation
		gHMI.General.ActUserLevel       := 3;  // set the user level to super admin for simulation 
		gHMI_Permanent.UserPasswords[0] := '';
		gHMI_Permanent.UserPasswords[1] := '';
		gHMI_Permanent.UserPasswords[2] := '';
	END_IF
	
	// Hide the jog speed settings layer
	PManual.hideStartFanBtn := HIDE_OBJ;
	
	// Hide the message box
	gHMI.Others.PopUpRequest := HIDE_OBJ;
	
	// Change to the last active language
	gHMI.General.ChangeLanguage := gHMI_Permanent.Language;
	
	stAlarms.LayerCtrl := HIDE_OBJ;
	
	
	
	// --------------------------------------
	// hide the msg layer
	stGlobalArea.MsgBox.HideBtnCancle := HIDE_OBJ;
	stGlobalArea.MsgBox.HideBtnOk     := HIDE_OBJ;
	stGlobalArea.MsgBox.HideLayer     := HIDE_OBJ;
	stGlobalArea.LoginPopUp.PopUp     := HIDE_OBJ;
	// --------------------------------------
	// init. HW settings from the memory
	HwCfgSetByTheUser := gHMI_Permanent.HwCfg;
	
	// --------------------------------------
	
	// ----------------------------

	
	// Get the SW versions
	// Machine version
	memset(ADR(pMoVer), 0, SIZEOF(pMoVer));
	MO_ver(ADR('AxisHandli'), 0, ADR(pMoVer));
	memcpy(ADR(machineVer), ADR(pMoVer.version), SIZEOF(pMoVer.version));
	// Get the date of the MainControl
	memset(ADR(auxStr), 0, SIZEOF(auxStr));
	itoa(pMoVer.day, ADR(auxStr));
	strcat(ADR(buildDate), ADR(auxStr));
	strcat(ADR(buildDate), ADR('.'));
	memset(ADR(auxStr), 0, SIZEOF(auxStr));
	itoa(pMoVer.month, ADR(auxStr));
	strcat(ADR(buildDate), ADR(auxStr));
	strcat(ADR(buildDate), ADR('.'));
	memset(ADR(auxStr), 0, SIZEOF(auxStr));
	itoa(pMoVer.year, ADR(auxStr));
	strcat(ADR(buildDate), ADR(auxStr));
	memset(ADR(auxStr), 0, SIZEOF(auxStr));
	// MC version
	memset(ADR(pMoVer), 0, SIZEOF(pMoVer));
	MO_ver(ADR('acp10cfg'), 0, ADR(pMoVer));
	memcpy(ADR(motionVer), ADR(pMoVer.version), SIZEOF(pMoVer.version));


	// VC version
	memset(ADR(pMoVer), 0, SIZEOF(pMoVer));
	MO_ver(ADR('visapi'), 0, ADR(pMoVer));
	memcpy(ADR(vcVer), ADR(pMoVer.version), SIZEOF(pMoVer.version));

	// mapp version
	memset(ADR(pMoVer), 0, SIZEOF(pMoVer));
	MO_ver(ADR('MpCom'), 0, ADR(pMoVer));
	memcpy(ADR(mappVersion), ADR(pMoVer.version), SIZEOF(pMoVer.version));	
	// set the unitgroup datapoint from the memory
	gHMI.General.ChangeUnit := gHMI_Permanent.Unitgroup;
	
	hidePrgInspection := 1; // temp. added until the program inspection is implemented
	cncColorDatapoint := 15;
	
	//hides the layers of motor setup page
	memset(ADR(PMotorSetup.LayerContWXGA.HideLayer), HIDE_OBJ, SIZEOF(PMotorSetup.LayerContWXGA.HideLayer));
	PMotorSetup.LayerContWXGA.HideLayer[0] := DISPLAY_OBJ; // admin should be logged in
	memset(ADR(PMotorSetup.LayerContWXGA.LockLayerBtn[0]), LOCK_OBJ, SIZEOF(PMotorSetup.LayerContWXGA.LockLayerBtn));

	//Specified the value of Tuning Mode 
//
//	ptrMotorSetupNew.Tuning.Mode   := 2 ;
	stUserLogin.HideUserMgmt := HIDE_OBJ;
	stUserLogin.HideLogInMsg := DISPLAY_OBJ;

	
END_PROGRAM