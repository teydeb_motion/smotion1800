(*********************************************************************
********* Common Menu ****************************************
*********************************************************************)
(**)
(**)
(**)

TYPE
	Alarms_axis_typ : 	STRUCT 
		ErrInfo : gHMIAlarm_AxisAlarmInfo_typ;
		LockAckOne : USINT;
		hideAxisErrDetail : USINT; (*Status datapoint of axis error details layer*)
		cmdAckOne : BOOL; (*Acknowledge single error*)
		cmdBack : BOOL; (*Set Flag to hide actually shown layer*)
		axisDetailsNumber : USINT; (*Index of axis which details should be shown*)
		BtnNextAxis : BOOL; (*Show next axis details*)
		BtnPrewAxis : BOOL; (*Show previous axis details*)
	END_STRUCT;
END_TYPE

(**)
(*
***********************************************************
******** MAIN PAGES **********************************
************************************************************)
(*
Alarms Page
-----------------------------------------------------------------------*)

TYPE
	Alarms_Details_typ : 	STRUCT 
		ErrorIDMapp : UINT; (*Error ID from the related mapp component*)
		LockAckOne : USINT;
		MaxIdx : USINT;
		DetailIdx : USINT; (*Axis Index Datapoint*)
		cmdAckOne : BOOL; (*Acknowledge single error*)
		cmdAckAll : BOOL; (*Acknowledge all error record*)
		cmdBack : BOOL; (*Set Flag to hide actually shown layer*)
		BtnNext : BOOL; (*Show next axis details*)
		BtnPrev : BOOL; (*Show previous axis details*)
		ErrorIf : ErrorInterface_typ; (*Error interface type from SysDiag library*)
	END_STRUCT;
	Alarms_typ : 	STRUCT 
		ShowDetails : BOOL;
		HideLastAlarm : USINT; (*Status datapoint for last alarm msg in global area*)
		NoAlarm : USINT; (*Indication that in machine is any unacknowledged alarm*)
		Axis : Alarms_axis_typ; (*Structure for handling axis alarms details*)
		MaxAxSelectDP : USINT;
		AxisIndex : USINT; (*Axis Index Datapoint*)
		ErrBitmapIndex : USINT; (*Bitmap index datapoing - to indicate error in respective axis*)
		LayerCtrl : USINT; (*Status datapoint to hide the layers*)
		Details : Alarms_Details_typ; (*Structure for handling axis alarms details*)
	END_STRUCT;
END_TYPE

(*- Date and time*)

TYPE
	DateAndTime_typ : 	STRUCT  (*Structure for handling date and time setings*)
		Year : UINT; (*Time information*)
		Month : USINT; (*Time information*)
		Day : USINT; (*Time information*)
		Hour : INT; (*Time information*)
		Minute : INT; (*Time information*)
		Second : INT; (*Time information*)
		MaxDay : UINT; (*Maximum day available*)
		btnSetDateTime : BOOL; (*btn to set up time given by user*)
		DateAndTimeStructure : DTStructure; (*Structure to store the date and time information*)
		DTStructureGetTime_System : DTStructureGetTime; (*FUB: get date and time of the system*)
		DateTimeSys : DTStructure; (*Structure with the system Date and Time*)
		DTStructureSetTime_SetTime : DTStructureSetTime; (*FUB: set Time and Date*)
	END_STRUCT;
	EncSettings_Homing_Par_typ : 	STRUCT 
		LimitSwitchPos : REAL; (*Position of limit switch*)
		HomingSpeedTrigger : REAL; (*Speed of searching for Limit sensor signal after finding a trigger*)
		HomingSpeedSwitch : REAL; (*Speed of searching for Limit sensor signal*)
	END_STRUCT;
	GlobalAreaBtnHighlit_typ : 	STRUCT  (*Button on global area highliting*)
		Alarms : USINT; (*Make Btn pressed*)
		Home : USINT; (*Make Btn pressed*)
		Manual : USINT; (*Make Btn pressed*)
		Loader : USINT; (*Make Btn pressed*)
		Settings : USINT; (*Make Btn pressed*)
		Process : USINT; (*Make Btn pressed*)
		Omac : USINT; (*Make Btn pressed*)
	END_STRUCT;
END_TYPE

(*Process PAge*)
(*Insert your comment here.*)
(*
Machine Info - Top Menu
-----------------------------------------------------------------*)
(*
***********************************************************
******* GLOBAL AREA *********************************
************************************************************)

TYPE
	GlobalArea_typ : 	STRUCT 
		Alarm : stGlobalArea_Alarm_typ;
		LoginPopUp : stGlobalArea_UserLogin_typ;
		AutomatStateLayer : USINT; (*Runtime status control for Automat State*)
		MachineStateLayer : USINT; (*Runtime status control for Machine State*)
		StartStopBlink : USINT; (*Indication of blinking of START STOP button*)
		BtnHighlit : GlobalAreaBtnHighlit_typ;
		LoginPopUpCtrl : USINT;
		HideHwButtons : USINT := 1; (*Status datapoint to show / hide HW buttons layer*)
		HideStopBtnSign : USINT; (*Status datapoint to hide stop bitmap next to start btn*)
		HideBuzerSignal : USINT; (*Hide signalization about buzzer*)
		LoderPageBtnStatusCtrl : USINT; (*Hide the loader page button depending on the machine type*)
		bHelp : BOOL; (*Help button pressed*)
		uHelpStatus : USINT; (*Help system control datapoint*)
		sHelpString : STRING[250]; (*Addres string for online help*)
		HideTopMenu_QVGA : USINT; (*Hide Tom menu buttons - used for QVGA*)
		MsgBox : stGlobalAreaMsgBox_typ; (*Handling of Pop Up message *)
	END_STRUCT;
	PMotorSetupPageLayerConWXGAType : 	STRUCT 
		HideLayer : ARRAY[0..4]OF USINT; (*Status Datapoint of layer*)
		LockLayerBtn : ARRAY[0..4]OF USINT; (*Lock buttons for changing layer*)
		BtnLayerSelected : ARRAY[0..4]OF USINT; (*Cmd to change layer*)
	END_STRUCT;
	PMotorSetupAxisNavigationType : 	STRUCT 
		LockAxisMinus : BOOL; (*lock btn to Decrease index of actualy shown axis*)
		LockAxisPlus : BOOL; (*lock btn to Increase index of actualy shown axis*)
		BtnAxisMinus : BOOL; (*Decrease index of actualy shown axis*)
		BtnAxisPlus : BOOL; (*Increase index of actualy shown axis*)
	END_STRUCT;
	MotorSetupPageEncSetingsLock_typ : 	STRUCT  (*Locking buttons for motor setup page*)
		btnJogPos : USINT; (*Locking of joging command*)
		btnJogNeg : USINT; (*Locking of joging command*)
		btnSetPosition : USINT; (*Locking of set position buttons*)
		btnLoadFromRecipe : USINT;
		btnHomeToLimSwitch : USINT;
	END_STRUCT;
	MotorSetupPageEncSetings_typ : 	STRUCT  (*Variables related to encoder settings layer*)
		CmdJogPos : BOOL; (*Command to jog possitive with selected axis*)
		CmdJogNeg : BOOL; (*Command to jog possitive with selected axis*)
		CmdSetPosition : BOOL; (*Command to set encoder zero position*)
		SetPosition : REAL; (*Set encoder position value*)
		MinPosValue : REAL; (*Minimum position value*)
		MaxPosValue : REAL; (*Maximum position value*)
		HomingOk : {REDUND_UNREPLICABLE} BOOL; (*Flag: Homing is OK for the selected axis*)
		ActualEncoderPosition : REAL; (*Actual axis position*)
		LockBtns : MotorSetupPageEncSetingsLock_typ;
		HomingPar : EncSettings_Homing_Par_typ;
		HideHomeOffsetInterface : USINT; (*Datapoint to hide home offset interface (visible only for Multiturn ENC)*)
		HideHomeLimSwitchInterface : USINT; (*Datapoint to hide limit switch homing interface (visible only for Multiturn ENC)*)
		HideAxisIsNotReady : USINT; (*Hide Msg that axis is not ready for homing*)
		EncoderOffset : REAL;
		CmdHomeLimSwitch : BOOL; (*Command to perform homing using limit switch*)
		CmdLoadOffsetFromRecipe : BOOL; (*Command load offset from recipe*)
		CmdHomeSetOffset : BOOL; (*Command to home axis with offset from recipe*)
	END_STRUCT;
	PageMotorSetupType : 	STRUCT 
		InitConfig : BOOL;
		Enable : BOOL;
		Index : PMotorSetupIndexType;
		Button : PMotorSetupButtonType;
		Layer : PMotorSetupLayerType;
		Lock : PMotorSetupLockType;
		Par : PMotorSetupParType;
		AxisNavigation : PMotorSetupAxisNavigationType; (*Navigation arrows for changing axis*)
		LayerContWXGA : PMotorSetupPageLayerConWXGAType; (*Structure to control layers shown to user*)
		EncSetings : MotorSetupPageEncSetings_typ; (*Structure to control multiturn encoder settings*)
	END_STRUCT;
	PMotorSetupButtonType : 	STRUCT 
		Page : ARRAY[0..MAX_PAGE_BTN_MIN1_IN_PMSETUP]OF BOOL;
		OldPage : ARRAY[0..MAX_PAGE_BTN_MIN1_IN_PMSETUP]OF BOOL;
		TuningStart : BOOL;
		TuningStop : BOOL;
		ParIDWrite : BOOL;
		ParIDRead : BOOL;
		Apply : BOOL;
		SaveToMachine : BOOL;
		LoadFromMachine : BOOL;
		btnLeft : BOOL;
		btnRight : BOOL;
	END_STRUCT;
	PMotorSetupIndexType : 	STRUCT 
		MaxAxis : USINT;
		OldSelectedAxis : USINT;
		SelectedAxis : USINT;
		SelectedCont : USINT;
		SelectedLimit : USINT;
	END_STRUCT;
	PMotorSetupLayerType : 	STRUCT 
		Gen : ARRAY[0..1]OF USINT;
		Cont : ARRAY[0..2]OF USINT;
		Limit : ARRAY[0..3]OF USINT;
	END_STRUCT;
	PMotorSetupLockType : 	STRUCT 
		Page : ARRAY[0..MAX_PAGE_BTN_MIN1_IN_PMSETUP]OF USINT;
		InputFields : USINT;
		OperationButtons : USINT;
		TuningStop : USINT;
	END_STRUCT;
	PMotorSetupParType : 	STRUCT 
		Actual : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF PMotorSetupParIngredientsType;
		New : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF PMotorSetupParIngredientsType;
		ParID : PMotorSetupParIDType;
	END_STRUCT;
	PMotorSetupParIDType : 	STRUCT 
		ID : UINT;
		Data : STRING[33];
	END_STRUCT;
	PMotorSetupParIngredientsType : 	STRUCT 
		Encoder : UPAEncoderType;
		Controller : UPAControllerType;
		Limit : UPALimitType;
		Tuning : PMotorSetupTuningType;
		ParID : PMotorSetupParIDType;
	END_STRUCT;
	PMotorSetupTuningType : 	STRUCT 
		MaxCurrent : REAL;
		MaxSpeed : REAL;
		MaxDistance : REAL;
		MaxKv : REAL;
		Mode : UDINT;
		Vertical : BOOL;
	END_STRUCT;
END_TYPE

(*
Settings - Top Menu
-----------------------------------------------------------------*)
(*- Network settings*)

TYPE
	NetworkSettings_typ : 	STRUCT 
		CfgGetIPAddr_0 : CfgGetIPAddr; (*FUB: get IP address*)
		CfgGetSubnetMask_0 : CfgGetSubnetMask; (*FUB: get subnet mask*)
		CfgSetIPAddr_0 : CfgSetIPAddr; (*FUB: set IP address (new one)*)
		CfgSetIPAddr_1 : CfgSetIPAddr; (*FUB: set IP address (default one)*)
		CfgSetSubnetMask_0 : CfgSetSubnetMask; (*FUB: set subnet mask (new one)*)
		CfgSetSubnetMask_1 : CfgSetSubnetMask; (*FUB: set subnet mask (default one)*)
		btnSetNewIP : BOOL; (*Btn to set new IP*)
		btnSetDefIP : BOOL; (*Btn to set default IP*)
		NewIPAddress : STRING[16];
		ActualIPAddress : STRING[16];
		NewSubnetMask : STRING[16];
		ActualSubnetMask : STRING[16];
		Device : STRING[16];
		TON_DefaultIPRest : TON; (*Timer for restoring default IP*)
	END_STRUCT;
	PageManualType : 	STRUCT  (*Local variables for Manual Page*)
		Lock : stManualPageLock_typ; (*variables for locking elements on manual page*)
		Cmd : PManualCommandType; (*Commands from manual page*)
		Setings : stManualPageSetings_typ; (*Setings of manual movements*)
		evrOK : BOOL; (*Help flag for checking in loops*)
		SignalsPage : stManualSignalsPage_typ;
		hideStartFanBtn : USINT; (*Status datapoint of Jog speeds layer*)
		EndLimit : USINT; (*Flag that End switch of active axis is reached - 0 - not in limit; 1 - in neg. limit; 2 - in pos. limit*)
		Higlight : stManualPageHiglight_typ;
		JogSpeedX : REAL;
		Hide : stManPageHide_typ; (*Status Datapoints*)
		GoToPos : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF USINT; (*lock go to pos. button*)
		Status : PManualStatusType; (*STRUCT - Status*)
		Distance : LREAL;
	END_STRUCT;
	PManualCommandType : 	STRUCT  (**** Manual Command Type*)
		SetPos : REAL; (* - Set position*)
		MovePos : BOOL; (* - Positive Movement*)
		MoveNeg : BOOL; (* - Negative movement*)
		GoPos : BOOL; (* - Go to position*)
		FilmSticker : BOOL; (* - Bobbin sticker valve*)
		MoveSoldier : BOOL; (* - Move Product conveyor for 1 soldier distance*)
	END_STRUCT;
	PManualStatusType : 	STRUCT  (**** Manual Status Type*)
		ActualPos : LREAL; (* - Actual position*)
		Homed : BOOL; (* - Axis homed*)
		PoweredOn : BOOL; (* - Axis powered on*)
		Alarm : BOOL; (* - Alarm on axis*)
		MoveActive : BOOL; (* - Move active flag on selected axis*)
		ActualVel : LREAL;
		ActVelStepMot : LREAL;
	END_STRUCT;
END_TYPE

(*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*)
(*                                                                                                         PAGE  PACK ML STRUCTUREs*)
(*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*)
(*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*)
(*                                                                                                          PAGE MOTOR SETUP TYPE*)
(*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*)

TYPE
	stHomePage_typ : 	STRUCT 
		Mode : stHomeMode_typ;
		cmd : stHomeCmd_typ;
	END_STRUCT;
	stHomeMode_typ : 	STRUCT 
		SelectedMode : USINT;
		BtnStatus : stHomeBtnStatus_typ;
	END_STRUCT;
	stHomeCmd_typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
	END_STRUCT;
	stHomeBtnStatus_typ : 	STRUCT 
		ModeManual : BOOL;
		ModeAuto : BOOL;
	END_STRUCT;
END_TYPE

(**)
(**)
(*
Production - Top Menu
-----------------------------------------------------------------*)
(*- Recepie Page*)

TYPE
	stGlobalAreaMsgBox_typ : 	STRUCT  (*Handling of Pop Up message *)
		HideLayer : USINT;
		MsgIndex : USINT;
		HideBtnOk : USINT;
		HideBtnCancle : USINT;
		BtnOk : BOOL;
		BtnCancle : BOOL;
	END_STRUCT;
	stGlobalArea_Alarm_typ : 	STRUCT 
		PopUpColorDP : USINT;
		WarAct : BOOL;
		AlarmAct : BOOL;
		AlarmPrioFilter : USINT;
		AlarmBitmap : USINT;
		HideAlarmLayer : USINT;
	END_STRUCT;
END_TYPE

(*Insert your comment here.*)

TYPE
	stGlobalArea_UserLogin_typ : 	STRUCT 
		loginLock : USINT;
		cmdCancel : BOOL;
		cmdShow : BOOL;
		PopUp : USINT;
	END_STRUCT;
	stIoControlLayers_typ : 	STRUCT 
		IOBitmapLayer : USINT;
		AcpTrigger : USINT; (*Hide layer with acopos triggers*)
		IO_Layer : USINT;
	END_STRUCT;
	stIoControlPageAcoposInputs_typ : 	STRUCT 
		Show : BOOL;
		X1Trig1 : USINT; (*Color datapoint to show trigger status of respective acopos input*)
		X1Trig2 : USINT; (*Color datapoint to show trigger status of respective acopos input*)
	END_STRUCT;
	stIoControlPageQXGAHide_typ : 	STRUCT 
		Ch_1To8 : USINT; (*Hide layer with forcing Channel 1 to 8*)
		Ch_9To16 : USINT; (*Hide layer with forcing Channel 9 to 16*)
		LayerOthers : USINT; (*Hide layer with rest of element (Err ack and Acopos Triggers)*)
	END_STRUCT;
	stIoControlPageQXGA_typ : 	STRUCT 
		Hide : stIoControlPageQXGAHide_typ;
		LockLayerBtn : ARRAY[0..2]OF BOOL; (*Lock buttons for changing *)
		BtnLayerSelected : ARRAY[0..2]OF BOOL;
	END_STRUCT;
END_TYPE

(*- Hardware config change*)
(*-IO Control*)

TYPE
	stIoControlPage_typ : 	STRUCT 
		AcoposInputs : stIoControlPageAcoposInputs_typ; (*Values for acopos inputs signalization*)
		MinForceValueLimit : UDINT; (*Minimum force value limit for I/O control page*)
		MaxForceValueLimit : UDINT; (*Maximum force value limit for I/O control page*)
		BitmapSelector : USINT; (*Datapoin to select the picture to be shown on IO control page*)
		Layers : stIoControlLayers_typ;
		DpNames : ARRAY[0..15]OF STRING[250];
		NameSelector : ARRAY[0..15]OF INT; (*IO Datapoint name selector for the active IO card*)
		MaxIOcardIndex : USINT;
		HideForceIOErr : USINT; (*Status datapoing to show error msg and Err Acknowlege button in case of error fro ForceIO block*)
		QXGA : stIoControlPageQXGA_typ; (*Variables needed for modification for QXGA HMI*)
		HsButtons : ARRAY[0..1]OF BOOL;
	END_STRUCT;
	stLanguagePage_typ : 	STRUCT 
		NextLang : BOOL;
		PrevLang : BOOL;
	END_STRUCT;
END_TYPE

(*- Production Protokol Page*)

TYPE
	stMachineLock_typ : 	STRUCT 
		UserPassCodeInput : INT; (*Entry from the user to the password field in machine lock page*)
		CmdBtnOk : BOOL; (*OK Button - To confirm unlock code*)
		LockElements : BOOL; (*Lock OK btn and Input field for some time after wrong password*)
		HideWrongPassAnim : ARRAY[0..5]OF USINT; (*Status datapoints to animate waiting mesage after wrong password*)
		TON_MachineLock : TON; (*Timer to measure wait time after wrong password*)
	END_STRUCT;
	stMachineSetupCmd_typ : 	STRUCT 
		RestartMachine : BOOL; (*Cmd to restart the machine*)
	END_STRUCT;
	stMachineSetupHide_typ : 	STRUCT 
		NOT_USED : USINT; (*Not used variable - only to avoid warning about emty structure*)
	END_STRUCT;
	stMachineSetupLock_typ : 	STRUCT 
		BtnRestartMachine : USINT; (*Locking cmd to restart machine*)
	END_STRUCT;
END_TYPE

(**)

TYPE
	stMachineSetup_typ : 	STRUCT  (*Structure for machine setu page*)
		Cmd : stMachineSetupCmd_typ; (*commands*)
		Hide : stMachineSetupHide_typ; (*status datapoints*)
		Lock : stMachineSetupLock_typ; (*locking datapoints*)
	END_STRUCT;
END_TYPE

(*
Settings Page
-----------------------------------------------------------------------*)

TYPE
	stMainSettingsPage_typ : 	STRUCT  (*Main page setting - 3000*)
		Lock : stSettingsPageLock_typ; (*Locking of elements of page*)
	END_STRUCT;
	stManPageAlarms_typ : 	STRUCT  (*Handling of alarms signalization and page change*)
	END_STRUCT;
	stTestParType : 	STRUCT 
		SetSpeed : ARRAY[0..1]OF REAL;
		Distance : ARRAY[0..1]OF REAL;
		MoveAbs : ARRAY[0..1]OF BOOL;
		MoveRel : ARRAY[0..1]OF BOOL;
	END_STRUCT;
	stTestPageType : 	STRUCT 
		cmd : USINT;
		Par : stTestParType;
	END_STRUCT;
END_TYPE

(*
Manual Page
-----------------------------------------------------------------------*)

TYPE
	stManPageCmds_typ : 	STRUCT 
		JogPos : BOOL; (*Start jog. pos.*)
		JogNeg : BOOL; (*Start jog. neg*)
		GoToSetPos : BOOL; (*Go to position*)
		SetPos : REAL; (*Position for absout move*)
		Stop : BOOL; (*Stop cmd*)
		SetPosCap : BOOL;
		SetPosHlz : BOOL;
		HideDetails : BOOL;
		ShowDetails : BOOL;
	END_STRUCT;
	stManPageHide_typ : 	STRUCT 
		SafetyDoorOpen : USINT;
		DetailLayer : USINT;
		HideErr : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF USINT; (*Hide error signalization if axis is without error*)
	END_STRUCT;
	stManualPageHiglight_typ : 	STRUCT 
		Reference : BOOL;
		StopTest : BOOL;
		StartTest : BOOL;
	END_STRUCT;
	stManualPageLock_typ : 	STRUCT  (*variables for locking elements on manual page*)
		B1_JogPos : USINT; (*Lock inverter command*)
		B1_JogNeg : USINT; (*Lock inverter command*)
		GoToPos : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF USINT; (*lock go to pos. button*)
		XJogNeg : USINT; (*lock jog neg. commands X  ax. group*)
		XJogPos : USINT; (*lock jog pos commands X ax. group*)
		StandJogNeg : USINT; (*lock jog neg. commands standard ax. group*)
		StandJogPos : USINT; (*lock jog pos commands standard ax. group*)
		JogNeg : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF USINT; (*lock jog neg. commands X  ax. group*)
		JogPos : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF USINT; (*lock jog pos commands X ax. group*)
		Stop : USINT;
	END_STRUCT;
	stManualPageSetings_typ : 	STRUCT  (*Setings from manual page*)
		SetPos : REAL; (*Position where to move selected axis*)
		SetPosMax : REAL; (*Maximum value of set position - adjusted acordingly to actually selected axis*)
		SetPosMin : REAL; (*Minimum value of set position - adjusted acordingly to actually selected axis*)
	END_STRUCT;
	stManualPage_typ : 	STRUCT  (*Local variables for Manual Page*)
		Lock : stManualPageLock_typ; (*variables for locking elements on manual page*)
		Cmd : stManPageCmds_typ; (*Commands from manual page*)
		Setings : stManualPageSetings_typ; (*Setings of manual movements*)
		EndLimits : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF USINT; (*Flag that End switch of active axis is reached - 0 - not in limit; 1 - in neg. limit; 2 - in pos. limit*)
		LimitsJogSpeed : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF REAL; (*Limits for jog speeds*)
		JogSpeed : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF REAL;
		Hide : stManPageHide_typ; (*Status Datapoints*)
		SelectedAxis : {REDUND_UNREPLICABLE} ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF BOOL;
	END_STRUCT;
	stManualSignalsPage_Higlight_typ : 	STRUCT 
		ConveyorDown : BOOL;
		GlassEntry : BOOL;
		SquaringSysLimit : BOOL;
		ResetButton : BOOL;
		ConveyorFault : BOOL;
		GlassExit : BOOL;
		LowAir : BOOL;
		SafetyPhotocell : BOOL;
		ConveyorActive : BOOL;
		LeftCarrierActive : BOOL;
		NegativeButton : BOOL;
		PressureReached : BOOL;
		ConveyorUp : BOOL;
		RightCarrierActive : BOOL;
		PauseButton : BOOL;
		StartButton : BOOL;
		FanMotorFault : BOOL;
		HeadDownLimit : BOOL;
		PhaseFault : BOOL;
		TableDown : BOOL;
		FanPedal : BOOL;
		LeftCarrierBack : BOOL;
		PositiveButton : BOOL;
		XAxisRef : BOOL;
		XAxisTrigger : BOOL;
		XLimitPos : BOOL;
		XLimitNeg : BOOL;
		YAxisRef : BOOL;
		YAxisTrigger : BOOL;
		CAxisRef : BOOL;
		SignalsHighlight : BOOL;
		ManualHighlight : BOOL;
	END_STRUCT;
	stManualSignalsPage_typ : 	STRUCT 
		HideLoader : USINT;
		Higlight : stManualSignalsPage_Higlight_typ;
	END_STRUCT;
	stMotionStatusArowBtn_typ : 	STRUCT  (*Navigation arrows handling on Motion status page*)
		LeftCmd : BOOL;
		LeftLock : BOOL;
		LeftHide : USINT;
		RightCmd : BOOL;
		RightLock : BOOL;
		RightHide : USINT;
	END_STRUCT;
	stMotionStatusAxisData_typ : 	STRUCT 
		Name : STRING[48];
		NodeNum : STRING[48];
		Homing : BOOL;
		Controler : BOOL;
		Simulation : BOOL;
		MotionBmpIndex : USINT; (*Bitmap index to animate motion*)
		ActPos : REAL; (*Position in Units*)
		ActVelocity : REAL; (*Velocity in units*)
		Enable : BOOL;
		Trig1 : BOOL;
		Trig2 : BOOL;
		PosEndSwitch : BOOL;
		NegEndSwith : BOOL;
		RefSwitch : BOOL;
		Alarm : BOOL;
		ControllerReady : BOOL;
		NwPhase : USINT;
	END_STRUCT;
END_TYPE

(*- Motion status page*)
(*Insert your comment here.*)

TYPE
	stMotionStatus_typ : 	STRUCT 
		NoGantryLayerCtrl : USINT;
		GantryLayerCtrl : USINT;
		AxisData : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF stMotionStatusAxisData_typ; (*Datapoints conected to one of 5 HMI fields for axis*)
		HideAxisData : ARRAY[0..4]OF USINT; (*Hide axis data (whole column) in case that is in machine less than 4 axis*)
		FirstAxisIndex : UINT; (*Index of first axis to show*)
		LastAxisIndex : UINT; (*Index of last axis to show*)
		k : UINT; (*help var*)
		i : UINT; (*help var*)
		ArowBtn : stMotionStatusArowBtn_typ; (*Navigation arrows handling on Motion status page*)
		HideLayerQXGA : USINT; (*Hide Additional infor data - only for QXGA HMI*)
		SelectedIndex : ARRAY[0..1]OF USINT := [0,1];
		LayerVisibility : ARRAY[0..2]OF USINT;
		OptionDataPoint : ARRAY[0..MAX_AX_TOT]OF USINT;
		OldAxisIndex : ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF USINT;
	END_STRUCT;
	stMotorDiag_Hide_typ : 	STRUCT 
		NoGantryLayer : USINT;
		GantryLayer : USINT;
	END_STRUCT;
	stMotorDiag_Highlight_typ : 	STRUCT 
		AxesReady : ARRAY[0..3]OF BOOL; (*If TRUE it highligts the text box of the given axis on the MotorSetup Page*)
	END_STRUCT;
	stMotorDiag_typ : 	STRUCT 
		Higlight : stMotorDiag_Highlight_typ;
		Hide : stMotorDiag_Hide_typ;
	END_STRUCT;
	stMotorSetupPageHoming_typ : 	STRUCT 
		DistanceMin : REAL; (*Axis Position Limit*)
		DistanceMax : REAL; (*Axis Position Limit*)
		Distance : REAL; (*Homing Position*)
		CmdHome : BOOL;
		HideNotReady : USINT; (*Status dp of "Axis not ready msg"*)
		LockHomeCmd : USINT;
		ActPosition : REAL; (*Axis Actual position*)
		HomingOK : USINT; (*Status of axis - homed or not*)
	END_STRUCT;
END_TYPE

(*-Motor Setup page*)

TYPE
	stMotorSetupPageLayerConWXGA_typ : 	STRUCT 
		HideLayer : ARRAY[0..4]OF USINT; (*Status Datapoint of layer*)
		LockLayerBtn : ARRAY[0..4]OF USINT; (*Lock buttons for changing layer*)
		BtnLayerSelected : ARRAY[0..4]OF USINT; (*Cmd to change layer*)
	END_STRUCT;
	stNavArrowsInternal_typ : 	STRUCT 
		menuLevelsOld : ARRAY[0..3]OF USINT; (*Variable indicating  menu level in last item of pageHistBuffer(0 is highest, 3 is lowest menu level)*)
		menuLevels : ARRAY[0..3]OF USINT; (*Variable indicating actual menu level (0 is highest, 3 is lowest menu level)*)
		activeLevelOld : USINT; (*Lovest active level in hist. buffer*)
		activeLevel : USINT; (*Currently lovest active level*)
		string1 : STRING[80]; (*Help string*)
		string2 : STRING[80]; (*Help string*)
		pToSourceString : UDINT; (*Pointer to string with actual page index*)
		lastPageInLevel : UINT; (*Last defined page index in current level*)
		pageExist : BOOL; (*Indication that currently searched page exist*)
		histBufLastIndex : USINT; (*Index fo last valid variable in history buffer*)
	END_STRUCT;
	stNavArrowsIn_typ : 	STRUCT 
		pageNavigCmd : USINT; (*Navigation btn commands 1 left; 2 right; 3 up*)
		actPage : INT; (*Actual page index*)
		pageIndexes : ARRAY[0..PAGES_ARRAY_SIZE]OF INT; (*Indexes of actually used pages*)
		pageHistoryBuffer : ARRAY[0..SIZE_PAGE_HIST_BUFFER]OF INT; (*Buffer storring pages visited in higher levels*)
	END_STRUCT;
	stNavArrowsOut_typ : 	STRUCT 
		pageNavigationError : INT; (*Error from navigation FB - now isn't any error implemented*)
		nextPage : UINT; (*Number of next caculated page for navigation algorythm, 0 - no change*)
		hideNavigationLeft : USINT; (*Status datapoint to hide navigation arrow*)
		hideNavigationRight : USINT; (*Status datapoint to hide navigation arrow*)
		hideNavigationUp : USINT; (*Status datapoint to hide navigation arrow*)
	END_STRUCT;
	stNavArrows_typ : 	STRUCT  (*Structure to handle page navigation arrows*)
		In : stNavArrowsIn_typ; (*Inputs*)
		Out : stNavArrowsOut_typ; (*Outputs*)
		Internal : stNavArrowsInternal_typ; (*Internal variables*)
	END_STRUCT;
END_TYPE

(**)

TYPE
	stParametersPage_Hide_typ : 	STRUCT 
		LabelerSettings : USINT;
		YaxisMultiDP : USINT;
		HoldingBrakeLayer : USINT;
		NoMultiturnLayer : USINT;
		MultiturnLayer : USINT;
		NoGantryActual : USINT;
		GanrtyActual : USINT;
		ParLayerCtrl : ARRAY[0..6]OF USINT; (*Visibility control for the layers*)
		LoaderPar : USINT;
	END_STRUCT;
	stParametersPage_typ : 	STRUCT 
		HiglightOrigin : BOOL;
		HiglightRef : BOOL;
		BrakeStatus : ARRAY[0..1]OF STRING[32];
		Highlight : ARRAY[0..6]OF BOOL;
		Hide : stParametersPage_Hide_typ;
		OldSetLayer : USINT; (*Set layer value from the HMI*)
		SetLayer : USINT; (*Set layer value from the HMI*)
		TorqueRatio : USINT; (*5*)
		SelectedTorque : USINT;
	END_STRUCT;
END_TYPE

(**)
(*-Production  Protocol Page *)

TYPE
	stProdProtocol_Par_typ : 	STRUCT 
		TotalProductionTime : UDINT;
		PlcRunningTime : UDINT;
		TimeUntilNextSpWarmup : UDINT; (*Time remaining until the next spindle warm up*)
	END_STRUCT;
	stProdProtocol_typ : 	STRUCT 
		ResetDaily : BOOL; (*cmd to reset the daily counter*)
		Par : stProdProtocol_Par_typ;
	END_STRUCT;
	stProgramsInspectionCMD_typ : 	STRUCT 
		IndexMinus : BOOL; (*Navigation on page - decrease index of first shown program*)
		IndexPlus : BOOL; (*Navigation on page - increase index of first shown program*)
	END_STRUCT;
	stProgramsInspectionHide_typ : 	STRUCT 
		ProgramLine : ARRAY[0..6]OF USINT; (*Hide line in on Inspection page, if program block name is empty - not used in proj.*)
		HistLayer : USINT; (*Hide layer with program block history data*)
		NavArrow : USINT; (*Hide navigation arrows*)
	END_STRUCT;
	stProgramsInspectionHist_typ : 	STRUCT 
		Data : stProgInspect_typ; (*Data shown in history buffer*)
		TimeLoc : ARRAY[0..14]OF TIMEStructure;
		TimeMain : ARRAY[0..14]OF TIMEStructure;
		StrMain : ARRAY[0..14]OF STRING[40];
		StrLoc : ARRAY[0..14]OF STRING[40];
		StrHelp : STRING[40];
		FirstTime : BOOL; (*Flag that the hist layer is displayed for the first cycle - update data*)
		FreezeValues : BOOL; (*Flag to stop updating of values*)
	END_STRUCT;
	stProgramsInspectionLock_typ : 	STRUCT  (*Locking of elements on page*)
		IndexMinus : BOOL;
		IndexPlus : BOOL;
	END_STRUCT;
END_TYPE

(*- Programs Inspection*)

TYPE
	stProgramsInspection_typ : 	STRUCT 
		StartIndex : USINT; (*Index of first block data to show - to navigate through prog. blocks. if there is more of them than lines on page*)
		HistSelectedLine : USINT;
		Data : ARRAY[0..6]OF stProgInspect_typ; (*Program inspection data - Array conected to HMI (subset of all Prog. inspection data, if there are more than is array size  - 7)*)
		Cmd : stProgramsInspectionCMD_typ; (*Commands for program inspection layer*)
		Lock : stProgramsInspectionLock_typ; (*Locking of elements on page*)
		Hide : stProgramsInspectionHide_typ; (*Status datapoints for page*)
		Hist : stProgramsInspectionHist_typ; (*History page variables*)
		ArraySizeGlobal : UINT; (*Number of elements minus 1 in gHMI.ProgInspect*)
		ArraySizeLocal : UINT; (*Number of elements minus 1 in stProgInspect.Data*)
		i : UINT; (*Help variable*)
	END_STRUCT;
	stRecipePageCmd_typ : 	STRUCT 
		Refresh : BOOL; (*Command for recipe handling*)
		Load : BOOL; (*Command for recipe handling*)
		Save : BOOL; (*Command for recipe handling*)
		Delete : BOOL; (*Command for recipe handling*)
	END_STRUCT;
	stSettingsPageLock_typ : 	STRUCT 
		btnSample1 : BOOL; (*Lock Btn*)
		InputFields : USINT; (*Lock input fields on input page*)
	END_STRUCT;
	stStartPage_typ : 	STRUCT  (*Variable for start page*)
		DT_Structure_0 : DTStructure; (*Structure with date and time*)
		DTStructureGetTime_0 : DTStructureGetTime; (*FUB: get date and time of the system*)
		BarGraphProgress : INT; (*Progres of start page bar graph*)
	END_STRUCT;
	TopMenu_typ : 	STRUCT  (*Structure for control top menu*)
		HideBtn : ARRAY[0..39]OF USINT; (*Status datapoints of coresponding menu buttons (0-9 Production, 10-19 Parameters ets...)*)
		btnMenu : ARRAY[0..3]OF BOOL; (*Top menu btn was pressed*)
	END_STRUCT;
	UserLoginAutLogOut_typ : 	STRUCT 
		TON_1 : TON; (*Time to measure no activity time*)
		VC_Handle : UDINT;
		Ready : BOOL;
		TouchStatus : UDINT;
	END_STRUCT;
	UserLoginDTstring_typ : 	STRUCT 
		Hour : STRING[11];
		Minute : STRING[11];
	END_STRUCT;
END_TYPE

(*
User Settings - Top Menu
-----------------------------------------------------------------*)
(*- User Login*)

TYPE
	UserLogin_typ : 	STRUCT  (*Variables related to the user login page*)
		btnLogIn : BOOL; (*Btn to confirm login with given password*)
		btnLogOut : BOOL; (*Btn to log out user*)
		givenPassword : STRING[20]; (*Password given by user*)
		selectedUserIndex : USINT; (*selected user index in dropdown menu*)
		wrongPassword : BOOL; (*flag that given password is wrong*)
		serviceLayerStatusDatapoint : USINT := 1; (*Status datapoint to show respective pages when service user is loged in*)
		DTStructureGetTime_0 : DTStructureGetTime;
		DateString : UserLoginDTstring_typ;
		DateStructure : DTStructure;
		alternateServPass : UINT; (*Alternative pass for the service user*)
		superAdminLayerStatusDatapoint : USINT;
		adminLayerStatusDatapoint : USINT := 1; (*Status datapoint to show respective pages when asmin user is loged in*)
		HideLogInMsg : USINT;
		HideUserMgmt : USINT;
	END_STRUCT;
END_TYPE
