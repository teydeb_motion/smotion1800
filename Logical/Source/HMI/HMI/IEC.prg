﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.7.54 SP?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Objects>
    <Object Type="File" Description="Implementation code">HMI.st</Object>
    <Object Type="File">HMI_Init.st</Object>
    <Object Type="File" Description="Local data types" Private="true">HMI.typ</Object>
    <Object Type="File" Description="Local variables" Private="true">HMI.var</Object>
    <Object Type="Package" Description="Actions for HMI program">Actions</Object>
  </Objects>
</Program>