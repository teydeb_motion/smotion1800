(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: HMI
 * File: HMI.st
 * Author: turanskyl
 * Created: October 08, 2013
 ********************************************************************
 * Implementation of program HMI
 ********************************************************************)

PROGRAM _CYCLIC
	//============================================================================
	//	FirstInPage
	//============================================================================
	FirstInPage 		 := (gHMI.General.ActualPage <> gHMI.General.OldPage) ;
	gHMI.General.OldPage := gHMI.General.ActualPage; 
	//============================================================================
	//-------------------------------------------------
	//--- Blinking Timer
	IF(TON_Blink.Q) THEN	// reset timer
		gHMI.General.Blink_0_5s := TRUE;
		TON_Blink.IN	:= FALSE;
	ELSIF(TON_Blink.ET >= T#0.5s) THEN	// change blinking state
		gHMI.General.Blink_0_5s	:= FALSE;
	ELSE	// start timer
		TON_Blink.IN := TRUE;
	END_IF;	
	TON_Blink(); 

	//-------------------------------------------------
	//--- Status datapoint for different user levels
	gHMI.General.ShowForService	     	:= HIDE_OBJ;				// Hide elements if only "Operator" level is logged in
	gHMI.General.ShowForAdmin	     	:= HIDE_OBJ;
	gHMI.General.ShowForSuperAdmin   	:= HIDE_OBJ;
	IF(gHMI.General.ActUserLevel      	= USER_SERVICE)THEN	// Show elements for "Service" operator
		gHMI.General.ShowForService	 	:= SHOW_OBJ;
	ELSIF(gHMI.General.ActUserLevel   	= USER_ADMIN)THEN	// Show elements for "Admin" operator
		gHMI.General.ShowForService	 	:= SHOW_OBJ;
		gHMI.General.ShowForAdmin	 	:= SHOW_OBJ;
	ELSIF (gHMI.General.ActUserLevel  	= USER_SUPER_ADMIN) THEN
		gHMI.General.ShowForService	    := SHOW_OBJ;
		gHMI.General.ShowForAdmin	    := SHOW_OBJ;
		gHMI.General.ShowForSuperAdmin  := SHOW_OBJ;
	END_IF
	

	//----------------------------------------------	

	//----------------------------------------------
	//---- Top menu handling
	//	 2013_10_08_LT How TO:
	//	Appearance OF Top menu is simply configured BY initializing MAX_MENU_ITEMS array TO number OF 
	//	items in coresponding columns, var MENU_ITEMS is constant AND should be always 4 as long as in
	//	template are used 4 top menu btns.
	//	In order TO modify VISU is needed TO fill out lables OF menu buttons (TextGroup Common_TopMenu) AND TO
	//	individual buttons asigned keys FOR changing page
	TopMenuHandling;	
	
	//----------------------------------------------
	//---- Global Area
	GlobaArea;	
	
	//---------------------------------------------------------
	IF(oldStep <> gHMI.General.ActualPage)THEN
		firstInStep := TRUE;
	ELSE
		firstInStep	:= FALSE;
	END_IF
	
	// MAIN CASE MACHINE wrt THE SELECTED PAGE
	CASE gHMI.General.ActualPage OF
		//**********************************************
		//**** Start page  *****************************
		//**********************************************
		PAGE_START:
			StartPage;
		

		//**********************************************
		//**** User login ******************************
		//**********************************************
		PAGE_USER:
			IF gHMI.General.ActUserLevel < 1 THEN
				stUserLogin.HideUserMgmt := HIDE_OBJ;
				stUserLogin.HideLogInMsg := DISPLAY_OBJ;
			ELSE
				stUserLogin.HideUserMgmt := DISPLAY_OBJ;
				stUserLogin.HideLogInMsg := HIDE_OBJ;
			END_IF;
			
			// if cmd to log in or out -> execute action
			IF(stUserLogin.btnLogIn = TRUE OR stUserLogin.btnLogOut = TRUE OR stUserLogin.wrongPassword = TRUE) THEN
				UserLogin;	// execute action
			END_IF
			
		//**********************************************
		//**** Date & Time *****************************
		//**********************************************	
		PAGE_DATE_AND_TIME:
			DateAndTimePage;
		
		//**********************************************
		//**** Language ********************************
		//**********************************************	
		PAGE_LANGUAGE:
//			IF (stLanguage.NextLang) THEN // next lang cmd
//				mTemp.bEvrOk := FALSE;
//				IF gHMI.General.CurrentLanguage <= MAX_LANG_NUM_MIN_1 THEN // current lang. is less than the upper limit
//					FOR mTemp.ii := gHMI.General.CurrentLanguage+1 TO MAX_LANG_NUM_MIN_1 DO // loop over the rest of the lang. which are next
//						IF gFixData.Options.SW.LanguageEnable[mTemp.ii]  THEN // check for the next enabled language
//							gHMI.General.ChangeLanguage := mTemp.ii;
//							mTemp.bEvrOk := TRUE;
//							EXIT;						
//						END_IF; // end of the the next enabled lang. check
//					END_FOR; // end of the loop over the next lang.
//					// there were no enabled languages with greater index; start from 0
//					IF mTemp.ii = MAX_LANG_NUM AND NOT mTemp.bEvrOk THEN
//						FOR mTemp.ii := 0 TO MAX_LANG_NUM_MIN_1 DO
//							IF gFixData.Options.SW.LanguageEnable[mTemp.ii] THEN // check for the next enabled language
//								gHMI.General.ChangeLanguage := mTemp.ii;
//								mTemp.bEvrOk := TRUE;
//								EXIT;						
//							END_IF; // end of the the next enabled lang. check
//						END_FOR;
//						IF mTemp.ii = MAX_LANG_NUM AND NOT mTemp.bEvrOk THEN // no enabled languages were found; set the bEvrOk flag in order not to cause cyclic time violation and set the current language to English
//							gHMI.General.ChangeLanguage := LANG_ENGLISH;
//							mTemp.bEvrOk := TRUE;
//						END_IF;
//					END_IF;
//				END_IF; // end of the check for the upper limit
//				stLanguage.NextLang := NOT mTemp.bEvrOk;
//			ELSIF (stLanguage.PrevLang) THEN // previous lang. cmd
//				mTemp.bEvrOk := FALSE;
//				IF gHMI.General.CurrentLanguage >= 0 THEN
//					FOR mTemp.ii := gHMI.General.CurrentLanguage - 1 TO 0 BY -1 DO
//						IF gFixData.Options.SW.LanguageEnable[mTemp.ii] THEN
//							gHMI.General.ChangeLanguage := mTemp.ii;
//							mTemp.bEvrOk := TRUE;
//							EXIT;
//						ELSIF mTemp.ii = 255 THEN
//							EXIT;
//						END_IF;
//					END_FOR; // end of the loop over lang. with smaller indexes
//					IF mTemp.ii = 255 AND NOT mTemp.bEvrOk THEN
//						FOR mTemp.ii := MAX_LANG_NUM_MIN_1 TO mTemp.ii = 0 BY -1 DO
//							IF gFixData.Options.SW.LanguageEnable[mTemp.ii] THEN
//								gHMI.General.ChangeLanguage := mTemp.ii;
//								mTemp.bEvrOk := TRUE;
//								EXIT;
//							END_IF;
//						END_FOR;
//						IF mTemp.ii = 0 AND NOT mTemp.bEvrOk THEN
//							gHMI.General.ChangeLanguage := LANG_ENGLISH;
//							mTemp.bEvrOk := TRUE;
//						END_IF;
//					END_IF;
//				END_IF;	
//				stLanguage.PrevLang := NOT mTemp.bEvrOk;
//		END_IF; // end of the cmd check,
		

		//**********************************************
		//**** SDM         *****************************
		//**********************************************
		PAGE_SDM:
			
		PAGE_PRODUCTION_PROTOCOL:

		    
			ProductionProt;
		//**********************************************
		//**** Network Settings ************************
		//**********************************************
		PAGE_NETWORK_SETTINGS:
			// action is called in global area - to be able to restore Default IP
		
		//**********************************************
		//**** Program Inspection **********************
		//**********************************************
		PAGE_PROGRAM_INSPECTION:
			ProgInspectionPage;
		//**********************************************
		//**** Service *********************************
		//**********************************************
		PAGE_SERVICE:

			

		//**************************************************
		//**** MotorSetupPage 
		//**************************************************	
		PAGE_MOTOR_SETUP:

			PageMotorSetup;

		//**********************************************
		//**** Motion Status ***************************
		//**********************************************
		PAGE_MOTION_STATUS:
     		MotionStatusPage;

		
		//**********************************************
		//**** System Info *****************************
		//**********************************************	
		PAGE_SYSTEM_INFORMATION:
			
			
		//**********************************************
		//**** Customer Info ***************************
		//**********************************************	
		PAGE_COMPANY_INFORMATION:
		
		
		//**********************************************
		//**** Alarms **********************************
		//**********************************************
		PAGE_ALARMS_ACTUAL:
			AlarmPage;
		
		//**********************************************
		//**** Alarm History    ************************
		//**********************************************
		PAGE_ALARMS_HISTORY:
		
			
		//**********************************************
		//**** Home Page    ****************************
		//**********************************************	
		PAGE_HOME:
			HomePage;			
		
		//**********************************************
		//**** Manual Page *****************************
		//**********************************************
		PAGE_MANUAL:
			ManualPage;
		
		

		
	END_CASE;	
	//--------- end of main case machine  ---------------------

	
	
	
	//**********************************************
	//**** Operations executed every cycle *********
	//**********************************************	
	//AutoUserLogOut; // Log out user when there is no activity for set time
	// Network settings - In global area to be able to restore default IP
	NetworkSettingsPage;	
	// Save the current language to the permanent structure
	gHMI_Permanent.Language  := gHMI.General.CurrentLanguage;	
	// Save the current unitgroup to the permanent structure
	gHMI_Permanent.Unitgroup := gHMI.General.CurrentUnit;
	
	//---------------------------------------- 
	// Hide axis detail error layer
	IF( EDGEPOS(gHMI.General.ActualPage <> PAGE_ALARMS_ACTUAL)) THEN
		stAlarms.Axis.cmdBack := FALSE;
		stAlarms.Axis.axisDetailsNumber := 99; // do not copy data from axis to hmi structure
		stAlarms.Axis.hideAxisErrDetail := 1; // hide axis layer details
	END_IF; // cmdBack = true	
	oldStep := gHMI.General.ActualPage;
	// call the timer for the start page
	TON_StartPage();
END_PROGRAM
