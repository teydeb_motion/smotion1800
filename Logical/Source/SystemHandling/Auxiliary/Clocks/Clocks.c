/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:    abdullahoglf
* Created:   November 04, 2015
**********************************************************************************
* Description:
*	- This task handles the machine operation related clock events.
*
* Parameters:
*
* History:
*		04.11.2015		V1.00	Ferhat Abdullahoglu
*			  (new)			Core implementation of the functionality.	
*		02.01.2016		V.10.0 	Ferhat Abdullahoglu
*			  (new)			Offline clock type is added
**********************************************************************************/
#include <bur/plctypes.h> 
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#ifndef _REPLACE_CONST
	#define _REPLACE_CONST
#endif




#define CLK_PRECISION_SECONDS	= 0;  /*Clock precision [1 s]*/ 
#define CLK_PRECISION_MINUTES	= 1; /*Clock precision [1 min]*/ 
#define CLK_PRECISION_HOURS 	= 2;  	/*Clock precision [1 h]*/ 
#define CLK_PRECISION_DAYS 		= 3;	/*Clock precision [1 d]*/ 



unsigned long CalcTime(RTCtime_typ *act, RTCtime_typ *old);

unsigned long uAdr, uSize;
DINT stat;

/*************************************************************************
****    	I N I T  R O U T I N E									  ****
*************************************************************************/
void _INIT ClocksInit(void)
{
	// ----------------------------
	// Access to the Program Interfaces
	// ----------------------------
	// Main Control interface
//	uAdr 	= 0;	uSize = 0;
//	stat 	= (int)(PV_xgetadr((unsigned long) &("MainContro:Main"), &uAdr, &uSize));
//	pMain	= (!stat && uAdr) == 1 ? (gMain_typ*)uAdr : 0; // assign the pointer if the address is found
	// ----------------------------
	// PLC on clock cfg
	// ----------------------------
	Clocks[CLK_TYP_PLC_ON].Enable		= 1;
	Clocks[CLK_TYP_PLC_ON].Precision	= CLK_PRECISION_HOURS;
	Clocks[CLK_TYP_PLC_ON].Flags		= CLK_FLAG_NONE;
	Clocks[CLK_TYP_PLC_ON].AlarmID		= 0;
	Clocks[CLK_TYP_PLC_ON].MaxTime		= 0;
	
	// ----------------------------
	// Production clock cfg
	// ----------------------------
	Clocks[CLK_TYP_AUTO_ON].Precision	= CLK_PRECISION_DAYS;
	Clocks[CLK_TYP_AUTO_ON].Flags		= CLK_FLAG_NONE;
	Clocks[CLK_TYP_AUTO_ON].AlarmID		= 0;
	Clocks[CLK_TYP_AUTO_ON].MaxTime		= 0;
	
	// ----------------------------
	// Auto axis disable clock cfg
	// ----------------------------
	Clocks[CLK_TYP_AUTO_AX_DISABLE].Precision 	= CLK_PRECISION_MINUTES;
	Clocks[CLK_TYP_AUTO_AX_DISABLE].Flags     	= CLK_FLAG_RESET_DISABLE | CLK_FLAG_RESET_MAX | CLK_FLAG_TIMEOUT;
	Clocks[CLK_TYP_AUTO_AX_DISABLE].MaxTime		= 15;
	
	// ----------------------------
	// FixData backup clock cfg
	// ----------------------------
	Clocks[CLK_TYP_FIXDAT_BACKUP].Precision		= CLK_PRECISION_DAYS;
	Clocks[CLK_TYP_FIXDAT_BACKUP].Flags			= CLK_FLAG_RESET_DISABLE | CLK_FLAG_RESET_MAX | CLK_FLAG_TIMEOUT | CLK_FLAG_OFFLINE;
	Clocks[CLK_TYP_FIXDAT_BACKUP].MaxTime		= 7; // 1 week
	
	// ----------------------------
	// RecData backup clock cfg
	// ----------------------------
	Clocks[CLK_TYP_RECDAT_BACKUP].Precision		= CLK_PRECISION_DAYS;
	Clocks[CLK_TYP_RECDAT_BACKUP].Flags			= CLK_FLAG_RESET_DISABLE | CLK_FLAG_RESET_MAX | CLK_FLAG_TIMEOUT | CLK_FLAG_OFFLINE;
	Clocks[CLK_TYP_RECDAT_BACKUP].MaxTime		= 1; // 1 day
	
	// ----------------------------
	// Get the actual time 
	// ----------------------------
	RTC_gettime(&OldTime);
	
	// ----------------------------
	// Offline clocks handling
	// ----------------------------
	RTC_gettime(&ActTime);
	// loop over all timers 
	for (ii = 0; ii < CLK_MAX_NO_CLOCKS; ii++)
	{
		// check if the given timer is configured to work Offline
		if ( (Clocks[ii].Flags & CLK_FLAG_OFFLINE) != 0 )
		{
			// calculate how long the clock would have run when the PLC was off
			long runTimeOffline = CalcTime(&ActTime, &OldTimeRetain);
			Clocks[ii].RunningTime += runTimeOffline;     	
		} // end of the clock cfg check
	} // end of the loop over all timers
	
	// ----------------------------
	// Init the local variables
	// ----------------------------
	ii = 0; cnt = 0;
} // end of the INIT routine

/*************************************************************************
****    	C Y C L I C  R O U T I N E								  ****
*************************************************************************/
void _CYCLIC ClocksCyclic(void)
{
	unsigned char event;
	unsigned long seconds;
	unsigned short status;
	
	cnt = (cnt+1) % 10; // task runs at 100ms task class -> so every 10 cycle is a second
	// ----------------------------
	// check for seconds
	// ----------------------------
	if (!cnt)	// 1 second elapsed
	{
		status							= RTC_gettime(&ActTime);
		seconds							= CalcTime(&ActTime, &OldTime);
		OldTime							= ActTime;
		OldTimeRetain					= ActTime;
		/* --- */
		Clocks[CLK_TYP_PLC_ON].Enable	= 1;
		/* --- */
		Clocks[CLK_TYP_AUTO_ON].Enable	= 1/*( pMain->Out.MainStep == MAIN_AUTO_RUNNING )*/;
		
		// ----------------------------
		// loop over all clocks
		// ----------------------------
		for(ii=0; ii<CLK_MAX_NO_CLOCKS; ii++)
		{
			event = 0;
			// ----------------------------
			// check if the timer is enabled
			// -------------------------
			if (Clocks[ii].Enable) // timer is enabled
			{
				Clocks[ii].RunningTime += seconds;
				// ----------------------------
				// case machine for different precision types
				// ----------------------------
				switch (Clocks[ii].Precision)
				{
					case CLK_PRECISION_SECONDS:
						Clocks[ii].TotalTime 	+= Clocks[ii].RunningTime;
						Clocks[ii].RunningTime	= 0;
						event					= 1;
						break;
					
					case CLK_PRECISION_MINUTES:
						if (Clocks[ii].RunningTime >= 60)
						{
							Clocks[ii].TotalTime	+= 1;
							Clocks[ii].RunningTime	-= 60;
							event					= 1;
						} break;
					
					case CLK_PRECISION_HOURS:
						if (Clocks[ii].RunningTime >= 3600)
						{
							Clocks[ii].TotalTime	+= 1;
							Clocks[ii].RunningTime	-= 3600;
							event					= 1;
						} break;
					
					case CLK_PRECISION_DAYS:
						if (Clocks[ii].RunningTime >= 86400)
						{
							Clocks[ii].TotalTime	+= 1;
							Clocks[ii].RunningTime -= 86400;
							event					= 1;
						} break;
				} // end of the case machine for different clock precision types
				
				// ----------------------------
				// clock event handling
				// ----------------------------
				if (event && (Clocks[ii].Flags != CLK_FLAG_NONE))
				{
					// ----------------------------
					// check if the MaxTime is exceeded
					// ----------------------------
					if (Clocks[ii].TotalTime >= Clocks[ii].MaxTime)
					{
						// ----------------------------
						// check flag cfg
						// ----------------------------
						if ( (Clocks[ii].Flags & CLK_FLAG_ALARM_MAX) != 0 )	Clocks[ii].AlarmIDOut	= Clocks[ii].AlarmID;
						if ( (Clocks[ii].Flags & CLK_FLAG_TIMEOUT)   != 0 )	Clocks[ii].Timeout		= 1;
						if ( (Clocks[ii].Flags & CLK_FLAG_RESET_MAX) != 0 )	Clocks[ii].TotalTime	= 0;
						
					} // end of MaxTime check
				} // end of clock event handling in case there is an event defined
				
			}
			else // clock is disabled
			{
				// ----------------------------
				// check if the timer should be reset when disabled
				// ----------------------------
				if ( (Clocks[ii].Flags & CLK_FLAG_RESET_DISABLE) != 0 )	
				{
					Clocks[ii].TotalTime 	= 0;
					Clocks[ii].RunningTime 	= 0;
					Clocks[ii].Timeout		= 0;
				}
			} // end of clock enable check
		} // end of the loop over all clocks		
	} // end of check for 1 second 
} // end of the CYCLIC routine

/*************************************************************************
****	   Function Definitions							 			  ****
*************************************************************************/
unsigned long CalcTime(RTCtime_typ *act, RTCtime_typ *old)
{
	long elapsedTime = 0;
	if (act->day > old->day)
	{
		elapsedTime  = ((act->day - old->day) * 24 * 60 * 60)
						+ ((act->hour - old->hour) * 60 * 60)
						+ ((act->minute - old->minute) * 60)
						+ (act->second - old->second);
			
		return elapsedTime;
	}
	else if (act->day < old->day)
	{
		elapsedTime  = ((30 + act->day - old->day) * 24 * 60 * 60)
						+ ((act->hour - old->hour) * 60 * 60)
						+ ((act->minute - old->minute) * 60)
						+ (act->second - old->second);	
	
		return elapsedTime;
	}
	else if (act->hour > old->hour)
	{
		elapsedTime	 = 	((act->hour - old->hour) * 60 * 60)
						+ ((act->minute - old->minute) * 60)	
						+ (act->second - old->second);
		return elapsedTime;
	}  
	else if (act->hour < old->hour)
	{
		elapsedTime	 = 	((24 + act->hour - old->hour) * 60 * 60)
						+ ((act->minute - old->minute) * 60)	
						+ (act->second - old->second);
		return elapsedTime;
	}
	else if (act->minute > old->minute)
	{
		return (long)((act->minute - old->minute) * 60 + (act->second - old->second));
	}
	else if (act->minute < old->minute)
	{
		return (long)(((60 + act->minute) - old->minute) * 60 + (act->second - old->second));
	}
	else if (act->second > old->second)
	{
		return (long)(act->second - old->second);
	} 
	else
	{
		return 0;
	}
} // end of CalcTime implementation