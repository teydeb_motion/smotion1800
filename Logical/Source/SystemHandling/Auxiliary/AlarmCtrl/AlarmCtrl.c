/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:    abdullahoglf
* Created:   September 17, 2015
**********************************************************************************
* Description: 
*	- This task handles all the alarm activities (setting, resetting alarms + 
*		setting the reaction that needs to be taken)
*
*   - Pay attention to the "unitIndex" input of the SET_ALARM_SINGLE function.
*     	All the alarms are gathered under the MpAlarmBasicUIConnect.AlarmImage array.
*	  	These alarms are coming from all the tasks; by the SET_ALARM_SINGLE function
*     	necessary offset is applied depending on the unitIndex. Finally the required
*	  	alarm is set by MpAlarmSet function. The setting and resetting of the alarm
*	  	and acknowledge arrays are handled by the mapp components.
*
*   - In the current configuration the SW units are defined as:
*     	unitIndex 0: General machine errors
*     	unitIndex 1: Sealing ctrl task
*     	unitIndex 2: Manual handling task
*		unitIndex 3: Chin task
*		unitIndex 4: Rubber task
*       unitIndex 4: Edge task
*   - If there is a need to add another SW unit, say another additional machine unit,
*     	for this unit the alarms should be handled with the same fashion.
*   
* History:
*		17.09.2015	V1.0.0	Ferhat Abdullahoglu
*			(new)	Core implementation of the functionality
**********************************************************************************/
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif
#include <bur/plctypes.h> 
#include "Macros.h"

#define MU_MAIN   	    0
#define	MU_SEALING	    1
#define MU_MANUAL		2

DINT stat;
BOOL bNewAlm, bResetOld;
unsigned char uNewAlmChkLvl, jj;
unsigned long dWarImgRef, dWarImgSize, uAdr, uSize; // will be filled in by the PV_xgetadr

/*************************************************************************
****		I N I T  R O U T I N E								      ****
*************************************************************************/
void _INIT AlarmCtrlInit(void)
{
	// ----------------------------------------
	// access to the Main Control interface
	//----------------------------------------
	uAdr 	= 0;	uSize = 0;
	stat 	= (int)(PV_xgetadr((unsigned long) &("gUHandle"), &uAdr, &uSize));
	pMain	= (!stat && uAdr) == 1 ? (UnitHandleType*) uAdr : 0; // assign the pointer if the address is found
	//----------------------------------------
	

	// ----------------------------------------
	// access to the SEALING EM Control interface
	//----------------------------------------
	uAdr 		= 0;	uSize = 0; 
	stat 		= (int)(PV_xgetadr((unsigned long) &("gUHandle.Unit.Alarm.MainErrors"), &uAdr, &uSize));
	pMainErrors	= (!stat && uAdr) == 1 ? (UMainErrorType*) uAdr : 0; // assign the pointer if the address is found
	//----------------------------------------
	

	//----------------------------
	// access to the Warning image adrress
	//----------------------------
	dWarImgRef 	= 0;	 dWarImgSize = 0;
	stat		= (int)(PV_xgetadr((unsigned long) &("gUHandle.Unit.Alarm.Warnings"), &dWarImgRef, &dWarImgSize));
	pWarnings	= (!stat && dWarImgRef) == 1 ? (MpAlarmBasicUIConnect20Type*) dWarImgRef : 0; // assign the pointer if the address is obtained succesfully 
	// ----------------------------
	
	// save the mplink
	MpLink = (unsigned long) &gLinkAlarm;
	// ----------------------------
	// cfg MpAlarmBasic
	MpAlarmBasic_0.Enable 	= 1;
	MpAlarmBasic_0.MpLink 	= (MpComIdentType*) MpLink;
	// ----------------------------
	
	// ----------------------------
	// cfg MpAlarmBasicUI for user alarmsrx
	MpAlarmBasicUISetup.AlarmImage			= (unsigned long)&(MachineAlm.AlarmImage);
	MpAlarmBasicUISetup.AcknowledgeImage	= (unsigned long)&(MachineAlm.AcknowledgeImage);
	MpAlarmBasicUISetup.AddInfo				= (unsigned long)&(MachineAlm.AddInfo);
	MpAlarmBasicUISetup.AddInfoStringSize	= sizeof(MachineAlm.AddInfo[0]);
	MpAlarmBasicUISetup.ImageSize			= sizeof(MachineAlm.AlarmImage);
	//
	MpAlarmBasicUI_0.MpLink					= (MpComIdentType*) MpLink;
	MpAlarmBasicUI_0.Mode					= mpALARM_BASIC_UI_MODE_USER;
	// ----------------------------
	
	// ----------------------------
	// cfg MpAlarmBasicUI for the axes
	for (ii = 0; ii <= MAX_AX_TOT_MINUS_ONE ; ii++)
	{
		MpAlarmBasicUISetupAx[ ii ].AlarmImage		 	= (unsigned long)&(AxAlm[ ii ].AlarmImage);
		MpAlarmBasicUISetupAx[ ii ].AcknowledgeImage	= (unsigned long)&(AxAlm[ ii ].AcknowledgeImage);
		MpAlarmBasicUISetupAx[ ii ].AddInfo				= (unsigned long)&(AxAlm[ ii ].AddInfo);
		MpAlarmBasicUISetupAx[ ii ].AddInfoStringSize   = sizeof(AxAlm[ii].AddInfo[0]);
		MpAlarmBasicUISetupAx[ ii ].ImageSize			= sizeof(AxAlm[ii].AlarmImage);
		//
		MpAlarmBasicUI_Ax[ ii ].MpLink					= gUHandle.Ax[ii].MpLink;
		MpAlarmBasicUI_Ax[ ii ].Mode					= mpALARM_BASIC_UI_MODE_MAPP;	
	}
	



	// ----------------------------
} // end of the INIT routine

/*************************************************************************
****    	C Y C L I C  R O U T I N E								  ****
*************************************************************************/
void _CYCLIC AlarmCtrlCyclic(void)
{ 
	bClass0 = 0;	bClass1 = 0;	bClass2 = 0;
	bClass3 = 0;    bClass4 = 0;	bBuzzer = 0;	 // reset the alarm class flags;
	bClass5 = 0;	bClass6 = 0; 
	
	//---------------------------------------------------//
	//          MAIN TASK ERROR HANDLING                 //
	//---------------------------------------------------//
	for (ii = 0; ii < ERR_MU1_MAX_NO; ii++)
	{
		if (pMain) // check the reference
		{
		
			if (pMain->Unit.Alarm.MainErrors.ActiveImg[ ii ] > OldMainAlm[ ii ] ) // there is an active alarm
			{
				
				SET_ALARM(MU_MAIN, ii, pMain->Unit.Alarm.MainErrors.AddInfo[ ii ]);
				uNewAlmChkLvl = 1; // check if there is a new alarm
			}
		} /* if (pMain) */
	}
	//---------------------------------------------------//



	
	
	
	//---------------------------------------------------//
	
	//---------------------------------------------------//
	// check global warnings
	//---------------------------------------------------//
	for (ii = 0; ii < sizeof(pWarnings->AlarmImage); ii++)
	{
		if (pWarnings)
			if (pWarnings->AlarmImage[ ii ])
				SET_ALARM(255, ii, pWarnings->AddInfo[ ii ]);
	}
	
	
	
	
	
	// ----------------------------
	// RESET errors upon request
	//------------------------------
	if (pMain)
	{
		if (pMain->Unit.Alarm.ErrorReset)	
		{		
			// ----------------------------
			// send ack. cmd to the axes
			for (ii = 0; ii <= MAX_AX_TOT_MINUS_ONE; ii++)
			{
				gMpAxisBasic_0[ ii ].ErrorReset 	= (gMpAxisBasic_0[ ii ].StatusID != 0) 		&& (gMpAxisBasic_0[ ii ].Enable);
			}
			
		
			RESET_ALARM_ALL();		
		}
	}


	
	// --------------------------------------------------------
    //   ALARM HANDLING COMPOONENTS CONFIGURATION
	// --------------------------------------------------------
	// call the components
	MpAlarmBasicUI_0.UISetup = MpAlarmBasicUISetup;
	MpAlarmBasic(&MpAlarmBasic_0);
	MpAlarmBasicUI(&MpAlarmBasicUI_0);
	//
	gMpAlarmActive 			= MpAlarmBasic_0.Active;
	MpAlarmBasicUI_0.Enable	= gMpAlarmActive;
	//
	for (ii = 0; ii <= MAX_AX_TOT_MINUS_ONE; ii++)
	{
		MpAlarmBasicUI_Ax[ ii ].Enable	 		= (gMpAlarmActive && gURecData.AxEnabled[ii] ) ;
		MpAlarmBasicUI_Ax[ ii ].UISetup			= MpAlarmBasicUISetupAx[ ii ];		
		MpAlarmBasicUI(&MpAlarmBasicUI_Ax[ ii ]);
	}
	
	// check the set reaction bits
	CHK_ALARM(MpAlarmBasic_0.Reaction);
	
	// check new alarm
	CHK_NEW_ALARM(uNewAlmChkLvl);
	// ----------------------------		

	// ----------------------------
	// alarm images from the previous cycle
	if (gMpAlarmActive)
	{   	
		if (pMain)			memcpy( (unsigned long)&(OldMainAlm), 	  		(unsigned long)&(pMain->Unit.Alarm.MainErrors.ActiveImg),     		sizeof(OldMainAlm));  
	}
	
	// ----------------------------
	
	if (!MpAlarmBasic_0.ActiveAlarms &&	!MpAlarmBasic_0.PendingAlarms) {
		TON_ResetOld.PT = 500;
		TON_ResetOld.IN = 1;
		bResetOld		= 1;
	} else
		TON_ResetOld.IN = 0;
	
	if ( TON_ResetOld.Q && bResetOld )
	{
		bResetOld = 0;	
		memset( (unsigned long)&(OldMachineAlm.AlarmImage), 0, sizeof(OldMachineAlm.AlarmImage) );
	}
	// ----------------------------
	if (pMain)
	{   
		pMain->Unit.Alarm.NewAlm    	    	= bNewAlm;
		pMain->Unit.Alarm.Reaction[0]	    	= bClass0;
		pMain->Unit.Alarm.Reaction[1]		    = bClass1;
		pMain->Unit.Alarm.Reaction[2]		    = bClass2;
		pMain->Unit.Alarm.BuzzerEnable      	= bBuzzer;	
	}	
	TON(&TON_ResetOld);
} // end of the CYCLIC routine

/*************************************************************************
****    	E X I T  R O U T I N E									  ****
*************************************************************************/
void _EXIT AlarmCtrlExit(void)
{
	// ----------------------------
	// Reset the enables of the used mapp components
	MpAlarmBasic_0.Enable 		= 0;
	MpAlarmBasicUI_0.Enable		= 0;
	for (ii = 0; ii <= MAX_AX_TOT_MINUS_ONE; ii++)
	{
		MpAlarmBasicUI_Ax[ii].Enable = 0;	
	}
	// ----------------------------
} 
// end of the Exit routine
