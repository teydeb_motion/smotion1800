/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:    abdullahoglf
* Created:   September 17, 2015
**********************************************************************************
* Description: 
*   
* History:
**********************************************************************************/

/*************************************************************************
**** 							macros							      ****
*************************************************************************/
#define SET_ALARM(unitIndex, jj, addInfo)											\
{																					\
	unsigned char almIndex = jj;													\	
	/* Set the correct alarm index depending the machine unit */					\
	if 		( unitIndex == 0 )		almIndex = almIndex;							\
	else if ( unitIndex == 1 )		almIndex += ERR_MU1_MAX_NO;						\
	else if ( unitIndex == 255 )	almIndex += ERR_WAR_IMG_START_IDX;				\ 
	/* Set the active alarms on the Visu */											\
	if ( MpLink ) { /* check if the MpLink exists */								\
		stat =	MpAlarmSet( MpLink, almIndex, addInfo );							\
	} /* end of the MpLink check */													\
}                                                                                    
#define RESET_ALARM_ALL()																							\
{		                                                                                                            \
     																									        	\
	unsigned char almReset = 0;																						\
	/* Reset the alarm images of the sub units */																	\
	memset((unsigned long)&( pMain->Unit.Alarm.MainErrors.ActiveImg),  0, sizeof(pMain->Unit.Alarm.MainErrors.ActiveImg));	\
	memset((unsigned long)&( pMain->Unit.Alarm.MainErrors.AddInfo),    0, sizeof(pMain->Unit.Alarm.MainErrors.AddInfo));	\
	memset((unsigned long)&( pWarnings->AlarmImage),0, sizeof(pWarnings->AlarmImage));	\
	memset((unsigned long)&( pWarnings->AddInfo),   0, sizeof(pWarnings->AddInfo));		\
	/* Reset all alarms */																							\
	if ( MpLink ) /* check if MpLink exists */																		\			
	{																												\
		for (almReset = 0; almReset < ERR_MAX_CNT; almReset++)														\
		{																											\
	 		stat = MpAlarmReset( MpLink, almReset );																\
		}																											\
	} /* end of MpLink check */																						\
    pMain->Unit.Alarm.MainErrors.Error		= 0;                										\
	pMain->Unit.Alarm.MainErrors.ErrorID	= 0;					     								\
    pMain->Unit.Alarm.ErrorReset                        = 0;             										    \
	pMain->Unit.Alarm.WarningReset                      = 0;             										    \
	bNewAlm				= 0;														\
}                                                                                                                               
#define CHK_ALARM(reaction)						\	
{												\
	bClass0 |= ((ALM_CLASS0 & reaction) != 0);	\
	bClass1 |= ((ALM_CLASS1 & reaction) != 0);	\
	bClass2 |= ((ALM_CLASS2 & reaction) != 0);	\
	bBuzzer |= ((ALM_BUZZER & reaction) != 0);	\
}	
#define CHK_NEW_ALARM(uCheck) 																	\
{																								\
	unsigned char imgIdx = 0;																	\
	unsigned char almIdx = 0;																	\
	if (uCheck == 1)																			\
	{																							\	
		uCheck  = 0;																			\
		bNewAlm = 0;																			\		
		/* Compare the axis alarm image arrays */												\
		for (imgIdx = 0; imgIdx <= MAX_AX_TOT_MINUS_ONE; imgIdx++)										\
		{																						\
			for (almIdx = 0; almIdx < sizeof(AxAlm[0].AlarmImage); almIdx++)					\
			{																					\
				/* If there is a new alarm in the axis system set the flag and exit the loop */	\
				if ( OldAxAlm[imgIdx].AlarmImage[almIdx] < AxAlm[imgIdx].AlarmImage[almIdx] )	\
				{																				\
					bNewAlm = 1;																\
					break;																		\
				}																				\
			}																					\
		}																						\
		/* Compare the general machine alarm image array */										\
		for (almIdx = 0; almIdx < sizeof(MachineAlm.AlarmImage); almIdx++)						\
		{																						\
			if ( OldMachineAlm.AlarmImage[almIdx] < MachineAlm.AlarmImage[almIdx] )				\
			{																					\
				bNewAlm = 1;																	\
				break;																			\
			}																					\
		}																						\
		memcpy( (unsigned long)&(OldMachineAlm.AlarmImage), (unsigned long)&(MachineAlm.AlarmImage), sizeof(OldMachineAlm.AlarmImage) ); \
	}	\
}       