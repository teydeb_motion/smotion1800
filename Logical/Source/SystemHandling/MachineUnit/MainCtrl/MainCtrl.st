(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    tufekcio
 * Created:   January 16, 2017/4:28 PM 
 *********************************************************************************)

PROGRAM _INIT
	gUHandle.Unit.Mode.PositionControl  := TRUE ; 
	
	(* Timeouts *)
	Loc.Time.OutHoming	:= TIMEOUT_HOMING;
	Loc.Time.OutParking	:= TIMEOUT_PARKING;
	Loc.Time.OutPowerUp	:= TIMEOUT_PWR_UP;
	Loc.Time.OutBootUp  := 60000;
	mTemp.bFirstPowerUp := TRUE; 
END_PROGRAM

PROGRAM _CYCLIC
	
	// ================================================================================== //
	//				      M A I N    C O N T R O L    S E Q U E N C E                     // 
	// ================================================================================== //
	CASE Loc.MainStep OF 
		//==================================================================================
		MAIN_INIT:
		//==================================================================================
			//-------
			InitAx;
			//-------
			
			
			IF gUHandle.Unit.Status.Initialized THEN 
				Loc.MainStep := MAIN_WAIT_CMD;
			END_IF ;
		
		//==================================================================================
		MAIN_WAIT_CMD:
		//==================================================================================
			
			//-------
			WaitCmd;
			//-------
			
		//================================================================================== 
		MAIN_POWER_ON:
		//==================================================================================	
			//-------
			PowerUp;
			//-------
			IF mTemp.LocSubDone THEN 
				mTemp.LocSubDone 	:= FALSE;
				Loc.MainStep 		:= MAIN_WAIT_CMD;
				mTemp.PowerStep 	:= MAIN_POWER_START;
			ELSIF (*TimeOver(ADR(Loc.Time.OutPowerUp))*) FALSE THEN
				//Set Alarm // Time
			END_IF ; 
		
		//==================================================================================
		MAIN_HOME:
		//==================================================================================	
	
			IF gUHandle.Simulation THEN
				FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
					gMpAxisBasicPar[mTemp.ii].Home.Mode    := 	mpAXIS_HOME_MODE_DIRECT;
				END_FOR;
			END_IF  ;		
		
			//-------
			HomeProcess;
			//-------
			
			IF mTemp.LocSubDone THEN 
				mTemp.LocSubDone 	:= FALSE;
				Loc.MainStep 		:= MAIN_WAIT_CMD;
				mTemp.HomeStep 		:= MAIN_HOME_START;
			ELSIF (*TimeOver(ADR(Loc.Time.OutHoming))*) FALSE THEN
				//Set Alarm // Time
			END_IF ; 

		

		//==================================================================================
		MAIN_MANUAL_MODE:
		//==================================================================================	
			//-------	
			ManualMove;
			//-------
			FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
				IF gMpAxisBasic_0[mTemp.ii].Info.PLCopenState = mpAXIS_STANDSTILL 	 THEN 
					mTemp.Flag := TRUE ;
				ELSE 
					mTemp.Flag := FALSE ;
				END_IF;
				
			END_FOR;
			
			IF  gHMI.General.ActualPage <>  500  AND mTemp.Flag THEN 
				Loc.MainStep := MAIN_WAIT_CMD;
				mTemp.JogAxes := FALSE ;
			END_IF ; 
		//==================================================================================
		MAIN_MULTI_ENC_OFFSET:
		//==================================================================================
		
			mTemp.ii 	:= gHMI.DrvPar.SelectedAxIdx;
			IF mTemp.FirstInStep OR NOT gMpAxisBasic_0[ mTemp.ii ].PowerOn THEN // first in the step
				
			ELSE // after the first time in the step
				(* --- *)
				MultiturnEncSettings;
				(* --- *)
		END_IF;	// end of the step	
	END_CASE ; 
	// ================================================================================== //
	
	IF mTemp.OldMainStep <>  Loc.MainStep THEN 
		mTemp.FirstInStep 	:= TRUE;
		Loc.Time.OutHoming	:= TIMEOUT_HOMING;
		Loc.Time.OutParking	:= TIMEOUT_PARKING;
		Loc.Time.OutPowerUp	:= TIMEOUT_PWR_UP;
	ELSE 
		mTemp.FirstInStep := FALSE;
	END_IF ; 
	
	mTemp.OldMainStep :=  Loc.MainStep;
END_PROGRAM
