
ACTION ManualMove: 
	// Manual mode handle // 
	// Checks controller mode - Current Controller or Position
	IF gUHandle.Unit.Mode.PositionControl THEN 
		FOR mTemp.ii  := 0 TO  MAX_AX_TOT_MINUS_ONE DO		
			// Parameters Update 
			IF 	gUFixData.Ax[mTemp.ii].AxPar.Jog.Velocity <> gMpAxisBasicPar[mTemp.ii].Jog.Velocity THEN 
				gMpAxisBasicPar[mTemp.ii].Jog.Velocity     := gUFixData.Ax[mTemp.ii].AxPar.Jog.Velocity;
				gMpAxisBasicPar[mTemp.ii].Jog.Acceleration := gUFixData.Ax[mTemp.ii].AxPar.Jog.Velocity * 4;
				gMpAxisBasicPar[mTemp.ii].Jog.Deceleration := gUFixData.Ax[mTemp.ii].AxPar.Jog.Velocity * 4;
				gMpAxisBasic_0[mTemp.ii].Update            := TRUE; 
			END_IF;
			
			IF gMpAxisBasic_0[mTemp.ii].UpdateDone THEN 
				gMpAxisBasic_0[mTemp.ii].Update := FALSE; 
			END_IF;
		
			IF EDGEPOS(gUHandle.Unit.Cmd.Manual.JogNeg[mTemp.ii]) OR EDGEPOS(gUHandle.Unit.Cmd.Manual.JogPos[mTemp.ii]) THEN 
				mTemp.JogAxes := TRUE;
			END_IF; 
			
			IF mTemp.JogAxes AND NOT gMpAxisBasic_0[mTemp.ii].Update THEN 		
				IF NOT  gMpAxisBasic_0[mTemp.ii].PowerOn AND gMpAxisBasic_0[mTemp.ii].Active THEN  
					gMpAxisBasic_0[mTemp.ii].Power := TRUE;
				ELSIF gMpAxisBasic_0[mTemp.ii].PowerOn AND NOT gMpAxisBasic_0[mTemp.ii].IsHomed  THEN 
					IF gUHandle.Simulation THEN
						gMpAxisBasicPar[mTemp.ii].Home.Mode    := 	mpAXIS_HOME_MODE_DIRECT;
					ELSE 
						gMpAxisBasicPar[mTemp.ii].Home.Mode    := 	mpAXIS_HOME_MODE_DIRECT;
					END_IF;	
					IF mTemp.ii <> 0 THEN 
						gMpAxisBasic_0[mTemp.ii].Home := TRUE;
					END_IF ; 
					
					// If axis is ready yo move 
				ELSIF gMpAxisBasic_0[mTemp.ii].PowerOn AND  gMpAxisBasic_0[mTemp.ii].IsHomed  THEN
					gMpAxisBasic_0[mTemp.ii].Home := FALSE;	
					
					// Jogging  and move absolute Functinality
					gMpAxisBasic_0[mTemp.ii].JogNegative  :=  gUHandle.Unit.Cmd.Manual.JogNeg[mTemp.ii];
					gMpAxisBasic_0[mTemp.ii].JogPositive  :=  gUHandle.Unit.Cmd.Manual.JogPos[mTemp.ii];
					IF gUHandle.Unit.Cmd.Manual.GoPos[mTemp.ii] AND NOT gUHandle.Unit.Cmd.Manual.JogPos[mTemp.ii] AND NOT gUHandle.Unit.Cmd.Manual.JogNeg[mTemp.ii] THEN 
						gMpAxisBasic_0[mTemp.ii].MoveAbsolute := TRUE;
					ELSE 
						gMpAxisBasic_0[mTemp.ii].MoveAbsolute := FALSE;
					END_IF
					
					IF gMpAxisBasic_0[mTemp.ii].Position = gMpAxisBasicPar[mTemp.ii].Position  AND gMpAxisBasic_0[mTemp.ii].MoveAbsolute THEN 
						gMpAxisBasic_0[mTemp.ii].MoveAbsolute 		:= FALSE; 
						gUHandle.Unit.Cmd.Manual.GoPos[mTemp.ii] 	:= FALSE;
					END_IF; 				
				END_IF;	
			END_IF;
		END_FOR;
	END_IF; 	
END_ACTION
