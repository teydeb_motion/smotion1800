
ACTION DriveInitX: 
	//========================================================================
	CASE mTemp.LocInitStep OF 		
		//========================================================================
		GLOBAL_INIT:
		//========================================================================
			gDriveParInitX_0.inExecute 	:= TRUE ; 
			mTemp.LocInitStep  		 	:=  GLOBAL_W_INIT;
		
		//========================================================================
		GLOBAL_W_INIT:
		//========================================================================
		
			IF gDriveParInitX_0.outStatus = ERR_OK THEN 
				gDriveParInitX_0.inExecute				:= FALSE;
				gUHandle.Unit.Status.DriveInitDone		:= TRUE;
//				gUHandle.Unit.InitilizeAxis             := FALSE;
				mTemp.InitStep 					    	:= STATE_INIT_DONE;	
			END_IF;
		
	END_CASE ;
	//========================================================================
END_ACTION
