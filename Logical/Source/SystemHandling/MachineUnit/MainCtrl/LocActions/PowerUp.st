(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    tufekcio
 * Created:   January 16, 2017/4:58 PM 
 *********************************************************************************)
ACTION PowerUp: 
	
	CASE mTemp.PowerStep OF
		//=======================================================================
		MAIN_POWER_START:
		//=======================================================================

			FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
				IF NOT gMpAxisBasic_0[mTemp.ii].PowerOn AND gMpAxisBasic_0[mTemp.ii].Active AND gMpAxisBasic_0[mTemp.ii].Info.ReadyToPowerOn  THEN 
					gMpAxisBasic_0[mTemp.ii].Power  := TRUE ;
				END_IF 
			END_FOR  ;
			mTemp.PowerStep := MAIN_POWER_PROCESS;
		
		
		//=======================================================================
		MAIN_POWER_PROCESS:
		//=======================================================================	
			
			mTemp.Flag := TRUE ;
			FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
				IF gMpAxisBasic_0[mTemp.ii].Active AND NOT gMpAxisBasic_0[mTemp.ii].PowerOn THEN
					mTemp.Flag := FALSE ;
					EXIT;
				END_IF ; 
			END_FOR ; 
			IF mTemp.Flag THEN 
				 mTemp.PowerStep := MAIN_POWER_DONE;
			END_IF ; 
		
		//=======================================================================
		MAIN_POWER_DONE:
		//=======================================================================
		
			mTemp.LocSubDone 	:= TRUE ;
            
	END_CASE;
	// Step Status 
	IF mTemp.OldPowerStep <>  mTemp.PowerStep THEN 
		mTemp.FirstInLocStep := TRUE ;
	ELSE 
		mTemp.FirstInLocStep := FALSE ;
	END_IF ; 
	
	mTemp.OldPowerStep :=  mTemp.PowerStep;
END_ACTION
