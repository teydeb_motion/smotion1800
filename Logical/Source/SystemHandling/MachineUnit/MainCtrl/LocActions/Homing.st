(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    tufekcio
 * Created:   January 16, 2017/6:25 PM 
 *********************************************************************************)

ACTION HomeProcess: 
	
	CASE mTemp.HomeStep OF
		//=======================================================================
		MAIN_HOME_START:
		//=======================================================================		
			FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
				IF NOT gMpAxisBasic_0[mTemp.ii].IsHomed AND gMpAxisBasic_0[mTemp.ii].Active AND gMpAxisBasic_0[mTemp.ii].PowerOn  THEN 
					gMpAxisBasic_0[mTemp.ii].Home  := TRUE ;
				END_IF 
			END_FOR  ;
			mTemp.HomeStep := MAIN_HOME_PROGRESS;	
		
		//=======================================================================
		MAIN_HOME_PROGRESS:
		//=======================================================================		
			mTemp.Flag := TRUE ;
			FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
				IF gMpAxisBasic_0[mTemp.ii].Active AND NOT gMpAxisBasic_0[mTemp.ii].IsHomed THEN
					mTemp.Flag := FALSE ;
					EXIT;
				END_IF ; 
			END_FOR ; 
			IF mTemp.Flag THEN 
				mTemp.HomeStep := MAIN_HOME_DONE;
				FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO
					gMpAxisBasic_0[mTemp.ii].Home  := FALSE ;
				END_FOR 
			END_IF 
		 		
		//=======================================================================
		MAIN_HOME_DONE:
		//=======================================================================
			mTemp.LocSubDone 	:= TRUE ;
	END_CASE;	
	//=======================================================================	
	
	// Step Status 
	IF mTemp.OldHomeStep <>  mTemp.HomeStep THEN 
		mTemp.FirstInLocStep := TRUE ;
	ELSE 
		mTemp.FirstInLocStep := FALSE ;
	END_IF ; 
	
	mTemp.OldHomeStep :=  mTemp.HomeStep;
	
END_ACTION
