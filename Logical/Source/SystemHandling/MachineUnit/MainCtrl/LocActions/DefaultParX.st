
ACTION DefaultParX: 
	mTemp.Flag	:= TRUE;
	gMpAxisBasicConfig_0[mTemp.ii].Enable	:= TRUE;
	gMpAxisBasicConfig_0[mTemp.ii].Load		:= TRUE;		
	
	FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
		IF  gMpAxisBasicConfig_0[mTemp.ii].Load AND (NOT gMpAxisBasicConfig_0[mTemp.ii].CommandDone) AND  gURecData.AxEnabled[mTemp.ii]  THEN	
			mTemp.Flag	:= FALSE;
			EXIT;
		END_IF;	
	END_FOR
	
	IF mTemp.Flag THEN
		FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO
			gMpAxisBasicConfig_0[mTemp.ii].Load			:= FALSE;
			gUFixData.Ax[mTemp.ii].Cfg.Axis				:= gMpAxisBasicCfgType[mTemp.ii];
		END_FOR	
		gUHandle.Unit.Status.DefCfgParLoaded 	:= TRUE;
		mTemp.InitStep 							:= STATE_INIT_LOAD_DRIVE_PAR;
	END_IF;	
END_ACTION
