
ACTION InitAx: 
	//============================================================================
	//						INITIALIZATION PROCESS
	//		- Initializes ParID if there is any registered
	//		- Then, Power Up system to make sure that configuration downloaded
	//		- Sets initialized
	//============================================================================	
	CASE mTemp.InitStep OF 
	//============================================================================
		STATE_INIT_INIT:
	//============================================================================
			IF NOT gUHandle.Unit.Status.Initialized THEN 
				
				Loc.Time.InitTON.IN 		:= TRUE ; 
				Loc.Time.InitTON.PT 		:= T#0.5s ;
				Loc.Time.TON_Timeout.PT    	:= T#60s;
				Loc.Time.TON_Timeout.IN		:= TRUE;
				IF Loc.Time.InitTON.Q THEN
					IF mTemp.bFirstPowerUp OR gUHandle.Unit.Status.AfterEnabledAxis THEN
						 mTemp.InitStep 	:= STATE_INIT_LOAD_DRIVE_PAR;
					ELSE 
						 mTemp.InitStep 	:= STATE_INIT_DRIVE;
					END_IF ; 
					
					Loc.Time.InitTON.IN 	:= FALSE ; 
				END_IF ; 
			ELSIF Loc.Time.TON_Timeout.Q OR gUHandle.Unit.Status.Initialized  THEN
				Loc.Time.TON_Timeout.IN		:= FALSE;
				mTemp.InitStep 				:= STATE_INIT_DONE;
			END_IF
			Loc.Time.InitTON();
			Loc.Time.TON_Timeout();
		
		//============================================================================
		STATE_INIT_DRIVE:
		//============================================================================
			IF mTemp.FirstInLocStep THEN 
				mTemp.LocInitStep := GLOBAL_INIT;
				mTemp.ii := 0 ;
				
				memset(ADR(gDriveParInitX_0.inRefAxisConfig), 		0, 	SIZEOF(gDriveParInitX_0.inRefAxisConfig));
				memset(ADR(gDriveParInitX_0.inRefNewConfig), 		0, 	SIZEOF(gDriveParInitX_0.inRefNewConfig));
				memset(ADR(gDriveParInitX_0.inRefAxisBasic),		0,	SIZEOF(gDriveParInitX_0.inRefAxisBasic));
				memset(ADR(gDriveParInitX_0.inInitLimitsOnly ),		0,	SIZEOF(gDriveParInitX_0.inInitLimitsOnly));
				
				FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO 
					IF  gURecData.AxEnabled[mTemp.ii]  THEN
						gDriveParInitX_0.inRefAxisBasic[mTemp.ii] 	 	 := ADR(gMpAxisBasic_0[mTemp.ii]);
						gDriveParInitX_0.inRefAxisConfig[mTemp.ii]  	 := ADR(gMpAxisBasicConfig_0[mTemp.ii]);
						gDriveParInitX_0.inRefNewConfig[mTemp.ii]        := ADR(gUFixData.Ax[mTemp.ii].Cfg.Axis);
					END_IF;
				END_FOR;	
			ELSE 
				DriveInitX;
				memset(ADR(gUHandle.Unit.Cmd), 0, SIZEOF(gUHandle.Unit.Cmd));
			END_IF ; 
		
		
		
		//============================================================================
		STATE_INIT_LOAD_DRIVE_PAR:
		//============================================================================
		
			IF NOT gUHandle.Unit.Status.DefCfgParLoaded  THEN // first in the step
				(* --- *)
				DefaultParX;
				(* --- *)
			ELSE // after the first time in the step
				(* --- *)
				mTemp.bFirstPowerUp 					:= FALSE;
				gUHandle.Unit.Status.DriveInitDone		:= TRUE;
				gUHandle.Unit.Status.DefCfgParLoaded    := FALSE;
				mTemp.InitStep 					    	:= STATE_INIT_DONE;	
				
				FOR mTemp.ii := 0 TO MAX_AX_TOT_MINUS_ONE DO
					gMpAxisBasicConfig_0[mTemp.ii].Load				:= FALSE;
				END_FOR;
				
				// reset the cmd's to the MainControl
				memset(ADR(gUHandle.Unit.Cmd), 0, SIZEOF(gUHandle.Unit.Cmd));	
		END_IF;	// end of the step
		//============================================================================	
		
		//============================================================================
		STATE_INIT_DONE:
		//============================================================================
			gUHandle.Unit.Status.Initialized := TRUE;
		//============================================================================
	END_CASE ; 
	//============================================================================
	
	
	IF mTemp.OldInitStep <>  mTemp.InitStep THEN 
		mTemp.FirstInLocStep := TRUE ;
	ELSE 
		mTemp.FirstInLocStep := FALSE ;
	END_IF ; 
	mTemp.OldInitStep :=  mTemp.InitStep;
	
END_ACTION
