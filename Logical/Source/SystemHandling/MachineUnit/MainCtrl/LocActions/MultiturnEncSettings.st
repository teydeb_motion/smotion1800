(* Multiturn encoder settings *)
ACTION MultiturnEncSettings:
	CASE mTemp.encSettingStep OF
		// =========================================================== //
		MAIN_ENC_HOME_TO_LIM_SW:	// HOME TO LIMIT SWITCH 
		// =========================================================== //
			mTemp.ii := gHMI.DrvPar.SelectedAxIdx; // in case the .rAux is used somewhere else
			IF mTemp.FirstInLocStep THEN // first in the step				
				gMpAxisBasicPar[mTemp.ii].Home.Mode 			:= mpAXIS_HOME_MODE_LIMIT_SWITCH;
				gMpAxisBasicPar[mTemp.ii].Home.HomingVelocity	:= gUHandle.Settings.MultiEncSettings.TriggerSpeed;
				gMpAxisBasicPar[mTemp.ii].Home.StartVelocity    := gUHandle.Settings.MultiEncSettings.HomingSpeed;
				gMpAxisBasicPar[mTemp.ii].Home.Acceleration		:= gUHandle.Settings.MultiEncSettings.HomingSpeed * 4;
				gMpAxisBasicPar[mTemp.ii].Home.Position			:= gUHandle.Settings.MultiEncSettings.LimitSwPos;
				gMpAxisBasicPar[mTemp.ii].Home.HomingDirection	:= mpAXIS_HOME_DIR_POSITIVE;
				gMpAxisBasicPar[mTemp.ii].Home.ReferencePulse 	:= mpAXIS_HOME_OPTION_OFF;
				gMpAxisBasicPar[mTemp.ii].Home.SwitchEdge		:= mpAXIS_HOME_DIR_POSITIVE;
				gMpAxisBasic_0[ mTemp.ii ].Home    				:= TRUE;
			ELSE // after the first time in the step
				IF gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_HOMING THEN
					mTemp.encSettingStep := MAIN_ENC_W_HOME_DONE;
				END_IF;	
			END_IF;	// end of the step
		
		
		// =========================================================== //
		MAIN_ENC_W_HOME_DONE:	// WAIT UNTIL THE HOMING IS DONE
		// =========================================================== //
			mTemp.ii := gHMI.DrvPar.SelectedAxIdx;
			IF gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_STANDSTILL THEN
				gUHandle.Settings.MultiEncSettings.SetPosition := LREAL_TO_REAL(gMpAxisBasic_0[ mTemp.ii ].Position);
				gMpAxisBasic_0[ mTemp.ii ].Home := FALSE;
				mTemp.encSettingStep := MAIN_ENC_GET_OFFSET;
			END_IF;
		
		
		
		// =========================================================== //
		MAIN_ENC_GET_OFFSET:	// GET ENCODER RAW DATA
		// =========================================================== //
			mTemp.ii := gHMI.DrvPar.SelectedAxIdx;
			gUHandle.Unit.Cmd.SetMultiturnEncoder := FALSE ;
			IF mTemp.FirstInLocStep THEN // first in the step
				gMpAxisBasicPar[mTemp.ii].Home.Mode	    := mpAXIS_HOME_MODE_ABSOLUTE;
				gMpAxisBasicPar[mTemp.ii].Home.Position	:= 0; // in order to get raw offset
				gMpAxisBasic_0[ mTemp.ii ].Home			:= TRUE;			
			ELSE // after the first time in the step
				IF NOT gMpAxisBasic_0[ mTemp.ii ].CommandBusy AND gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_STANDSTILL THEN
					gUFixData.Ax[mTemp.ii].Cfg.MultiturnEncOffset := -LREAL_TO_REAL((gMpAxisBasic_0[ mTemp.ii ].Position - gUHandle.Settings.MultiEncSettings.SetPosition));
					mTemp.encSettingStep := MAIN_ENC_SET_NEW_OFFSET;
					gMpAxisBasic_0[ mTemp.ii ].Home  := FALSE;
				END_IF;
			END_IF;	// end of the step
		
		// =========================================================== //
		MAIN_ENC_SET_NEW_OFFSET:	// SET THE NEW OFFSET 
		// ============================================================ //
			mTemp.ii := gHMI.DrvPar.SelectedAxIdx;
			IF mTemp.FirstInLocStep THEN // first in the step

				gMpAxisBasicPar[ mTemp.ii ].Home.Position	:= gUFixData.Ax[mTemp.ii].Cfg.MultiturnEncOffset;
				gMpAxisBasic_0[ mTemp.ii ].Home			:= TRUE;
			ELSE // after the first time in the step
				IF NOT gMpAxisBasic_0[ mTemp.ii ].CommandBusy AND gMpAxisBasic_0[ mTemp.ii ].Info.PLCopenState = mpAXIS_STANDSTILL THEN
					mTemp.encSettingStep 					:= MAIN_ENC_HOME_TO_LIM_SW;
					gMpAxisBasic_0[ mTemp.ii ].Home  		:= FALSE;		
					gUHandle.Unit.Cmd.MultiturnEncHomeLimSw := FALSE; 
					Loc.MainStep := MAIN_WAIT_CMD;
				END_IF;
			END_IF;		
	END_CASE;
	
	IF mTemp.encSettingStep <> mTemp.oldEncSettingStep THEN
		mTemp.FirstInLocStep := TRUE;
	ELSE
		mTemp.FirstInLocStep := FALSE;
	END_IF;
	
	mTemp.oldEncSettingStep := mTemp.encSettingStep;
END_ACTION