(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    tufekcio
 * Created:   January 16, 2017/4:58 PM 
 *********************************************************************************)


ACTION WaitCmd: 
	IF gHMI.General.ActualPage = 500  AND  Loc.MainStep = MAIN_WAIT_CMD THEN 
		Loc.MainStep := MAIN_MANUAL_MODE;
		
	ELSIF gUHandle.Unit.Cmd.InitilizeAxis AND gUHandle.Unit.Status.Initialized THEN 
		gUHandle.Unit.Status.Initialized := FALSE ; 
		mTemp.InitStep := STATE_INIT_INIT;
		Loc.MainStep   := MAIN_INIT;
	ELSIF  (gUHandle.Unit.Cmd.SetMultiturnEncoder 	  	OR
		gUHandle.Unit.Cmd.MultiturnEncHomeLimSw 		OR
		gUHandle.Unit.Cmd.MultiturnEncHomeOffsetValue) 	THEN
		
			IF gUHandle.Unit.Cmd.SetMultiturnEncoder THEN                // home to set position
				mTemp.encSettingStep := MAIN_ENC_GET_OFFSET;
			ELSIF gUHandle.Unit.Cmd.MultiturnEncHomeLimSw THEN           // home to limit switch
				mTemp.encSettingStep := MAIN_ENC_HOME_TO_LIM_SW;
			ELSIF gUHandle.Unit.Cmd.MultiturnEncHomeOffsetValue THEN     // home to the entered offset
				mTemp.encSettingStep := MAIN_ENC_SET_NEW_OFFSET;
			END_IF;
			Loc.MainStep      := MAIN_MULTI_ENC_OFFSET;
	END_IF;
END_ACTION
