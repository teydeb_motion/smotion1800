(*Steps*)

TYPE
	LocInitSteps_enum : 
		(
		GLOBAL_INIT,
		GLOBAL_W_INIT
		);
	InitAxSteps_enum : 
		(
		STATE_INIT_INIT,
		STATE_INIT_DRIVE,
		STATE_INIT_LOAD_DRIVE_PAR,
		STATE_INIT_DONE
		);
	PowerSteps_Enum : 
		(
		MAIN_POWER_START,
		MAIN_POWER_PROCESS,
		MAIN_POWER_DONE
		);
	HomingSteps_enum : 
		(
		MAIN_HOME_START, (*Give CMD to start homing*)
		MAIN_HOME_PROGRESS, (*Wait until the homing is started*)
		MAIN_HOME_DONE := 3 (*Wait until the homing is done*)
		);
	MainCtrlSteps_enum : 
		(
		MAIN_INIT := 0,
		MAIN_WAIT_CMD,
		MAIN_POWER_ON,
		MAIN_HOME,
		MAIN_PARK,
		MAIN_AUTO_RUNNING,
		MAIN_MANUAL_MODE,
		MAIN_ERROR,
		MAIN_MULTI_ENC_OFFSET
		);
	ParkSteps_enum : 
		(
		MAIN_PARK_INIT := 0,
		MAIN_PARK_PARKING_AX_Y,
		MAIN_PARK_PARKING_AX_X,
		MAIN_PARK_PARKED
		);
	PosSeq_enum : 
		(
		POS_SEQ_POSITION_DETERMINATION,
		POS_SEQ_Y_AXIS_MOVING,
		POS_SEQ_X_AXIS_MOVING,
		POS_SEQ_MOVEMENT_DONE,
		POS_SEQ_PROCESS_DONE,
		POS_SEQ_ERROR
		);
END_TYPE

(**)

TYPE
	LocalTime_Type : 	STRUCT 
		DelayInit : UDINT;
		DelayPowerDown : UDINT;
		OutParking : UDINT;
		OutHoming : UDINT;
		OutPowerUp : UDINT;
		InitTON : TON;
		TON_Timeout : TON;
		OutBootUp : UDINT;
	END_STRUCT;
	Loc_Type : 	STRUCT 
		MainStep : {REDUND_UNREPLICABLE} MainCtrlSteps_enum;
		Time : {REDUND_UNREPLICABLE} LocalTime_Type;
	END_STRUCT;
	mTempType : 	STRUCT 
		ii : {REDUND_UNREPLICABLE} USINT;
		jj : {REDUND_UNREPLICABLE} USINT;
		FirstInStep : {REDUND_UNREPLICABLE} BOOL;
		FirstInLocStep : {REDUND_UNREPLICABLE} BOOL;
		PosSeqStep : {REDUND_UNREPLICABLE} PosSeq_enum;
		OldPosSeq : {REDUND_UNREPLICABLE} PosSeq_enum;
		ParkStep : {REDUND_UNREPLICABLE} ParkSteps_enum;
		HomeStep : {REDUND_UNREPLICABLE} HomingSteps_enum;
		OldHomeStep : {REDUND_UNREPLICABLE} HomingSteps_enum;
		OldParkStep : {REDUND_UNREPLICABLE} ParkSteps_enum;
		PowerStep : {REDUND_UNREPLICABLE} PowerSteps_Enum;
		OldPowerStep : {REDUND_UNREPLICABLE} PowerSteps_Enum;
		OldMainStep : {REDUND_UNREPLICABLE} MainCtrlSteps_enum;
		LocSubDone : {REDUND_UNREPLICABLE} BOOL;
		Flag : {REDUND_UNREPLICABLE} BOOL;
		OldInitStep : {REDUND_UNREPLICABLE} InitAxSteps_enum;
		InitStep : {REDUND_UNREPLICABLE} InitAxSteps_enum;
		LocInitStep : {REDUND_UNREPLICABLE} LocInitSteps_enum;
		bFirstPowerUp : {REDUND_UNREPLICABLE} BOOL;
		JogAxes : {REDUND_UNREPLICABLE} BOOL;
		encSettingStep : Main_EncSettings_Steps_enm;
		oldEncSettingStep : Main_EncSettings_Steps_enm;
	END_STRUCT;
END_TYPE
