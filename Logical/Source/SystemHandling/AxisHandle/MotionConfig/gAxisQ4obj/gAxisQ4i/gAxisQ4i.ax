﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.3.93 SP?>
<InitParameter Version="3.16.2" NcSwId="Acp10">
  <Group ID="ACP10AXIS_typ">
    <Group ID="dig_in" Description="Digital Inputs">
      <Group ID="level" Description="Active Input Level">
        <Parameter ID="reference" Value="ncACTIV_HI" Description="Reference switch" />
        <Parameter ID="pos_hw_end" Value="ncACTIV_HI" Description="Positive HW end switch" />
        <Parameter ID="neg_hw_end" Value="ncACTIV_HI" Description="Negative HW end switch" />
        <Parameter ID="trigger1" Value="ncACTIV_HI" Description="Trigger1" />
        <Parameter ID="trigger2" Value="ncACTIV_HI" Description="Trigger2" />
      </Group>
    </Group>
    <Group ID="encoder_if" Description="Encoder Interface">
      <Group ID="parameter" Description="Parameters">
        <Parameter ID="count_dir" Value="ncSTANDARD" Description="Count direction" />
        <Group ID="scaling" Description="Scaling">
          <Group ID="load" Description="Load">
            <Parameter ID="units" Value="36000" Description="Units at the load" />
            <Parameter ID="rev_motor" Value="1" Description="Motor revolutions" />
          </Group>
        </Group>
      </Group>
    </Group>
    <Group ID="limit" Description="Limit value">
      <Group ID="parameter" Description="Parameters">
        <Parameter ID="v_pos" Value="60000.0" Description="Speed in positive direction" />
        <Parameter ID="v_neg" Value="60000.0" Description="Speed in negative direction" />
        <Parameter ID="a1_pos" Value="300000.0" Description="Acceleration in positive direction" />
        <Parameter ID="a2_pos" Value="300000.0" Description="Deceleration in positive direction" />
        <Parameter ID="a1_neg" Value="300000.0" Description="Acceleration in negative direction" />
        <Parameter ID="a2_neg" Value="300000.0" Description="Deceleration in negative direction" />
        <Parameter ID="t_jolt" Value="0.0" Description="Jolt time" />
        <Parameter ID="t_in_pos" Value="0.0" Description="Settling time before message 'In Position'" />
        <Parameter ID="pos_sw_end" Value="50000000" Description="Positive SW end" />
        <Parameter ID="neg_sw_end" Value="-50000000" Description="Negative SW end" />
        <Parameter ID="ds_warning" Value="500.0" Description="Lag error limit for display of a warning" />
        <Parameter ID="ds_stop" Value="1000.0" Description="Lag error limit for stop of a movement" />
        <Parameter ID="a_stop" Value="1.0E+30" Description="Acceleration limit for stop of a movement" />
        <Parameter ID="dv_stop" Value="0.0" Description="Speed error limit for stop of a movement" />
        <Parameter ID="dv_stop_mode" Value="ncOFF" Description="Mode for speed error monitoring" />
      </Group>
    </Group>
    <Group ID="controller" Description="Controller">
      <Parameter ID="mode" Value="ncPOSITION" Description="Mode" />
      <Group ID="position" Description="Position Controller">
        <Parameter ID="kv" Value="50.0" Description="Proportional amplification" />
        <Parameter ID="tn" Value="0.0" Description="Integral action time" />
        <Parameter ID="t_predict" Value="0.0004" Description="Prediction time" />
        <Parameter ID="t_total" Value="0.0004" Description="Total time" />
        <Parameter ID="p_max" Value="1.0E+30" Description="Maximum proportional action" />
        <Parameter ID="i_max" Value="0.0" Description="Maximum integral action" />
      </Group>
      <Group ID="speed" Description="Speed Controller">
        <Parameter ID="kv" Value="0.0" Description="Proportional amplification" />
        <Parameter ID="tn" Value="0.0" Description="Integral action time" />
        <Parameter ID="t_filter" Value="0.0" Description="Filter time constant" />
        <Group ID="isq_filter1" Description="ISQ Filter1">
          <Parameter ID="type" Value="ncOFF" Description="Type" />
          <Parameter ID="a0" Value="0.0" Description="Coefficient a0" />
          <Parameter ID="a1" Value="0.0" Description="Coefficient a1" />
          <Parameter ID="b0" Value="0.0" Description="Coefficient b0" />
          <Parameter ID="b1" Value="0.0" Description="Coefficient b1" />
          <Parameter ID="b2" Value="0.0" Description="Coefficient b2" />
          <Parameter ID="c0_par_id" Value="0" Description="Parameter ID for coefficient c0" />
          <Parameter ID="c1_par_id" Value="0" Description="Parameter ID for coefficient c1" />
        </Group>
        <Group ID="isq_filter2" Description="ISQ Filter2">
          <Parameter ID="type" Value="ncOFF" Description="Type" />
          <Parameter ID="a0" Value="0.0" Description="Coefficient a0" />
          <Parameter ID="a1" Value="0.0" Description="Coefficient a1" />
          <Parameter ID="b0" Value="0.0" Description="Coefficient b0" />
          <Parameter ID="b1" Value="0.0" Description="Coefficient b1" />
          <Parameter ID="b2" Value="0.0" Description="Coefficient b2" />
          <Parameter ID="c0_par_id" Value="0" Description="Parameter ID for coefficient c0" />
          <Parameter ID="c1_par_id" Value="0" Description="Parameter ID for coefficient c1" />
        </Group>
        <Group ID="isq_filter3" Description="ISQ Filter3">
          <Parameter ID="type" Value="ncOFF" Description="Type" />
          <Parameter ID="a0" Value="0.0" Description="Coefficient a0" />
          <Parameter ID="a1" Value="0.0" Description="Coefficient a1" />
          <Parameter ID="b0" Value="0.0" Description="Coefficient b0" />
          <Parameter ID="b1" Value="0.0" Description="Coefficient b1" />
          <Parameter ID="b2" Value="0.0" Description="Coefficient b2" />
          <Parameter ID="c0_par_id" Value="0" Description="Parameter ID for coefficient c0" />
          <Parameter ID="c1_par_id" Value="0" Description="Parameter ID for coefficient c1" />
        </Group>
      </Group>
      <Group ID="uf" Description="U/f Control">
        <Parameter ID="type" Value="ncLINEAR" Description="Type" />
        <Parameter ID="auto_config" Value="ncMOTOR_PAR" Description="Automatic configuration" />
        <Parameter ID="u0" Value="0.0" Description="Boost voltage" />
        <Parameter ID="un" Value="0.0" Description="Rated voltage" />
        <Parameter ID="fn" Value="50.0" Description="Rated frequency" />
        <Parameter ID="k_f_slip" Value="0.0" Description="Slip compensation: Multiplication factor of compensated frequency" />
      </Group>
      <Group ID="ff" Description="Feed Forward Control">
        <Parameter ID="torque_load" Value="0.0" Description="Load torque" />
        <Parameter ID="torque_pos" Value="0.0" Description="Torque in positive direction" />
        <Parameter ID="torque_neg" Value="0.0" Description="Torque in negative direction" />
        <Parameter ID="kv_torque" Value="0.0" Description="Speed torque factor" />
        <Parameter ID="inertia" Value="0.0" Description="Mass moment of inertia" />
        <Parameter ID="t_filter_a" Value="0.0" Description="Acceleration filter time constant" />
      </Group>
    </Group>
    <Group ID="move" Description="Movement">
      <Group ID="stop" Description="Stop Movement">
        <Group ID="parameter[0]" Description="Parameter record">
          <Parameter ID="decel_ramp" Value="ncA_LIMIT" Description="Deceleration ramp" />
          <Parameter ID="controller" Value="ncON" Description="Controller state after movement abortion" />
        </Group>
        <Group ID="parameter[1]" Description="Parameter record">
          <Parameter ID="decel_ramp" Value="ncA_LIMIT" Description="Deceleration ramp" />
          <Parameter ID="controller" Value="ncON" Description="Controller state after movement abortion" />
        </Group>
        <Group ID="parameter[2]" Description="Parameter record">
          <Parameter ID="decel_ramp" Value="ncA_LIMIT" Description="Deceleration ramp" />
          <Parameter ID="controller" Value="ncON" Description="Controller state after movement abortion" />
        </Group>
        <Group ID="parameter[3]" Description="Parameter record">
          <Parameter ID="decel_ramp" Value="ncA_LIMIT" Description="Deceleration ramp" />
          <Parameter ID="controller" Value="ncON" Description="Controller state after movement abortion" />
        </Group>
        <Group ID="quickstop" Description="Quickstop">
          <Parameter ID="decel_ramp" Value="ncA_LIMIT" Description="Deceleration ramp" />
        </Group>
        <Group ID="drive_error" Description="Drive error">
          <Parameter ID="decel_ramp" Value="ncA_LIMIT" Description="Deceleration ramp" />
        </Group>
      </Group>
      <Group ID="homing" Description="Homing procedure">
        <Group ID="parameter" Description="Parameters">
          <Parameter ID="s" Value="0" Description="Reference position" />
          <Parameter ID="v_switch" Value="5000.0" Description="Speed for searching the reference switch" />
          <Parameter ID="v_trigger" Value="500.0" Description="Trigger Speed" />
          <Parameter ID="a" Value="50000.0" Description="Acceleration" />
          <Parameter ID="mode" Value="ncDIRECT" Description="Mode" />
          <Parameter ID="edge_sw" Value="ncPOSITIVE" Description="Edge of reference switch" />
          <Parameter ID="start_dir" Value="ncPOSITIVE" Description="Start direction" />
          <Parameter ID="trigg_dir" Value="ncPOSITIVE" Description="Trigger direction" />
          <Parameter ID="ref_pulse" Value="ncOFF" Description="Reference pulse" />
          <Parameter ID="fix_dir" Value="ncOFF" Description="Fixed direction" />
          <Parameter ID="tr_s_block" Value="0.0" Description="Distance for blocking the activation of 'triggering reference pulse'" />
          <Parameter ID="torque_lim" Value="0.0" Description="Torque limit for homing on block" />
          <Parameter ID="ds_block" Value="0.0" Description="Lag error for block detection" />
          <Parameter ID="ds_stop" Value="0.0" Description="Lag error for stop of a movement" />
        </Group>
      </Group>
      <Group ID="basis" Description="Basis movements">
        <Group ID="parameter" Description="Parameters">
          <Parameter ID="v_pos" Value="5000.0" Description="Speed in positive direction" />
          <Parameter ID="v_neg" Value="5000.0" Description="Speed in negative direction" />
          <Parameter ID="a1_pos" Value="50000.0" Description="Acceleration in positive direction" />
          <Parameter ID="a2_pos" Value="50000.0" Description="Deceleration in positive direction" />
          <Parameter ID="a1_neg" Value="50000.0" Description="Acceleration in negative direction" />
          <Parameter ID="a2_neg" Value="50000.0" Description="Deceleration in negative direction" />
        </Group>
      </Group>
    </Group>
    <Group ID="message" Description="Messages (errors, warnings)">
      <Group ID="text" Description="Text determination for current message record">
        <Group ID="parameter" Description="Parameters">
          <Parameter ID="format" Value="ncBREAK" Description="Format" />
          <Parameter ID="columns" Value="80" Description="Number of columns per line" />
          <Parameter ID="data_modul" Value="acp10etxen" Description="Name of the data module" />
        </Group>
      </Group>
    </Group>
  </Group>
</InitParameter>