(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    tufekcio
 * Created:   January 16, 2017/3:42 PM 
 *********************************************************************************)


PROGRAM _INIT
	//===============================================================================
	//							Axes Config
	//===============================================================================
	gUHandle.Ax[0].Axis		:= ADR(gAxisPwr);
	gUHandle.Ax[0].MpLink	:= ADR(gLinkAxPwr);
	gUHandle.Ax[0].Par  	:= ADR(gMpAxisBasicPar[0]);
	
	
	gUHandle.Ax[1].Axis 	:= ADR(gAxisQ1); 
	gUHandle.Ax[1].MpLink 	:= ADR(gLinkAxQ1);
	gUHandle.Ax[1].Par 		:= ADR(gMpAxisBasicPar[1]);
	
	gUHandle.Ax[2].Axis		:= ADR(gAxisQ2);
	gUHandle.Ax[2].MpLink	:= ADR(gLinkAxQ2);
	gUHandle.Ax[2].Par  	:= ADR(gMpAxisBasicPar[2]);
	
	
	gUHandle.Ax[3].Axis 	:= ADR(gAxisQ3); 
	gUHandle.Ax[3].MpLink 	:= ADR(gLinkAxQ3);
	gUHandle.Ax[3].Par 		:= ADR(gMpAxisBasicPar[3]);
	
	gUHandle.Ax[4].Axis		:= ADR(gAxisQ4);
	gUHandle.Ax[4].MpLink	:= ADR(gLinkAxQ4);
	gUHandle.Ax[4].Par  	:= ADR(gMpAxisBasicPar[4]);
	
	
	gUHandle.Ax[5].Axis 	:= ADR(gAxisQ5); 
	gUHandle.Ax[5].MpLink 	:= ADR(gLinkAxQ5);
	gUHandle.Ax[5].Par 		:= ADR(gMpAxisBasicPar[5]);
	
	
	gUHandle.Ax[6].Axis 	:= ADR(gAxisQ6); 
	gUHandle.Ax[6].MpLink 	:= ADR(gLinkAxQ6);
	gUHandle.Ax[6].Par 		:= ADR(gMpAxisBasicPar[6]);
	//===============================================================================
	ModuleParameters;
END_PROGRAM


PROGRAM _CYCLIC
	ModuleParameters;
	AxesStatus;
	FOR i := 0 TO MAX_AX_TOT_MINUS_ONE DO 		
		gMpAxisBasic_0[i].Axis 					:= gUHandle.Ax[i].Axis;
		gMpAxisBasic_0[i].MpLink 				:= gUHandle.Ax[i].MpLink;
		gMpAxisBasic_0[i].Parameters 			:= gUHandle.Ax[i].Par;
		gMpAxisBasic_0[i].Enable 				:= gURecData.AxEnabled[i];
		gMpAxisBasic_0[i]();
		
		gMpAxisBasicConfig_0[i].Configuration 	:= ADR(gMpAxisBasicCfgType[i]);
		gMpAxisBasicConfig_0[i].MpLink         	:= gUHandle.Ax[i].MpLink;
		gMpAxisBasicConfig_0[i]();
	END_FOR; 
END_PROGRAM
