
(* Axes Status *)
ACTION AxesStatus:
	
	EnabledFlag := TRUE ;
	FOR i := 0 TO MAX_AX_TOT_MINUS_ONE DO 
		IF NOT  gMpAxisBasic_0[i].Active AND gURecData.AxEnabled[i]  THEN 
			EnabledFlag := FALSE ; 
			EXIT;
		END_IF; 
	END_FOR ; 
	
	
	PowerFlag := TRUE ;
	FOR i := 0 TO MAX_AX_TOT_MINUS_ONE DO 
		IF NOT  gMpAxisBasic_0[i].PowerOn AND gURecData.AxEnabled[i] THEN 
			PowerFlag := FALSE ; 
			EXIT;
		END_IF; 
	END_FOR ; 
	
	ReferencedFlag := TRUE ;
	FOR i := 0 TO MAX_AX_TOT_MINUS_ONE DO 
		IF NOT  gMpAxisBasic_0[i].IsHomed AND gURecData.AxEnabled[i] THEN 
			ReferencedFlag := FALSE ; 
			EXIT;
		END_IF; 
	END_FOR ; 
	
	
	gUHandle.Unit.Status.AxEnabled  := EnabledFlag;
	gUHandle.Unit.Status.Powered   	:= PowerFlag;
	gUHandle.Unit.Status.Referenced := ReferencedFlag;
		
	END_ACTION

(* Module Parameters *)
ACTION ModuleParameters: 
	IF gUHandle.Simulation THEN 
		gMpAxisBasicPar[0].Home.Mode := mpAXIS_HOME_MODE_DIRECT;
		gMpAxisBasicPar[1].Home.Mode := mpAXIS_HOME_MODE_DIRECT;
		gMpAxisBasicPar[2].Home.Mode := mpAXIS_HOME_MODE_DIRECT;
		gMpAxisBasicPar[3].Home.Mode := mpAXIS_HOME_MODE_DIRECT;
		gMpAxisBasicPar[4].Home.Mode := mpAXIS_HOME_MODE_DIRECT;
		gMpAxisBasicPar[5].Home.Mode := mpAXIS_HOME_MODE_DIRECT;
	END_IF; 
END_ACTION

