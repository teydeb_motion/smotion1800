
TYPE
	S_Pneumatic : 	STRUCT 
		Max : LREAL := 8; (*Max Pressure (8 Bar)*)
		Set : LREAL; (*Set Pressure (3.5 Bar Initial)*)
		Offset : LREAL := 6553; (*Offset Pressure Integer Count (4 mA -> 0 Bar)*)
		AIMax : LREAL := 26214; (*Max Pressure Integer Count (20 mA -> 8 Bar)*)
		Res : LREAL; (*Analog Output Response Pressure (Bar)*)
		Tolerance : LREAL := 300;
		In : INT; (*Analog Input Integer Count Pressure (X20AI4622 - 1)*)
		Out : INT; (*Analog Output Integer Count Pressure (X20AO4632 - 1)*)
		OK : BOOL;
		PFlag : BOOL;
		Exist : BOOL := FALSE;
	END_STRUCT;
END_TYPE
