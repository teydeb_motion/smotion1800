/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:   abdullahoglf
* Created:  October 01, 2016
**********************************************************************************
* Description:
*	- Function to check possible errors within the axis and/or the FB itself.
* History:
*	
**********************************************************************************/


/*************************************************************************
****    	Includes 				         						  ****
*************************************************************************/
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "AxConInt.h"

/*************************************************************************
****    	Internal Declarations			          	   	 	 	  ****
*************************************************************************/



/*************************************************************************
****    	Function Implementation		         				      ****
*************************************************************************/
/* Check Errors */
unsigned short CheckError(void)
{	
	extern unsigned long	pMain;		/* Main FB handle */
	extern unsigned long 	pMainInt;	/* Internal structure of the main FB */
	struct AxCon			*pMainFb;	/* Pointer to the Main FB */
	AxCon_Internal_typ		*pInt;		/* Pointer to the internal structure of the main FB */
	struct MpAxisBasic		*pAxCtrl;	/* Pointer to the MpAxisBasic component */
	//
	// ----------------------------
	// Assign the pointers
	//	* All the references of the FB
	//		must be OK by this point
	// ----------------------------
	pMainFb	= (struct AxCon			*)	pMain;
	pInt	= (AxCon_Internal_typ	*) 	pMainInt;
	pAxCtrl	= (struct MpAxisBasic	*)	pMainFb->MpAxis;
	
	// ----------------------------
	// Check axis errors 
	// ----------------------------
	if ( pAxCtrl->StatusID ) // 
		return ERR_AXCON_AXIS_ERROR;
	
	return 0; // if the function reaches here it means there are no errors within the FB
} /* end of the CheckError() implementation */

#ifdef __cplusplus
	}
#endif
