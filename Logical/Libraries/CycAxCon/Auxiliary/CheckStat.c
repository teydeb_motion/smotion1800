/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:   abdullahoglf
* Created:  October 01, 2016
**********************************************************************************
* Description:
*	- Function to check various status bits regarding the axis.
* History:
*	
**********************************************************************************/


/*************************************************************************
****    	Includes 				         						  ****
*************************************************************************/
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "AxConInt.h"

/*************************************************************************
****    	Internal Declarations			          	   	 	 	  ****
*************************************************************************/



/*************************************************************************
****    	Function Implementation		         				      ****
*************************************************************************/
/* Check Commanded Status */
unsigned short CheckStat(AxCon_StatusCheck_enm Stat)
{	
	extern unsigned long	pMain;		/* Main FB handle */
	extern unsigned long 	pMainInt;	/* Internal structure of the main FB */
	struct AxCon			*pMainFb;	/* Pointer to the Main FB */
	AxCon_Internal_typ		*pInt;		/* Pointer to the internal structure of the main FB */
	ACP10AXIS_typ			*pAxObj;	/* Pointer to the axis structure */
	struct MpAxisBasic		*pAxCtrl;	/* Pointer to the MpAxisBasic component */
	//
	// ----------------------------
	// Assign the pointers
	//	* All the references of the FB
	//		must be OK by this point
	// ----------------------------
	pMainFb	= (struct AxCon			*)	pMain;
	pInt	= (AxCon_Internal_typ	*) 	pMainInt;
	pAxCtrl	= (struct MpAxisBasic	*)	pMainFb->MpAxis;
	pAxObj	= (ACP10AXIS_typ		*)	pAxCtrl->Axis;
	
	// ----------------------------
	// Case machine for the commanded status checks 
	// ----------------------------
	switch ( Stat )
	{
		// ----------------------------
		case axCon_STAT_AX_POWER:	// Axis power status  
		// ----------------------------
			return ( (unsigned short)(pAxCtrl->PowerOn) );
			break;
		
		// ----------------------------
		case axCon_STAT_AX_REF:		// Check the axis referencce status
		// ----------------------------
			return ( (unsigned short)(pAxCtrl->IsHomed) );
			break;
		
		// ----------------------------
		default:					// Undefined status 
		// ----------------------------
			return 0xffff;
			break;		
	} /* Switch (Stat) */	
} /* end of the CheckStat() implementation */

#ifdef __cplusplus
	};
#endif
