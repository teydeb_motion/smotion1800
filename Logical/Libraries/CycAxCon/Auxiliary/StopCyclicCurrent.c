/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Library:	CycAxCon
* Author:   abdullahoglf
* Created:  August 18, 2016
**********************************************************************************
* Description:
* 	- Carry out the stopping procedure for the CyclicCurrent
* History:
*	
**********************************************************************************/

#include "AxConInt.h"

unsigned short StopCyclicCurrent(plcbit Enable)
{
	// -------------------------------------------------------------------------------------------------------- //
	//										INTERNAL DECLARATIONS												// 
	// -------------------------------------------------------------------------------------------------------- //
	extern unsigned long	pMain;		/* Main FB handle */
	extern unsigned long 	pMainInt;	/* Main FB internal strucutre handle */
	struct AxCon			*pMainFb;	/* Pointer to the Main FB */
	float  					setCurrent;
	AxCon_Internal_typ		*pInt;
	struct MpAxisBasic		*pAxCtrl;	/* Pointer to the axis to be controlled */
	ACP10AXIS_typ			*pAxObj;	/* Pointer to the axis object */
	//
	// ----------------------------
	// Assign the pointers
	//	* All the references of the FB
	//		must be OK by this point
	// ----------------------------
	pMainFb	= (struct AxCon			*) 	pMain;
	pInt	= (AxCon_Internal_typ	*)	pMainInt;
	pAxCtrl	= (struct MpAxisBasic	*)	pMainFb->MpAxis;	
	pAxObj	= (ACP10AXIS_typ		*)	pAxCtrl->Axis;
	
	// -------------------------------------------------------------------------------------------------------- //
	//							M A I N  C O N T R O L  S E Q U E N C E											// 
	// -------------------------------------------------------------------------------------------------------- //
	switch ( pInt->locFun.StopCyclicCurrent.Step )
	{
		// ----------------------------
		case STOP_CUR_DISABLED: 	// Function is disabled
			// ----------------------------	
			if ( Enable )
			{
				pInt->locFun.StopCyclicCurrent.Step++;		
			}
			else
			{
				return 0xfffe; // function is disabled	
			} break;
		
		// ----------------------------
		case STOP_CUR_RESET_CURRENT: // Set 0 amp current -> from this point the value in the Buffer will not be sent to the drive anymore  
			// ----------------------------
			if ( pInt->locFun.StopCyclicCurrent.bFirstInStep ) // first time in the step
			{
				// DataAddress can be changed only when the Fb is disabled
				setCurrent	= 0;
				pInt->FB.MC_BR_CyclicWrite_0.Enable	 		= 0;
			}
			else // after the first time in the step
			{	
				// ----------------------------
				// Wait until the cyclicWrite is disabled 
				// ----------------------------
				if ( !pInt->FB.MC_BR_CyclicWrite_0.Valid && !pInt->FB.MC_BR_CyclicWrite_0.Error )
				{					
					pInt->FB.MC_BR_CyclicWrite_0.DataAddress	= (unsigned long)&setCurrent;
					pInt->FB.MC_BR_CyclicWrite_0.Enable			= 1;			
					pInt->locFun.StopCyclicCurrent.Step++;
				}
				else if ( pInt->FB.MC_BR_CyclicWrite_0.Error ) // Error occured within the CyclicWrite FB
				{
					return pInt->FB.MC_BR_CyclicWrite_0.ErrorID;	
				}
			} break; // end of the step	
		
	
		// ----------------------------
		case STOP_CUR_ENABLE_PCTRL:	// Enable the position controller so that the drive can function normally
		// ----------------------------
			if ( pInt->locFun.StopCyclicCurrent.bFirstInStep ) // first time in the step
			{	
				pInt->locFun.StopCyclicCurrent.iTemp		= ACP10PAR_CONST_I4_ONE;
				pInt->FB.MC_BR_WriteParID_0.DataAddress		= (unsigned long) &(pInt->locFun.StopCyclicCurrent.iTemp);
				pInt->FB.MC_BR_WriteParID_0.DataType		= ncPAR_TYP_UINT;
				pInt->FB.MC_BR_WriteParID_0.ParID			= ACP10PAR_PCTRL_ENABLE_PARID;
				pInt->FB.MC_BR_WriteParID_0.Execute			= 1;
			}
			else // after the first time in the step
			{
				// ----------------------------
				// Check the FB results
				// ----------------------------
				if ( pInt->FB.MC_BR_WriteParID_0.Busy ) {;} 	// FB is busy -> wait
				else if ( pInt->FB.MC_BR_WriteParID_0.Done )	// FB is done
				{
					pInt->FB.MC_BR_WriteParID_0.Execute		= 0;			
					//pInt->locFun.StopCyclicCurrent.Step++;			
					return 0;
				}
				else if ( pInt->FB.MC_BR_WriteParID_0.Error ) 	// Error occured
				{
					pInt->FB.MC_BR_WriteParID_0.Execute		= 0;
					return pInt->FB.MC_BR_WriteParID_0.ErrorID;
				}
			} break;		
		
		
		// ----------------------------
		case STOP_CUR_RESTORE_SCTRL:	// Restore the speed controller values
		// ----------------------------
			if ( pInt->locFun.StopCyclicCurrent.bFirstInStep ) // first time in the step
			{					
				pAxObj->controller.speed = pInt->Controller.speed;	 
			}
			else // after the first time in the step
			{
				pInt->locFun.StopCyclicCurrent.iTemp = ncaction(pAxCtrl->Axis, ncCONTROLLER, ncINIT);
				if ( pInt->locFun.CfgCyclicCurrent.iTemp == ncOK )
				{
					return 0;	
				}
			} break; // end of the step
	} /* switch ( ) */	
	return 0xffff; // return busy if the function gets to this point
} // end of the StopCyclicCurrent() implementation
