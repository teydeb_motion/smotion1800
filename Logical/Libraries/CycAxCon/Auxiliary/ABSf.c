/********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: CycAxCon
 * File: ABSf.c
 * Author: abdullahoglf
 * Created: January 25, 2013
 ********************************************************************
 * Implementation of library CycAxCon
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
extern "C"
{
#endif

float ABSf(float x)
{
	return( (x>=0) ? x:-x );
}

#ifdef __cplusplus
};
#endif
