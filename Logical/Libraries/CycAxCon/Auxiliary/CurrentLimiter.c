/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Library:	CycAxCon
* Author:   abdullahoglf
* Created:  August 18, 2016
**********************************************************************************
* Description:
* 	- Limits the set current according the limit which is set in the FB parameters
* History:
*	
**********************************************************************************/

#include "AxConInt.h"

void CurrentLimiter(float* pSetCurrent)
{
	// -------------------------------------------------------------------------------------------------------- //
	//										INTERNAL DECLARATIONS												// 
	// -------------------------------------------------------------------------------------------------------- //
	extern unsigned long	pMain;		/* Main FB handle */
	struct AxCon			*pMainFb;	/* Pointer to the Main FB */
	AxCon_Parameters_typ	*pPar;		/* Pointer to the Main parameters structure */
	float MaxCurrent = 0;
	//	
	// ----------------------------
	// Assign the pointers
	//	* All the references of the FB
	//		must be OK by this point
	// ----------------------------
	pMainFb		= (struct AxCon			*) 	pMain;
	pPar		= (AxCon_Parameters_typ	*)	pMainFb->Parameters;
	MaxCurrent	= pPar->CycCurrent.MaxCurrent;
	//
	if ( pSetCurrent )
	{
		if ( ABSf(*pSetCurrent) > MaxCurrent )
		{
			if ( *pSetCurrent < 0 )
				*pSetCurrent = -1.0 * MaxCurrent;
			else
				*pSetCurrent = MaxCurrent;
		}
	}
}
