/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
**********************************************************************************
* Library:	CycAxCon 
* Author:   abdullahoglf
* Created:  August 18, 2016
**********************************************************************************
* Description:
*
* History:
*	
**********************************************************************************/


/*************************************************************************
****    	Includes 				         						  ****
*************************************************************************/
#include "AxConInt.h"
#ifdef __cplusplus
	extern "C"
	{
#endif

/*************************************************************************
****    	Internal Declarations			          	   	 	 	  ****
*************************************************************************/
unsigned long 	pMain		= 0;
unsigned long 	pMainInt	= 0;

/*************************************************************************
****    	FB Implementation			         				      ****
*************************************************************************/
/* Axis Control */
void AxCon(struct AxCon* inst)
{
	pMain		= 0;
	pMainInt	= 0;
	
	if ( inst == 0 )
		return;
	else
	{
		pMain		= (unsigned long)inst;
		pMainInt	= (unsigned long)&inst->Internal;
		if ( inst->Enable < inst->Internal.bOldEnable && inst->Internal.Step != AXCON_ERROR )	{inst->Internal.bDisable=1;}		
		inst->Internal.bOldEnable	= inst->Enable;		
	}	

	// ----------------------------
	// Check the FB enable
	// ----------------------------
	if (inst->Enable || inst->Internal.bDisable) // FB is enabled
	{
		// ----------------------------
		// Define the required pointers
		AxCon_Parameters_typ		*pPar;
		MpAxisBasicParType			*pAxPar;
		struct MpAxisBasic			*pAxCtrl;
		ACP10AXIS_typ				*pAxObj;
		AxCon_Buffer_typ			*pBuffer;
		// ----------------------------
		
		// -------------------------------------------------------------------------------------------------------- //
		//						CHECK REFERENCES																	// 
		// -------------------------------------------------------------------------------------------------------- //
		// ----------------------------
		// Check FB inputs to see whether any reference is missing 
		// ----------------------------
		if ( !inst->Buffer ) // reference to cyclic set point buffer is missing
		{
			inst->Status			= ERR_AXCON_NULL_POINTER;
			inst->Info.StatusInfo	= axCon_ERR_NULL_BUFFER;					
		}
		//
		if ( !inst->Parameters ) // reference to the parameters structure is missing
		{
			inst->Status			= ERR_AXCON_NULL_POINTER;
			inst->Info.StatusInfo	= axCon_ERR_NULL_PARAMETERS;
		}
		//
		if ( !inst->MpAxis ) // MpLink connection is missing
		{
			inst->Status			= ERR_AXCON_NULL_POINTER;
			inst->Info.StatusInfo	= axCon_ERR_NULL_MPAXIS;
		}
		//
		if ( inst->Status != ERR_AXCON_NULL_POINTER )
		{
			// ----------------------------
			// Assign the pointers 
			// ----------------------------	
			pPar	= (AxCon_Parameters_typ	*)	inst->Parameters;
			pBuffer	= (AxCon_Buffer_typ		*)	inst->Buffer;
			//
			pAxCtrl	= (struct MpAxisBasic	*)	inst->MpAxis;
			if ( !pAxCtrl->Parameters ) // axis parameters structure is null
			{
				inst->Status			= ERR_AXCON_NULL_POINTER;
				inst->Info.StatusInfo	= axCon_ERR_NULL_AX_PAR;
			}
			else
				pAxPar					= (MpAxisBasicParType	*)	pAxCtrl->Parameters;
			//
			if ( !pAxCtrl->Axis )
			{
				inst->Status			= ERR_AXCON_NULL_POINTER;
				inst->Info.StatusInfo	= axCon_ERR_NULL_AX_OBJ;
			}
			else
				pAxObj					= (ACP10AXIS_typ		*) pAxCtrl->Axis;			
		} // end of the general reference check
		
		// -------------------------------------------------------------------------------------------------------- //
		//						M A I N   C O N T R O L   S E Q U E N C E	                                        // 
		// -------------------------------------------------------------------------------------------------------- //
		if ( inst->Status != ERR_OK && inst->Status != ERR_FUB_BUSY && inst->Internal.Step != AXCON_DISABLED )	{inst->Internal.Step	= AXCON_ERROR;}		
		switch (inst->Internal.Step)
		{
			// =========================================================== //
			case AXCON_DISABLED:		// COMPONENT IS DISABLED
				// =========================================================== //
				// ----------------------------
				// Wait for enable
				// ----------------------------
				if ( inst->Enable )
				{
					inst->Internal.Step 	= AXCON_CFG_COMMON;
					inst->Status			= 0xffff;
					inst->Info.StatusInfo	= axCon_CMD_INIT;
					inst->Internal.bDisable         		= 0;
				} break;
				
			
			// =========================================================== //
			case AXCON_CFG_COMMON:		// FB COMMON CFG
				// =========================================================== //
				if ( inst->Internal.bFirstInStep ) // first time in the step
				{
					pAxCtrl->Enable										= 1;
					//
					inst->Internal.FB.MC_BR_CyclicRead_0.Axis			= pAxCtrl->Axis;
					inst->Internal.FB.MC_BR_CyclicRead_0.DataAddress	= (unsigned long)&(inst->Internal.EncRawPos);
					inst->Internal.FB.MC_BR_CyclicRead_0.DataType		= ncPAR_TYP_DINT;
					inst->Internal.FB.MC_BR_CyclicRead_0.Mode			= mcEVERY_RECORD;
					inst->Internal.FB.MC_BR_CyclicRead_0.ParID			= ACP10PAR_ENCOD1_S_ACT; // ParID 91: Actual encoder position
					inst->Internal.FB.MC_BR_CyclicRead_0.Enable			= 1;
					//
					inst->Internal.FB.TON_Timeout.IN					= 1;
					inst->Internal.FB.TON_Timeout.PT					= 10000; // 10s
				}
				else // after the first time in the step
				{
					// ----------------------------
					// Check if the activations are valid 
					// ----------------------------
					if ( pAxCtrl->Active && inst->Internal.FB.MC_BR_CyclicRead_0.Valid )
					{
						inst->Internal.Step								= AXCON_CFG_MODE_SPECIFIC;
						inst->Info.pActPosEnc							= (unsigned long)&(inst->Internal.EncRawPos);
						inst->Internal.FB.TON_Timeout.IN				= 0;
					}
					else if ( inst->Internal.FB.TON_Timeout.Q ) // timeout occurred
					{
						inst->Internal.ErrorCheck						= ERR_AXCON_INIT_TO;
						inst->Info.StatusInfo							= axCon_ERR_INIT_TO_COMMON;
					}
				} break; // end of the step
			
			
			// =========================================================== //
			case AXCON_CFG_MODE_SPECIFIC:		// MODE SPECIFIC CFG
				// =========================================================== //
				if ( inst->Internal.bFirstInStep ) // first time in the step
				{
					inst->Internal.FB.TON_Timeout.IN					= 1;
					inst->Internal.FB.TON_Timeout.PT					= 10000; // 10s
				}
				else // after the first time in the step
				{
					// ----------------------------
					// Case machine for different modes 
					// ----------------------------
					switch ( inst->Mode )
					{						
						// ----------------------------
						case axCon_MODE_CYCLIC_CURRENT:		//  Cyclic set current config
							// ----------------------------
							inst->Info.StatusInfo 		= axCon_CMD_CONFIG_SET_CURRENT;
							inst->Internal.locFunStat	= CfgCyclicCurrent(1);							
							break;
						
						
						// ----------------------------
						case axCon_MODE_CYCLIC_POSITION:	//  Cyclic set position config
							// ----------------------------
							inst->Info.StatusInfo		= axCon_CMD_CONFIG_SET_POSITION;
							break;
						
						
						// ----------------------------
						default:							//  Invalid operation mode
							// ----------------------------
							inst->Status				= ERR_AXCON_INVALID_OPR_MODE;
							inst->Info.StatusInfo		= axCon_ERR_INVALID_OPR_MODE;
							break;						
					} /* switch ( inst-> Mode ) */
					
					// ----------------------------
					// Check for function results 
					// ----------------------------
					if ( inst->Internal.locFunStat == ERR_FUB_BUSY && !inst->Internal.FB.TON_Timeout.Q ) 	{;}	// Function still busy -> do nothing
					else if ( !inst->Internal.locFunStat )	// Function is done
					{
						inst->Active			= 1;
						inst->Status			= 0;
						inst->Internal.Step		= AXCON_READY;
						inst->Internal.FB.TON_Timeout.IN = 0;
						if 		( inst->Mode == axCon_MODE_CYCLIC_CURRENT )		{ inst->CyclicSetCurrentActive 	= 1; }
						else if ( inst->Mode == axCon_MODE_CYCLIC_POSITION )	{ inst->CyclicSetPositionActive	= 1; } 
					}		
					else if ( inst->Internal.FB.TON_Timeout.Q )
					{
						inst->Internal.FB.TON_Timeout.IN	= 0;
						inst->Internal.ErrorCheck			= ERR_AXCON_INIT_TO;
						inst->Info.StatusInfo				= axCon_ERR_INIT_TO_MODE_SPECIFIC;
						inst->Internal.locFunStat			= 0;
					}
					else // Error occurred (not a timeout)
					{
						inst->Internal.ErrorCheck			= inst->Internal.locFunStat;
						inst->Internal.FB.TON_Timeout.IN	= 0;
						inst->Info.StatusInfo				= axCon_ERR_INIT_TO_MODE_SPECIFIC;
						inst->Internal.locFunStat			= 0;
					}					
				} break; // end of the step	
			
			
			// =========================================================== //
			case AXCON_READY:		// READY FOR COMMANDS
				// =========================================================== //
				if ( inst->Internal.bFirstInStep ) // first time in the step
				{
					inst->Info.StatusInfo					= axCon_CMD_IDLE;
				}
				else // after the first time in the step
				{
					// ----------------------------
					// Check for commands 
					// ----------------------------	
					if ( inst->Home )
					{
						inst->Home							= 0;
						inst->Internal.Step					= AXCON_HOME;					
					}
				} break; // end of the step
			
			
			// =========================================================== //
			case AXCON_HOME:		// AXIS HOMING
				// =========================================================== //
				if ( inst->Internal.bFirstInStep ) // first time in the step
				{
					inst->Info.StatusInfo					= axCon_CMD_HOME;
					//					pAxPar->Home.PositionErrorStopLimit		= 1000000;
					// ----------------------------
					// Check if the simulation is active 
					// ----------------------------
					if ( inst->Simulation )
					{
						pAxPar->Home.Mode					= mpAXIS_HOME_MODE_DIRECT;
						pAxPar->Home.Position				= 0;
					}
					else // Simulation is not active
					{
						pAxPar->Home.Mode					= mpAXIS_HOME_MODE_ABSOLUTE_CORR;
						pAxPar->Home.Position				= (LREAL)pPar->General.AxisHomeOffset;
					}
					//
					pAxCtrl->Home							= 1;
				}
				else // after the first time in the step
				{
					// ----------------------------
					// Wait until the homing is completed 
					// ----------------------------
					if ( pAxCtrl->IsHomed && pAxCtrl->Info.PLCopenState != mpAXIS_HOMING )
					{	
						pAxCtrl->Home						= 0;			// Reset the command to the MpAxisBasic
						inst->Internal.Step					= AXCON_READY;	// Change the step
					}
				} break; // end of the step
			
			
			// =========================================================== //
			case AXCON_STOPPING:	// STOPPING
				// =========================================================== //
				if ( inst->Internal.bFirstInStep ) // first time in the step
				{
					inst->Internal.FB.TON_Timeout.PT		= 20000;
					inst->Internal.FB.TON_Timeout.IN		= 0;
				}
				else // after the first time in the step
				{
					// ----------------------------
					// Check actual mode 
					// ----------------------------
					switch ( inst->Mode )
					{
						// ----------------------------
						case axCon_MODE_CYCLIC_CURRENT:		// Cyclic set current mode
							// ----------------------------
							inst->Info.StatusInfo			= axCon_CMD_STOP_SET_CURRENT;
							inst->Internal.locFunStat		= StopCyclicCurrent(1);
							break;					
					} /* switch (oprMode) */
			
					// ----------------------------
					// Check the local function results  
					// ----------------------------	
					if ( inst->Internal.locFunStat == ERR_FUB_BUSY && !inst->Internal.FB.TON_Timeout.Q ) 	{;}	// Function still busy -> do nothing
					else if ( !inst->Internal.locFunStat )	// Function is done
					{
						inst->Internal.bDisable							= 0;
						inst->Internal.FB.TON_Timeout.IN	= 0;
						inst->Power							= 0;
						inst->Internal.Step					= AXCON_DISABLED;
					}		
					else if ( inst->Internal.FB.TON_Timeout.Q )
					{
						inst->Internal.FB.TON_Timeout.IN	= 0;
						inst->Status						= ERR_AXCON_STOP_TO;
						inst->Info.StatusInfo				= axCon_ERR_STOP_TO_MODE_SPECIFIC;
						inst->Internal.bDisable							= 0;
						inst->Enable						= 1;
						inst->Internal.locFunStat			= 0;
					}
					else // Error occurred (not a timeout)
					{
						inst->Status						= inst->Internal.locFunStat;
						inst->Internal.FB.TON_Timeout.IN	= 0;
						inst->Info.StatusInfo				= axCon_ERR_STOP_MODE_SPECIFIC;
						inst->Internal.bDisable							= 0;
						inst->Enable						= 1;
						inst->Internal.locFunStat			= 0;
					}
				} break; // end of the step		
			
			
			// =========================================================== //
			case AXCON_ERROR:	// ERROR OCCURRED
				// =========================================================== //
				if ( inst->Internal.bFirstInStep ) // first time in the step
				{
					inst->Error.Error						= 1;
					inst->Status							= inst->Internal.ErrorCheck;
				}
				else // after the first time in the step
				{
					inst->Error.AxisErrorID					= pAxCtrl->Info.Diag.Internal.Code;
				} break; // end of the step
		} /* switch (inst->Internal.Step) */
		if ( inst->Internal.Step == AXCON_READY && inst->Internal.bDisable )	{inst->Internal.Step = AXCON_STOPPING;}
		
		// ----------------------------
		// If cyclic current mode is active
		// check if the set current is 
		// within the limits -- if not it will be 
		// overriden
		if ( inst->Mode == axCon_MODE_CYCLIC_CURRENT && inst->Active )	{CurrentLimiter((float *)inst->Buffer);} // Note here that ->Buffer is already an address		
		// ----------------------------
		// ---------------------------		 END OF THE MAIN CONTROL SEQUENCE		-------------------------------- //
		
		// ----------------------------
		// Other FB calls
		MC_BR_CyclicRead(&inst->Internal.FB.MC_BR_CyclicRead_0);
		MC_BR_CyclicWrite(&inst->Internal.FB.MC_BR_CyclicWrite_0);
		MC_BR_WriteParID(&inst->Internal.FB.MC_BR_WriteParID_0);		
		TON(&inst->Internal.FB.TON_Timeout);
		// ----------------------------
		
		// ----------------------------
		// Set outputs
		if ( inst->Active )
		{
			inst->Info.pActPosAxis	= (unsigned long)&(pAxCtrl->Position);	
			
			// Map the axis power on command to the axis
			if ( pAxCtrl )
				pAxCtrl->Power			= inst->Power && pAxCtrl->Info.ReadyToPowerOn;
			
			// ----------------------------
			// Check various statuses 
			// ----------------------------
			inst->Info.PoweredOn		= (plcbit)CheckStat(axCon_STAT_AX_POWER);
			inst->Info.IsHomed			= (plcbit)CheckStat(axCon_STAT_AX_REF);		
			
			// ----------------------------
			// Check errors within the FB 
			// ----------------------------
			inst->Internal.ErrorCheck	= CheckError();
		}
		// ----------------------------
		
		// ----------------------------
		// If there is an error change the
		// step to AXCON_ERROR
		// ----------------------------
		if ( inst->Internal.ErrorCheck && inst->Internal.Step != AXCON_ERROR ) // Error occurred
			inst->Internal.Step		= AXCON_ERROR;
		
		
		// ----------------------------
		//  bFirstInStep flag handling
		inst->Internal.bFirstInStep								= _FIS(inst->Internal.Step,inst->Internal.OldStep);
		inst->Internal.locFun.CfgCyclicCurrent.bFirstInStep 	= _FIS(inst->Internal.locFun.CfgCyclicCurrent.Step,inst->Internal.locFun.CfgCyclicCurrent.OldStep);
		inst->Internal.locFun.CfgCyclicPosition.bFirstInStep	= _FIS(inst->Internal.locFun.CfgCyclicPosition.Step,inst->Internal.locFun.CfgCyclicPosition.OldStep);
		inst->Internal.locFun.StopCyclicCurrent.bFirstInStep	= _FIS(inst->Internal.locFun.StopCyclicCurrent.Step,inst->Internal.locFun.StopCyclicCurrent.OldStep);
		// ----------------------------
		
		
		// ----------------------------
		// Local functions are reset when not needed 		
		// CfgCyclicCurrent() & CfgCyclicPosition
		if ( inst->Internal.Step != AXCON_CFG_MODE_SPECIFIC )		
		{
			memset((unsigned long)&(inst->Internal.locFun.CfgCyclicCurrent),	0, sizeof(inst->Internal.locFun.CfgCyclicCurrent));	
			memset((unsigned long)&(inst->Internal.locFun.CfgCyclicPosition),	0, sizeof(inst->Internal.locFun.CfgCyclicPosition));
		}	
		
		// StopCyclicCurrent & StopCyclicPos
		if ( inst->Internal.Step != AXCON_STOPPING )
		{
			memset((unsigned long)&(inst->Internal.locFun.StopCyclicCurrent),	0, sizeof(inst->Internal.locFun.StopCyclicCurrent));
			memset((unsigned long)&(inst->Internal.locFun.StopCyclicPos),		0, sizeof(inst->Internal.locFun.StopCyclicPos));
		}
		// ----------------------------		
	}
	else // FB is disabled
	{
		inst->Status	 										= 0xfffe;
		inst->Internal.bEvrOk 									= 0;
		inst->Internal.bFirstInStep								= 0;
		inst->Internal.ii										= 0;
		inst->Internal.FB.MC_BR_CyclicRead_0.Enable				= 0;
		inst->Internal.FB.MC_BR_CyclicWrite_0.Enable			= 0;
		inst->Internal.FB.MC_BR_WriteParID_0.Execute			= 0;
		inst->Internal.FB.TON_Timeout.IN						= 0;
		inst->Internal.ErrorCheck								= 0;
		inst->Internal.Step										= AXCON_DISABLED;
		inst->Internal.OldStep									= AXCON_DISABLED;
		inst->Info.StatusInfo									= axCon_INACTIVE;
		inst->CyclicSetCurrentActive							= 0;
		inst->CyclicSetPositionActive							= 0;
		inst->Active											= 0;
		inst->Error.Error										= 0;
		inst->Error.AxisErrorID									= 0;
		inst->Internal.bDisable												= 0;
		inst->Internal.bOldEnable												= 0;
		memset((unsigned long)&(inst->Internal.locFun), 0, sizeof(inst->Internal.locFun));
	} /* if (enable) */	
} /* end of the AxCon() implementation */

#ifdef __cplusplus
	};
#endif
