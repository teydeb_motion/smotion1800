/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Library:	CycAxCon
* Author:   abdullahoglf
* Created:  August 18, 2016
**********************************************************************************
* Description:
* 	- Carry out the necessary configurations for the cyclic set current mode
* History:
*	
**********************************************************************************/
#include "AxConInt.h"

unsigned short CfgCyclicCurrent(plcbit Enable)
{	
	// -------------------------------------------------------------------------------------------------------- //
	//										INTERNAL DECLARATIONS												// 
	// -------------------------------------------------------------------------------------------------------- //
	extern unsigned long	pMain;		/* Main FB handle */
	extern unsigned long 	pMainInt;	/* Main FB internal strucutre handle */
	struct AxCon			*pMainFb;	/* Pointer to the Main FB */
	AxCon_Parameters_typ	*pPar;		/* Pointer to the Main parameters structure */
	AxCon_Internal_typ		*pInt;		/* Pointer to the main fb internal structure */
	struct MpAxisBasic		*pAxCtrl;	/* Pointer to the axis to be controlled */
	ACP10AXIS_typ			*pAxObj;	/* Pointer to the axis object */
	//	
	// ----------------------------
	// Assign the pointers
	//	* All the references of the FB
	//		must be OK by this point
	// ----------------------------
	pMainFb	= (struct AxCon			*) 	pMain;
	pPar	= (AxCon_Parameters_typ	*)	pMainFb->Parameters;
	pAxCtrl	= (struct MpAxisBasic	*)	pMainFb->MpAxis;
	pAxObj	= (ACP10AXIS_typ		*)	pAxCtrl->Axis;
	pInt	= (AxCon_Internal_typ	*)	pMainInt;
	
	// -------------------------------------------------------------------------------------------------------- //
	//							M A I N  C O N T R O L  S E Q U E N C E											// 
	// -------------------------------------------------------------------------------------------------------- //
	switch ( pInt->locFun.CfgCyclicCurrent.Step )
	{
		// ----------------------------
		case CFG_CUR_DISABLED:			// Function is disabled 
		// ----------------------------
			if ( Enable )
			{
				pInt->locFun.CfgCyclicCurrent.Step++;
			} break;
		
		// ----------------------------
		case CFG_CUR_DISABLE_POS_CTRL:	// Disable the position controller
		// ----------------------------
			if ( pInt->locFun.CfgCyclicCurrent.bFirstInStep ) // first time in the step
			{
				if ( pAxObj->controller.status != 0 && pAxCtrl->Info.PLCopenState != mpAXIS_DISABLED ) // controller must be disabled
				{
					return ERR_AXCON_INIT_CONTROLLER_IS_ON;
				}
				else // controller is disabled
				{
					// ----------------------------
					// Initialzie the MC_BR_WriteParID fb 
					// ----------------------------		
					pInt->FB.MC_BR_WriteParID_0.Axis		= (unsigned long)pAxObj;	
					pInt->locFun.CfgCyclicCurrent.iTemp		= 0;					
					pInt->FB.MC_BR_WriteParID_0.DataAddress	= (unsigned long)&(pInt->locFun.CfgCyclicCurrent.iTemp);
					pInt->FB.MC_BR_WriteParID_0.DataType	= ncPAR_TYP_UINT;
					pInt->FB.MC_BR_WriteParID_0.ParID		= ACP10PAR_PCTRL_ENABLE_PARID;
					pInt->FB.MC_BR_WriteParID_0.Execute		= 1;
				}
			}
			else // after the first time in the step
			{
				// ----------------------------
				// Check FB results 
				// ----------------------------	
				if ( pInt->FB.MC_BR_WriteParID_0.Busy )		{;} // fb busy - do nothing
				else if ( pInt->FB.MC_BR_WriteParID_0.Done ) // fb is done
				{
					pInt->FB.MC_BR_WriteParID_0.Execute 	= 0;
					pInt->locFun.CfgCyclicCurrent.Step++;
				}
				else if ( pInt->FB.MC_BR_WriteParID_0.Error ) // error occured
				{
					pInt->FB.MC_BR_WriteParID_0.Execute		= 0;
					return pInt->FB.MC_BR_WriteParID_0.ErrorID;
				}
			} break; // end of the step
		
		
		// ----------------------------
		case CFG_CUR_ASSIGN_ICTRL_ADD:		// Configure the CyclicWrite ParID 
		// ----------------------------
			if ( pInt->locFun.CfgCyclicCurrent.bFirstInStep ) // first time in the step
			{
				// ----------------------------
				// Initialzie the MC_BR_WriteParID fb 
				// ----------------------------		
				pInt->FB.MC_BR_WriteParID_0.Axis		= (unsigned long)pAxObj;	
				pInt->locFun.CfgCyclicCurrent.iTemp		= ACP10PAR_VAR_R4_0; // [4128]
				pInt->FB.MC_BR_WriteParID_0.DataAddress	= (unsigned long)&(pInt->locFun.CfgCyclicCurrent.iTemp); 
				pInt->FB.MC_BR_WriteParID_0.DataType	= ncPAR_TYP_UINT;
				pInt->FB.MC_BR_WriteParID_0.ParID		= ACP10PAR_ICTRL_ADD_SET_PARID;
				pInt->FB.MC_BR_WriteParID_0.Execute		= 1;					
			}
			else // after the first time in the step
			{
				// ----------------------------
				// Check FB results 
				// ----------------------------	
				if ( pInt->FB.MC_BR_WriteParID_0.Busy )		{;} // fb busy - do nothing
				else if ( pInt->FB.MC_BR_WriteParID_0.Done ) // fb is done
				{
					pInt->FB.MC_BR_WriteParID_0.Execute 	= 0;
					pInt->locFun.CfgCyclicCurrent.Step++;
				}
				else if ( pInt->FB.MC_BR_WriteParID_0.Error ) // error occured
				{
					pInt->FB.MC_BR_WriteParID_0.Execute		= 0;
					return pInt->FB.MC_BR_WriteParID_0.ErrorID;
				}		
			} break; // end of the step
		
		
		// ----------------------------
		case CFG_CUR_ENABLE_CYC_WRITE:		// Enable the cyclic write connection
		// ----------------------------
			if ( pInt->locFun.CfgCyclicCurrent.bFirstInStep ) // first time in the step
			{
				pInt->FB.MC_BR_CyclicWrite_0.Axis			= (unsigned long)pAxObj;
				pInt->FB.MC_BR_CyclicWrite_0.DataAddress	= (unsigned long)(pMainFb->Buffer);
				pInt->FB.MC_BR_CyclicWrite_0.DataType		= ncPAR_TYP_REAL;
				pInt->FB.MC_BR_CyclicWrite_0.Mode			= mcEVERY_RECORD;
				pInt->FB.MC_BR_CyclicWrite_0.ParID			= ACP10PAR_VAR_R4_0;
				pInt->FB.MC_BR_CyclicWrite_0.Enable			= 1;
			}
			else // after the first time in the step
			{
				if ( pInt->FB.MC_BR_CyclicWrite_0.Busy && !pInt->FB.MC_BR_CyclicWrite_0.Valid ) 		{;} 		// FB is busy -> do nothing
				else if ( pInt->FB.MC_BR_CyclicWrite_0.Valid )	// FB is done
				{
					pInt->locFun.CfgCyclicCurrent.Step++;
				} 
				else if ( pInt->FB.MC_BR_CyclicWrite_0.Error ) 
				{
					pInt->FB.MC_BR_CyclicWrite_0.Enable	 	= 0;
					return pInt->FB.MC_BR_CyclicWrite_0.ErrorID;
				}				
			} break; // end of the step	
		
		// ----------------------------
		case CFG_CUR_DISABLE_SPEED_CTRL:	// Disable speed controller
		// ----------------------------
			if ( pInt->locFun.CfgCyclicCurrent.bFirstInStep ) // first time in the step
			{
				pInt->Controller = pAxObj->controller;
				memset((unsigned long)&pAxObj->controller.speed, 0, sizeof(ACP10CTRSP_typ));
			}
			else // after the first time in the step
			{
				pInt->locFun.CfgCyclicCurrent.iTemp = ncaction(pAxCtrl->Axis, ncCONTROLLER, ncINIT);
				if ( pInt->locFun.CfgCyclicCurrent.iTemp == ncOK )
				{
					return 0;	
				}
			} break; // end of the step		
	} /* switch ( pInt->Step ) */	
	return 0xffff;	// return busy once the function gets to this point
}
