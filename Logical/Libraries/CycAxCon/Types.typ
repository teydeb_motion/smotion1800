(*AxCon FB parameters*)

TYPE
	AxCon_Parameters_General_typ : 	STRUCT 
		AxisHomeOffset : REAL;
	END_STRUCT;
	AxCon_Parameters_CycCurrent_typ : 	STRUCT 
		MaxCurrent : REAL;
	END_STRUCT;
	AxCon_Parameters_typ : 	STRUCT 
		General : AxCon_Parameters_General_typ;
		CycCurrent : AxCon_Parameters_CycCurrent_typ;
	END_STRUCT;
END_TYPE

(**)

TYPE
	AxCon_Buffer_typ : 	STRUCT 
		Buffer : ARRAY[0..MAX_NO_BUFFER_MIN_1]OF REAL;
	END_STRUCT;
END_TYPE

(**)

TYPE
	AxCon_Error_typ : 	STRUCT 
		AxisErrorID : UINT;
		Error : BOOL; (*Error flag*)
	END_STRUCT;
END_TYPE

(*AxCon detailed FB information structure*)

TYPE
	AxCon_StatusCheck_enm : 
		(
		axCon_STAT_AX_REF,
		axCon_STAT_AX_POWER
		);
	AxCon_StatusInfo_enm : 
		(
		axCon_INACTIVE := 0,
		axCon_CMD_IDLE := 5,
		axCon_CMD_INIT := 7,
		axCon_CMD_CONFIG_SET_CURRENT := 10,
		axCon_CMD_CONFIG_SET_POSITION := 15,
		axCon_CMD_STOP_SET_CURRENT := 20,
		axCon_CMD_STOP_SET_POSITION := 25,
		axCon_CMD_HOME := 30,
		axCon_ERR_NULL_PARAMETERS := 200,
		axCon_ERR_NULL_BUFFER := 205,
		axCon_ERR_NULL_MPAXIS := 210,
		axCon_ERR_NULL_AX_OBJ := 215,
		axCon_ERR_NULL_AX_PAR := 220,
		axCon_ERR_INVALID_OPR_MODE := 222,
		axCon_ERR_INIT_TO_COMMON := 225,
		axCon_ERR_INIT_TO_MODE_SPECIFIC := 230,
		axCon_ERR_INIT_MODE_SPECIFIC := 235,
		axCon_ERR_STOP_TO_MODE_SPECIFIC := 240,
		axCon_ERR_STOP_MODE_SPECIFIC := 245
		);
	AxCon_Info_typ : {REDUND_UNREPLICABLE} 	STRUCT 
		StatusInfo : {REDUND_UNREPLICABLE} AxCon_StatusInfo_enm; (*FB status info -> can be used to check what the FB is currently doing*)
		pActPosAxis : {REDUND_UNREPLICABLE} UDINT; (*Reference to the actual scaled position of the axis [Points to type LREAL]*)
		pActPosEnc : {REDUND_UNREPLICABLE} UDINT; (*Reference to the actual raw encoder value [Points to type UDINT]*)
		PoweredOn : {REDUND_UNREPLICABLE} BOOL;
		IsHomed : {REDUND_UNREPLICABLE} BOOL;
	END_STRUCT;
END_TYPE

(**)
(*FB Internal control structure*)

TYPE
	AxCon_Steps_enm : 
		(
		AXCON_DISABLED := 0,
		AXCON_CFG_COMMON := 10, (*Common fb configurations*)
		AXCON_CFG_MODE_SPECIFIC := 15, (*FB mode specific configurations*)
		AXCON_READY := 20,
		AXCON_HOME := 22, (*Home the axis*)
		AXCON_RUNNING := 25,
		AXCON_STOPPING := 30,
		AXCON_ERROR := 255
		);
	AxCon_Internal_typ : 	STRUCT 
		ii : USINT; (*Internal iteration variable*)
		bDisable : BOOL;
		Step : AxCon_Steps_enm; (*Actual step value of the FB*)
		OldStep : AxCon_Steps_enm; (*Step value from the previous cycle*)
		EncRawPos : DINT;
		bOldEnable : BOOL;
		bFirstInStep : BOOL; (*First time in a step*)
		bEvrOk : BOOL; (*Dummy flag*)
		locFunStat : UINT;
		locFun : _Internal_SubFunctions_typ; (*Local functions that are used *)
		ErrorCheck : UINT; (*Local result of the cyclic error monitoring*)
		Controller : ACP10CTRL_typ;
		FB : AxCon_Internal_FBs_typ;
	END_STRUCT;
	AxCon_Internal_FBs_typ : 	STRUCT 
		TON_Timeout : TON;
		MC_BR_CyclicRead_0 : MC_BR_CyclicRead;
		MC_BR_CyclicWrite_0 : MC_BR_CyclicWrite;
		MC_BR_WriteParID_0 : MC_BR_WriteParID;
	END_STRUCT;
END_TYPE

(**)
(*Sub Function interfaces*)

TYPE
	_StopCyclicPos_typ : {REDUND_UNREPLICABLE} 	STRUCT 
		Step : {REDUND_UNREPLICABLE} UINT;
		OldStep : {REDUND_UNREPLICABLE} UINT;
		bFirstInStep : {REDUND_UNREPLICABLE} UINT;
		Stat : {REDUND_UNREPLICABLE} USINT;
	END_STRUCT;
	_StopCyclicCurrent_typ : {REDUND_UNREPLICABLE} 	STRUCT 
		iTemp : {REDUND_UNREPLICABLE} UINT;
		Step : {REDUND_UNREPLICABLE} UINT;
		OldStep : {REDUND_UNREPLICABLE} UINT;
		bFirstInStep : {REDUND_UNREPLICABLE} UINT;
		Stat : {REDUND_UNREPLICABLE} USINT;
	END_STRUCT;
	_CfgCyclicPosition_typ : {REDUND_UNREPLICABLE} 	STRUCT 
		Step : {REDUND_UNREPLICABLE} UINT;
		OldStep : {REDUND_UNREPLICABLE} UINT;
		bFirstInStep : {REDUND_UNREPLICABLE} BOOL;
		Stat : {REDUND_UNREPLICABLE} UINT;
	END_STRUCT;
	_CfgCyclicCurrent_typ : 	STRUCT 
		Step : {REDUND_UNREPLICABLE} UINT;
		OldStep : {REDUND_UNREPLICABLE} UINT;
		bFirstInStep : {REDUND_UNREPLICABLE} BOOL;
		iTemp : UINT;
		Stat : UINT;
	END_STRUCT;
	_Internal_SubFunctions_typ : 	STRUCT 
		StopCyclicPos : _StopCyclicPos_typ;
		StopCyclicCurrent : _StopCyclicCurrent_typ;
		CfgCyclicPosition : _CfgCyclicPosition_typ;
		CfgCyclicCurrent : _CfgCyclicCurrent_typ;
	END_STRUCT;
END_TYPE
