/*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Library:	CycAxCon
* Author:   abdullahoglf
* Created:  August 18, 2016
**********************************************************************************
* Description:
*
* History:
*	
**********************************************************************************/

// ----------------------------------------------------------------------------- //
//						GLOBAL DEFINITIONS	                                     // 
// ----------------------------------------------------------------------------- //
#ifndef _REPLACE_CONST
	#define	_REPLACE_CONST 
#endif

#ifndef _DEFAULT_INCLUDES
	#define _DEFAULT_INCLUDES
#endif

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif
#include <bur/plctypes.h>
#include "String.h"

// ----------------------------------------------------------------------------- //
//						INTERNAL STATE DEFINITIONS                               // 
// ----------------------------------------------------------------------------- //
/* CfgCyclicCurrent */
#define		CFG_CUR_DISABLED				0		/* Function is disabled */
#define 	CFG_CUR_DISABLE_POS_CTRL		1		/* Disable the position controller - ParID: PCTRL_ENABLE_PARID [231] */
#define		CFG_CUR_ASSIGN_ICTRL_ADD		2		/* Enabling of the ICTRL_ADD_SET_PARID [325] */
#define		CFG_CUR_ENABLE_CYC_WRITE		3		/* Enabling of the CyclicWrite to the set current ParID: 4128 */
#define 	CFG_CUR_DISABLE_SPEED_CTRL		4		/* Disable the speed controller via resetting controller parameters */
#define 	CFG_CUR_ERROR					0xffff	/* Error occurred */

/* StopCyclicCurrent */
#define		STOP_CUR_DISABLED				0		/* Function is disabled */
#define		STOP_CUR_RESET_CURRENT			1		/* Reset the set current value to 0 */
#define 	STOP_CUR_ENABLE_PCTRL			2		/* Enable the position controller	*/
#define		STOP_CUR_RESTORE_SCTRL			3		/* Restore the speed controller values */
#define		STOP_CUR_ERROR					0xffff	/* Error occurred */

// ----------------------------------------------------------------------------- //
//						INTERNAL MACROS		                                     // 
// ----------------------------------------------------------------------------- //
#define _FIS(Var,Old) 			  	    \
({plcbit Result; 						\
	Result = (Var != Old) ? (1) : (0);  \
    Old=Var;             				\
    Result;                         	\
})		


// ----------------------------------------------------------------------------- //
//						INTERNAL FUNCTION PROTOTYPES                             // 
// ----------------------------------------------------------------------------- //
unsigned short CfgCyclicCurrent(plcbit Enable);
unsigned short StopCyclicCurrent(plcbit Enable);
float ABSf(float x);
void CurrentLimiter(float* pSetCurrent);
unsigned short CheckStat(AxCon_StatusCheck_enm Stat);
unsigned short CheckError(void);

