
{REDUND_UNREPLICABLE} FUNCTION_BLOCK AxCon (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : {REDUND_UNREPLICABLE} BOOL; (*FB Enable*)
		Power : {REDUND_UNREPLICABLE} BOOL; (*Power on the axis*)
		Home : {REDUND_UNREPLICABLE} BOOL; (*Home the axis*)
		Simulation : {REDUND_UNREPLICABLE} BOOL; (*FB Simulation flag*)
		Parameters : {REDUND_UNREPLICABLE} UDINT; (*Reference to the parameters*)
		Mode : {REDUND_UNREPLICABLE} AxCon_FB_Modes_enm; (*Operation mode of the FB*)
		MpAxis : {REDUND_UNREPLICABLE} UDINT; (*Reference to the MpAxisBasic mapp component*)
		Buffer : {REDUND_UNREPLICABLE} UDINT; (*Rerence to the cyclic setpoints buffer*)
	END_VAR
	VAR_OUTPUT
		Active : {REDUND_UNREPLICABLE} BOOL; (*FB is active*)
		CyclicSetPositionActive : {REDUND_UNREPLICABLE} BOOL; (*Cyclic set position control is activated*)
		CyclicSetCurrentActive : {REDUND_UNREPLICABLE} BOOL; (*Cyclic set current control is activated*)
		Status : {REDUND_UNREPLICABLE} UINT; (*Status ID of the FB*)
		Info : {REDUND_UNREPLICABLE} AxCon_Info_typ; (*More information from the FB*)
		Error : {REDUND_UNREPLICABLE} AxCon_Error_typ; (*Error occurred within the FB*)
	END_VAR
	VAR
		Internal : {REDUND_UNREPLICABLE} AxCon_Internal_typ; (*Internal control structure*)
	END_VAR
END_FUNCTION_BLOCK
