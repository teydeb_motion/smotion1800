(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: SysDiag
 * File: BrakeControl.st
 * Author: abdullahoglf
 * Created: November 09, 2014
 ********************************************************************
 * Implementation of library SysDiag
 ********************************************************************) 

(* Motor Holding Brake Control *)
FUNCTION_BLOCK BrakeControl
	// ----------------------------
	// check for FB Enable
	IF Enable THEN // FB is enabled
		// ---------------------------
		// check for axis address
		IF Reference <> 0 THEN // there is a reference
			// -----------------------
			// load the default settings if the input is active
			IF DefaultSettings THEN
				// all the values below are the default variables on the drive
				BrakeMode.AutomaticControl       	  := TRUE;
				BrakeMode.RestrictBrakeControl   	  := TRUE;
				BrakeMode.ControlMonitoring      	  := TRUE;
				BrakeMode.MovementMonitoring     	  := TRUE;
				BrakeMode.VoltageMonitoring      	  := TRUE;
				BrakeMode.TestAtPowerOff              := FALSE;
				BrakeMode.TestAtPowerOn               := FALSE;
				BrakeMode.AutomaticInductionStop      := FALSE;
				BrakeMode.ControlMonitoringFilterTime := 0.5; // [s]
	 			  
				BrakeTest.PositionLimit			      := 1000; // [units]
				BrakeTest.TestTorque                  := 0; // [Nm] 
				BrakeTest.TestDuration                := 0; // [s]
				BrakeTest.TestMode                    := 0;
			END_IF; // end of default settings
			
			(* ---------------------------------------------------------------------------------- *)
			(* -------------------			MAIN CONTROL SEQUENCE           --------------------- *)
			(* ---------------------------------------------------------------------------------- *)
			
			CASE Step OF
				//-----------------------------------//
				BRAKE_WAIT: // WAIT
				//-----------------------------------//
					IF firstInStep THEN // first time in step
						Busy   := FALSE;
						MC_BR_BrakeControl_0.Configuration.BrakeMode := BrakeMode;
						MC_BR_BrakeControl_0.Configuration.BrakeTest := BrakeTest;
						test := TRUE;
					ELSE // after the first time in step
						// -------------------
						// release brakes
						IF BrakeOpen THEN
							BrakeOpen := FALSE;
							Step      := BRAKE_OPEN;						
						// -------------------
						// close the brakes	
						ELSIF BrakeClose THEN
							BrakeClose := FALSE;
							Step       := BRAKE_CLOSE;
						// --------------------
						// get brake status
						ELSIF GetBrakeStatus THEN
							GetBrakeStatus := FALSE;
							Step           := BRAKE_GET_STATUS;							
						END_IF; // end of cmd check							
					END_IF; // end of the step
				
				//-------------------------------------//
				BRAKE_OPEN: // release the brakes
				//-------------------------------------//
					IF firstInStep THEN // first time in step
						Busy := TRUE;
						Done := FALSE;
						MC_BR_BrakeControl_0.Command := mcOPEN;
						MC_BR_BrakeControl_0.Execute := TRUE;
					ELSE // after the first time in step
						// -------------------
						// check for FB status
						IF MC_BR_BrakeControl_0.Done THEN // fb is done w.o. an error
							MC_BR_BrakeControl_0.Execute := FALSE;
							Step := BRAKE_GET_STATUS;
						
						ELSIF MC_BR_BrakeControl_0.Error THEN // fb has an error
							ErrorInfo := ERROR_RELEASING_BRAKE;
							Step      := BRAKE_ERROR;								  
						END_IF; // end of FB status check
					END_IF; // end of the step
				
				//-------------------------------------//
				BRAKE_CLOSE: // close the brakes
				//-------------------------------------//
					IF firstInStep THEN // first time in step
						Busy := TRUE;
						Done := FALSE;
						MC_BR_BrakeControl_0.Command := mcCLOSE;
						MC_BR_BrakeControl_0.Execute := TRUE;
					ELSE // after the first time in step
						// -------------------
						// check for FB status
						IF MC_BR_BrakeControl_0.Done THEN // fb is done w.o. an error
							MC_BR_BrakeControl_0.Execute := FALSE;
							Step := BRAKE_GET_STATUS;							
						ELSIF MC_BR_BrakeControl_0.Error THEN // fb has an error
							ErrorInfo := ERROR_CLOSING_BRAKE;
							Step      := BRAKE_ERROR;								  
						END_IF; // end of FB status check
					END_IF; // end of the step
				
				//-------------------------------------//
				BRAKE_GET_STATUS: // get the current status of the brakes
				//-------------------------------------//
					IF firstInStep THEN // first time in the step
						Busy := TRUE;
						Done := FALSE;
						MC_BR_BrakeControl_0.Command := mcGET_BRAKE_STATUS;
						MC_BR_BrakeControl_0.Execute := TRUE;
					ELSE // after the first time in step
						// -------------------
						// check for FB status
						IF MC_BR_BrakeControl_0.Done THEN // fb is done w.o. an error
							MC_BR_BrakeControl_0.Execute := FALSE;
							Done := TRUE;
							// save the status
							IF MC_BR_BrakeControl_0.BrakeStatus THEN
								BrakeStatus := BRAKES_RELEASED;
							ELSE
								BrakeStatus := BRAKES_ENGAGED;
							END_IF;
							Step := BRAKE_WAIT;							
						ELSIF MC_BR_BrakeControl_0.Error THEN // fb has an error
							ErrorInfo := ERROR_GETTING_STATUS;
							Step      := BRAKE_ERROR;								  
						END_IF; // end of FB status check
					END_IF; // end of the step
				
				//-------------------------------------//
				BRAKE_ERROR: // get the current status of the brakes
				//-------------------------------------//
					IF firstInStep THEN // first time in the step
						Busy    := FALSE;
						Done    := FALSE;
						Error   := TRUE;
						ErrorID := MC_BR_BrakeControl_0.ErrorID;
						MC_BR_BrakeControl_0.Execute := FALSE;
					END_IF; // end of the step
			END_CASE; // end of the main case machine 
			
			// -------------------
			// firstInStep control
			IF Step <> OldStep THEN
				firstInStep := TRUE;
			ELSE
				firstInStep := FALSE;
			END_IF;
			OldStep := Step;
			
			// -------------------
			// FB call 
			MC_BR_BrakeControl_0.Axis := Reference;
			MC_BR_BrakeControl_0();
		ELSE // axis reference is 0
			BrakeOpen      := FALSE;
			BrakeClose     := FALSE;
			GetBrakeStatus := FALSE;
			ErrorID        := 55555;
			ErrorInfo      := ERROR_AXIS_REFERENCE_IS_EMPTY;			
		END_IF; // end of axis reference check
		
	ELSE // FB is not enabled
		BrakeOpen      := FALSE;
		BrakeClose     := FALSE;
		GetBrakeStatus := FALSE;
		Busy           := FALSE;
		Done           := FALSE;
		Error          := FALSE;
		ErrorID        := 0;
		ErrorInfo      := NO_ERROR_BRAKES;
		Step           := BRAKE_WAIT;
		firstInStep    := TRUE;
	END_IF; // end of FB enable check

END_FUNCTION_BLOCK