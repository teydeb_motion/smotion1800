(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: SysDiag
 * File: SysDiag.typ
 * Author: abdullahoglf
 * Created: March 14, 2014
 ********************************************************************
 * Data types of library SysDiag
 ********************************************************************)
(*==============================================================================================================================================*)
(*===========================                                                                   AxisDiag FB STRUCTURE                                              =====================================*)
(*==============================================================================================================================================*)

TYPE
	AxisDiagStep_enm : 
		(
		STATE_WAIT_FOR_ENABLE := 0,
		STATE_WAIT_DIAG,
		STATE_AX_WAIT_POWER_ON,
		STATE_WAIT_DIRECT_HOME,
		STATE_DIRECT_HOME,
		STATE_JOG_POS,
		STATE_JOG_NEG,
		STATE_JOG_WAIT_STOP,
		STATE_READ_PARID,
		STATE_WRITE_PARID,
		STATE_ERROR_DIAG
		);
	AxisDiagOutErrorID_enm : 
		(
		NO_ERROR,
		FB_ENABLE_FALSE,
		AX_ERROR,
		POWER_ON_ERROR,
		HOMING_ERROR,
		PARID_READ_ERROR,
		PARID_WRITE_ERROR,
		POINTER_TO_INVALID_ADDRESS,
		INTERNAL_ERROR
		);
	AxisDiagDriveInPar_typ : 	STRUCT 
		ParID : UINT; (*ParID to read/write*)
	END_STRUCT;
	AxisDiagAxInPar_typ : 	STRUCT 
		Distance : UDINT; (*Distance in units*)
		Position : UDINT; (*Position in units *)
		Velocity : UDINT; (*Velocity in units*)
	END_STRUCT;
	AxisDiagOut_typ : 	STRUCT 
		ParIDResponseHmi : STRING[33];
		ParIDResponseValue : STRING[33]; (*ParID response value*)
		ErrorID : AxisDiagOutErrorID_enm := NO_ERROR; (*FB error ID*)
		Error : BOOL; (*FB error flag*)
		Done : BOOL; (*FB done flag*)
		Busy : BOOL; (*FB busy flag*)
	END_STRUCT;
	AxisDiagAxisCmd_typ : 	STRUCT 
		Power : BOOL; (*Power on the axis*)
		Home : BOOL; (*Direct homing of the axis*)
		JogNeg : BOOL; (*Jog negative*)
		JogPos : BOOL; (*Jog Positive*)
		MoveVelocity : BOOL; (*Move velocity  (CURRENTLY NOT USED)*)
		MoveRelative : BOOL; (*Move relative  (CURRENTLY NOT USED)*)
		MoveAbsolute : BOOL; (*Move absolute  (CURRENTLY NOT USED)*)
		Stop : BOOL; (*Stop  (CURRENTLY NOT USED)*)
	END_STRUCT;
	AxisDiagDriveCMd_typ : 	STRUCT 
		DirectAck : BOOL; (*Acknowledge the errors on the drive*)
		WriteParID : BOOL; (*Write ParID*)
		ReadParID : BOOL; (*Read ParID*)
	END_STRUCT;
	AxisDiagInCmd_typ : 	STRUCT 
		ErrAck : BOOL; (*Error acknowledge command*)
		Axis : AxisDiagAxisCmd_typ; (*Axis related commands*)
		Drive : AxisDiagDriveCMd_typ; (*Drive related commands*)
	END_STRUCT;
	AxisDiagInPar_typ : 	STRUCT 
		Axis : AxisDiagAxInPar_typ;
		Drive : AxisDiagDriveInPar_typ;
	END_STRUCT;
	AxisDiagIn_typ : 	STRUCT 
		Par : AxisDiagInPar_typ;
		Cmd : AxisDiagInCmd_typ; (*FB command structure*)
	END_STRUCT;
	AxisDiagHandle_typ : 	STRUCT 
		In : AxisDiagIn_typ; (*FB input structure*)
		Out : AxisDiagOut_typ; (*FB output stracture*)
	END_STRUCT;
END_TYPE

(**)
(*==============================================================================================================================================*)
(*===========================                                                                  AxisStatus FB STRUCTURE                                             =====================================*)
(*==============================================================================================================================================*)

TYPE
	AxisStatFBError_enm : 
		(
		AX_STAT_NO_ERROR,
		FB_NOT_ENABLED,
		INVALID_AXIS_STRUCTURE
		);
	AxisStatStatus_typ : 	STRUCT 
		NetworkInit : USINT;
		NetworkPhase : USINT;
		ErrorID : UINT;
		Error : USINT;
		HomingOk : USINT;
		NodeNr : UINT;
		ControllerReady : USINT;
		ControllerStatus : USINT;
		ReferenceSwitch : USINT;
		NegHwSwitch : USINT;
		PosHwSwitch : USINT;
		Trigger1 : USINT;
		Trigger2 : USINT;
		ActPosition : REAL;
		ActVelocity : REAL;
	END_STRUCT;
	AxisStatOut_typ : 	STRUCT 
		Status : AxisStatStatus_typ;
		Error : BOOL;
		ErrorID : AxisStatFBError_enm := AX_STAT_NO_ERROR;
	END_STRUCT;
	AxisStatIn_typ : 	STRUCT 
	END_STRUCT;
	AxisStatHandle_typ : 	STRUCT 
		In : AxisStatIn_typ;
		Out : AxisStatOut_typ;
	END_STRUCT;
END_TYPE

(**)
(*==============================================================================================================================================*)
(*===========================                                                              BrakeControl FB STRUCTURE                                             =====================================*)
(*==============================================================================================================================================*)

TYPE
	ErrorInfo_enm : 
		(
		NO_ERROR_BRAKES := 0,
		ERROR_AXIS_REFERENCE_IS_EMPTY := 100,
		ERROR_RELEASING_BRAKE := 200,
		ERROR_CLOSING_BRAKE := 300,
		ERROR_GETTING_STATUS := 400
		);
	BrakeControl_BrakeStatus_enm : 
		( (*Brake status structure*)
		BRAKES_ENGAGED := 0,
		BRAKES_RELEASED := 1
		);
	BrakeControl_Steps_enm : 
		( (*FB main states*)
		BRAKE_WAIT := 0,
		BRAKE_OPEN := 10,
		BRAKE_CLOSE := 20,
		BRAKE_GET_STATUS := 30,
		BRAKE_ERROR := 40
		);
END_TYPE

(**)
(*==============================================================================================================================================*)
(*============================                                                      AxisErrorHandler FB STRUCTURE                                             =====================================*)
(*==============================================================================================================================================*)

TYPE
	ErrorInterface_typ : 	STRUCT 
		AxisRef : UDINT; (*Reference to the axis object*)
		Acknowledge : BOOL; (*Command upon which a single error on the selected axis is acknowledged*)
		DataObjName : STRING[12]; (*Data object from which the error text should be read*)
		ErrorCount : UINT; (*Current error count on the selected axis*)
		ErrorID : UINT; (*Current error ID on the axis*)
		ErrorText : ARRAY[0..3]OF STRING[79]; (*Text output reagarding the current error on the axis*)
	END_STRUCT;
END_TYPE
