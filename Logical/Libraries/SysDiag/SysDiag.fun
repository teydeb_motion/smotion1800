(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: SysDiag
 * File: SysDiag.fun
 * Author: abdullahoglf
 * Created: March 14, 2014
 ********************************************************************
 * Functions and function blocks of library SysDiag
 ********************************************************************)

FUNCTION_BLOCK AxisErrorHandler
	VAR_INPUT
		Enable : BOOL; (*FB enable*)
		pInterface : UDINT; (*Pointer to the user interface*)
	END_VAR
	VAR_OUTPUT
		Status : UINT; (*Status of the FB*)
	END_VAR
	VAR
		pErrorIf : REFERENCE TO ErrorInterface_typ; (*Reference to the error interface*)
		oldAck : BOOL; (*Acknowledge cmd from the previous cycle*)
		firstInStep : BOOL; (*Flag: First time in a step of a case machine*)
		oldStep : USINT; (*Step value from the previous cycle*)
		step : USINT; (*Step value*)
		MC_ReadAxisError_0 : MC_ReadAxisError; (*Axis error handling FB*)
		oldCnt : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK AxisDiag (*Axis&Drive Diagnose Features*)
	VAR_INPUT
		Enable : BOOL; (*Enable of the FB*)
		Reference : UDINT; (*Axis reference handle*)
		AxisDiagHandleRef : UDINT; (*Internal FB control structure handle*)
	END_VAR
	VAR
		AxisDiagHandle : REFERENCE TO AxisDiagHandle_typ; (*FB control*)
		AxisStructure : REFERENCE TO ACP10AXIS_typ; (*Selected Axis Structure*)
		MC_BR_WriteParIdText_0 : MC_BR_WriteParIDText; (*Write to the ParIDs on the drive*)
		MC_BR_ReadParIdText_0 : MC_BR_ReadParIDText; (*Read from the ParIDs on the drive*)
		JoggingCancelled : BOOL;
		JoggingNeg : BOOL;
		JoggingPos : BOOL;
	END_VAR
	VAR CONSTANT
		WAIT_POWER_ON : TIME := T#15S; (*Time to wait before declaring a power on error*)
	END_VAR
	VAR
		TON_PowerOn : TON; (*Timer for detecting being not able to power on the controller*)
		HomingPosTemp : DINT; (*Operational homing position of the axis before carrying out a direct hmoing is saved*)
		HomingModeTemp : USINT; (*Operational homing mode of the axis before carrying out a direct hmoing is saved*)
		FirstInStep : BOOL; (*Flag to determine that the SW is in a step for the first time*)
		NcActionStatus : UINT; (*Status of any nc action*)
		OldStep : AxisDiagStep_enm;
		Step : AxisDiagStep_enm := STATE_WAIT_FOR_ENABLE; (*FB state step*)
		i : INT;
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
		zzEdge00002 : BOOL;
		zzEdge00003 : BOOL;
		zzEdge00004 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK AxisStatus (*FB to monitor the axis*)
	VAR_INPUT
		Enable : BOOL;
		Reference : UDINT; (*Axis reference handle*)
		AxisStatHandleRef : UDINT; (*Internal FB control structure handle*)
	END_VAR
	VAR
		AxisStructure : REFERENCE TO ACP10AXIS_typ; (*Selected Axis Structure*)
		AxisStatHandle : REFERENCE TO AxisStatHandle_typ; (*FB control*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrakeControl (*Motor Holding Brake Control*) (*$GROUP=User*)
	VAR_INPUT
		Reference : UDINT; (*Selected axis handle*)
		Enable : BOOL; (*Enable of the FB*)
		BrakeTest : MC_BRAKE_TEST_REF; (*Brake control structure*)
		BrakeMode : MC_BRAKE_MODE_REF; (*Brake control structure*)
		BrakeOpen : BOOL; (*Brake release command*)
		GetBrakeStatus : BOOL; (*Cmd to get the current status of the holding brake*)
		BrakeClose : BOOL; (*Brake close command*)
		DefaultSettings : BOOL; (*Input flag with which the FB loads the default parameters to BrakeControl & BrakeTest structures*)
	END_VAR
	VAR_OUTPUT
		BrakeStatus : BrakeControl_BrakeStatus_enm; (*Actual status of the brake*)
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL; (*Error flag*)
		ErrorID : UINT; (*Error ID*)
		ErrorInfo : ErrorInfo_enm; (*Additional information about the error*)
	END_VAR
	VAR
		Step : BrakeControl_Steps_enm; (*Actual step of the program*)
		OldStep : BrakeControl_Steps_enm := BRAKE_ERROR; (*Step value from the previous cycle*)
		firstInStep : BOOL; (*First time in a step flag [Internal use only]*)
		MC_BR_BrakeControl_0 : MC_BR_BrakeControl; (*FB to control the holding brake*)
		test : BOOL;
	END_VAR
END_FUNCTION_BLOCK
