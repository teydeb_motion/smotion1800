(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: SysDiag
 * File: AxisDiag.st
 * Author: abdullahoglf
 * Created: March 14, 2014
 ********************************************************************
 * Implementation OF library SysDiag
 ********************************************************************) 

(* Axis&Drive Diagnose Features *)
FUNCTION_BLOCK AxisDiag	
	// If FB is not enabled switch to STATE_WAIT_FOR_ENABLE
	IF EDGENEG(Enable) THEN
		// Reset the FB inputs & outputs
		memset(ADR(AxisDiagHandle.In.Cmd), 0, SIZEOF(AxisDiagHandle.In.Cmd));
		memset(ADR(AxisDiagHandle.Out), 0, SIZEOF(AxisDiagHandle.Out));			
		JoggingCancelled := FALSE;
		JoggingPos       := FALSE;
		JoggingNeg       := FALSE;
		ncaction(Reference, ncCONTROLLER, ncSWITCH_OFF);
		Step             := STATE_WAIT_FOR_ENABLE;
	END_IF; // end of enable check
	
	//=================================================================//
	//				           MAIN CONTROL                            //
	//=================================================================//
	CASE Step OF
		//================================================
		// WAIT FOR ENABLE
		STATE_WAIT_FOR_ENABLE:			
			IF FirstInStep THEN  // events for the first time around
				TON_PowerOn.PT     := WAIT_POWER_ON;  // initialize and reset the timer
				TON_PowerOn.IN     := FALSE;  
			ELSE  // events for after the first time around;
				IF Enable THEN // FB enabled
					IF Reference <> 0 AND AxisDiagHandleRef <> 0 THEN	
						AxisStructure  ACCESS Reference;       // assign the pointer to the selected axis structure
						AxisDiagHandle ACCESS AxisDiagHandleRef;
						Step := STATE_WAIT_DIAG;
					END_IF; // end of reference validity check
				END_IF;	// end of enable check
		END_IF; // end of state passing check

		//================================================
		// WAIT FOR DIAGNOSE COMMANDS
		STATE_WAIT_DIAG:
			IF FirstInStep THEN  // events for the first time around
				// Reset the cmd structure of the FB
				AxisDiagHandle.Out.Busy         := FALSE;
				// save the operation parameters of the selected axis
				HomingModeTemp := AxisStructure.move.homing.parameter.mode;
				HomingPosTemp  := AxisStructure.move.homing.parameter.s;
			ELSE  // events for after the first time around
				// Carry out direct homing upon request
				IF AxisDiagHandle.In.Cmd.Axis.Home AND AxisDiagHandle.Out.ErrorID <> AX_ERROR THEN
					AxisDiagHandle.In.Cmd.Axis.Home        := FALSE;
					AxisDiagHandle.Out.Busy                := TRUE;
					AxisDiagHandle.Out.Done                := FALSE;					
					Step                                   := STATE_DIRECT_HOME;
					// Jog positive
				ELSIF EDGEPOS(AxisDiagHandle.In.Cmd.Axis.JogPos) AND AxisDiagHandle.Out.ErrorID <> AX_ERROR THEN
					IF AxisDiagHandle.In.Cmd.Axis.Power THEN // axis is powered on, proceed
						AxisDiagHandle.Out.Busy            := TRUE;
						AxisDiagHandle.Out.Done            := FALSE;
						Step                               := STATE_JOG_POS;
					ELSE  // axis is not powered on, reset the cmd
						AxisDiagHandle.In.Cmd.Axis.JogPos  := FALSE;						
					END_IF;
					// Jog negative
				ELSIF EDGEPOS(AxisDiagHandle.In.Cmd.Axis.JogNeg) AND AxisDiagHandle.Out.ErrorID <> AX_ERROR  THEN
					IF AxisDiagHandle.In.Cmd.Axis.Power THEN // axis is powered on, proceed
						AxisDiagHandle.Out.Busy            := TRUE;
						AxisDiagHandle.Out.Done            := FALSE;
						Step  						       := STATE_JOG_NEG;
					ELSE  // axis is not powered on, reset the cmd
						AxisDiagHandle.In.Cmd.Axis.JogNeg  := FALSE;
					END_IF;
					// Read any parID upon request	
				ELSIF AxisDiagHandle.In.Cmd.Drive.ReadParID THEN
					AxisDiagHandle.In.Cmd.Drive.ReadParID  := FALSE;
					AxisDiagHandle.Out.Busy                := TRUE;
					AxisDiagHandle.Out.Done                := FALSE;
					Step                                   := STATE_READ_PARID;
					// Write to a parID upon request
				ELSIF AxisDiagHandle.In.Cmd.Drive.WriteParID THEN
					AxisDiagHandle.In.Cmd.Drive.WriteParID := FALSE;
					AxisDiagHandle.Out.Busy                := TRUE;
					AxisDiagHandle.Out.Done                := FALSE;
					Step                                   := STATE_WRITE_PARID;	
				END_IF;	// end of cmd check					
		END_IF; // end of state passing check
		
		//================================================
		// POWER ON THE AXIS
		STATE_AX_WAIT_POWER_ON:
			IF FirstInStep THEN  // events for the first time around
				TON_PowerOn.IN := TRUE;	
			ELSE  // events for after the first time around
				(*NcActionStatus := ncaction(Reference, ncCONTROLLER, ncSWITCH_ON);*)
				// Timer is done, give out the error that the controller was not able to switch on
				IF TON_PowerOn.Q THEN  
					TON_PowerOn.IN := FALSE;
					AxisDiagHandle.Out.ErrorID := POWER_ON_ERROR;
					Step                       := STATE_ERROR_DIAG;	
					// Controller is switched on before the timer is done, opr. successful	
				ELSIF NcActionStatus = ncOK THEN
					TON_PowerOn.IN := FALSE;
					AxisDiagHandle.Out.Done := TRUE;
					Step                    := STATE_WAIT_DIAG;
				END_IF;
		END_IF; //end of state passing check
		
		//================================================
		// DIRECT HOMING
		STATE_DIRECT_HOME:
			IF FirstInStep THEN  // events for the first time around
				// set the values for direct homing
				AxisStructure.move.homing.parameter.s    := 0;
				AxisStructure.move.homing.parameter.mode := 0;
			ELSE  // events for after the first time around				
				// init. the new homing parameters and carry out homing
				NcActionStatus := ncaction(Reference, ncHOMING, ncINIT+ncSTART);				
				IF NcActionStatus = ncOK THEN  // ncaction is done
					Step                                     := STATE_WAIT_DIRECT_HOME;					
				ELSE	// ncaction has an error
					AxisDiagHandle.Out.Error                 := TRUE;
					AxisDiagHandle.Out.ErrorID               := HOMING_ERROR;
					Step                                     := STATE_ERROR_DIAG;
				END_IF;
		END_IF; //end of state passing check
			
		//================================================
		// WAIT FOR DIRECT HOME
		STATE_WAIT_DIRECT_HOME:
			IF FirstInStep THEN  // events for the first time around
							
			ELSE  // events for after the first time around
				IF AxisStructure.move.homing.status.ok = 1 AND NcActionStatus = ncOK THEN // set the operation parameters for the homing procedure of the axis
					NcActionStatus := ncaction(Reference, ncHOMING, ncINIT);
					IF NcActionStatus = ncOK THEN  // ncaction is done						
						AxisStructure.move.homing.parameter.s    := HomingPosTemp;
						AxisStructure.move.homing.parameter.mode := HomingModeTemp;
						AxisDiagHandle.Out.Done                  := TRUE;
						HomingModeTemp 			                 := 0;
						HomingPosTemp  			                 := 0; 
						Step           	                         := STATE_WAIT_DIAG;
					ELSE		// ncaction has an error						
						AxisStructure.move.homing.parameter.s    := HomingPosTemp;
						AxisStructure.move.homing.parameter.mode := HomingModeTemp;
						AxisDiagHandle.Out.Error                 := TRUE;
						AxisDiagHandle.Out.ErrorID               := INTERNAL_ERROR;
						Step                                     := STATE_ERROR_DIAG;
					END_IF; // end of ncaction status check					
				ELSIF AxisStructure.message.count.error <> 0 THEN // homing not possible
					AxisStructure.move.homing.parameter.s        := HomingPosTemp;
					AxisStructure.move.homing.parameter.mode     := HomingModeTemp;
					AxisDiagHandle.Out.Error                     := TRUE;
					AxisDiagHandle.Out.ErrorID                   := HOMING_ERROR;
					Step                                         := STATE_ERROR_DIAG;
				END_IF; // end of homing done check
		END_IF; //end of state passing check
			
		//================================================
		// JOG POSITIVE
		STATE_JOG_POS:
			IF FirstInStep THEN  // events for the first time around
				AxisStructure.move.basis.parameter.v_pos  := AxisDiagHandle.In.Par.Axis.Velocity;
				AxisStructure.move.basis.parameter.a1_pos := AxisStructure.move.basis.parameter.v_pos * 5;
				AxisStructure.move.basis.parameter.a1_neg := AxisStructure.move.basis.parameter.a1_pos;
				
				AxisStructure.move.basis.parameter.v_neg  := AxisDiagHandle.In.Par.Axis.Velocity;
				AxisStructure.move.basis.parameter.a2_pos := AxisStructure.move.basis.parameter.v_neg * 5;
				AxisStructure.move.basis.parameter.a2_neg := AxisStructure.move.basis.parameter.a2_pos;
			ELSE  // events for after the first time around
				// Check if the jog command is released
				IF NOT AxisDiagHandle.In.Cmd.Axis.JogPos THEN
					AxisDiagHandle.Out.Busy := FALSE;
					AxisDiagHandle.Out.Done := TRUE;
					Step                    := STATE_WAIT_DIAG;
				ELSE  // start the movement
					NcActionStatus := ncaction(Reference, ncPOS_MOVE, ncINIT+ncSTART);
					// movement started, wait for the stop
					IF NcActionStatus = ncOK THEN
						Step := STATE_JOG_WAIT_STOP;
					ELSE // nacaction has an error
						AxisDiagHandle.In.Cmd.Axis.JogPos := FALSE;
						AxisDiagHandle.Out.ErrorID        := INTERNAL_ERROR;
						Step                              := STATE_ERROR_DIAG;
					END_IF; // end of ncaction status check
				END_IF; // end of jog start check
		END_IF; //end of state passing check
		
		//================================================
		// JOG NEGATIVE
		STATE_JOG_NEG:
			IF FirstInStep THEN  // events for the first time around
				AxisStructure.move.basis.parameter.v_neg  := AxisDiagHandle.In.Par.Axis.Velocity;
				AxisStructure.move.basis.parameter.a2_pos := AxisStructure.move.basis.parameter.v_neg * 5;
				AxisStructure.move.basis.parameter.a2_neg := AxisStructure.move.basis.parameter.a2_pos;
				
				AxisStructure.move.basis.parameter.v_pos  := AxisDiagHandle.In.Par.Axis.Velocity;
				AxisStructure.move.basis.parameter.a1_pos := AxisStructure.move.basis.parameter.v_pos * 5;
				AxisStructure.move.basis.parameter.a1_neg := AxisStructure.move.basis.parameter.a1_pos;
			ELSE  // events for after the first time around
				// Check if the jog command is released
				IF NOT AxisDiagHandle.In.Cmd.Axis.JogNeg THEN
					AxisDiagHandle.Out.Busy := FALSE;
					AxisDiagHandle.Out.Done := TRUE;
					Step                    := STATE_WAIT_DIAG;
				ELSE  // start the movement
					NcActionStatus := ncaction(Reference, ncNEG_MOVE, ncINIT+ncSTART);
					// movement started, wait for the stop
					IF NcActionStatus = ncOK THEN
						Step := STATE_JOG_WAIT_STOP;
					ELSE // nacaction has an error
						AxisDiagHandle.In.Cmd.Axis.JogNeg := FALSE;
						AxisDiagHandle.Out.ErrorID        := INTERNAL_ERROR;
						Step                              := STATE_ERROR_DIAG;
					END_IF; // end of ncaction status check
				END_IF; // end of jog start check
		END_IF; //end of state passing check
		
		//================================================
		// WAIT FOR JOG COMMAND TO BE RELEASED 
		STATE_JOG_WAIT_STOP:
			IF FirstInStep THEN  // events for the first time around
				// Check to which direction the jogging is active
				IF AxisDiagHandle.In.Cmd.Axis.JogPos THEN
					JoggingPos       := TRUE;
					JoggingNeg       := FALSE;
					JoggingCancelled := FALSE;
					
				ELSIF AxisDiagHandle.In.Cmd.Axis.JogNeg THEN
					JoggingPos       := FALSE;
					JoggingNeg       := TRUE;
					JoggingCancelled := FALSE;
					
				ELSE  // Jog button released before the program switched the CASE machine
					JoggingPos       := FALSE;
					JoggingNeg       := FALSE;
					JoggingCancelled := TRUE;
				END_IF; // end of direction check		
			ELSE  // events for after the first time around
				// Check for stop condition
				IF JoggingCancelled OR (JoggingPos AND EDGENEG(AxisDiagHandle.In.Cmd.Axis.JogPos)) OR (JoggingNeg AND EDGENEG(AxisDiagHandle.In.Cmd.Axis.JogNeg)) THEN
					JoggingCancelled            := TRUE;
					NcActionStatus              := ncaction(Reference, ncMOVE, ncSTOP);
					// stop is successful
					IF NcActionStatus = ncOK THEN
						JoggingPos              := FALSE;
						JoggingNeg              := FALSE;
						JoggingCancelled        := FALSE;
						AxisDiagHandle.Out.Done := TRUE;
						Step                    := STATE_WAIT_DIAG;
					END_IF; // end of ncaction status check
				END_IF; // end of stop condition check
		END_IF; //end of state passing check		
		
		//================================================
		// READ PARID
		STATE_READ_PARID:
			IF FirstInStep THEN  // events for the first time around
				MC_BR_ReadParIdText_0.ParID   := AxisDiagHandle.In.Par.Drive.ParID;
				MC_BR_ReadParIdText_0.Execute := TRUE;
				MC_BR_ReadParIdText_0.Axis    := Reference;
			ELSE  // events for after the first time around
				IF MC_BR_ReadParIdText_0.Done THEN  // FB completed w/o an error
					MC_BR_ReadParIdText_0.Execute := FALSE;
					memcpy(ADR(AxisDiagHandle.Out.ParIDResponseHmi), ADR(MC_BR_ReadParIdText_0.DataText), SIZEOF(AxisDiagHandle.Out.ParIDResponseHmi));
					AxisDiagHandle.Out.Done       := TRUE;
					Step                          := STATE_WAIT_DIAG;
				ELSIF MC_BR_ReadParIdText_0.Error THEN // FB completed with an error
					MC_BR_ReadParIdText_0.Execute := FALSE;
					AxisDiagHandle.Out.Error      := TRUE;
					AxisDiagHandle.Out.ErrorID    := PARID_READ_ERROR;
					Step                          := STATE_ERROR_DIAG;
				END_IF; // end of FB status check
		END_IF; //end of state passing check	
			
		//================================================
		// WRITE PARID
		STATE_WRITE_PARID:
			IF FirstInStep THEN  // events for the first time around
				MC_BR_WriteParIdText_0.Axis     := Reference;				
				MC_BR_WriteParIdText_0.DataText := AxisDiagHandle.Out.ParIDResponseHmi;
				MC_BR_WriteParIdText_0.ParID    := AxisDiagHandle.In.Par.Drive.ParID;
				MC_BR_WriteParIdText_0.Execute  := TRUE;
			ELSE  // events for after the first time around
				IF MC_BR_WriteParIdText_0.Done THEN  // FB completed w/o an error
					MC_BR_WriteParIdText_0.Execute := FALSE;
					AxisDiagHandle.Out.Done        := TRUE;
					Step                           := STATE_WAIT_DIAG;
				ELSIF MC_BR_WriteParIdText_0.Error THEN // FB completed with an error
					MC_BR_WriteParIdText_0.Execute := FALSE;
					AxisDiagHandle.Out.Error       := TRUE;
					AxisDiagHandle.Out.ErrorID     := PARID_WRITE_ERROR;
					Step                           := STATE_ERROR_DIAG;
				END_IF;
		END_IF; //end of state passing check
			
		//================================================
		// ERROR STATE
		STATE_ERROR_DIAG:
			IF FirstInStep THEN  // events for the first time around
				AxisDiagHandle.Out.Busy  := FALSE;
				AxisDiagHandle.Out.Done  := TRUE;
				AxisDiagHandle.Out.Error := TRUE;
				JoggingCancelled         := FALSE;
				JoggingPos               := FALSE;
				JoggingNeg               := FALSE;
				// load back the old homing parameters for the axis if changed
				IF (AxisStructure.move.homing.parameter.s <> HomingPosTemp) OR (AxisStructure.move.homing.parameter.mode <> HomingModeTemp) THEN
					AxisStructure.move.homing.parameter.s    := HomingPosTemp;
					AxisStructure.move.homing.parameter.mode := HomingModeTemp;
				END_IF;
			ELSE  // events for after the first time around					
				NcActionStatus := ncaction(Reference, ncHOMING, ncINIT); // set back the operational homing parameters for the axis
				
				IF AxisDiagHandle.In.Cmd.ErrAck THEN
					AxisDiagHandle.Out.ErrorID := NO_ERROR;
					IF NcActionStatus = ncOK THEN  // ncaction has no error
						AxisDiagHandle.In.Cmd.ErrAck        := FALSE;
						AxisDiagHandle.Out.Error            := FALSE;
						AxisDiagHandle.Out.ParIDResponseHmi := '';
						Step                                := STATE_WAIT_DIAG;
					ELSE	// ncaction has an error
						AxisDiagHandle.In.Cmd.ErrAck := FALSE;
						AxisDiagHandle.Out.ErrorID   := INTERNAL_ERROR;
						Step                         := STATE_ERROR_DIAG;
					END_IF;					
				END_IF;
			END_IF; // end of state passing check	
	END_CASE;  // end of main CASE machine
	
	// Controller Enable and axis error case behaviour
	IF Enable AND Reference <> 0 AND AxisDiagHandleRef <> 0 THEN
		// if the cmd is there call the ncaction to keep the controller
		// switched on; if not then stop calling it which will switch off
		// the controller
		IF AxisDiagHandle.In.Cmd.Axis.Power THEN
			ncaction(Reference, ncCONTROLLER, ncSWITCH_ON);
		END_IF;
		
		IF AxisStructure.message.record.number <> 0 THEN
			// error present on the drive, reset the cmd's related to the axis
			// drive cmd's are still possible in case further diagnostics are 
			// required in case of an error
			memset(ADR(AxisDiagHandle.In.Cmd.Axis), 0, SIZEOF(AxisDiagHandle.In.Cmd.Axis));
			AxisDiagHandle.Out.Error   := TRUE;
			AxisDiagHandle.Out.ErrorID := AX_ERROR;			
			JoggingCancelled           := FALSE;
			JoggingPos                 := FALSE;
			JoggingNeg                 := FALSE;
			// Change the step back to WAIT if one of the following steps were active
			IF (Step = STATE_JOG_POS OR Step = STATE_JOG_NEG OR Step = STATE_JOG_WAIT_STOP OR
				Step = STATE_DIRECT_HOME OR Step = STATE_WAIT_DIRECT_HOME)			   	 THEN			
				Step  := STATE_WAIT_DIAG;				
			END_IF;
		ELSE  // no error on the axis
			IF AxisDiagHandle.Out.ErrorID = AX_ERROR THEN
				AxisDiagHandle.Out.Error   := FALSE;
				AxisDiagHandle.Out.ErrorID := NO_ERROR;
			END_IF;
		END_IF;
		
		// check for the direct ack cmd 
		IF AxisDiagHandle.In.Cmd.Drive.DirectAck THEN
			NcActionStatus := ncaction(Reference, ncMESSAGE, ncACKNOWLEDGE);
			IF NcActionStatus = ncOK THEN
				AxisDiagHandle.In.Cmd.Drive.DirectAck := FALSE;
			ELSE
				AxisDiagHandle.In.Cmd.Drive.DirectAck := FALSE;
				AxisDiagHandle.Out.Error              := TRUE;
				AxisDiagHandle.Out.ErrorID            := INTERNAL_ERROR;
				Step                                  := STATE_ERROR_DIAG;
			END_IF;
		END_IF; // end of direct ack. cmd check
	END_IF; // of error case behaviour check
	
	//===================           END OF MAIN CONTROL            ===================//	
	
	// set the flag for being for the first time in a state
	IF OldStep <> Step THEN
		FirstInStep := TRUE;		
	ELSE
		FirstInStep := FALSE;
	END_IF;
	
	OldStep := Step;	
	
	// FB calls
	TON_PowerOn();
	MC_BR_ReadParIdText_0();
	MC_BR_WriteParIdText_0();
	
	//	IF FirstInStep THEN  // events for the first time around
	//				
	//	ELSE  // events for after the first time around
	//	
	//	END_IF; //end of state passing check
END_FUNCTION_BLOCK