(*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:    abdullahoglf
* Created:   November 09, 2015
**********************************************************************************
* Description:
*	- This function block can be used as an expansion to the MpAxisBasic component
*		to display the related error text as well as acknowledging only 1 error
*		at a time to give the user the possibility to correctly diagnose the situtation.
*
* Parameters:
*	- Enable			: Enable of the FB
*	- pInterface		: Reference to ErrorInterface_typ which is used by the higher control.
*	- pErrorIf			: Pointer to ErrorInterface_typ 
*
*   - The following elements of the pErrorIf should be configured by the higher control:
*		.AxisRef		: Reference handle of the controlled axis object
*		.DataObjName	: Name of the data object from which the error texts should be read.
*							(default: acp10etxen)
*		.Acknowledge	: Should come from the higher control (Edge Sensitive)
*
*	- The following elements will be filled in by this FB and will be available for the user.
*		.ErrorID		: Actual error ID of the axis (will be filled in by this FB -> Can be used by the higher control)
*		.ErrorCount		: Actual error count on the controlled axis.
*		.ErrorText		: Text of the currently recorded alarm
*
*	- Status 			: Output of the FB
*		0				: No more error is left on the axis
*		52001			: pInterface is 0
*		52002			: Axis reference is 0
*		65534			: FB enable is false
*		65535			: FB busy -> checking for errors
* 		Anything else	: ErrorID from MC_ReadAxisError
*
* Notes:
*	- Make sure to disable the FB at least for one cycle before changing the .AxisRef otherwise
*		an error will be reported by the MC_ReadAxisError
*
* History:
*		09.11.2015	V1.0.0	Ferhat Abdullahoglu
*			(new)	Core implementation of the functionality
**********************************************************************************)

(* Axis Error Handling Expansion *)
FUNCTION_BLOCK AxisErrorHandler
	// ----------------------------
	// Check for function block enable
	// ----------------------------
	IF Enable THEN // FB is enabled
		// ----------------------------
		// Check the pointer reference
		// ----------------------------
		IF pInterface <> 0 THEN
			pErrorIf ACCESS pInterface; // assign the pointer
			// ----------------------------
			// check if the axis refrence is given
			// ----------------------------
			IF pErrorIf.AxisRef <> 0 THEN
				// ----------------------------
				// Main control sequence
				// ----------------------------
				CASE step OF
					// =========================================================== //
					0:		// cfg the MC_ReadAxisError and execute it
					// =========================================================== //
						MC_ReadAxisError_0.Axis				:= pErrorIf.AxisRef;
						MC_ReadAxisError_0.DataAddress		:= ADR(pErrorIf.ErrorText);
						MC_ReadAxisError_0.DataLength		:= UDINT_TO_UINT(SIZEOF(pErrorIf.ErrorText));
						IF strlen(ADR(pErrorIf.DataObjName)) <> 0 THEN // DataObjName is configured
							MC_ReadAxisError_0.DataObjectName	:= pErrorIf.DataObjName;	
						ELSE // use the default value
							MC_ReadAxisError_0.DataObjectName	:= 'acp10etxen';	
						END_IF;						
						MC_ReadAxisError_0.Enable := TRUE;
						Status 	:= ERR_FUB_BUSY;
						step 	:= 5;
					
					// =========================================================== //
					5:		// wait for the FB results 
					// =========================================================== //
						IF MC_ReadAxisError_0.Valid AND MC_ReadAxisError_0.AxisErrorID <> 0 THEN // there is an active error, start waiting for acknowledge
							pErrorIf.ErrorID 	:= MC_ReadAxisError_0.AxisErrorID;
							pErrorIf.ErrorCount	:= MC_ReadAxisError_0.AxisErrorCount;
							step := 10;
						ELSIF MC_ReadAxisError_0.AxisErrorID = 0 THEN
							memset(ADR(pErrorIf.ErrorText), 0, SIZEOF(pErrorIf.ErrorText));
							pErrorIf.ErrorCount := 0;
							pErrorIf.ErrorID	:= 0;							
						ELSIF MC_ReadAxisError_0.Error THEN
							step := 254; 
						END_IF;
					
					// =========================================================== //
					10:		// wait for acknowledge 
					// =========================================================== //
						IF pErrorIf.Acknowledge > oldAck THEN
							MC_ReadAxisError_0.Acknowledge := TRUE;
						ELSE
							MC_ReadAxisError_0.Acknowledge := FALSE;
						END_IF;
						pErrorIf.ErrorID 	:= MC_ReadAxisError_0.AxisErrorID;
						pErrorIf.ErrorCount	:= MC_ReadAxisError_0.AxisErrorCount;
						// ----------------------------
						// check if the error count is changed
						// ----------------------------
						IF oldCnt <> pErrorIf.ErrorCount THEN
							IF pErrorIf.ErrorCount = 0 THEN // all errors are acknowledged
								step := 255; // evaluation finished
							ELSE
								step := 5; // more errors -> get the new text
							END_IF;
						END_IF;
					
					// =========================================================== //
					254:		// internal error occured 
					// =========================================================== //
						Status := MC_ReadAxisError_0.ErrorID;	
						pErrorIf.ErrorID				:= 0;
						pErrorIf.ErrorCount				:= 0;
						memset(ADR(pErrorIf.ErrorText), 0, SIZEOF(pErrorIf.ErrorText));
						MC_ReadAxisError_0.Enable 		:= FALSE;
						MC_ReadAxisError_0.Acknowledge	:= FALSE;
					
					// =========================================================== //
					255:		// no more errors on the axis 
					// =========================================================== //
						Status := 0;
						pErrorIf.ErrorID				:= 0;
						pErrorIf.ErrorCount				:= 0;
						memset(ADR(pErrorIf.ErrorText), 0, SIZEOF(pErrorIf.ErrorText));
						MC_ReadAxisError_0.Enable 		:= FALSE;
						MC_ReadAxisError_0.Acknowledge	:= FALSE;
				END_CASE;				
				// firstInStep handling
				IF oldStep <> step THEN
					firstInStep := TRUE;
				ELSE
					firstInStep := FALSE;
				END_IF;
				oldStep := step;				
				// old acknowldege value
				oldAck	:= pErrorIf.Acknowledge;
				// old error count
				oldCnt  := pErrorIf.ErrorCount;
			ELSE // NULL AXIS REFERENCE
				pErrorIf.ErrorID				:= 0;
				pErrorIf.ErrorCount				:= 0;
				memset(ADR(pErrorIf.ErrorText), 0, SIZEOF(pErrorIf.ErrorText));
				MC_ReadAxisError_0.Enable 		:= FALSE;
				MC_ReadAxisError_0.Acknowledge	:= FALSE;
				Status 							:= AX_ERR_HAND_NULL_AX_REF;
			END_IF; // end of the pAxis check
		ELSE // NULL POINTER
			pErrorIf.ErrorID				:= 0;
			pErrorIf.ErrorCount				:= 0;
			memset(ADR(pErrorIf.ErrorText), 0, SIZEOF(pErrorIf.ErrorText));
			MC_ReadAxisError_0.Enable 		:= FALSE;
			MC_ReadAxisError_0.Acknowledge	:= FALSE;
			Status 							:= AX_ERR_HAND_NULL_POINTER;
		END_IF; // end of the pInterface check
	ELSE // FB is not enabled	
		Status		:= ERR_FUB_ENABLE_FALSE;	
		oldStep		:= 0;
		oldCnt		:= 0;
		step		:= 0;
		firstInStep	:= TRUE;				
		MC_ReadAxisError_0.Enable 		:= FALSE;
		MC_ReadAxisError_0.Acknowledge	:= FALSE;
	END_IF; // end of the FB enable check
	// call the FB
	MC_ReadAxisError_0();
END_FUNCTION_BLOCK