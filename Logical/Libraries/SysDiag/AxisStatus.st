(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: SysDiag
 * File: AxisStatus.st
 * Author: abdullahoglf
 * Created: April 24, 2014
 ********************************************************************
 * Implementation of library SysDiag
 ********************************************************************) 

(* FB to monitor the axis *)
FUNCTION_BLOCK AxisStatus
	IF Enable THEN
		IF AxisStatHandleRef <> 0 AND Reference <> 0 THEN
			// Assign the pointers
			AxisStatHandle ACCESS AxisStatHandleRef;
			AxisStructure  ACCESS Reference;
			
			// Read the drive status from the axis structure
			AxisStatHandle.Out.Status.ReferenceSwitch  := AxisStructure.dig_in.status.reference;
			AxisStatHandle.Out.Status.PosHwSwitch      := AxisStructure.dig_in.status.pos_hw_end;
			AxisStatHandle.Out.Status.NegHwSwitch      := AxisStructure.dig_in.status.neg_hw_end;
			AxisStatHandle.Out.Status.Trigger1         := AxisStructure.dig_in.status.trigger1;
			AxisStatHandle.Out.Status.Trigger2         := AxisStructure.dig_in.status.trigger2;
			AxisStatHandle.Out.Status.ControllerStatus := AxisStructure.controller.status;
			AxisStatHandle.Out.Status.ControllerReady  := AxisStructure.controller.ready;
			AxisStatHandle.Out.Status.NodeNr           := AxisStructure.nc_obj_inf.node_nr;
			AxisStatHandle.Out.Status.HomingOk         := AxisStructure.move.homing.status.ok;
			AxisStatHandle.Out.Status.Error            := AxisStructure.message.count.error;
			AxisStatHandle.Out.Status.ErrorID          := AxisStructure.message.record.number;
			AxisStatHandle.Out.Status.NetworkInit      := AxisStructure.network.init;
			AxisStatHandle.Out.Status.NetworkPhase     := AxisStructure.network.phase;
			
			// FB is operational
			AxisStatHandle.Out.Error   := FALSE;
			AxisStatHandle.Out.ErrorID := AX_STAT_NO_ERROR;	
		END_IF;
	END_IF;

END_FUNCTION_BLOCK