(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: ComonFun
 * File: CircleCenter.st
 * Author: turanskyl
 * Created: October 30, 2014
 ********************************************************************
 * Implementation of library ComonFun
 ********************************************************************) 

(* Calculates the end&start points of the glass edges *)
FUNCTION_BLOCK GlassPoints
	
	// Get values of sin and cos
	CosAngleCur := COS(Angle_Cur);
	SinAngleCur := SIN(Angle_Cur);
	
	CosAngleNew := COS(Angle_New);
	SinAngleNew := SIN(Angle_New);
	
	// Calculate finish points of previous edge
	Out_FinishX := PosX - B2C_second * CosAngleCur - Eccentricity_X * SinAngleCur;
	Out_FinishY := PosY - B2C_second * SinAngleCur + Eccentricity_Y * CosAngleCur;
	
	// Calculate starting points of next edge
	Out_StartX := PosX + B2C_first * CosAngleNew - Eccentricity_X * SinAngleNew;
	Out_StartY := PosY + B2C_first * SinAngleNew + Eccentricity_Y * CosAngleNew;
	
		
END_FUNCTION_BLOCK

		


