(*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:   abdullahoglf
* Created:  July 27, 2016
**********************************************************************************
* Description:
*
* History:
*	
**********************************************************************************)

(* PBC Activation on the drive *)
FUNCTION_BLOCK ActivatePBC
	
	// ----------------------------
	// FB disable handling
	IF NOT Enable THEN
		Internal.Step	:= ACT_PBC_DISABLED;
	END_IF;
	// ----------------------------
	
	// -------------------------------------------------------------------------------------------------------- //
	//					M A I N		C O N T R O L		S E Q U E N C E											// 
	// -------------------------------------------------------------------------------------------------------- //
	// ----------------------------
	// Check errors in the MC_BR_Mech...
	// ----------------------------
	IF Internal.MC_BR_MechPos_0.Error AND Enable THEN
		Status			:= Internal.MC_BR_MechPos_0.ErrorID;
		Error			:= TRUE;
		Internal.Step	:= ACT_PBC_ERROR;
	END_IF;	
	
	CASE Internal.Step OF 
		// =========================================================== //
		ACT_PBC_DISABLED:		// BLOCK DISALED
		// =========================================================== //
			Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
			Internal.MC_BR_MechPos_0.Enable			:= FALSE;
			Internal.MC_BR_CyclicRead_0.Enable		:= FALSE;
			StartComp								:= FALSE;
			StopComp								:= FALSE;
			Active									:= FALSE;
			Error									:= FALSE;
			CompActive								:= FALSE;
			Status									:= 16#FFFE;
			Internal.ii								:= 0;
			Internal.OldStep						:= 255;
			IF Enable THEN
				IF Axis <> 0 AND Parameters <> 0 THEN
					IF Type = 0 THEN
						Internal.Step	:= ACT_PBC_WAIT;
					END_IF;	
					pPar			ACCESS Parameters;
					Status			:= 16#FFFF;
				ELSE
					Error			:= TRUE;
					Status			:= ERR_COM_NULL_PTR;
					Internal.Step	:= ACT_PBC_ERROR;
				END_IF;	
				//
				IF Type = 1 AND Status  = 16#FFFF THEN
					Internal.MC_BR_CyclicRead_0.Axis		:= Axis;
					Internal.MC_BR_CyclicRead_0.DataAddress	:= ADR(Internal.iMode);
					Internal.MC_BR_CyclicRead_0.DataType	:= ncPAR_TYP_UINT;
					Internal.MC_BR_CyclicRead_0.Mode		:= mcEVERY_RECORD;
					Internal.MC_BR_CyclicRead_0.ParID		:= ACP10PAR_PBC_MODE;
					Internal.MC_BR_CyclicRead_0.Enable		:= TRUE;
					//
					IF Internal.MC_BR_CyclicRead_0.Valid THEN
						Internal.Step						:= ACT_PBC_WAIT;
					END_IF;	
				END_IF;	
			END_IF;
			
		
		// =========================================================== //
		ACT_PBC_WAIT:		// WAIT FOR COMMANDS
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				Status				:= 0;
				Active				:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Check for commands
				// ----------------------------
				IF StartComp AND NOT CompActive THEN	// compensation is not actie -> activate it 
					IF Type = 0 THEN
						Internal.Step	:= ACT_PBC_SET_MAX_RATE;
					ELSIF Type = 1 THEN
						Internal.ii		:= 0;
						Internal.Step	:= ACT_PBC_ACTIVATE_TYP_1;
					END_IF;						
					Status			:= 16#FFFF;
					StartComp		:= FALSE;
				ELSIF StopComp AND CompActive THEN // compensation is active -> deactivate it
					IF Type = 0 THEN
						Internal.Step	:= ACT_PBC_DEACTIVATE;
					ELSIF Type = 1 THEN
						Internal.Step	:= ACT_PBC_DEACTIVATE_TYP_1;
					END_IF;
					Status			:= 16#FFFF;
					StopComp		:= FALSE;
				ELSE // no suitable commands
					StartComp		:= FALSE;
					StopComp		:= FALSE;
				END_IF; // end of the command check					
			END_IF;	// end of the step	
		
		
		// =========================================================== //
		ACT_PBC_SET_MAX_RATE:	// SET THE MAX. COMP. RATE -- somehow it's not set by the MC_BR_MechPos...
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				memset(ADR(Internal.sTemp), 0, SIZEOF(Internal.sTemp));
				ftoa(pPar.Parameters.DirectionDependent.MaxVelocity, ADR(Internal.sTemp));
				Internal.MC_BR_WriteParIDText_0.Axis		:= Axis;
				Internal.MC_BR_WriteParIDText_0.DataText	:= Internal.sTemp;
				Internal.MC_BR_WriteParIDText_0.ParID		:= ACP10PAR_PBC_MAX_RATE;
				Internal.MC_BR_WriteParIDText_0.Execute		:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Check the FB results
				// ----------------------------
				IF Internal.MC_BR_WriteParIDText_0.Busy THEN // FB is busy
					; // continue calling the FB
				ELSIF Internal.MC_BR_WriteParIDText_0.Done THEN // FB is done
					Internal.Step							:= ACT_PBC_ACTIVATE;
					Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				ELSIF Internal.MC_BR_WriteParIDText_0.Error THEN // error occured
					Status									:= Internal.MC_BR_WriteParIDText_0.ErrorID;
					Internal.Step							:= ACT_PBC_ERROR;
					Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				END_IF;	
			END_IF;	// end of the step			
		
			
		// =========================================================== //
		ACT_PBC_ACTIVATE:		// ACTIVATE THE FEATURE ON THE DRIVE
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				// ----------------------------
				// Prepare the Cfg
				// ----------------------------
				Internal.MC_BR_MechPos_0.Enable	:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Check the FB results
				// ----------------------------
				IF Internal.MC_BR_MechPos_0.Busy AND NOT Internal.MC_BR_MechPos_0.Active THEN 
					; // continue calling the FB
				ELSIF Internal.MC_BR_MechPos_0.Active THEN // compensation is activated
					Internal.Step	:= ACT_PBC_WAIT;
				ELSIF Internal.MC_BR_MechPos_0.Error THEN // error occured
					Status			:= Internal.MC_BR_MechPos_0.ErrorID;
					Internal.Step	:= ACT_PBC_ERROR;
				END_IF;	
			END_IF;	// end of the step	
		
		
		// =========================================================== //
		ACT_PBC_ACTIVATE_TYP_1:	// ACTIVATE THE PBC -- only with ParIDs
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				// ----------------------------
				// Prepare the Cfg
				// ----------------------------
				CASE Internal.ii OF
					0: // Write the aPos value
						memset(ADR(Internal.sTemp), 0, SIZEOF(Internal.sTemp));
						ftoa(pPar.Backlash ,ADR(Internal.sTemp));
						Internal.MC_BR_WriteParIDText_0.Axis			:= Axis;
						Internal.MC_BR_WriteParIDText_0.ParID			:= ACP10PAR_VAR_R4_0;
						IF pPar.Mode = 130 THEN
							Internal.MC_BR_WriteParIDText_0.DataText	:= '0';
						ELSE
							Internal.MC_BR_WriteParIDText_0.DataText	:= Internal.sTemp;
						END_IF;	
						Internal.MC_BR_WriteParIDText_0.Execute			:= TRUE;
					
					1: // Write the aNeg value	
						memset(ADR(Internal.sTemp), 0, SIZEOF(Internal.sTemp));
						ftoa(pPar.Backlash ,ADR(Internal.sTemp));
						Internal.MC_BR_WriteParIDText_0.Axis		:= Axis;
						Internal.MC_BR_WriteParIDText_0.ParID		:= ACP10PAR_VAR_R4_0+1;
						IF pPar.Mode = 2 THEN
							Internal.MC_BR_WriteParIDText_0.DataText	:= '0';
						ELSE
							Internal.MC_BR_WriteParIDText_0.DataText	:= Internal.sTemp;
						END_IF;	
						Internal.MC_BR_WriteParIDText_0.Execute		:= TRUE;					
					
					2: // Write the PBC mode
						memset(ADR(Internal.sTemp), 0, SIZEOF(Internal.sTemp));
						ftoa(pPar.Mode,ADR(Internal.sTemp));
						Internal.MC_BR_WriteParIDText_0.Axis		:= Axis;
						Internal.MC_BR_WriteParIDText_0.ParID		:= ACP10PAR_PBC_MODE;
						Internal.MC_BR_WriteParIDText_0.DataText	:= Internal.sTemp;
						Internal.MC_BR_WriteParIDText_0.Execute		:= TRUE;	
					ELSE
						Internal.ii									:= 0;
						memset(ADR(Internal.sTemp), 0, SIZEOF(Internal.sTemp));
						Status										:= 0;
						Internal.Step								:= ACT_PBC_WAIT;					
				END_CASE;		
			ELSE // after the first time in the step
				// ----------------------------
				// Check the FB results
				// ----------------------------
				IF Internal.MC_BR_WriteParIDText_0.Busy THEN // FB is busy
					; // continue calling the FB
				ELSIF Internal.MC_BR_WriteParIDText_0.Done THEN // FB is done
					Internal.Step							:= ACT_PBC_SWITCH;
					Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				ELSIF Internal.MC_BR_WriteParIDText_0.Error THEN // error occured
					Status									:= Internal.MC_BR_WriteParIDText_0.ErrorID;
					Internal.Step							:= ACT_PBC_ERROR;
					Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				END_IF;				
			END_IF;	// end of the step			
			
			
		// =========================================================== //
		ACT_PBC_SWITCH:		// PREPARE FOR THE NEXT PAR.
		// =========================================================== //	
			Internal.ii		:= Internal.ii + 1;
			Internal.Step	:= ACT_PBC_ACTIVATE_TYP_1;
		
		
		// =========================================================== //
		ACT_PBC_DEACTIVATE:		// STOP THE COMPENSATION
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				Internal.MC_BR_MechPos_0.Enable	:= FALSE;
			ELSE // after the first time in the step
				IF NOT Internal.MC_BR_MechPos_0.Active THEN
					Internal.Step				:= ACT_PBC_WAIT;
				END_IF;	
			END_IF;	// end of the step	
		
		
		// =========================================================== //
		ACT_PBC_DEACTIVATE_TYP_1:	// STOP THE COMPENSTAION - TYPE1
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				Internal.MC_BR_WriteParIDText_0.Axis		:= Axis;
				Internal.MC_BR_WriteParIDText_0.ParID		:= ACP10PAR_PBC_MODE;
				Internal.MC_BR_WriteParIDText_0.DataText	:= '0';
				Internal.MC_BR_WriteParIDText_0.Execute		:= TRUE;	
			ELSE // after the first time in the step
				// ----------------------------
				// Check the FB results
				// ----------------------------
				IF Internal.MC_BR_WriteParIDText_0.Busy THEN // FB is busy
					; // continue calling the FB
				ELSIF Internal.MC_BR_WriteParIDText_0.Done THEN // FB is done
					Internal.Step							:= ACT_PBC_WAIT;
					Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				ELSIF Internal.MC_BR_WriteParIDText_0.Error THEN // error occured
					Status									:= Internal.MC_BR_WriteParIDText_0.ErrorID;
					Internal.Step							:= ACT_PBC_ERROR;
					Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				END_IF;	
			END_IF;	// end of the step	
		
				
		// =========================================================== //
		ACT_PBC_ERROR:		// ERROR OCCURED
		// =========================================================== //
			Internal.MC_BR_MechPos_0.Enable			:= FALSE;
			Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
			Internal.MC_BR_CyclicRead_0.Enable		:= FALSE;
			StartComp								:= FALSE;
			StopComp								:= FALSE;
			CompActive								:= FALSE;
			Internal.ii								:= FALSE;
			
	END_CASE;
	
	// ----------------------------
	// Parameterize the MC_BR_MechPos 
	// if the component is active
	IF Active THEN
		Internal.MC_BR_MechPos_0.Axis				:= Axis;
		Internal.MC_BR_MechPos_0.Parameters			:= pPar.Parameters;
		Internal.MC_BR_MechPos_0.AdvancedParameters	:= pPar.AdvParameters;
	END_IF;	
	// ----------------------------
	
	// ----------------------------
	// Call other FBs 
	// ----------------------------
	Internal.MC_BR_WriteParIDText_0();
	Internal.MC_BR_MechPos_0();
	Internal.MC_BR_CyclicRead_0();
	//
	IF Type = 0 THEN
		CompActive	:= Internal.MC_BR_MechPos_0.Active;
	ELSIF Type = 1 AND Active THEN
		CompActive	:= (Internal.iMode <> 0);
	END_IF;	
		
	//
	// ----------------------------
	// Old values handling
	// ----------------------------
	IF Internal.Step <> Internal.OldStep THEN
		Internal.bFirstInStep	:= TRUE;
	ELSE
		Internal.bFirstInStep	:= FALSE;
	END_IF;
	Internal.OldStep			:= Internal.Step;
	
	
END_FUNCTION_BLOCK
