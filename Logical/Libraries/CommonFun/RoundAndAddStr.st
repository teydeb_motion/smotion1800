(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: ComonFun
 * File: ComonFun.st
 * Author: turanskyl
 * Created: June 23, 2014
 *******************************************************************)


FUNCTION RoundAndAddStr

	tempREAL		:= (rNumber * EXPT(10,DecDigits)) + 0.5;	// Add 0.5 to round corectly using TRUNC
	tempDINT		:= TRUNC(tempREAL);
	tempREAL		:= DINT_TO_REAL(tempDINT) / EXPT(10,DecDigits);
	ftoa(tempREAL,ADR(tempSTRING));
	strcat(pStr,ADR(tempSTRING));
	
	RoundAndAddStr := TRUE;	// Return value to avoid warning
	
END_FUNCTION