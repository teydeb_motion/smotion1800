(*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:    abdullahoglf
* Created:   January 29, 2016
**********************************************************************************
* Description:
*	- Function for forcing axis triggers
* History:
*
**********************************************************************************)

FUNCTION_BLOCK ForceTrig
	// ----------------------------
	// wair for cmd
	// ----------------------------
	IF EDGEPOS(Execute) THEN
		Internal.StartEdge := TRUE;
		Execute := FALSE;
	END_IF;	
	IF Internal.StartEdge THEN
		// ----------------------------
		// Check for null pointer 
		// ----------------------------
		IF AxisHandle <> 0 THEN
			pAxis	ACCESS AxisHandle;
			CASE Step OF
				// =========================================================== //
				0:	//	WAIT FOR COMMAND 
				// =========================================================== //
					Step	:= 10;
					Status  := 16#FFFF; // return busy	
				
				// =========================================================== //
				10:		// SET FORCE CONFIGURATION
				// =========================================================== //
					IF ForceValue THEN // set force value to ncCLOSED
						Internal.ForceValue := ncCLOSED;
					ELSE // set force value to ncOPEN
						Internal.ForceValue	:= ncOPEN;
					END_IF;
					IF Trigger = 1 THEN // set trigger 1
						Internal.Trigger			:= ncTRIGGER1;
						pAxis.dig_in.force.trigger1 := Internal.ForceValue;
					ELSIF Trigger = 2 THEN // set trigger 2
						Internal.Trigger			:= ncTRIGGER2;
						pAxis.dig_in.force.trigger2	:= Internal.ForceValue;
					END_IF;
					Step 	:= 20;
					Status 	:= 16#FFFF; // return busy
				
				// =========================================================== //
				20:		// ACTIVATE FORCE
				// =========================================================== //
					Internal.TON_Timeout.PT	:= T#5S;
					Internal.TON_Timeout.IN	:= TRUE;
					Internal.NcActionStat	:= ncaction(AxisHandle, ncDIG_IN, ncFORCE);
					IF Internal.NcActionStat = ncOK THEN // force successful
						Internal.TON_Timeout.IN := FALSE;
						Internal.StartEdge		:= FALSE;
						Step					:= 0;
						Status					:= 0;
					ELSIF Internal.TON_Timeout.Q THEN // timeout occurred
						Internal.TON_Timeout.IN := FALSE;
						Internal.StartEdge		:= FALSE;
						Status					:= ERR_FT_ACTION_FAILED;
					END_IF;	
			END_CASE;
			Internal.TON_Timeout();		
		ELSE // NULL POINTER
			Status := ERR_COM_NULL_PTR;
		END_IF;	// end of null pointer check
	ELSE // FB not executed
		Status := 16#FFFE;
	END_IF;	
END_FUNCTION_BLOCK