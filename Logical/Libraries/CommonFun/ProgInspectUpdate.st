(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: ComonFun
 * File: UpdateStepHist.st
 * Author: turanskyl
 * Created: July 10, 2014
 ********************************************************************
 * Implementation of library ComonFun
 ********************************************************************) 

(* Update Main and Local Step history of Program Inspection *)
FUNCTION ProgInspectUpdate
	
		// Main step history
	IF(pProgInspect.MainHist[0].Step <> ActMainStep)THEN //New Main step - update prog. inspection variables
		FOR i := (SIZEOF(pProgInspect.MainHist)/SIZEOF(pProgInspect.MainHist[0]) - 1) TO 1 BY -1 DO	// Update hist. buffer
			pProgInspect.MainHist[i].Step := pProgInspect.MainHist[i-1].Step;
			pProgInspect.MainHist[i].Time := pProgInspect.MainHist[i-1].Time;
		END_FOR
		pProgInspect.MainHist[0].Step := ActMainStep;
		pProgInspect.MainHist[0].Time := clock_ms();		
	END_IF
	
	// Local step history
	IF(pProgInspect.LocHist[0].Step <> ActLocStep)THEN //New Main step - update prog. inspection variables
		FOR i := (SIZEOF(pProgInspect.LocHist)/SIZEOF(pProgInspect.LocHist[0]) - 1) TO 1 BY -1 DO	// Update hist. buffer
			pProgInspect.LocHist[i].Step := pProgInspect.LocHist[i-1].Step;
			pProgInspect.LocHist[i].Time := pProgInspect.LocHist[i-1].Time;
		END_FOR
		pProgInspect.LocHist[0].Step := ActLocStep;
		pProgInspect.LocHist[0].Time := clock_ms();	
	END_IF
		
	// Others Program Inspection variables
	pProgInspect.ErrActive := BOOL_TO_USINT(ErrorActive);			// Error signalization for program inspection
	strcpy(ADR(pProgInspect.ProgName), pProgName);					// Program name
		
	ProgInspectUpdate := TRUE;
		
END_FUNCTION