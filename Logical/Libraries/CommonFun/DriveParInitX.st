(*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:   abdullahoglf
* Created:  October 21, 2016
**********************************************************************************
* Description:
* 	- Axis parameter handling with mapp config
* History:
*	
**********************************************************************************)

(* Drive Par Initializations Using mapp Config *)
FUNCTION_BLOCK DriveParInitX
	// -------------------------------------------------------------------------------------------------------- //
	//							M A I N		C O N T R O L		S E Q U E N C E									// 
	// -------------------------------------------------------------------------------------------------------- //
	// ----------------------------
	// If execute is gone, reset the FB 
	// ----------------------------
	IF _Internal.bOldExec > inExecute THEN
		_Internal.Step						:= PAR_INIT_X_DISABLED;
		_pAxCfg.Save						:= FALSE; // reset the command just in case
	END_IF;	
	CASE _Internal.Step OF
		// =========================================================== //
		PAR_INIT_X_DISABLED:		// WAIT FOR THE EXEC. COMMAND
		// =========================================================== //
			outError						:= FALSE;
			outStatus						:= 16#FFFE;
			// Check for the rising edge of the Execute input
			IF _Internal.bOldExec < inExecute THEN
				outStatus					:= 16#FFFF;
				_Internal.Step				:= PAR_INIT_X_CHECK_PAR;
			ELSE // No need to evaluate the FB any further
				memset(ADR(_Internal), 0, SIZEOF(_Internal));
				_Internal.bOldExec			:= inExecute;
				RETURN;
			END_IF;
		
		
		// =========================================================== //
		PAR_INIT_X_CHECK_PAR:		// CHECK FB PARAMETERS
		// =========================================================== //
			IF _Internal.bFirstInStep THEN // first in the step
				_Internal.bEvrOk			:= TRUE;
				// ----------------------------
				// Check references
				IF inRefAxisBasic[_Internal.idxAsync] <> 0 AND inRefAxisConfig[_Internal.idxAsync] <> 0 AND inRefAxisConfig[_Internal.idxAsync] <> 0 THEN
					_pAxCtrl				ACCESS inRefAxisBasic[_Internal.idxAsync];
					_pAxCfg					ACCESS inRefAxisConfig[_Internal.idxAsync];
					//
					IF _pAxCtrl.Axis <> 0 AND _pAxCtrl.Parameters <> 0 AND _pAxCfg.Configuration <> 0 THEN	
						_pAxObj				ACCESS _pAxCtrl.Axis;
						_pAxPar				ACCESS _pAxCtrl.Parameters;
						_pAxCfgTyp			ACCESS _pAxCfg.Configuration;
						_pAxCfgNew			ACCESS inRefNewConfig[_Internal.idxAsync];
					ELSE
						_Internal.bEvrOk	:= FALSE;
					END_IF;						
				ELSE
					_Internal.bEvrOk		:= FALSE;
				END_IF;	
				// ----------------------------
				IF _Internal.bEvrOk THEN
					_Internal.Step			:= PAR_INIT_X_SET_PAR;
				ELSE
					IF _Internal.idxAsync = 0 THEN
						outStatus			:= ERR_COM_NULL_PTR;
						outError			:= TRUE;
						_Internal.Step		:= PAR_INIT_X_ERROR;
					ELSE
						outStatus			:= 0;
						_Internal.Step		:= PAR_INIT_X_DISABLED;
					END_IF;	
				END_IF;	
			ELSE // after the first time in the step
				;	
			END_IF;	// end of the step	
		
		
		// =========================================================== //
		PAR_INIT_X_SET_PAR:		// SAVE NEW PARAMETERS 
		// =========================================================== //
			IF _Internal.bFirstInStep THEN // first in the step
				// ----------------------------
				// The parameters must have been sanitized before
				// this FB has been called. Therefore there won't 
				// be any parameter check done here.
				// Only the modifications to Trigger configurations
				// and controller parameters will be done here 
				// depending on the current network type and 
				// the status of the simulation on the drive
				// ----------------------------
				//
				// First copy the parameters
				_pAxCfgTyp := _pAxCfgNew;
				//
				// Then modify them if needed
				//
				// ----------------------------
				// Check if the axis is an SDC cfg. or is a simulated axis
				IF _pAxObj.nc_obj_inf.net_if_typ = ncSDC_IF OR _pAxObj.nc_obj_inf.hardware.acp_typ = ncACP_TYP_SIM OR 
					_pAxObj.nc_obj_inf.nc_obj_typ <> ncAXIS THEN
					_pAxCfgNew.Drive.DigitalInputs.Level.Trigger1	:= mpAXIS_IO_ACTIVE_HI_EXTERNAL;
					_pAxCfgNew.Drive.DigitalInputs.Level.Trigger2	:= mpAXIS_IO_ACTIVE_HI_EXTERNAL;
				END_IF;
				// ----------------------------
				
				// ----------------------------
				// Check if the MpAxisConfig is active
				// ----------------------------
				IF _pAxCfg.Active THEN
					_Internal.Step		:= PAR_INIT_X_INIT;	
				ELSE
					_Internal.Step		:= PAR_INIT_X_ENABLE_MPAXISCFG;
				END_IF;						
			ELSE // after the first time in the step
				;
			END_IF;	// end of the step	
		
		
		// =========================================================== //
		PAR_INIT_X_ENABLE_MPAXISCFG:		// ENABLE THE MpAxisConfig COMPONENT
		// =========================================================== //
			IF _Internal.bFirstInStep THEN // first in the step
				_pAxCfg.Enable					:= TRUE;
				_Internal.bResetMpAxisCfg		:= TRUE;
				_Internal.TON_Timeout.PT		:= T#5S;
				_Internal.TON_Timeout.IN		:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Wait for MpAxisConfig component to react 
				// ----------------------------
				IF _pAxCfg.Active THEN 
					_Internal.TON_Timeout.IN	:= FALSE;
					_Internal.Step				:= PAR_INIT_X_INIT;
				ELSIF _pAxCfg.Error THEN
					_Internal.TON_Timeout.IN	:= FALSE;
					_Internal.Step				:= PAR_INIT_X_ERROR;
					outStatus					:= ERR_PAR_INIT_X_CFG;
					outError					:= TRUE;
				ELSIF _Internal.TON_Timeout.Q THEN
					_Internal.TON_Timeout.IN	:= FALSE;
					_Internal.Step				:= PAR_INIT_X_ERROR;
					outStatus					:= ERR_PAR_INIT_X_CFG_INIT_TO;
					outError					:= TRUE;					
				END_IF;	
			END_IF;	// end of the step	
				
				
		// =========================================================== //
		PAR_INIT_X_INIT:		// SAVE THE NEW CFG.
		// =========================================================== //
			IF _Internal.bFirstInStep THEN // first in the step
				_pAxCfg.Save					:= TRUE;
				_Internal.TON_Timeout.PT		:= T#5S;
				_Internal.TON_Timeout.IN		:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Wait for the results
				// ----------------------------
				IF _pAxCfg.CommandDone THEN	// Operation successfull
					_pAxCfg.Save				:= FALSE;	
					_Internal.TON_Timeout.IN	:= FALSE;				
					
					// ----------------------------
					// Check if the MpAxisConfig component should be disabled 
					// ----------------------------
					IF _Internal.bResetMpAxisCfg THEN 
						_pAxCfg.Enable			:= FALSE;
					END_IF;
					
					// ----------------------------
					// Check if the SW limtis should be disabled
					// ----------------------------
					IF NOT inDisableSwLimits[_Internal.idxAsync] THEN 
						_Internal.idxAsync		:= _Internal.idxAsync + 1;
						_Internal.Step			:= PAR_INIT_X_CHECK_PAR;
					ELSE
						_Internal.Step			:= PAR_INIT_X_DISABLE_SW_LIMITS;
					END_IF;	
					
				ELSIF _pAxCfg.Error THEN // Error occured within the MpAxisConfig component
					_pAxCfg.Save				:= FALSE;
					_Internal.TON_Timeout.IN	:= FALSE;
					_Internal.Step				:= PAR_INIT_X_ERROR;
					outStatus					:= ERR_PAR_INIT_X_CFG;
					outError					:= TRUE;
				
				ELSIF _Internal.TON_Timeout.Q THEN // Timeout occured during configuration update]
					_pAxCfg.Save				:= FALSE;
					_Internal.TON_Timeout.IN	:= FALSE;
					_Internal.Step				:= PAR_INIT_X_ERROR;
					outStatus					:= ERR_PAR_INIT_X_PAR_INIT_TO;
					outError					:= TRUE;
				END_IF;	
			END_IF;	// end of the step
		
		
		// =========================================================== //
		PAR_INIT_X_DISABLE_SW_LIMITS:		// DISABLE THE SW LIMITS ON THE AXIS
		// =========================================================== //
			IF _Internal.bFirstInStep THEN // first in the step
				_Internal.MC_BR_WriteParIDText_0.Axis		:= _pAxCtrl.Axis;
				_Internal.MC_BR_WriteParIDText_0.DataText	:= '1';
				_Internal.MC_BR_WriteParIDText_0.ParID		:= ACP10PAR_SGEN_SW_END_IGNORE;
				_Internal.MC_BR_WriteParIDText_0.Execute	:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Wait for the FB results
				// ----------------------------
				IF _Internal.MC_BR_WriteParIDText_0.Busy THEN // FB is busy
					;
				ELSIF _Internal.MC_BR_WriteParIDText_0.Done	THEN // FB is done
					_Internal.idxAsync	:= _Internal.idxAsync + 1;
					_Internal.Step		:= PAR_INIT_X_CHECK_PAR;
				ELSIF _Internal.MC_BR_WriteParIDText_0.Error THEN 
					outStatus			:= ERR_PAR_INIT_X_WRITE_PARID;
					outError			:= TRUE;
					_Internal.Step		:= PAR_INIT_X_ERROR;
				END_IF;
				IF _Internal.Step <> PAR_INIT_X_DISABLE_SW_LIMITS THEN 
					_Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				END_IF;	
			END_IF;	// end of the step			
	END_CASE;	
	
	// ----------------------------
	// Call other FBs 
	// ----------------------------
	_Internal.TON_Timeout();
	_Internal.MC_BR_WriteParIDText_0();
	
	// ----------------------------
	// Old value handling
	// ----------------------------
	IF _Internal.oldStep <> _Internal.Step THEN
		_Internal.bFirstInStep	:= TRUE;
	ELSE
		_Internal.bFirstInStep	:= FALSE;
	END_IF;
	_Internal.oldStep			:= _Internal.Step;
	//
	_Internal.bOldExec			:= inExecute;
END_FUNCTION_BLOCK
