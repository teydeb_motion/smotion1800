(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: InvCont
 * File: InvExit2Conv.st
 * Author: abdullahoglf
 * Created: December 19, 2013
 ********************************************************************
 * Implementation of library InvCont
 ********************************************************************) 

(* FB to control exit2 station conveyor *)
FUNCTION_BLOCK InvAnalogCtrl

	// FB execution
	TON_InverterSleep.PT := T#1000ms; // waiting time after jog. command
	TON_InverterSleep();

	IF (Enable = FALSE) THEN  // The FB is not enabled
		 // Reset outputs
		 DO_ConvBrake         	:= FALSE;
		 DO_ConvMoveNeg       	:= FALSE;
		 DO_ConvMovePos       	:= FALSE;
		 AO_ConvSpeed         	:= 0;
		 Error				  	:= FALSE;
		 ErrorID			  	:= 0;
		 JogNeg					:= FALSE;
		 JogPos					:= FALSE;
		 JogSlow				:= FALSE;
		 ReadyCmd				:= FALSE;
		 // Reset internal var
		 TON_InverterSleep.IN 	:= FALSE;
		 convStep             	:= CONV_WAIT;
		 
	 ELSE   // The FB is enabled
		// Check inverter alarm		
		IF (DI_InverterAlarm = TRUE) THEN
			convStep 	:= CONV_ERROR;
			Error 		:= TRUE;
			ErrorID		:= ERR_CONV_INVERTER_ALARM; // Inverter alarm
		END_IF
		 
	 	CASE convStep OF
			// ---------------------------
			// Wait for command
			CONV_WAIT:
				TON_InverterSleep.IN 	:= FALSE;
				ReadyCmd				:= TRUE;
				IF (JogPos) THEN  // cmd to jog positive
					convStep   	:= CONV_JOG_POS;
					ReadyCmd	:= FALSE;
				ELSIF (JogNeg) THEN // cmd to jog negative
					convStep	:= CONV_JOG_NEG;
					ReadyCmd	:= FALSE;
				END_IF
	

			//------------------------------------
			// Positive jogging
			CONV_JOG_POS:
				// Set the movements commands
				DO_ConvBrake    := TRUE;
				DO_ConvMovePos  := TRUE;
				IF(JogSlow) THEN
					AO_ConvSpeed    := 20; // Fix slow speed for slow move
				ELSE
					AO_ConvSpeed    := JogSpeed;
				END_IF
				// Stop command				
				IF NOT JogPos THEN					
					convStep       := CONV_WAIT_AFTER_JOG;
				END_IF;	
				
			//------------------------------------
			// Negative jogging
			CONV_JOG_NEG:
				// Set the movements commands
				DO_ConvBrake    := TRUE;
				DO_ConvMoveNeg  := TRUE;
				IF(JogSlow) THEN
					AO_ConvSpeed    := 20; // Fix slow speed for slow move
				ELSE
					AO_ConvSpeed    := JogSpeed;
				END_IF
				// Stop the movement
				IF NOT JogNeg THEN
					convStep       := CONV_WAIT_AFTER_JOG;
				END_IF;	
				
			
			//------------------------------------
			// Wait after jogging command to stop the motor with ramp
			CONV_WAIT_AFTER_JOG:
				// Start timer
				TON_InverterSleep.IN := TRUE;			
				// Stop movement
				DO_ConvBrake   := FALSE;
				DO_ConvMovePos := FALSE;
				DO_ConvMoveNeg := FALSE;	
				AO_ConvSpeed   := 0;
				// Delay time is up -> go to wait state and be ready for next cmd
				IF(TON_InverterSleep.Q)THEN
					TON_InverterSleep.IN := FALSE; // reset timer
					convStep	:= CONV_WAIT;
				END_IF	
	
				
			//-------------------------------------------
			// Error step
			//-------------------------------------------
			CONV_ERROR:
				Error 				 := TRUE;
				DO_ConvBrake         := FALSE;
				DO_ConvMoveNeg       := FALSE;
				DO_ConvMovePos       := FALSE;
				AO_ConvSpeed         := 0;
				ReadyCmd			 := FALSE;
				TON_InverterSleep.IN := FALSE;

		END_CASE; 
	END_IF; // end of FB enable check
	
	

END_FUNCTION_BLOCK