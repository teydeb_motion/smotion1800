
(* Validation of the Axis Parameters Using mapp Axis Cfg *)
FUNCTION CheckDriveParX
	_bCheck	:= TRUE;
	// ----------------------------
	// Check the configuration handle
	// ----------------------------
	IF inRefCfg <> 0 THEN
		_pAxCfg	ACCESS inRefCfg;
		
		// ----------------------------
		// check the controller parameters
		// ----------------------------
		// Controller mode is not valid
		IF _pAxCfg.Drive.Controller.Mode	<> mpAXIS_CTRL_MODE_POSITION 	AND
			_pAxCfg.Drive.Controller.Mode	<> mpAXIS_CTRL_MODE_POSITION_FF	AND
			_pAxCfg.Drive.Controller.Mode	<> mpAXIS_CTRL_MODE_UF			THEN
			_bCheck	:= FALSE;
		END_IF;
		// Position controller is not valid
		IF _pAxCfg.Drive.Controller.Mode	<> mpAXIS_CTRL_MODE_UF THEN
			IF _pAxCfg.Drive.Controller.Position.ProportionalGain 	<= 0	OR
				_pAxCfg.Drive.Controller.Position.IntegralTime		< 0		OR
				_pAxCfg.Drive.Controller.Position.PredictionTime	< 0		OR
				_pAxCfg.Drive.Controller.Position.TotalDelayTime	< 0		THEN
				_bCheck	:= FALSE;
			END_IF;	
			// Speed controller is not valid
			IF _pAxCfg.Drive.Controller.Speed.ProportionalGain	<= 0	OR
				_pAxCfg.Drive.Controller.Speed.FilterTime		< 0		OR
				_pAxCfg.Drive.Controller.Speed.IntegralTime		< 0 	THEN
				_bCheck	:= FALSE;
			END_IF;
		ELSIF _pAxCfg.Drive.Controller.Mode = mpAXIS_CTRL_MODE_UF THEN
			IF _pAxCfg.Drive.Controller.VoltageFrequency.RatedFrequency 	<= 0	OR
				_pAxCfg.Drive.Controller.VoltageFrequency.BoostVoltage		< 0		OR
				_pAxCfg.Drive.Controller.VoltageFrequency.RatedVoltage		<= 0	OR
				_pAxCfg.Drive.Controller.VoltageFrequency.SlipCompensation	< 0		THEN
				_bCheck	:= FALSE;				
				END_IF;	
		END_IF; // end of the controller mode check
		
		// ----------------------------
		// Check encoder interface parameters
		// ----------------------------
		IF (_pAxCfg.Drive.Gearbox.Direction <> mpAXIS_DIR_CLOCKWISE			AND
			_pAxCfg.Drive.Gearbox.Direction	<> mpAXIS_DIR_COUNTERCLOCKWISE)	OR
			_pAxCfg.Drive.Gearbox.Input 					<= 0			OR
			_pAxCfg.Drive.Gearbox.Output					<= 0			OR
			_pAxCfg.Drive.Transformation.ReferenceDistance 	<= 0			OR
			_pAxCfg.Axis.MeasurementResolution				<= 0			THEN
			_bCheck	:= FALSE;
		END_IF;
		
		// ----------------------------
		// Check limit parameters
		// ----------------------------
		// Position limits are not correct
		IF _pAxCfg.Axis.SoftwareLimitPositions.LowerLimit	= 0	AND
			_pAxCfg.Axis.SoftwareLimitPositions.UpperLimit	= 0 THEN
			_bCheck	:= FALSE;
		END_IF;
		// Lag error is not valid
		IF _pAxCfg.Axis.MovementLimits.PositionErrorStopLimit	<= 0 THEN
			_bCheck := FALSE;
		END_IF;
		// Rest of the limits are invalid
		IF _pAxCfg.Axis.MovementLimits.Acceleration				<= 0	OR
			_pAxCfg.Axis.MovementLimits.Deceleration			<= 0	OR
			_pAxCfg.Axis.MovementLimits.JerkTime				< 0		OR
			_pAxCfg.Axis.MovementLimits.VelocityErrorStopLimit	< 0		OR
			_pAxCfg.Axis.MovementLimits.VelocityNegative		<= 0	OR
			_pAxCfg.Axis.MovementLimits.VelocityPositive		<= 0	THEN
			_bCheck	:= FALSE;
		END_IF;	
		// -------------------------------------------------------------------------------------------------------- //
		//	  There are much more values to check - add them whenever it is necessary - abdullahoglf, TBA			// 
		// -------------------------------------------------------------------------------------------------------- //		
				
	ELSE // null reference
		_bCheck	:= FALSE;
	END_IF;	
	// set the result
	CheckDriveParX	:= BOOL_TO_USINT(_bCheck);
END_FUNCTION
