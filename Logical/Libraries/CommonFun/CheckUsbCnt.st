(*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:    abdullahoglf
* Created:   November 19, 2015
**********************************************************************************
* Description:
*	This FB returns the number of USB devices that are currently plugged in
*		to the PLC.
* History:
*
**********************************************************************************)
FUNCTION_BLOCK CheckUsbCnt
	IF Enable THEN
		CASE Step OF
			0:
				Busy := TRUE;
				UsbNodeListGet_0.filterInterfaceClass 		:= asusb_CLASS_MASS_STORAGE;
				UsbNodeListGet_0.filterInterfaceSubClass	:= 0;
				UsbNodeListGet_0.pBuffer					:= ADR(Buffer[0]);
				UsbNodeListGet_0.bufferSize					:= SIZEOF(Buffer);
				UsbNodeListGet_0.enable						:= TRUE;
				Step := 5;
			5:
				IF UsbNodeListGet_0.status = 0 THEN
					Done 		:= TRUE;
					UsbDvcCnt 	:= UDINT_TO_USINT(UsbNodeListGet_0.allNodes);
					UsbNodeListGet_0.enable := FALSE;
					Step := 254;
				ELSIF UsbNodeListGet_0.status <> ERR_FUB_BUSY THEN
					Error := TRUE;
					Busy  := FALSE;
					Step  := 255;
					UsbNodeListGet_0.enable := FALSE;
				END_IF;
		END_CASE;
	ELSE // FB is disabled
		Done 		:= FALSE;
		Busy 		:= FALSE;
		Error		:= FALSE;
		UsbDvcCnt 	:= 0;
		Step		:= 0;
		UsbNodeListGet_0.enable := FALSE;
	END_IF;
	UsbNodeListGet_0();
END_FUNCTION_BLOCK