(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: ComonFun
 * File: CommonFun.st
 * Author: turanskyl
 * Created: June 23, 2014
 *******************************************************************)


FUNCTION CalcAcc
	t 	:= S_dist / (0.5*(V_f + V_i));
	Acc := (2/(t*t)) * (S_dist - (V_i*t));
	
	CalcAcc := Acc;
	
END_FUNCTION