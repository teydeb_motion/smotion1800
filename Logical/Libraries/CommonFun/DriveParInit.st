 (*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:    abdullahoglf
* Created:   May 06, 2016
**********************************************************************************
* Description:
*	- This FB can be used to init the parameters on the drive
* History:
*
**********************************************************************************)
(* Driver Parameters Initialization *)
FUNCTION_BLOCK DriveParInit
	// -------------------------------------------------------------------------------------------------------- //
	//						M A I N		C O N T R O L		S E Q U N C E                                       // 
	// -------------------------------------------------------------------------------------------------------- //
	// ----------------------------
	// If execute is gone, reset the FB
	// ----------------------------
	IF Internal.bOldExec > Execute THEN 
		Internal.Step := PAR_INIT_DISABLED;
	END_IF;	
	CASE Internal.Step OF
		// =========================================================== //
		PAR_INIT_DISABLED:		// WAIT FOR THE EXEC. COMMAND
		// =========================================================== //
			Error 				:= FALSE;
			Status				:= 16#FFFE;
			Internal.idxAsync	:= 0;
			Internal.MC_BR_WriteParIDText_0.Execute		:= FALSE;
			Internal.MC_BR_InitAxisSubjectPar_0.Execute	:= FALSE;
			IF Internal.bOldExec < Execute THEN 
				Status			:= 16#FFFF;
				Internal.Step 	:= PAR_INIT_CHECK_PAR;
			END_IF;			
		

		// =========================================================== //
		PAR_INIT_CHECK_PAR:		// CHECK THE FB PARAMETERS
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				// ----------------------------
				// Check the pointers
				IF AxRef[Internal.idxAsync] <> 0 AND AxParRef[Internal.idxAsync] <> 0 THEN
					pAxObj	ACCESS AxRef[Internal.idxAsync];
					pAxPar	ACCESS AxParRef[Internal.idxAsync];
					Internal.Step 	:= PAR_INIT_SET_PAR;
				ELSE
					Status			:= 0;
					Internal.Step	:= PAR_INIT_DISABLED;
				END_IF;	
				// ----------------------------
			ELSE // after the first time in the step
				;
			END_IF;	// end of the step
		
		
		// =========================================================== //
		PAR_INIT_SET_PAR:		// SET THE PARAMETERS DEPENDING ON THE TYPES
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				// ----------------------------
				// The parameters must have been sanitized before
				// this FB has been called. Therefore there won't 
				// be any parameter check done here.
				// Only the modifications to Trigger configurations
				// and controller parameters will be done here 
				// depending on the current network type and 
				// the status of the simulation on the drive
				// ----------------------------
				//
				// First copy the parameters
				pAxObj.controller.ff		:= pAxPar.controller.ff;
				pAxObj.controller.mode		:= pAxPar.controller.mode;
				pAxObj.controller.position	:= pAxPar.controller.position;
				pAxObj.controller.speed		:= pAxPar.controller.speed;
				pAxObj.controller.uf		:= pAxPar.controller.uf;
				pAxObj.dig_in.level			:= pAxPar.dig_in.level;
				pAxObj.encoder_if.parameter	:= pAxPar.encoder_if.parameter;
				pAxObj.limit.parameter		:= pAxPar.limit.parameter;
				//
				// Then modify them if needed
				//
				// ----------------------------
				// Check if the axis is an SDC cfg. or is a simulated axis
				IF pAxObj.nc_obj_inf.net_if_typ = ncSDC_IF OR pAxObj.nc_obj_inf.hardware.acp_typ = ncACP_TYP_SIM OR pAxObj.nc_obj_inf.nc_obj_typ <> ncAXIS THEN // delete the FF values
					pAxObj.controller.mode	:= ncPOSITION;
					//
					memset(ADR(pAxObj.controller.ff), 0, SIZEOF(pAxObj.controller.ff));
					memset(ADR(pAxObj.controller.speed.isq_filter1), 0, SIZEOF(pAxObj.controller.speed.isq_filter1));
					memset(ADR(pAxObj.controller.speed.isq_filter2), 0, SIZEOF(pAxObj.controller.speed.isq_filter1));
					memset(ADR(pAxObj.controller.speed.isq_filter3), 0, SIZEOF(pAxObj.controller.speed.isq_filter1));
					// set the trigger cfg
					pAxObj.dig_in.level.trigger1	:= ncACTIV_HI + ncFORCE;
					pAxObj.dig_in.level.trigger2	:= ncACTIV_HI + ncFORCE;
				END_IF;					
				// ----------------------------
				Internal.Step := PAR_INIT_INIT;				
			ELSE // after the first time in the step
				;
			END_IF;	// end of the step
		
		
		// =========================================================== //
		PAR_INIT_INIT:		// PAR. INITIALIZE
		// =========================================================== //
			IF Internal.bFirstInStep THEN 
				IF NOT InitLimitsOnly[Internal.idxAsync] AND NOT InitEncodIfOnly[Internal.idxAsync] THEN
					Internal.MC_BR_InitAxisSubjectPar_0.Subject	:= ncGLOBAL;
				ELSIF InitLimitsOnly[Internal.idxAsync] THEN
					Internal.MC_BR_InitAxisSubjectPar_0.Subject	:= ncLIMITS;
				ELSE
					Internal.MC_BR_InitAxisSubjectPar_0.Subject	:= ncENCODER_IF;
				END_IF;
				Internal.MC_BR_InitAxisSubjectPar_0.Axis		:= AxRef[Internal.idxAsync];
				Internal.MC_BR_InitAxisSubjectPar_0.Execute		:= TRUE;
			ELSE
				IF Internal.MC_BR_InitAxisSubjectPar_0.Done THEN
					Internal.MC_BR_InitAxisSubjectPar_0.Execute	:= FALSE;
					IF NOT DisableSwLimits[Internal.idxAsync] AND NOT SaveToInit[Internal.idxAsync] THEN
						Internal.idxAsync	:= Internal.idxAsync + 1;
						Internal.Step		:= PAR_INIT_CHECK_PAR;
					ELSIF NOT SaveToInit[Internal.idxAsync] THEN
						Internal.Step		:= PAR_INIT_DISABLE_SW_LIMITS;
					ELSE
						Internal.Step		:= PAR_INIT_SAVE_TO_INIT;
					END_IF;	
				ELSIF Internal.MC_BR_InitAxisSubjectPar_0.Error THEN 
					Status					:= Internal.MC_BR_InitAxisSubjectPar_0.ErrorID;
					Internal.Step			:= PAR_INIT_ERROR;
					Internal.MC_BR_InitAxisSubjectPar_0.Execute	:= FALSE;
				END_IF; // end of the FB status check	
			END_IF;
		
		// =========================================================== //
		PAR_INIT_W_INIT:	// WAIT UNTIL THE INIT. IS COMPLETED
		// =========================================================== //
			IF pAxObj.global.init = ncTRUE THEN
				IF NOT DisableSwLimits[Internal.idxAsync] AND NOT SaveToInit[Internal.idxAsync] THEN
					Internal.idxAsync	:= Internal.idxAsync + 1;
					Internal.Step		:= PAR_INIT_CHECK_PAR;
				ELSIF NOT SaveToInit[Internal.idxAsync] THEN
					Internal.Step		:= PAR_INIT_DISABLE_SW_LIMITS;
				ELSE
					Internal.Step		:= PAR_INIT_SAVE_TO_INIT;
				END_IF;	
			END_IF;	
		
		// =========================================================== //
		PAR_INIT_SAVE_TO_INIT:		// SAVE THE PARAMETERS TO THE RELATED INIT TABLE
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				Internal.MC_BR_SaveAxisPar_0.Axis			:= AxRef[Internal.idxAsync];
				memcpy(ADR(Internal.MC_BR_SaveAxisPar_0.DataObjectName), ADR(pAxObj.global.init_par.data_modul[0]), SIZEOF(Internal.MC_BR_SaveAxisPar_0.DataObjectName));
				Internal.MC_BR_SaveAxisPar_0.Execute		:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Check the FB results
				// ----------------------------
				IF Internal.MC_BR_SaveAxisPar_0.Busy THEN
					;
				ELSIF Internal.MC_BR_SaveAxisPar_0.Done THEN
					IF NOT DisableSwLimits[Internal.idxAsync] THEN
						Internal.idxAsync	:= Internal.idxAsync + 1;
						Internal.Step		:= PAR_INIT_CHECK_PAR;
					ELSE
						Internal.Step		:= PAR_INIT_DISABLE_SW_LIMITS;
					END_IF;
				ELSIF Internal.MC_BR_SaveAxisPar_0.Error THEN
					Status					:= Internal.MC_BR_SaveAxisPar_0.ErrorID;
					Internal.Step			:= PAR_INIT_ERROR;
				END_IF;	// end of the FB results check
				//
				IF Internal.Step <> PAR_INIT_SAVE_TO_INIT THEN 
					Internal.MC_BR_SaveAxisPar_0.Execute	:= FALSE;
				END_IF;	
			END_IF;	// end of the step	
		
		
		// =========================================================== //
		PAR_INIT_DISABLE_SW_LIMITS:		// DISABLE SW LIMITS
		// =========================================================== //
			IF Internal.bFirstInStep THEN // first in the step
				Internal.MC_BR_WriteParIDText_0.Axis		:= AxRef[Internal.idxAsync];
				Internal.MC_BR_WriteParIDText_0.DataText	:= '1';
				Internal.MC_BR_WriteParIDText_0.ParID		:= ACP10PAR_SGEN_SW_END_IGNORE;
				Internal.MC_BR_WriteParIDText_0.Execute		:= TRUE;
			ELSE // after the first time in the step
				// ----------------------------
				// Wait for the FB results
				// ----------------------------
				IF Internal.MC_BR_WriteParIDText_0.Busy THEN // FB is busy
					; // continue calling the FB
				ELSIF Internal.MC_BR_WriteParIDText_0.Done THEN // FB is done
					Internal.idxAsync	:= Internal.idxAsync + 1;
					Internal.Step		:= PAR_INIT_CHECK_PAR;
				ELSIF Internal.MC_BR_WriteParIDText_0.Error THEN // error occured
					Status				:= Internal.MC_BR_WriteParIDText_0.ErrorID;
					Internal.Step		:= PAR_INIT_ERROR;
				END_IF;
				IF Internal.Step <> PAR_INIT_DISABLE_SW_LIMITS THEN 
					Internal.MC_BR_WriteParIDText_0.Execute	:= FALSE;
				END_IF;	
			END_IF;	// end of the step
	END_CASE;
	
	// ----------------------------
	// Call other FBs
	// ----------------------------
	Internal.MC_BR_WriteParIDText_0();
	Internal.MC_BR_SaveAxisPar_0();
	Internal.MC_BR_InitAxisSubjectPar_0();
	
	// ----------------------------
	// Old value handling
	// ----------------------------
	IF Internal.oldStep <> Internal.Step THEN 
		Internal.bFirstInStep	:= TRUE;
	ELSE
		Internal.bFirstInStep	:= FALSE;
	END_IF;
	//
	Internal.oldStep	:= Internal.Step;
	Internal.bOldExec	:= Execute;
	
	
END_FUNCTION_BLOCK
