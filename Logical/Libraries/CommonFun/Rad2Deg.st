
(* Conversion from radiants to degrees *)
FUNCTION Rad2Deg
	Rad2Deg := inRad * 57.2957795;
END_FUNCTION