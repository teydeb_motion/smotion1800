(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: ComonFun
 * File: CreateTable.st
 * Author: turanskyl
 * Created: January 22, 2015
 ********************************************************************
 * Implementation of library ComonFun
 ********************************************************************) 

(* Create Tool or Zero table data obj *)
FUNCTION_BLOCK CreateTable
	
	IF(Enable = FALSE)THEN
		Step := DISABLED;
	END_IF
	
		
	
	CASE Step OF
	
		//---------------------------------------------
		// Dissabled
		//---------------------------------------------
		DISABLED:
			ErrorID := 0;
			Done	:= FALSE;
			strcpy(ADR(ErrAditionalInfoText),ADR(''));
			
			// Reset Function blocks
			DatObjDelete_0.enable := FALSE;
			DatObjDelete_0(); //Execute FB
			DatObjMove_0.enable := FALSE;
			DatObjMove_0();
			
			// Change step upon enable
			IF(Enable)THEN
				Step := DETERMINE_ID;
			END_IF
	
		
		
		//---------------------------------------------
		// Determine Data obj ID 
		//---------------------------------------------
		DETERMINE_ID:
			// Get DO ID
			FbStatus := ncda_id(DataTableType,DataObjNameADR,ADR(IdentDO));
			
			IF(FbStatus = 0)THEN					// Obj exist
				Step := DETERMINE_INFO;
			ELSIF(FbStatus = ERR_BUR_ILLOBJ)THEN 	// Object does not exist
				Step := CREATE_DATAOB;
			ELSE 									// Error
				Step :=	ERROR_STATE;
				ErrorID := FbStatus;
				ErrAditionalInfoText := 'Error during reading Data Obj. ID';
			END_IF
			
		
		
		//---------------------------------------------
		// Determine Data obj Info
		//---------------------------------------------
		DETERMINE_INFO:		
			FbStatus:= ncda_inf(IdentDO,ADR(Info_mo_dat_p),ADR(Info_dat_len), ADR(Info_mem_type));
			IF(FbStatus = 0)THEN	// info obtained correctly
				IF(Info_dat_len >= DataSize)THEN // Data obj. size is big enough for new data
					Step	:= WRITE_TO_DATAOBJ;
				ELSE	// Data obj. is too small
					Step	:= DELETE_DATAOBJ;
				END_IF
			ELSE	// Status <> 0;
				ErrorID := FbStatus;
				ErrAditionalInfoText := 'Error during reading Data Obj. Info';
			END_IF
				
			
			
		//---------------------------------------------
		// Write to Data Object
		//---------------------------------------------	
		WRITE_TO_DATAOBJ:
			FbStatus:= ncda_wr(IdentDO,DataAdr,DataSize, 0);
			IF(FbStatus = 0)THEN
				Step	:= DONE;
			ELSE	// Status <> 0;
				ErrorID  := FbStatus;
				ErrAditionalInfoText := 'Error during writing to Data Obj.';
			END_IF
			
			

		//---------------------------------------------
		// Delete Data Object
		//---------------------------------------------
		DELETE_DATAOBJ:
			DatObjDelete_0(enable := 1, ident := IdentDO);
			IF (DatObjDelete_0.status = 0) THEN
				Step:= CREATE_DATAOB;
			ELSIF DatObjDelete_0.status <> ERR_FUB_BUSY THEN
				ErrorID  := DatObjDelete_0.status;
				ErrAditionalInfoText := 'Error during deleting Data Obj.';
			END_IF
			
			

		//---------------------------------------------
		// Create Data Object
		//---------------------------------------------			
		CREATE_DATAOB:
			FbStatus:= ncda_cr(DataTableType, DataObjNameADR, DataSize, DataAdr,ADR(Create_mo_dat_p), ADR(IdentDO));
			
			IF FbStatus = 0 THEN
				Step:= MOVE_DATAOBJ_TO_USERROM;
			ELSE
				ErrorID  := FbStatus;
				ErrAditionalInfoText := 'Error during Creating Data Obj.';
			END_IF
		
		
		
		//---------------------------------------------
		// Move Data Object to USERROM
		//---------------------------------------------
		MOVE_DATAOBJ_TO_USERROM:
			DatObjMove_0(enable := 1, ident := IdentDO, MemType:= doUSRROM, Option:= 0);
			IF (DatObjMove_0.status = 0) THEN
				Step:= DONE;
			ELSIF DatObjMove_0.status <> ERR_FUB_BUSY THEN
				ErrorID  := DatObjMove_0.status;
				ErrAditionalInfoText := 'Error during moving Data Obj. to USERROM';
			END_IF

			
	
		//---------------------------------------------
		// Done
		//---------------------------------------------
		DONE:
			Done := TRUE;
		
		
		
		//---------------------------------------------
		// Error
		//---------------------------------------------
		ERROR_STATE:
			// Wait in this step for FB disabled

				
	END_CASE
	
	//Evaluate Error ID
	IF(ErrorID <> 0)THEN
		Step := ERROR_STATE;
	END_IF
	
	// Assign Status
	Status := Step;	
	
	
	
END_FUNCTION_BLOCK