
(* Validation of the Axis Parameters Using ACP10AXIS_typ axis structure *)
FUNCTION CheckDrivePar	
	bCheck := TRUE;
	// ----------------------------
	// check the axis handle
	// ----------------------------
	IF pFixPar <> 0 THEN
		AxPar ACCESS pFixPar;	
		
		// ----------------------------
		// check the controller parameters
		// ----------------------------
		// controller mode not valid
		IF AxPar.controller.mode 	<> ncPOSITION 		AND
			AxPar.controller.mode	<> ncUF				AND
			AxPar.controller.mode   <> ncPOSITION+ncFF	THEN
			bCheck := FALSE;			
		END_IF;		
		// position controller not valid
		IF AxPar.controller.mode <> ncUF THEN
			IF AxPar.controller.position.kv 		<= 0	OR
				AxPar.controller.position.tn 		< 0  	OR
				AxPar.controller.position.t_predict	< 0		OR
				AxPar.controller.position.t_total   < 0		THEN
				bCheck := FALSE;
			END_IF;
			// speed controller not valid
			IF AxPar.controller.speed.kv <= 0 	OR
				AxPar.controller.speed.tn < 0	THEN
				bCheck := FALSE;
			END_IF;
		ELSIF AxPar.controller.mode = ncUF THEN
			IF AxPar.controller.uf.fn 	 = 0	OR
				AxPar.controller.uf.type = 0	OR
				AxPar.controller.uf.u0	 = 0	OR
				AxPar.controller.uf.un	 = 0	THEN
				bCheck := FALSE;
			END_IF;
		END_IF;
		
		// ----------------------------
		// check encoder interface parameters
		// ----------------------------
		IF (AxPar.encoder_if.parameter.count_dir <> ncSTANDARD 		AND
			AxPar.encoder_if.parameter.count_dir <> ncINVERSE)		OR
			AxPar.encoder_if.parameter.scaling.load.rev_motor <= 0	OR
			AxPar.encoder_if.parameter.scaling.load.units <= 0		THEN
			bCheck := FALSE;
		END_IF;
		
		// ----------------------------
		// check limit parameters 
		// ----------------------------
		// position limits not correct
		IF AxPar.limit.parameter.pos_sw_end  = 0  AND
			AxPar.limit.parameter.neg_sw_end = 0  THEN
			bCheck := FALSE;	
		END_IF;
		// lag error not valid
		IF AxPar.limit.parameter.ds_stop <= 0 THEN
			bCheck := FALSE;
		END_IF;
		IF AxPar.limit.parameter.a1_neg 	<= 0 	OR
			AxPar.limit.parameter.a1_pos	<= 0	OR
			AxPar.limit.parameter.a2_neg	<= 0	OR
			AxPar.limit.parameter.a2_pos	<= 0	OR
			AxPar.limit.parameter.a_stop	<= 0	OR
			AxPar.limit.parameter.ds_stop	<= 0	OR
			AxPar.limit.parameter.v_neg		<= 0	OR
			AxPar.limit.parameter.v_pos		<= 0	THEN
			bCheck := FALSE;
		END_IF;	
		// -------------------------------------------------------------------------------------------------------- //
		//	  There are much more values to check - add them whenever it is necessary - abdullahoglf, TBA			// 
		// -------------------------------------------------------------------------------------------------------- //
	
	ELSE // invalid handle
		bCheck := FALSE;
	END_IF;	
	// set the result
	CheckDrivePar := BOOL_TO_USINT(bCheck);
END_FUNCTION