(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: CommonFun
 * File: CommonFun.fun
 * Author: abdullahoglf
 * Created: June 23, 2014
 ********************************************************************
 * Functions and function blocks of library ComonFun
 ********************************************************************)

FUNCTION CalcAcc : REAL (*Calculates approximately the necessary acc/dec value to stop within a given distance*)
	VAR_INPUT
		V_i : REAL;
		V_f : REAL;
		S_dist : REAL;
	END_VAR
	VAR
		Acc : REAL;
		t : REAL;
	END_VAR
END_FUNCTION

FUNCTION ProgInspectUpdate : BOOL (*Update Program Inspection structure*) (*$GROUP=User*)
	VAR_INPUT
		pProgInspect : REFERENCE TO stProgInspect_typ; (*Pointer to Program Inspection structure*)
		ActMainStep : UINT; (*Actual main step*)
		ActLocStep : UINT; (*Actual local step*)
		pProgName : UDINT; (*Program block name*)
		ErrorActive : BOOL; (*Error flag*)
	END_VAR
	VAR
		i : UINT; (*Internal help variable*)
	END_VAR
END_FUNCTION

FUNCTION CheckDrivePar : USINT
	VAR_INPUT
		pFixPar : UDINT; (*Pointer to the parameter structure*)
	END_VAR
	VAR
		bCheck : BOOL;
		AxPar : REFERENCE TO Ax_Init_typ;
	END_VAR
END_FUNCTION

FUNCTION CheckDriveParX : USINT (*Validation of the Axis Parameters Using mapp Axis Cfg*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		inRefCfg : UDINT; (*Reference to the axis configuration type*)
	END_VAR
	VAR
		_bCheck : BOOL;
		_pAxCfg : REFERENCE TO MpAxisBasicConfigType; (*Pointer to the axis configuration type *)
	END_VAR
END_FUNCTION

FUNCTION RoundAndAddStr : BOOL (*Round given number to certain decimal digits and add it to string*)
	VAR_INPUT
		rNumber : REAL; (*Number to be rounded*)
		pStr : UDINT; (*pointer to string where to add number*)
		DecDigits : USINT; (*Number Of decimal digits after rounding*)
	END_VAR
	VAR
		tempREAL : REAL;
		tempDINT : DINT;
		tempSTRING : STRING[128];
	END_VAR
END_FUNCTION

FUNCTION Rad2Deg : REAL (*Converts radiants to degrees*)
	VAR_INPUT
		inRad : REAL; (*Input in radiants*)
	END_VAR
END_FUNCTION

{REDUND_UNREPLICABLE} FUNCTION_BLOCK PulseGen (*Pulse generation *)
	VAR_INPUT
		inEnable : {REDUND_UNREPLICABLE} BOOL; (*FB Execute -> the pulse will be generated as long as the fb is active*)
		inMode : {REDUND_UNREPLICABLE} PulseGen_Modes_enm; (*Pulse mode*)
	END_VAR
	VAR_OUTPUT
		outPulse : {REDUND_UNREPLICABLE} BOOL; (*Configured pulse output*)
	END_VAR
	VAR
		Internal : {REDUND_UNREPLICABLE} PulseGen_Internal_typ; (*FB Internal structure*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK ForceTrig (*FB to force a given trigger on any kind of axes*)
	VAR_INPUT
		Execute : {REDUND_UNREPLICABLE} BOOL; (*Force command*)
		AxisHandle : {REDUND_UNREPLICABLE} UDINT; (*Reference to the axis structure*)
		Trigger : {REDUND_UNREPLICABLE} USINT; (*Trigger to be forced*)
		ForceValue : {REDUND_UNREPLICABLE} BOOL; (*Force value*)
	END_VAR
	VAR_OUTPUT
		Status : {REDUND_UNREPLICABLE} UINT;
	END_VAR
	VAR
		Step : {REDUND_UNREPLICABLE} USINT;
		pAxis : REFERENCE TO ACP10AXIS_typ;
		TON_Timeout : {REDUND_UNREPLICABLE} TON;
		Internal : {REDUND_UNREPLICABLE} ForceTrig_Internal;
		zzEdge00000 : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK CheckUsbCnt (*Returns the actual number of the USB devices plugged in *)
	VAR_INPUT
		Enable : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		UsbDvcCnt : {REDUND_UNREPLICABLE} USINT; (*Number of USB devices currently plugged in the machine*)
		Busy : {REDUND_UNREPLICABLE} BOOL;
		Error : {REDUND_UNREPLICABLE} BOOL;
		Done : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR
		Buffer : {REDUND_UNREPLICABLE} ARRAY[0..4] OF UDINT;
		Step : {REDUND_UNREPLICABLE} USINT;
		UsbNodeListGet_0 : {REDUND_UNREPLICABLE} UsbNodeListGet;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK CreateTable (*Create Tool or Zero table data obj*) (*$GROUP=User*)
	VAR_INPUT
		Enable : {REDUND_UNREPLICABLE} BOOL;
		DataObjNameADR : {REDUND_UNREPLICABLE} UDINT; (*Pointer to data object name*)
		DataTableType : {REDUND_UNREPLICABLE} USINT; (*ncTOOL / ncZERO_DAT*)
		DataAdr : {REDUND_UNREPLICABLE} UDINT; (*Addres of data to be written*)
		DataSize : {REDUND_UNREPLICABLE} UDINT; (*Size of data to be written*)
	END_VAR
	VAR_OUTPUT
		Done : {REDUND_UNREPLICABLE} BOOL; (*Done flag - stays true until block is disabled*)
		ErrorID : {REDUND_UNREPLICABLE} UINT; (*In case of error <> 0*)
		Status : {REDUND_UNREPLICABLE} CreateTableStep_Enum; (*Actual step of FB*)
		ErrAditionalInfoText : {REDUND_UNREPLICABLE} STRING[150];
	END_VAR
	VAR
		Step : {REDUND_UNREPLICABLE} CreateTableStep_Enum; (*State machine step*)
		FbStatus : {REDUND_UNREPLICABLE} UINT; (*Status of function block*)
		IdentDO : {REDUND_UNREPLICABLE} UDINT; (*Data obj Ident*)
		Info_mo_dat_p : {REDUND_UNREPLICABLE} UDINT;
		Info_mem_type : {REDUND_UNREPLICABLE} USINT;
		Info_dat_len : {REDUND_UNREPLICABLE} UDINT;
		DatObjMove_0 : {REDUND_UNREPLICABLE} DatObjMove;
		DatObjDelete_0 : {REDUND_UNREPLICABLE} DatObjDelete;
		Create_mo_dat_p : {REDUND_UNREPLICABLE} UDINT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK GlassPoints (*Calculates the end&start points of the glass edges*)
	VAR_INPUT
		PosY : {REDUND_UNREPLICABLE} REAL; (*Actual / current glass point - Y axis*)
		PosX : {REDUND_UNREPLICABLE} REAL; (*Actual / current glass point - X axis*)
		Angle_Cur : {REDUND_UNREPLICABLE} REAL; (*Angle for calculation of Finish points <= Angle new [rad]*)
		Angle_New : {REDUND_UNREPLICABLE} REAL; (*Angle for calculation of Start points >= Angle cur [rad]*)
		B2C_second : {REDUND_UNREPLICABLE} REAL; (*Second belt center to head rotation center distance*)
		B2C_first : {REDUND_UNREPLICABLE} REAL; (*First belt center to head rotation center distance*)
		Eccentricity_Y : {REDUND_UNREPLICABLE} REAL; (*Eccentricity in Y direction*)
		Eccentricity_X : {REDUND_UNREPLICABLE} REAL; (*Eccentricity in X direction*)
	END_VAR
	VAR_OUTPUT
		Out_StartX : {REDUND_UNREPLICABLE} REAL; (*Start position for next edge*)
		Out_StartY : {REDUND_UNREPLICABLE} REAL; (*Start position for next edge*)
		Out_FinishX : {REDUND_UNREPLICABLE} REAL; (*Finish position for last edge*)
		Out_FinishY : {REDUND_UNREPLICABLE} REAL; (*Finish position for last edge*)
	END_VAR
	VAR
		CosAngleNew : {REDUND_UNREPLICABLE} REAL; (*Cosinus value of angle*)
		SinAngleNew : {REDUND_UNREPLICABLE} REAL; (*Sinus value of angle*)
		CosAngleCur : {REDUND_UNREPLICABLE} REAL; (*Cosinus value of angle*)
		SinAngleCur : {REDUND_UNREPLICABLE} REAL; (*Sinus value of angle*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK ActivatePBC (*Activation of the PBC feature on the drive*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : {REDUND_UNREPLICABLE} BOOL; (*FB Enable*)
		StartComp : {REDUND_UNREPLICABLE} BOOL; (*Start the compensation*)
		StopComp : {REDUND_UNREPLICABLE} BOOL; (*Stop the compensation*)
		Axis : {REDUND_UNREPLICABLE} UDINT;
		Parameters : {REDUND_UNREPLICABLE} UDINT;
		Type : {REDUND_UNREPLICABLE} USINT;
	END_VAR
	VAR_OUTPUT
		Status : {REDUND_UNREPLICABLE} UINT;
		Error : {REDUND_UNREPLICABLE} BOOL;
		Active : {REDUND_UNREPLICABLE} BOOL; (*Component is activated*)
		CompActive : {REDUND_UNREPLICABLE} BOOL; (*Compensation is active*)
	END_VAR
	VAR
		pPar : REFERENCE TO ActivatePBC_Par_typ; (*Pointer to the parameters structure*)
		Internal : {REDUND_UNREPLICABLE} ActivatePBC_Internal_typ;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK InvAnalogCtrl (*FB to control inverters with analog output*)
	VAR_INPUT
		Enable : {REDUND_UNREPLICABLE} BOOL; (*Enable input for the FB*)
		DI_InverterAlarm : {REDUND_UNREPLICABLE} BOOL; (*Invertor alarm input*)
		JogSpeed : {REDUND_UNREPLICABLE} UINT; (*Jog Speed [%]*)
		JogPos : {REDUND_UNREPLICABLE} BOOL; (*Command to move the conveyor in postive direction*)
		JogNeg : {REDUND_UNREPLICABLE} BOOL; (*Command to move the conveyor in negative direction*)
		JogSlow : {REDUND_UNREPLICABLE} BOOL; (*Command to move with half speed*)
	END_VAR
	VAR_OUTPUT
		DO_ConvBrake : {REDUND_UNREPLICABLE} BOOL;
		DO_ConvMovePos : {REDUND_UNREPLICABLE} BOOL;
		DO_ConvMoveNeg : {REDUND_UNREPLICABLE} BOOL;
		AO_ConvSpeed : {REDUND_UNREPLICABLE} INT;
		Error : {REDUND_UNREPLICABLE} BOOL;
		ReadyCmd : {REDUND_UNREPLICABLE} BOOL; (*Flag that block is ready for commands*)
		ErrorID : {REDUND_UNREPLICABLE} INT;
	END_VAR
	VAR
		TON_InverterSleep : {REDUND_UNREPLICABLE} TON; (*Give the Siemens inverter some time before switching commands*)
		convStep : {REDUND_UNREPLICABLE} ConvCtrlStep_enm;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK DriveParInit (*Driver Parameters Initialization*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Execute : {REDUND_UNREPLICABLE} BOOL; (*Execute -> FB will start the init. process*)
		AxRef : {REDUND_UNREPLICABLE} ARRAY[0..49] OF UDINT; (*References to the axes parameters of which should be initialized*)
		AxParRef : {REDUND_UNREPLICABLE} ARRAY[0..49] OF UDINT; (*References to the respective axis parameters -> the indexes must correspond to the axis which has the same index in AxRef*)
		InitLimitsOnly : {REDUND_UNREPLICABLE} ARRAY[0..49] OF BOOL; (*If set, only the limits of the respected axis will be initialized*)
		InitEncodIfOnly : {REDUND_UNREPLICABLE} ARRAY[0..49] OF BOOL; (*If set, only the encoder interface will be initialized*)
		DisableSwLimits : {REDUND_UNREPLICABLE} ARRAY[0..49] OF BOOL; (*If set, the sw limits for the respective axis will be disabled*)
		SaveToInit : {REDUND_UNREPLICABLE} ARRAY[0..49] OF BOOL;
	END_VAR
	VAR_OUTPUT
		Status : {REDUND_UNREPLICABLE} UINT; (*Status of the FB*)
		Error : {REDUND_UNREPLICABLE} BOOL; (*Error occurred*)
	END_VAR
	VAR
		Internal : {REDUND_UNREPLICABLE} DriveParInit_Internal_typ; (*Internal control structure*)
		pAxPar : REFERENCE TO Ax_Init_typ; (*Pointer to drive parameter structure*)
		pAxObj : REFERENCE TO ACP10AXIS_typ; (*Pointer to the axis object*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_UNREPLICABLE} FUNCTION_BLOCK DriveParInitX (*Drive Par Initializations Using mapp Config*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		inExecute : {REDUND_UNREPLICABLE} BOOL; (*Execute of the FB *)
		inRefAxisBasic : {REDUND_UNREPLICABLE} ARRAY[0..49] OF UDINT; (*Reference to the MpAxisBasic compoennt*)
		inRefAxisConfig : {REDUND_UNREPLICABLE} ARRAY[0..49] OF UDINT; (*Reference to the MpAxisConfig component*)
		inRefNewConfig : {REDUND_UNREPLICABLE} ARRAY[0..49] OF UDINT; (*Reference to the new configuration that needs to be saved to the mapp component*)
		inInitLimitsOnly : {REDUND_UNREPLICABLE} ARRAY[0..49] OF BOOL;
		inDisableSwLimits : {REDUND_UNREPLICABLE} ARRAY[0..49] OF BOOL;
	END_VAR
	VAR_OUTPUT
		outStatus : {REDUND_UNREPLICABLE} UINT; (*FB status*)
		outError : {REDUND_UNREPLICABLE} BOOL; (*FB error bit*)
	END_VAR
	VAR
		_pAxObj : REFERENCE TO ACP10AXIS_typ; (*Pointer to the axis object*)
		_pAxCtrl : REFERENCE TO MpAxisBasic; (*Pointer to the MpAxisBasic component*)
		_pAxPar : REFERENCE TO MpAxisBasicParType; (*Pointer to the MpAxisBasicPar structure*)
		_pAxCfg : REFERENCE TO MpAxisBasicConfig;
		_pAxCfgTyp : REFERENCE TO MpAxisBasicConfigType; (*Pointer to the axis configuration *)
		_pAxCfgNew : REFERENCE TO MpAxisBasicConfigType; (*Pointer to the new axis configuration*)
		_Internal : {REDUND_UNREPLICABLE} DriveParInitX_Internal_typ; (*FB Internal structure*)
	END_VAR
END_FUNCTION_BLOCK
