(*********************************************************************************
 * COPYRIGHT --  Bernecker + Rainer 
********************************************************************************** 
* Author:   abdullahoglf
* Created:  October 16, 2014
**********************************************************************************
* Description:
*	- Pulse generation.
* History:
*	
**********************************************************************************)

(* Pulse generation *)
FUNCTION_BLOCK PulseGen
	// ----------------------------
	// Check FB execute 
	// ----------------------------
	IF inEnable THEN // Pulse generation is active
		// ----------------------------
		// Detect the start time of the FB
		// ----------------------------
		IF Internal.TimeStart = 0 THEN
			Internal.TimeStart 	:= clock_ms();
		END_IF;	
		// ----------------------------
		
		// ----------------------------
		// Get the actual time 
		// ----------------------------
		Internal.TimeAct		:= clock_ms();
		// ----------------------------
		
		// ----------------------------
		// Get the elapsed time
		// ----------------------------
		Internal.TimeSince		:= Internal.TimeAct - Internal.TimeStart;
		Internal.TimeSinceREAL	:= TIME_TO_REAL(Internal.TimeSince);
		// ----------------------------
		(*
		// ----------------------------
		// Set the output depending on the mode 
		// ----------------------------
		CASE inMode OF
			// ----------------------------
			plsGen_0_1_HZ:	// 0.1 HZ 
			// ----------------------------
				IF fmod(Internal.TimeSinceREAL,20000) < 10000 THEN
					outPulse	:= TRUE;
				ELSE
					outPulse	:= FALSE;
				END_IF;
			
			// ----------------------------
			plsGen_0_2_HZ:	// 0.2 HZ
			// ----------------------------
				IF fmod(Internal.TimeSinceREAL,10000) < 5000 THEN
					outPulse	:= TRUE;
				ELSE
					outPulse	:= FALSE;
				END_IF;
			
			// ----------------------------
			plsGen_0_5_HZ:	// 0.5 HZ 
			// ----------------------------
				IF fmod(Internal.TimeSinceREAL,4000) < 2000 THEN
					outPulse	:= TRUE;
				ELSE
					outPulse	:= FALSE;
				END_IF;
			
			// ----------------------------
			plsGen_1_HZ:	// 1 HZ 
			// ----------------------------
				IF fmod(Internal.TimeSinceREAL,2000) < 1000 THEN 
					outPulse	:= TRUE;
				ELSE
					outPulse	:= FALSE;
				END_IF;
			
			// ----------------------------
			plsGen_2_HZ:	// 2 HZ 
			// ----------------------------
				IF fmod(Internal.TimeSinceREAL,1000) < 500 THEN
					outPulse	:= TRUE;
				ELSE
					outPulse	:= FALSE;
				END_IF;
			
			// ----------------------------
			plsGen_5_HZ:	// 5 HZ 
			// ----------------------------
				IF fmod(Internal.TimeSinceREAL,400) < 200 THEN
					outPulse	:= TRUE;
				ELSE
					outPulse	:= FALSE;
				END_IF;	
			
			// ----------------------------
			plsGen_10_HZ:	// 10 HZ 
			// ----------------------------
				IF fmod(Internal.TimeSinceREAL,200) < 100 THEN
					outPulse	:= TRUE;
				ELSE
					outPulse	:= FALSE;
				END_IF;
			
			// ----------------------------
			ELSE 	// undefined mode 
			// ----------------------------
				outPulse		:= FALSE;					
		END_CASE;	// end of the mode check
*)	
	ELSE // Pulse generation is deactivated
		Internal.TimeAct		:= 0;
		Internal.TimeStart		:= 0;
		Internal.TimeSince		:= 0;
		Internal.TimeSinceREAL	:= 0;
		outPulse				:= FALSE;
	END_IF;	// end of the execute check
END_FUNCTION_BLOCK