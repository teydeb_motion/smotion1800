(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: ComonFun
 * File: ComonFun.typ
 * Author: turanskyl
 * Created: June 23, 2014
 ********************************************************************
 * Data types of library ComonFun
 ********************************************************************)
(*           Programs Inspection*)

TYPE
	stProgInspect_typ : 	STRUCT 
		LocHist : ARRAY[0..14]OF stProgInspectStepHist_typ; (*History of small steps*)
		MainHist : ARRAY[0..14]OF stProgInspectStepHist_typ; (*History of main steps*)
		ErrActive : USINT; (*Signalization that error is active*)
		ProgName : STRING[80]; (*Name of program block to be displayed in HMI*)
	END_STRUCT;
	stProgInspectStepHist_typ : 	STRUCT 
		Time : TIME; (*Timestamp (continous ms) of step change*)
		Step : UINT; (*Step value*)
	END_STRUCT;
	CreateTableStep_Enum : 
		(
		DISABLED := 0,
		DETERMINE_ID := 1,
		DETERMINE_INFO := 2,
		WRITE_TO_DATAOBJ := 3,
		DELETE_DATAOBJ := 4,
		CREATE_DATAOB := 5,
		MOVE_DATAOBJ_TO_USERROM := 6,
		DONE := 7,
		ERROR_STATE := 99
		);
END_TYPE

(**)

TYPE
	Ax_Init_typ : 	STRUCT  (*Input parameters of respective block*)
		dig_in : ACP10DIGIN_typ; (* Digital Inputs *)
		encoder_if : ACP10ENCIF_typ; (* Encoder Interface *)
		limit : ACP10AXLIM_typ; (* Limit value *)
		controller : ACP10CTRL_typ; (* Controller *)
	END_STRUCT;
END_TYPE

(**)

TYPE
	ForceTrig_Internal : 	STRUCT 
		ForceValue : USINT;
		Trigger : USINT;
		NcActionStat : UINT;
		TON_Timeout : TON;
		StartEdge : BOOL;
	END_STRUCT;
	ConvCtrlStep_enm : 
		(
		CONV_WAIT := 0,
		CONV_JOG_NEG := 5,
		CONV_WAIT_AFTER_JOG := 10,
		CONV_JOG_POS := 15,
		CONV_ERROR := 255
		);
END_TYPE

(**)
(*=====================================================================*)
(*DriveParInit structures*)
(*=====================================================================*)

TYPE
	DriveParInit_Steps_typ : 
		(
		PAR_INIT_DISABLED := 0,
		PAR_INIT_CHECK_PAR := 5,
		PAR_INIT_SET_PAR := 10,
		PAR_INIT_INIT := 15,
		PAR_INIT_W_INIT := 20,
		PAR_INIT_DISABLE_SW_LIMITS := 25,
		PAR_INIT_SAVE_TO_INIT := 27,
		PAR_INIT_CHECK_CONTROLLER := 30,
		PAR_INIT_DISABLE_CONTROLLER := 35,
		PAR_INIT_W_DISABLE_CONTROLLER := 40,
		PAR_INIT_ERROR := 255
		);
	DriveParInit_Internal_typ : 	STRUCT 
		idxAsync : USINT; (*Async. iteration variable*)
		bOldExec : BOOL;
		MC_BR_WriteParIDText_0 : MC_BR_WriteParIDText;
		MC_BR_InitAxisSubjectPar_0 : MC_BR_InitAxisSubjectPar;
		MC_BR_SaveAxisPar_0 : {REDUND_UNREPLICABLE} MC_BR_SaveAxisPar;
		Step : DriveParInit_Steps_typ; (*Step of the FB*)
		oldStep : DriveParInit_Steps_typ; (*Step of the FB from the previous cycle*)
		Stat : UINT;
		bFirstInStep : BOOL; (*Flag: First time in a step*)
		ii : USINT; (*Iteration variable*)
		bEvrOk : BOOL;
	END_STRUCT;
END_TYPE

(**)
(*=====================================================================*)
(*ActivatePBC*)
(*=====================================================================*)

TYPE
	ActivatePBC_Steps_enm : 
		(
		ACT_PBC_DISABLED := 0,
		ACT_PBC_WAIT := 5,
		ACT_PBC_SET_MAX_RATE := 10,
		ACT_PBC_ACTIVATE := 15,
		ACT_PBC_ACTIVATE_TYP_1 := 17,
		ACT_PBC_DEACTIVATE := 20,
		ACT_PBC_DEACTIVATE_TYP_1 := 22,
		ACT_PBC_SWITCH := 50,
		ACT_PBC_DONE := 100,
		ACT_PBC_ERROR := 255
		);
	ActivatePBC_Par_typ : 	STRUCT 
		Parameters : MC_MPDC_PARAM_REF;
		AdvParameters : MC_ADV_MPDC_REF;
		Mode : USINT; (*Only for Type1*)
		Backlash : REAL; (*Only for Type1*)
		MaxVelocity : REAL; (*Maximum velocity for the compensation movement [units/s]*)
	END_STRUCT;
	ActivatePBC_Internal_typ : 	STRUCT 
		MC_BR_WriteParIDText_0 : MC_BR_WriteParIDText;
		MC_BR_MechPos_0 : MC_BR_MechPosDeviationComp;
		MC_BR_CyclicRead_0 : MC_BR_CyclicRead;
		bFirstInStep : BOOL;
		OldStep : ActivatePBC_Steps_enm;
		sTemp : STRING[11];
		iMode : UINT;
		Step : ActivatePBC_Steps_enm;
		ii : USINT;
	END_STRUCT;
END_TYPE

(**)
(*=====================================================================*)
(*PulseGen*)
(*=====================================================================*)

TYPE
	PulseGen_Modes_enm : 
		(
		plsGen_0_1_HZ,
		plsGen_0_2_HZ,
		plsGen_0_5_HZ,
		plsGen_1_HZ,
		plsGen_2_HZ,
		plsGen_5_HZ,
		plsGen_10_HZ
		);
	PulseGen_Internal_typ : 	STRUCT 
		TimeAct : TIME; (*Actual time *)
		TimeSinceREAL : REAL;
		TimeSince : TIME; (*Time difference*)
		TimeStart : TIME; (*Start time*)
	END_STRUCT;
END_TYPE

(**)

TYPE
	stDancerOutputType : 	STRUCT 
		HideArrow : {REDUND_UNREPLICABLE} USINT;
	END_STRUCT;
END_TYPE

(*=====================================================================*)
(*DriveParInitX structures*)
(*=====================================================================*)

TYPE
	DriveParInitX_Steps_enm : 
		(
		PAR_INIT_X_DISABLED,
		PAR_INIT_X_CHECK_PAR := 5,
		PAR_INIT_X_SET_PAR := 10,
		PAR_INIT_X_ENABLE_MPAXISCFG := 12,
		PAR_INIT_X_INIT := 15,
		PAR_INIT_X_W_INIT := 20,
		PAR_INIT_X_DISABLE_SW_LIMITS := 25,
		PAR_INIT_X_CHECK_CONTROLLER := 30,
		PAR_INIT_X_DISABLE_CONTROLLER := 35,
		PAR_INIT_X_W_DISABLE_CONTROLLER := 40,
		PAR_INIT_X_ERROR := 255
		);
	DriveParInitX_Internal_typ : 	STRUCT 
		idxAsync : USINT; (*Async. iteration variable*)
		bOldExec : BOOL;
		Step : DriveParInitX_Steps_enm; (*Step of the FB*)
		oldStep : DriveParInitX_Steps_enm; (*Step of the FB from the previous cycle*)
		Stat : UINT;
		bFirstInStep : BOOL; (*Flag: First time in a step*)
		ii : USINT; (*Iteration variable*)
		bEvrOk : BOOL;
		bResetMpAxisCfg : BOOL; (*If the MpAxisConfig component was initially disabled, the DriveParInitX FB will enable it due to its operation and then disable it afterwards once it is done. If an error occurs during the operation MpAxisConfig must be disabled externally*)
		MC_BR_WriteParIDText_0 : MC_BR_WriteParIDText;
		TON_Timeout : TON;
	END_STRUCT;
END_TYPE
