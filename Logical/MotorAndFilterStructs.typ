
TYPE
	MotorStruct : 	STRUCT  (*Motor Struct*)
		Kn : LREAL; (*Motor torque - current coefficent (Nm/A).*)
		Mn : LREAL; (*Motor inertia moment (kgm�).*)
		Vm : LREAL; (*Maximum motor speed (rad/s).*)
		Pos : ARRAY[0..1]OF LREAL; (*Motor position (rad)*)
		Vel : ARRAY[0..1]OF LREAL; (*Motor velocity (rad/s)*)
		VelVOB : ARRAY[0..1]OF LREAL;
		Acc : ARRAY[0..1]OF LREAL; (*Motor acceleration (rad/s�)*)
		IDes : ARRAY[0..1]OF LREAL; (*Desired motor current (A).*)
		IDis : ARRAY[0..1]OF LREAL; (*Disturbance motor current (A)*)
		IRef : ARRAY[0..1]OF LREAL; (*Reference motor current (A).*)
		IAct : ARRAY[0..1]OF LREAL; (*Actual motor current (A).*)
		RefPos : ARRAY[0..1]OF LREAL; (*Reference motor position (rad).*)
		RefVel : ARRAY[0..1]OF LREAL; (*Reference motor velocity (rad/s).*)
		RefAcc : ARRAY[0..1]OF LREAL; (*Reference motor acceleration (rad/s�).*)
		DummyAccDes_AccLim : LREAL; (*Desired motor acceleration (rad/s�).*)
		DummyAccDes_VelLim : LREAL; (*Desired motor acceleration (rad/s�).*)
		DummyAccDes : LREAL; (*Desired motor acceleration (rad/s�).*)
		AccDes : LREAL; (*Desired motor acceleration (rad/s�).*)
		Power : LREAL; (*Consumed Power of Motor (W)*)
		MotorOffsetForTraining : LREAL; (*Motor offset position for training (rad)*)
		NeutralPos : LREAL := 0.0001;
		EndPos : LREAL;
		DummyVel2 : LREAL;
		DummyVel : LREAL;
		SGNVal : LREAL;
	END_STRUCT;
	LegStruct : 	STRUCT  (*Leg Struct*)
		RefPos : ARRAY[0..2]OF LREAL; (*Motor leg reference position (m).*)
		RefVel : ARRAY[0..2]OF LREAL; (*Motor leg reference velocity (m/s).*)
		RefAcc : ARRAY[0..2]OF LREAL; (*Motor leg reference acceleration (m/s�).*)
		Pos : ARRAY[0..2]OF LREAL; (*Motor leg position (m).*)
		Vel : ARRAY[0..2]OF LREAL; (*Motor leg velocity (m/s).*)
		Acc : ARRAY[0..2]OF LREAL; (*Motor leg acceleration (m/s�).*)
	END_STRUCT;
	FilterStruct : 	STRUCT  (*Filter Struct*)
		G : LREAL; (*Filter Gain*)
		U0 : LREAL; (*Recent input struct value.*)
		U1 : LREAL; (*Past input struct value.*)
		Y0 : LREAL; (*Recent output struct value.*)
		Y1 : LREAL; (*Past output struct value.*)
	END_STRUCT;
	IntegratorStruct : 	STRUCT  (*Integrator Struct*)
		U0 : LREAL; (*Recent input struct value.*)
		U1 : LREAL; (*Past input struct value.*)
		Y0 : LREAL; (*Recent output struct value.*)
		Y1 : LREAL; (*Past output struct value.*)
	END_STRUCT;
	InterpStruct : 	STRUCT 
		Coeffs : ARRAY[0..4]OF LREAL; (*// Current motor velocity array (while calling write: Vel[i] for the ith previous step)*)
		DataInput : ARRAY[0..3]OF LREAL; (*// Current motor position array (while calling write: Pos[i] for the ith previous step)*)
	END_STRUCT;
	RobotStruct : 	STRUCT 
		Rb : LREAL; (*// Radius of Base Platform*)
		Re : LREAL; (*// Radius of End Effector*)
		B1 : LREAL; (*// Angle of 1st joint of base platform related to world 0*)
		B2 : LREAL; (*// Angle of 2nd joint of base platform related to world 0*)
		B3 : LREAL; (*// Angle of 3rd joint of base platform related to world 0*)
		B4 : LREAL; (*// Angle of 4th joint of base platform related to world 0 *)
		B5 : LREAL; (*// Angle of 5th joint of base platform related to world 0*)
		B6 : LREAL; (*// Angle of 6th joint of base platform related to world 0*)
		T1 : LREAL; (*// Angle of 1st joint of end effector related to world 0*)
		T2 : LREAL; (*// Angle of 2nd joint of end effector related to world 0*)
		T3 : LREAL; (*// Angle of 3rd joint of end effector related to world 0*)
		T4 : LREAL; (*// Angle of 4th joint of end effector related to world 0*)
		T5 : LREAL; (*// Angle of 5th joint of end effector related to world 0*)
		T6 : LREAL; (*// Angle of 6th joint of end effector related to world 0*)
		EndX : ARRAY[0..1]OF LREAL; (*// Linear longitudinal (front/back) motion of end effector (Surge)*)
		EndY : ARRAY[0..1]OF LREAL; (*// Linear lateral (side-to-side) motion of end effector (Sway)*)
		EndZ : ARRAY[0..1]OF LREAL; (*// Linear vertical (up/down) motion of end effector (Heave)*)
		EndR : ARRAY[0..1]OF LREAL; (*// Roll angle orientation of end effector (Roll)*)
		EndP : ARRAY[0..1]OF LREAL; (*// Pitch angle orientation of end effector (Pitch)*)
		EndQ : ARRAY[0..1]OF LREAL; (*// Yaw angle orientation of end effector (Yaw)*)
		Link1 : ARRAY[0..1]OF LREAL; (*// Actuator length of 1st actuator (Output of Inverse Kinematics)*)
		Link2 : ARRAY[0..1]OF LREAL; (*// Actuator length of 2nd actuator (Output of Inverse Kinematics)*)
		Link3 : ARRAY[0..1]OF LREAL; (*// Actuator length of 3rd actuator (Output of Inverse Kinematics)*)
		Link4 : ARRAY[0..1]OF LREAL; (*// Actuator length of 4th actuator (Output of Inverse Kinematics)*)
		Link5 : ARRAY[0..1]OF LREAL; (*// Actuator length of 5th actuator (Output of Inverse Kinematics)*)
		Link6 : ARRAY[0..1]OF LREAL; (*// Actuator length of 6th actuator (Output of Inverse Kinematics)*)
		Mass : LREAL;
		MaxMass : LREAL;
		MassCalculated : BOOL;
		MotionType : Motion_Type := SMOTION1800;
	END_STRUCT;
END_TYPE
