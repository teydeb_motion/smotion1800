(*GUI -> BR'a Gelen Verilerin Structlarý*)

TYPE
	H2M_Signal : 	STRUCT  (*Signal File Struct variables from UI.*)
		mainForm : H2M_Main; (*Common Form Struct.*)
		mot1Pos : ARRAY[0..399]OF LREAL;
		mot2Pos : ARRAY[0..399]OF LREAL;
		mot3Pos : ARRAY[0..399]OF LREAL;
		mot4Pos : ARRAY[0..399]OF LREAL;
		mot5Pos : ARRAY[0..399]OF LREAL;
		mot6Pos : ARRAY[0..399]OF LREAL;
		mot1Vel : ARRAY[0..399]OF LREAL;
		mot2Vel : ARRAY[0..399]OF LREAL;
		mot3Vel : ARRAY[0..399]OF LREAL;
		mot4Vel : ARRAY[0..399]OF LREAL;
		mot5Vel : ARRAY[0..399]OF LREAL;
		mot6Vel : ARRAY[0..399]OF LREAL;
		mot1Acc : ARRAY[0..399]OF LREAL;
		mot2Acc : ARRAY[0..399]OF LREAL;
		mot3Acc : ARRAY[0..399]OF LREAL;
		mot4Acc : ARRAY[0..399]OF LREAL;
		mot5Acc : ARRAY[0..399]OF LREAL;
		mot6Acc : ARRAY[0..399]OF LREAL;
		totalDataSize : DINT;
		packetSize : INT;
		packetNo : INT;
		packetInfo : BYTE;
	END_STRUCT;
	H2M_Manual : 	STRUCT  (*Manual Axis Control Struct variables from UI.*)
		mainForm : H2M_Main; (*Common Form Struct.*)
		axisTrans : TranslationVel; (*Motion Platform leg positions (rad).*)
		axisAngular : AngularVel;
	END_STRUCT;
	H2M_Simulator : 	STRUCT  (*Simulator Struct variables from UI.*)
		mainForm : H2M_Main; (*Common Form Struct.*)
		axisTrans : TranslationAcc;
		axisAngular : AngularVel; (*Vehicle X Angular Velocity (rad/s).*)
		axisEuler : AngularEuler; (*Vehicle X Euler Angle (rad).*)
		SurgeAccGain : REAL; (*Acceleration Gain along X Axis*)
		SwayAccGain : REAL; (*Acceleration Gain along Y Axis*)
		HeaveAccGain : REAL; (*Acceleration Gain along Z Axis*)
		RollVelGain : REAL; (*Euler Angle Gain aroud X Axis (Roll)*)
		PitchVelGain : REAL; (*Euler Angle Gain aroud Y Axis (Pitch)*)
		YawVelGain : REAL; (*Euler Angle Gain aroud Z Axis (Yaw)*)
		RollEulGain : REAL; (*Vehicle X Euler Angle Gain.*)
		PitchEulGain : REAL; (*Vehicle Y Euler Angle Gain.*)
		YawEulGain : REAL; (*Vehicle X Euler Angle Gain.*)
		RollTiltGain : REAL; (*Tilt Coordination Gain around X Axis (Roll)*)
		PitchTiltGain : REAL; (*Tilt Coordination Gain around Y Axis (Pitch)*)
		Surge1stHPGain : REAL; (*Vehicle X Acceleration 1st HP Filter Gain.*)
		Sway1stHPGain : REAL; (*Vehicle Y Acceleration 1st HP Filter Gain.*)
		Heave1stHPGain : REAL; (*Vehicle Z Acceleration 1st HP Filter Gain.*)
		Surge2ndHPGain : REAL; (*Vehicle X Acceleration 2nd HP Filter Gain.*)
		Sway2ndHPGain : REAL; (*Vehicle Y Acceleration 2nd HP Filter Gain.*)
		Heave2ndHPGain : REAL; (*Vehicle Z Acceleration 2nd HP Filter Gain.*)
		Roll2ndHPGain : REAL; (*Vehicle X Angular Velocity 2nd HP Filter Gain.*)
		Pitch2ndHPGain : REAL; (*Vehicle Y Angular Velocity 2nd HP Filter Gain.*)
		Yaw2ndHPGain : REAL; (*Vehicle Z Angular Velocity 2nd HP Filter Gain.*)
		RollTiltLPGain : REAL; (*Tilt Coordination 1st LP Filter Gain around X Axis (Roll)*)
		PitchTiltLPGain : REAL; (*Tilt Coordination 1st LP Filter Gain around Y Axis (Pitch)*)
	END_STRUCT;
	H2M_Main : 	STRUCT  (*Main Form Struct variables from UI.*)
		securityID : BYTE;
		formID : BYTE;
		command : BYTE;
		parameterCount : BYTE;
	END_STRUCT;
	H2M_Limit : 	STRUCT 
		X_PosVel : LREAL;
		X_NegVel : LREAL;
		Y_PosVel : LREAL;
		Y_NegVel : LREAL;
		Z_PosVel : LREAL;
		Z_NegVel : LREAL;
		R_PosVel : LREAL;
		R_NegVel : LREAL;
		P_PosVel : LREAL;
		P_NegVel : LREAL;
		Q_PosVel : LREAL;
		Q_NegVel : LREAL;
	END_STRUCT;
END_TYPE

(**)
(*BR -> GUI'e Giden Verilerin Structlarý*)

TYPE
	Motion2Host : 	STRUCT  (*BR to PC Variables Struct*)
		Mot1 : ARRAY[0..5]OF LREAL; (*Motor 1 Reference-Response Position, Velocity and Acceleration values.*)
		Leg1 : ARRAY[0..5]OF LREAL; (*Leg 1 Reference-Response Position, Velocity and Acceleration values. *)
		Mot2 : ARRAY[0..5]OF LREAL; (*Motor 2 Reference-Response Position, Velocity and Acceleration values.*)
		Leg2 : ARRAY[0..5]OF LREAL; (*Leg 2 Reference-Response Position, Velocity and Acceleration values.*)
		Mot3 : ARRAY[0..5]OF LREAL; (*Motor 3 Reference-Response Position, Velocity and Acceleration values.*)
		Leg3 : ARRAY[0..5]OF LREAL; (*Leg 3 Reference-Response Position, Velocity and Acceleration values.*)
		Mot4 : ARRAY[0..5]OF LREAL; (*Motor 4 Reference-Response Position, Velocity and Acceleration values.*)
		Leg4 : ARRAY[0..5]OF LREAL; (*Leg 4 Reference-Response Position, Velocity and Acceleration values.*)
		Mot5 : ARRAY[0..5]OF LREAL; (*Motor 5 Reference-Response Position, Velocity and Acceleration values.*)
		Leg5 : ARRAY[0..5]OF LREAL; (*Leg 5 Reference-Response Position, Velocity and Acceleration values.*)
		Mot6 : ARRAY[0..5]OF LREAL; (*Motor 6 Reference-Response Position, Velocity and Acceleration values.*)
		Leg6 : ARRAY[0..5]OF LREAL; (*Leg 6 Reference-Response Position, Velocity and Acceleration values.*)
		MassInfo : ARRAY[0..2]OF LREAL; (*Leg 6 Reference-Response Position, Velocity and Acceleration values.*)
		Time_Running : LREAL; (*Real-Time Time*)
		Time_SystemTraining : LREAL; (*Training Time information.*)
		XPos : LREAL;
		YPos : LREAL;
		ZPos : LREAL;
		RollPos : LREAL;
		PitchPos : LREAL;
		YawPos : LREAL;
		XVel : LREAL;
		YVel : LREAL;
		ZVel : LREAL;
		RollVel : LREAL;
		PitchVel : LREAL;
		YawVel : LREAL;
		XAcc : LREAL;
		YAcc : LREAL;
		ZAcc : LREAL;
		RollAcc : LREAL;
		PitchAcc : LREAL;
		YawAcc : LREAL;
		IMUData : ARRAY[0..8]OF REAL; (*IMU Datas (Accelerations, Velocities and Euler Angles)*)
		Mot_Current : ARRAY[0..5]OF REAL; (*Motor Currents (A)*)
		Mot_Torque : ARRAY[0..5]OF REAL; (*Motor Torques (Nm)*)
		Mot_Temperature : ARRAY[0..5]OF REAL; (*Motor Temperatures (kW)*)
		Mot_Power : ARRAY[0..5]OF REAL; (*Motor Powers (kW)*)
		Power : ARRAY[0..3]OF REAL; (*Avarage Active Power of the System (kW), Consumed and Regenerated Energy (Ws)*)
		Time_System : ARRAY[0..2]OF INT; (*System Total Time*)
		Time_Working : ARRAY[0..2]OF INT; (*System Working Time*)
		BRPacketNo : INT; (*B&R Requested data packet.*)
		State_Motion : BYTE; (*Motion State*)
		SecurityID1 : BYTE := 201; (*GUI Security ID 1*)
		SecurityID2 : BYTE := 30; (*GUI Security ID 2*)
		State_Error : ARRAY[0..14]OF BOOL; (*Error States.*)
		Is_Park : BOOL; (*Is Motion in Park Mode?*)
		EMGStatus : BOOL; (*Emergency Button Status*)
		ParkingProcess : BOOL; (*Process information for parking*)
		Byte_LimitSensor : BYTE; (*Limit Sensor Information (Byte).*)
		Byte_DriverState : BYTE; (*Driver State Information (Byte).*)
		Byte_DriverPower : BYTE; (*Driver Power Information (Byte).*)
		Byte_MotorState : BYTE; (*Motor State Information (Byte).*)
		Byte_LimitSensorCrash : BYTE; (*Limit Sensor Crash Information (Byte).*)
		Byte_EncoderState : BYTE; (*Encoder State Information (Byte).*)
		Byte_HighTemperature : BYTE; (*High Temperature Information (Byte).*)
		Byte_HighTorque : BYTE; (*High Torque Information (Byte).*)
		Byte_MotCable : BYTE; (*High Torque Information (Byte).*)
	END_STRUCT;
	Client_typ : 	STRUCT  (*UDP Client Variables*)
		sStep : UINT; (*UDP Client Step Variable*)
		server_address : STRING[20]; (*Address of the Server*)
		server_portnumber : UINT; (*Portnumber of the Server*)
		no_data_received_count : UINT; (*Counts up if no data are received*)
		UdpOpen_0 : UdpOpen; (*AsUDP.UdpOpen FUB*)
		UdpConnect_0 : UdpConnect; (*AsUDP.UdpConnect FUB*)
		UdpSend_0 : UdpSend; (*AsUDP.UdpSend FUB*)
		UdpRecv_0 : UdpRecv; (*AsUDP.UdpRecv FUB*)
		UdpDisconnect_0 : UdpDisconnect; (*AsUDP.UdpDisconnect FUB*)
		UdpClose_0 : UdpClose; (*AsUDP.UdpClose FUB*)
	END_STRUCT;
END_TYPE

(**)
(*Insert your comment here.*)

TYPE
	TranslationVel : 	STRUCT 
		Surge : REAL;
		Sway : REAL;
		Heave : REAL;
	END_STRUCT;
	TranslationAcc : 	STRUCT 
		Surge : REAL;
		Sway : REAL;
		Heave : REAL;
	END_STRUCT;
	AngularEuler : 	STRUCT 
		Roll : REAL;
		Pitch : REAL;
		Yaw : REAL;
	END_STRUCT;
	AngularVel : 	STRUCT 
		Roll : REAL;
		Pitch : REAL;
		Yaw : REAL;
	END_STRUCT;
	AngularAcc : 	STRUCT 
		Roll : REAL;
		Pitch : REAL;
		Yaw : REAL;
	END_STRUCT;
END_TYPE
