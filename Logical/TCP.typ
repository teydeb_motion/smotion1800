(********************************************************************
 * Copyright (c) 2009, Sanlab Simulation, All Rights Reserved 
 ********************************************************************
 * File: TCP.typ
 * Author: Cem Polat
 * Created: August 11, 2017
 * NO_SSL
 ********************************************************************
 * TCP types of project SMotion3000_SakaryaUniversity
 ********************************************************************)
(*SERVER SIDE - PC to B&R (H2M)*)

TYPE
	Server_typ : 	STRUCT  (*TCP Server Variables*)
		step : STEP_SERVER; (*TCP Server Step Variable*)
		oldStep : STEP_SERVER; (*TCP Server Step Variable*)
		client_address : STRING[20]; (*Address of the client connection to the Server*)
		TcpOpen_0 : TcpOpen; (*AsTCP.TcpOpen FUB*)
		TcpServer_0 : TcpServer; (*AsTCP.TcpServer FUB*)
		TcpRecv_0 : TcpRecv; (*AsTCP.TcpRecv FUB*)
		TcpSend_0 : TcpSend; (*AsTCP.TcpSend FUB*)
		TcpClose_0 : TcpClose; (*AsTCP.TcpClose FUB*)
		TcpIoctl_0 : TcpIoctl; (*AsTCP.TcpIoctl*)
		linger_opt : tcpLINGER_typ; (*AsTCP.tcpLINGER_typ*)
		recv_timeout : UDINT; (*receive timeout*)
		recv_timeout_for_hard_close : UDINT; (*receive timeout for hard close*)
	END_STRUCT;
	STEP_SERVER : 
		(
		STEP_OPEN_SOCKET_s := 0,
		STEP_SET_IO := 5,
		STEP_WAIT_FOR_CLIENT := 10,
		STEP_RECEIVE_DATA := 20,
		STEP_CLOSE_SERVER := 40,
		STEP_CLOSE_SOCKET_s := 50,
		STEP_ERROR_s := 100
	);
END_TYPE

(*CLIENT SIDE - B&R to PC (M2H)*)

TYPE
	ClientType : 	STRUCT  (*TCP Client Variables*)
		Step : STEP; (*TCP Client Step Variable*)
		Configuration : ConfigType;
		Internal : InternalType;
		ServerIp : STRING[31];
		ServerPort : UINT;
		TcpOptions : UDINT;
		FB : FubsType;
		send_timeout : UDINT; (*send timeout*)
	END_STRUCT;
	STEP : 
		(
		STEP_START,
		STEP_OPEN_SOCKET,
		STEP_CONNECT_TO_SERVER,
		STEP_SEND_DATA,
		STEP_CLOSE_SOCKET,
		STEP_ERROR,
		STEP_WAIT
	);
	FubsType : 	STRUCT  (*Function Blocks's Struct*)
		TcpOpen_0 : TcpOpen;
		TcpClient_0 : TcpClient;
		TcpSend_0 : TcpSend;
		TcpClose_0 : TcpClose;
	END_STRUCT;
	ConfigType : 	STRUCT 
		UpdateTimeMs : UDINT;
	END_STRUCT;
	InternalType : 	STRUCT 
		LastUpdateTimestamp : DATE_AND_TIME;
		LastUpdateTime : TIME;
		ErrorFlag : BOOL;
		LastError : UINT;
		DTGetTime_0 : DTGetTime;
		Ident : UDINT;
	END_STRUCT;
	UpdateTimeType : 	STRUCT 
		UpdateTimeEveryMs : UDINT;
		LastUpdateTimeMs : UDINT;
		UpdateValue : REAL;
	END_STRUCT;
END_TYPE
