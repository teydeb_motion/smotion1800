/* Libraries */
#include <bur/plctypes.h>
#include <AsDefault.h>

/********************************Data Explanation****************************/
/********************************/		/************************************/
/*  Sys_Time_All[0] : Second	*/		/*  Sys_WorkingTime[0] : Second		*/
/*  Sys_Time_All[1] : Minute	*/		/*  Sys_WorkingTime[1] : Minute     */
/*  Sys_Time_All[2] : Hour		*/		/*  Sys_WorkingTime[2] : Hour	    */
/********************************/		/************************************/

/***********************************Program**********************************/	

void _CYCLIC ProgramCyclic(void)
{	
	// System Total Time Calculation
	Time_SystemAll[0]++;				// Increases 1 in every second.
	if (Time_SystemAll[0] > 59)		// If 1 minute has passed.
	{
		Time_SystemAll[0] = 0;		// Seconds value is set to 0.
		Time_SystemAll[1]++;			// Increases 1 in every minute.
		if (Time_SystemAll[1] > 59) { Time_SystemAll[1] = 0; Time_SystemAll[2]++; }			// Minute value is set to 0 and Increases 1 in every hour. 
	}

	// System Time Calculation after last activity
	Time_SystemWorking[0]++;			// Increases 1 in every second.
	if (Time_SystemWorking[0] > 59)	// If 1 minute has passed.
	{
		Time_SystemWorking[0] = 0;		// Seconds value is set to 0.
		Time_SystemWorking[1]++;		// Increases 1 in every minute.
		if (Time_SystemWorking[1] > 59) { Time_SystemWorking[1] = 0; Time_SystemWorking[2]++; }	// Minute value is set to 0 and Increases 1 in every hour.
	}
}
