
#include <bur/plctypes.h>  
#include <math.h>  
#include <AsDefault.h>
#include <sys_lib.h> 
#include "../SanlabLibrary/ForwardKinFunc.c"

void _CYCLIC ProgramCyclic(void)
{
	if (SystemInitialized && !FlagFwd)
		FlagFwd = 1;
	
	if (FlagFwd)
		ForwardKinematicCalculation(&FwdKin, &SMotion);
	
	CalcAxisVel();
	CalcAxisAcc();
	
	UpdateKinematic(&FwdKin);
}

