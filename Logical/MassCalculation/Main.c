
#include <bur/plctypes.h>
#include <math.h>  

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

double SAT(double Input, double SaturationLevel)
{
	if (Input > SaturationLevel)
		Input = SaturationLevel;
	else if (Input < (-1 * SaturationLevel))
		Input = -1 * SaturationLevel;
	
	return Input;
}

void _CYCLIC ProgramCyclic(void)
{
	if (!SMotion.MassCalculated && Re_Initialize)
	{
		if ((Sys_State == SYSTEM_INITIALIZING) || (Sys_State == SYSTEM_GO_TO_ZERO)) 
		{
			if ((Time_Total >= Time_Initialize + 0.5) && (Time_Total <= Time_Initialize + 8.5))
			{
				TorqueTotal += fabs(gMpAxisBasic_0[1].Info.CyclicRead.Torque.Value) + 
					fabs(gMpAxisBasic_0[2].Info.CyclicRead.Torque.Value) + 
					fabs(gMpAxisBasic_0[3].Info.CyclicRead.Torque.Value) +
					fabs(gMpAxisBasic_0[4].Info.CyclicRead.Torque.Value) + 
					fabs(gMpAxisBasic_0[5].Info.CyclicRead.Torque.Value) +
					fabs(gMpAxisBasic_0[6].Info.CyclicRead.Torque.Value);					
			}
	
			else if ((Time_Total >= Time_Initialize + 11.0) && (Time_Total <= Time_Initialize + 18.1))
			{
				TorqueTotal += fabs(gMpAxisBasic_0[1].Info.CyclicRead.Torque.Value) + 
					fabs(gMpAxisBasic_0[2].Info.CyclicRead.Torque.Value) +
					fabs(gMpAxisBasic_0[3].Info.CyclicRead.Torque.Value) +
					fabs(gMpAxisBasic_0[4].Info.CyclicRead.Torque.Value) + 
					fabs(gMpAxisBasic_0[5].Info.CyclicRead.Torque.Value) + 
					fabs(gMpAxisBasic_0[6].Info.CyclicRead.Torque.Value);					
			}
		}
		if (SystemInitialized && !SMotion.MassCalculated)
		{
			SMotion.Mass = (TorqueTotal - MassOffset) * MassCoef;
			SMotion.MassCalculated = 1;
		}
	}
	
	if (PNEU.Exist)
	{	
		PNEU.Res = (PNEU.Max * (PNEU.Out - PNEU.Offset)) / (PNEU.AIMax  - PNEU.Offset);

		if (Sys_State == SYSTEM_SETTING_PRESSURE)
		{			
			if (!PNEU.PFlag)
			{
				
				PNEU.Set = SAT(SMotion.Mass * (PNEU.Max / SMotion.MaxMass), PNEU.Max);
				PNEU.In = (INT)((PNEU.Set * (PNEU.AIMax - PNEU.Offset) / PNEU.Max) + PNEU.Offset);
				PNEU.PFlag = 1;
			}
		
			if(((PNEU.In - PNEU.Tolerance) < PNEU.Out) && (Time_Pressure + 0.5 < Time_Total))
			{
				PNEU.OK = 1;
				Sys_State = SYSTEM_READY_FOR_DATA;
			}
			else
				Sys_State = SYSTEM_SETTING_PRESSURE;
		}
	}
}
