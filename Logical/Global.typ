(*GUI Commands*)

TYPE
	PlatformGUIForm : 
		(
		ParkMode := 2, (*Maintenance Form ID.*)
		PlayFromFile := 8, (*Play From File Form ID.*)
		SignalGenerator := 16, (*Signal Generator Form ID.*)
		SignalReplication := 32, (*Signal Replication Form ID.*)
		ManualAxisControl := 64, (*Manual Axis Control Form ID.*)
		Simulator := 128 (*Simulator Form ID.*)
		);
	MotionState : 
		(
		SYSTEM_ENERGIZED := 0, (*System Energized Value.*)
		SYSTEM_CHECKING := 1, (*System Checking Value.*)
		SYSTEM_READY_FOR_INITIALIZING := 2, (*System Ready For Initializing Value.*)
		SYSTEM_INITIALIZING := 3, (*System Initializing Value.*)
		SYSTEM_GO_TO_ZERO := 4, (*System Go To Zero Value.*)
		SYSTEM_READY_FOR_DATA := 5, (*System Ready For Data Value.*)
		SYSTEM_GETTING_DATA := 6, (*System Getting Data Value.*)
		SYSTEM_CANCELLED_DATA := 7, (*System Cancelled Data Value.*)
		SYSTEM_READY_FOR_TRAINING := 8, (*System Ready For Training Value.*)
		SYSTEM_ON_TRAINING := 9, (*System On Training Value.*)
		SYSTEM_READY_FOR_STOP_TRAINING := 10, (*System Ready For Stop Training Value.*)
		SYSTEM_STOP_TRAINING := 11, (*System Stop Training Value.*)
		SYSTEM_SETTING_PRESSURE := 12, (*System Setting Pressure Value.*)
		SYSTEM_SERVICE := 13, (*System On Service Value.*)
		SYSTEM_ON_ERROR := 14, (*System On Error Value.*)
		SYSTEM_PASSIVE := 15, (*System On Passive Value.*)
		SYSTEM_PARK_MODE := 16, (*System Park Mode Value.*)
		SYSTEM_EMERGENCY := 17, (*System On Emergency Value.*)
		SYSTEM_NOT_CONNECTED := 18 (*System Not Connected Value.*)
		);
	MotionCommand : 
		(
		COMMAND_INITIALIZE := 1, (*Initialize Command.*)
		COMMAND_START_TRAINING := 2, (*Start Training Command.*)
		COMMAND_STOP_TRAINING := 3, (*Stop Training Command.*)
		COMMAND_ERROR_RESET := 4, (*Error Reset Command.*)
		COMMAND_SEND_DATA := 5, (*Send Data Command.*)
		COMMAND_CANCEL_DATA := 6, (*Send Data Cancel Command.*)
		COMMAND_PLATFORM_GO_DOWN := 7, (*Platform Go Down Command.*)
		COMMAND_PLATFORM_GO_CENTER := 8, (*Platform Go Center Command.*)
		COMMAND_PLATFORM_GO_UP := 9 (*Platform Go Up Command.*)
		);
	PacketCommand : 
		(
		PACKET_OK := 1,
		PACKET_COMPLETED := 2,
		PACKET_RECALCULATE := 3
		);
	ParkState : 
		(
		NEUTRAL,
		UP,
		DOWN
		);
	Motion_Type : 
		(
		SMOTION1800,
		SMOTION2500,
		SMOTION3000
		);
END_TYPE

(**)
(*BR Variables*)

TYPE
	UAInSettingsMultiEncType : 	STRUCT  (*Structure with parameters for settings multiturn encoders*)
		SetPosition : REAL; (*Set position of actual encoder position*)
		LimitSwPos : REAL; (*Home position in case of homing to the limit swtich*)
		TriggerSpeed : REAL;
		HomingSpeed : REAL;
	END_STRUCT;
	UASettingsType : 	STRUCT 
		MultiEncSettings : UAInSettingsMultiEncType; (*Structure with parameters for settings multiturn encoders*)
		ContInit : BOOL; (*Cmd to initialize the controller*)
	END_STRUCT;
	UHAxisType : 	STRUCT 
		MpLink : {REDUND_UNREPLICABLE} UDINT;
		Axis : {REDUND_UNREPLICABLE} UDINT;
		Par : {REDUND_UNREPLICABLE} UDINT;
	END_STRUCT;
	UHClock_typ : 	STRUCT  (*Structure has been adjusted in a way that there are no filler bytes -> Filler bytes create problems with PV_xgetadr*)
		AlarmID : UINT; (*Related AlarmID that will be triggered if the MaxTime is reached*)
		Enable : USINT; (*Clock counting enable*)
		Flags : USINT; (*Flags that determine the clock behaviour*)
		Precision : USINT; (*Required precision -> Seconds, Minutes, Days*)
		Timeout : USINT; (*Timeout occured *)
		AlarmIDOut : UINT; (*Alarm ID which will be sent to higher control*)
		MaxTime : UDINT; (*Maximum working time before the alarm in the AlarmID will be set (Disabled if equal to 0) [in defined precision units]*)
		TotalTime : UDINT; (*Total time the clock is enabled [in defined precision units]*)
		RunningTime : UDINT; (*Elapsed time since the increase of the total time [s]*)
	END_STRUCT;
	UHCommandType : 	STRUCT 
		Home : {REDUND_UNREPLICABLE} BOOL;
		Start : {REDUND_UNREPLICABLE} BOOL;
		PowerOff : {REDUND_UNREPLICABLE} BOOL;
		Reset : {REDUND_UNREPLICABLE} BOOL;
		Park : {REDUND_UNREPLICABLE} BOOL;
		Stop : {REDUND_UNREPLICABLE} BOOL;
		Manual : {REDUND_UNREPLICABLE} UHManualCmdType;
		InitilizeAxis : {REDUND_UNREPLICABLE} BOOL;
		SetMultiturnEncoder : BOOL; (*Set multiturn offset*)
		MultiturnEncHomeLimSw : BOOL; (*Home to limit switch command*)
		MultiturnEncHomeOffsetValue : BOOL; (*Home to given encoder offset*)
	END_STRUCT;
	UHManualCmdType : 	STRUCT 
		JogPos : {REDUND_UNREPLICABLE} ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF BOOL;
		JogNeg : {REDUND_UNREPLICABLE} ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF BOOL;
		GoPos : {REDUND_UNREPLICABLE} ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF BOOL;
	END_STRUCT;
	UHModeType : 	STRUCT 
		PositionControl : BOOL;
		CurrentControl : BOOL;
	END_STRUCT;
	UHStatusType : 	STRUCT 
		Powered : {REDUND_UNREPLICABLE} BOOL;
		Referenced : {REDUND_UNREPLICABLE} BOOL;
		Initialized : {REDUND_UNREPLICABLE} BOOL;
		AfterEnabledAxis : {REDUND_UNREPLICABLE} BOOL;
		AxEnabled : {REDUND_UNREPLICABLE} BOOL;
		DefCfgParLoaded : {REDUND_UNREPLICABLE} BOOL;
		DriveInitDone : BOOL;
	END_STRUCT;
	UHUnitAlarmType : 	STRUCT 
		MainErrors : {REDUND_UNREPLICABLE} UMainErrorType;
		Reaction : ARRAY[0..7]OF BOOL; (* - Reaction of alarms.  (Check for ALM_ constants to get more info about bits - or mapp configuration)*)
		Warning : BOOL; (* - Warning flag*)
		ErrorReset : BOOL; (* - Cmd to reset error*)
		WarningReset : BOOL; (* - Cmd to reset warning*)
		Error : BOOL;
		BuzzerEnable : BOOL;
		NewAlm : BOOL;
		Warnings : MpAlarmBasicUIConnect20Type;
	END_STRUCT;
	UHUnitType : 	STRUCT 
		Cmd : {REDUND_UNREPLICABLE} UHCommandType;
		Status : {REDUND_UNREPLICABLE} UHStatusType;
		Alarm : {REDUND_UNREPLICABLE} UHUnitAlarmType;
		Mode : UHModeType;
	END_STRUCT;
	UMainErrorType : 	STRUCT 
		AddInfo : ARRAY[0..19]OF STRING[100];
		AckImg : ARRAY[0..ERR_MU1_MAX_NO_MINUS_1]OF BOOL;
		ActiveImg : ARRAY[0..ERR_MU1_MAX_NO_MINUS_1]OF BOOL;
		ErrorID : UINT;
		Error : BOOL;
	END_STRUCT;
	UnitFixDataType : 	STRUCT 
		Ax : {REDUND_UNREPLICABLE} ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF UPAxisType;
	END_STRUCT;
	UnitHandleType : 	STRUCT 
		Unit : {REDUND_UNREPLICABLE} UHUnitType;
		Ax : {REDUND_UNREPLICABLE} ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF UHAxisType;
		Simulation : {REDUND_UNREPLICABLE} BOOL;
		Settings : UASettingsType;
	END_STRUCT;
	UnitRecDataType : 	STRUCT 
		AxEnabled : {REDUND_UNREPLICABLE} ARRAY[0..MAX_AX_TOT_MINUS_ONE]OF BOOL;
		Count : {REDUND_UNREPLICABLE} USINT;
		ParkPos : {REDUND_UNREPLICABLE} UPParkPosType;
	END_STRUCT;
	UPAConfigType : 	STRUCT  (**** HW - Axis Config Parameters*)
		Axis : MpAxisBasicConfigType;
		AcpMultiPwrAxis : BOOL;
		MultiturnEncOffset : REAL;
		MultiturnEnc : BOOL;
	END_STRUCT;
	UPAControllerType : 	STRUCT  (**** HW - Axis Controller Parameters*)
		Mode : UDINT; (* - Controller Mode (Pos, Pos + FF)*)
		Speed : MpAxisControllerSpeedType; (*STRUCT - Speed Controller Parameters*)
		Position : MpAxisControllerPositionType; (*STRUCT - Position Controller Parameters*)
		FF : MpAxisControllerFeedForwardType; (*STRUCT - FF Controller Parameters*)
		LoopFilter : MpAxisControllerLoopFiltersType;
	END_STRUCT;
	UPAEncoderType : 	STRUCT  (**** HW - Axis Encoder Parameters*)
		GearboxIn : UDINT; (* - Gearbox in ratio*)
		GearboxOut : UDINT; (* - Gearbox out ratio*)
		Period : LREAL; (* - Axis period*)
		Direction : UDINT; (* - Counting direction*)
		LoadUnitPerRev : LREAL; (* - Unit per revolution (on load side)*)
	END_STRUCT;
	UPALimitMovementType : 	STRUCT  (**** HW - Axis Movement Limit Parameters*)
		LagError : REAL; (* - Lag Error value*)
		JoltTime : REAL; (* - Jolt Time value*)
	END_STRUCT;
	UPALimitSoftwareEndType : 	STRUCT  (**** HW - Axis Software Limit Parameters*)
		PosLimit : LREAL; (* - Positive limit on software*)
		NegLimit : LREAL; (* - Negative limit on software*)
	END_STRUCT;
	UPALimitType : 	STRUCT  (**** HW - Axis Limit Parameters*)
		SoftwareEnd : UPALimitSoftwareEndType; (*STRUCT - Software End Limit*)
		Velocity : UPALimitVelocityType; (*STRUCT - Velocity Limit*)
		Movement : UPALimitMovementType; (*STRUCT - Movement Limit*)
	END_STRUCT;
	UPALimitVelocityType : 	STRUCT  (**** HW - Axis Velocity Limit Parameters*)
		VelocityNeg : REAL; (* - Limit for negative velocity*)
		VelocityPos : REAL; (* - Limit for positive velocity*)
		Acceleration : REAL; (* - Limit for acceleration*)
		Deceleration : REAL; (* - Limit for deceleration*)
		VelocityVG : REAL;
	END_STRUCT;
	UPAxisType : 	STRUCT 
		Cfg : {REDUND_UNREPLICABLE} UPAConfigType;
		AxPar : MpAxisBasicParType;
	END_STRUCT;
	UPParkPosType : 	STRUCT 
		PosX : {REDUND_UNREPLICABLE} LREAL;
		PosY : {REDUND_UNREPLICABLE} LREAL;
		Vel : {REDUND_UNREPLICABLE} REAL;
		Acc : {REDUND_UNREPLICABLE} REAL;
		Dec : {REDUND_UNREPLICABLE} REAL;
	END_STRUCT;
	Struct_Spline : 	STRUCT 
		Mot1 : Spline_Coeff;
		Mot2 : Spline_Coeff;
		Mot3 : Spline_Coeff;
		Mot4 : Spline_Coeff;
		Mot5 : Spline_Coeff;
		Mot6 : Spline_Coeff;
	END_STRUCT;
	Spline_Coeff : 	STRUCT 
		A : LREAL;
		B : LREAL;
		C : LREAL;
		D : LREAL;
	END_STRUCT;
END_TYPE
